$(window).on("load", function() { CreativeTemplate.WindowLoad(); });

$(function() {
    CreativeTemplate.Init();
});

    window.CreativeTemplate = {
        //PROPERTIES
        "KeyCodes": { 
            'tab': 'Tab', 
            'enter': 'Enter', 
            'esc': 'Escape', 
            'space': ' ', 
            'end': 'End', 
            'home': 'Home', 
            'left': 'ArrowLeft', 
            'up': 'ArrowUp', 
            'right': 'ArrowRight', 
            'down': 'ArrowDown' 
        },
        "IsMyViewPage": false,
        "ShowDistrtictHome": false,
        "ShowSchoolList": true,
        "ShowTranslate": true,
        "DefaultLogoSrc": "/src/template/assets/default.png",

        //METHODS
        "Init": function() {
            //FOR SCOPE
            var template = this;

            this.SetTemplateProps();
            this.MyStart();
            this.BankLinks();
            this.JsMediaQueries();
            this.Header();
            this.RsMenu();
            this.ChannelBar();
            this.MegaMenu();
            if ($("#gb-page.hp").length) {
                this.Homepage();
                this.MoveButtons();
                this.MMGGrid();
                this.Spotlight();
            }
            this.MoveHeader();
            this.Body();
            this.GlobalIcons();
            this.SocialIcons();
            this.MMGPlugin();
            this.StreamingVideo();
            this.ModEvents();
            this.Search();
            this.Footer();
            
            //WINDOW RESIZE AND SCROLL METHOD
            $(window).resize(function() { template.WindowResize(); });
            $(window).scroll(function() { template.WindowScroll(); });
            
        },
        
        "WindowLoad": function() {

        },

        "WindowResize": function() {
            //RUN QUERIES ON RESIZE
            this.JsMediaQueries();
            this.MoveButtons();
            this.MoveHeader();

        },

        "WindowScroll": function() {

        },

        "JsMediaQueries": function() {
            switch(this.GetBreakPoint()) {
                case "desktop":
                    
                break;
                case "768":

                break;
                case "640":


                break;
                case "480":


                break;
                case "320":

                break;
            }
        },
        
        "SetTemplateProps": function SetTemplateProps () {
            // FOR SCOPE
            var template = this;
            
            //MYVIEW PAGE CHECK
            if ($('#pw-body').length) this.IsMyViewPage = true;

            //SCHOOL LIST CHECK
            if($(".sw-mystart-dropdown.schoollist").length) this.ShowSchoolList = true;
        },

        "MyStart": function() {
            //FOR SCOPE
            var template = this;

            //BUILD USER OPTIONS DROPDOWN
            var userOptionsItems = "";

            [$IF LOGGED IN$]
                if ($(".sw-mystart-button.manage").length) {
                  $(".sw-mystart-button.manage a").attr("onclick", $(".sw-mystart-button.manage a").attr("onclick") + "return false;");
                  userOptionsItems += '<li id="user-options-manage"><a href="#" onclick="' + $(".sw-mystart-button.manage a").attr("onclick") + '"><span>Site Manager</span></a></li>';
                } // MY ACCOUNT BUTTONS


                $("#sw-myaccount-list > li:first-child a span").text("My Account");
                $("#sw-myaccount-list > li").each(function () {
                  userOptionsItems += "<li>" + $(this).html() + "</li>";
                }); // MY PASSKEYS BUTTON

                if ($("#sw-mystart-mypasskey").length) {
                  $("#sw-mystart-mypasskey").removeClass("sw-mystart-button").addClass("cs-mystart-dropdown passkeys");
                  $("#ui-btn-mypasskey").addClass("cs-dropdown-selector");
                  $("#sw-mystart-mypasskey").insertBefore(".cs-mystart-dropdown.user-options");
                }
            [$ELSE IF LOGGED$]

                // SIGNIN BUTTON
                if($(".sw-mystart-button.signin").length) {
                    userOptionsItems += "<li>" + $(".sw-mystart-button.signin").html() + "</li>";
                }

                // REGISTER BUTTON
                if($(".sw-mystart-button.register").length) {
                    userOptionsItems += "<li>" + $(".sw-mystart-button.register").html() + "</li>";
                }
            [$END IF LOGGED IN$]

            // ADD USER OPTIONS DROPDOWN TO THE DOM
            $(".cs-mystart-dropdown.user-options .cs-dropdown-list").html(userOptionsItems);

            //CLONE AND DUMP SCHOOLLIST INTO DOM
            $('.sw-mystart-dropdown.schoollist .sw-dropdown-list').clone().appendTo('#school-dropdown');

            //RUN TRANSLATE FUNCTION
            this.Translate();

            // BIND DROPDOWN EVENTS
            template.DropdownActions({ //USER OPTIONS
                "dropdownParent": ".cs-mystart-dropdown.user-options",
                "dropdownSelector": ".mystart-selector",
                "dropdown": ".cs-dropdown",
                "dropdownList": ".cs-dropdown-list"
            });
            template.DropdownActions({ //TRANSLATE
                "dropdownParent": "#gb-translate",
                "dropdownSelector": ".translate-selector",
                "dropdown": "#translate-dropdown",
            });
            template.DropdownActions({ //SCHOOLS DROPDOWN
                "dropdownParent": "#cs-schools",
                "dropdownSelector": ".mystart-selector",
                "dropdown": "#school-dropdown",
                "dropdownList": "#school-dropdown .sw-dropdown-list"
            });
            template.DropdownActions({ //SCHOOLS DROPDOWN
                "dropdownParent": "#cs-bank",
                "dropdownSelector": ".mystart-selector",
                "dropdown": "#bank-dropdown",
                "dropdownList": "#bank-list"
            });
        },

        "BankLinks": function(){
            //FOR SCOPE
            var template = this;

            //BUILD LINK LIST ARRAY
            var listItem = [
                [//LINK LIST ONE
                   '<SWCtrl controlname="Custom" props="Name:showbankLink1" />', //LINK TOGGLE
                   '<SWCtrl controlname="Custom" props="Name:bankLink1" />', //LINK NAME
                   '<SWCtrl controlname="Custom" props="Name:bankLink1Url" />', //LINK URL
                   '<SWCtrl controlname="Custom" props="Name:bankLink1Target" />' //LINK TARGET
                ],
                [//LINK LIST TWO
                   '<SWCtrl controlname="Custom" props="Name:showbankLink2" />', //LINK TOGGLE
                   '<SWCtrl controlname="Custom" props="Name:bankLink2" />', //LINK NAME
                   '<SWCtrl controlname="Custom" props="Name:bankLink2Url" />', //LINK URL
                   '<SWCtrl controlname="Custom" props="Name:bankLink2Target" />' //LINK TARGET
                ],
                [//LINK LIST THREE
                   '<SWCtrl controlname="Custom" props="Name:showbankLink3" />', //LINK TOGGLE
                   '<SWCtrl controlname="Custom" props="Name:bankLink3" />', //LINK NAME
                   '<SWCtrl controlname="Custom" props="Name:bankLink3Url" />', //LINK URL
                   '<SWCtrl controlname="Custom" props="Name:bankLink3Target" />' //LINK TARGET
                ],
                [//LINK LIST FOUR
                   '<SWCtrl controlname="Custom" props="Name:showbankLink4" />', //LINK TOGGLE
                   '<SWCtrl controlname="Custom" props="Name:bankLink4" />', //LINK NAME
                   '<SWCtrl controlname="Custom" props="Name:bankLink4Url" />', //LINK URL
                   '<SWCtrl controlname="Custom" props="Name:bankLink4Target" />' //LINK TARGET
                ],
                [//LINK LIST FIVE
                   '<SWCtrl controlname="Custom" props="Name:showbankLink5" />', //LINK TOGGLE
                   '<SWCtrl controlname="Custom" props="Name:bankLink5" />', //LINK NAME
                   '<SWCtrl controlname="Custom" props="Name:bankLink5Url" />', //LINK URL
                   '<SWCtrl controlname="Custom" props="Name:bankLink5Target" />' //LINK TARGET
                ],
                [//LINK LIST SIX
                   '<SWCtrl controlname="Custom" props="Name:showbankLink6" />', //LINK TOGGLE
                   '<SWCtrl controlname="Custom" props="Name:bankLink6" />', //LINK NAME
                   '<SWCtrl controlname="Custom" props="Name:bankLink6Url" />', //LINK URL
                   '<SWCtrl controlname="Custom" props="Name:bankLink6Target" />' //LINK TARGET
                ],
                [//LINK LIST SEVEN
                   '<SWCtrl controlname="Custom" props="Name:showbankLink7" />', //LINK TOGGLE
                   '<SWCtrl controlname="Custom" props="Name:bankLink7" />', //LINK NAME
                   '<SWCtrl controlname="Custom" props="Name:bankLink7Url" />', //LINK URL
                   '<SWCtrl controlname="Custom" props="Name:bankLink7Target" />' //LINK TARGET
                ],
                [//LINK LIST EIGHT
                   '<SWCtrl controlname="Custom" props="Name:showbankLink8" />', //LINK TOGGLE
                   '<SWCtrl controlname="Custom" props="Name:bankLink8" />', //LINK NAME
                   '<SWCtrl controlname="Custom" props="Name:bankLink8Url" />', //LINK URL
                   '<SWCtrl controlname="Custom" props="Name:bankLink8Target" />' //LINK TARGET
                ]
            ]

            //BUILD LINKS BY LOOPING EACH ITEM IN ARRAY
            var linkHTML = '';

            $.each(listItem, function(index){

                //BUILD LIST ITEMS
                linkHTML +=  '<li class="bank-list=item" data-toggle="'+ listItem[index][0] +'"><a class="bank-link" href="' + listItem[index][2] + '" target="'+ listItem[index][3] +'"  aria-label="'+ listItem[index][1] +'"><span>'+ listItem[index][1] +'</span></a></li>';

            });

            //PUSH HTML TO DOM
            $('#bank-list').html(linkHTML);
            //$('#ar-list-mobile').html(linkHTML);
        },

        "DropdownActions": function(params) {
            // FOR SCOPE
            var template = this;

            var dropdownParent = params.dropdownParent;
            var dropdownSelector = params.dropdownSelector;
            var dropdown = params.dropdown;
            var dropdownList = params.dropdownList;

            $(dropdownParent + " " + dropdownList + " a").attr("tabindex", "-1");

            // MYSTART DROPDOWN SELECTOR CLICK EVENT
            $(dropdownParent).on("click", dropdownSelector, function(e) {
                e.preventDefault();

                if($(this).parent().hasClass("open")){
                    $("+ " + dropdownList + " a").attr("tabindex", "-1");
                    $(this).attr("aria-expanded", "false").parent().removeClass("open").find(dropdown).attr("aria-hidden", "true").slideUp(300, "swing");
                } else {
                    $(this).attr("aria-expanded", "true").parent().addClass("open").find(dropdown).attr("aria-hidden","false").slideDown(300, "swing");
                }
            });

            // MYSTART DROPDOWN SELECTOR KEYDOWN EVENTS
            $(dropdownParent).on("keydown", dropdownSelector, function(e) {
                // CAPTURE KEY CODE
                switch(e.key) {
                    // CONSUME LEFT AND UP ARROWS
                    case template.KeyCodes.enter:
                    case template.KeyCodes.space:
                        e.preventDefault();

                        // IF THE DROPDOWN IS OPEN, CLOSE IT
                        if($(dropdownParent).hasClass("open")){
                            $("+ " + dropdown + " " + dropdownList + " a").attr("tabindex", "-1");
                            $(this).attr("aria-expanded", "false").parent().removeClass("open").find(dropdown).attr("aria-hidden", "true").slideUp(300, "swing");
                        } else {
                            $(this).attr("aria-expanded", "true").parent().addClass("open").find(dropdown).attr("aria-hidden", "false").slideDown(300, "swing", function(){
                                if($(dropdownParent).hasClass("translate")){
                                    $("#cs-branded-translate-dropdown").focus();
                                } else {
                                    $(dropdownList + " li:first-child a", this).attr("tabindex", "0").focus();
                                }
                            });
                        }
                    break;

                    // CONSUME TAB KEY
                    case template.KeyCodes.tab:
                        if($("+ " + dropdown + " " + dropdownList + " a").length) {
                            $("+ " + dropdown + " " + dropdownList + " a").attr("tabindex", "-1");
                            $(this).attr("aria-expanded", "false").parent().removeClass("open").find(dropdown).attr("aria-hidden", "true").slideUp(300, "swing");
                        }
                    break;

                    // CONSUME LEFT AND UP ARROWS
                    case template.KeyCodes.down:
                    case template.KeyCodes.right:
                        e.preventDefault();

                        $("+ " + dropdown + " " + dropdownList + " a").attr("tabindex", "-1");
                        $("+ " + dropdown + " " + dropdownList + " li:first-child > a", this).attr("tabindex", "0").focus();
                    break;
                }
            });

            // MYSTART DROPDOWN LINK KEYDOWN EVENTS
            $(dropdownParent).on("keydown", dropdownList + " li a", function(e) {
                // CAPTURE KEY CODE
                switch(e.key) {
                    // CONSUME LEFT AND UP ARROWS
                    case template.KeyCodes.left:
                    case template.KeyCodes.up:
                        e.preventDefault();

                        // IS FIRST ITEM
                        if($(this).parent().is(":first-child")) {
                            // FOCUS DROPDOWN BUTTON
                            $(this).closest(dropdownList).find("a").attr("tabindex", "-1");
                            $(this).closest(dropdownParent).find(dropdownSelector).focus();
                        } else {
                            // FOCUS PREVIOUS ITEM
                            $(this).closest(dropdownList).find("a").attr("tabindex", "-1");
                            $(this).parent().prev("li").find("> a").attr("tabindex", "0").focus();
                        }
                    break;

                    // CONSUME RIGHT AND DOWN ARROWS
                    case template.KeyCodes.right:
                    case template.KeyCodes.down:
                        e.preventDefault();

                        // IS LAST ITEM
                        if($(this).parent().is(":last-child")) {
                            // FOCUS FIRST ITEM
                            $(this).closest(dropdownList).find("a").attr("tabindex", "-1");
                            $(this).closest(dropdownList).find("li:first-child > a").attr("tabindex", "0").focus();
                        } else {
                            // FOCUS NEXT ITEM
                            $(this).closest(dropdownList).find("a").attr("tabindex", "-1");
                            $(this).parent().next("li").find("> a").attr("tabindex", "0").focus();
                        }
                    break;

                    // CONSUME TAB KEY
                    case template.KeyCodes.tab:
                        if(e.shiftKey) {
                            e.preventDefault();

                            // FOCUS DROPDOWN BUTTON
                            $(this).closest(dropdownList).find("a").attr("tabindex", "-1");
                            $(this).closest(dropdownParent).find(dropdownSelector).focus();
                        }
                    break;

                    // CONSUME HOME KEY
                    case template.KeyCodes.home:
                        e.preventDefault();

                        // FOCUS FIRST ITEM
                        $(this).closest(dropdownList).find("a").attr("tabindex", "-1");
                        $(this).closest(dropdownList).find("li:first-child > a").attr("tabindex", "0").focus();
                    break;

                    // CONSUME END KEY
                    case template.KeyCodes.end:
                        e.preventDefault();

                        // FOCUS LAST ITEM
                        $(this).closest(dropdownList).find("a").attr("tabindex", "-1");
                        $(this).closest(dropdownList).find("li:last-child > a").attr("tabindex", "0").focus();
                    break;

                    // CONSUME ESC KEY
                    case template.KeyCodes.esc:
                        e.preventDefault();

                        // FOCUS DROPDOWN BUTTON AND CLOSE DROPDOWN
                        $(this).closest(dropdownParent).find(dropdownSelector).focus();
                        $(this).closest(dropdownList).find("a").attr("tabindex", "-1");
                        $(dropdownSelector).attr("aria-expanded", "false").parent().removeClass("open").find(dropdown).attr("aria-hidden", "true").slideUp(300, "swing");
                    break;
                }
            });

            $(dropdownParent).mouseleave(function() {
                $(dropdownList + " a", this).attr("tabindex", "-1");
                $(dropdownSelector, this).attr("aria-expanded", "false").parent().removeClass("open").find(dropdown).attr("aria-hidden", "true").slideUp(300, "swing");
            }).focusout(function() {
                var thisDropdown = this;

                setTimeout(function () {
                    if(!$(thisDropdown).find(":focus").length) {
                        $(dropdownSelector, thisDropdown).attr("aria-expanded", "false").parent().removeClass("open").find(dropdown).attr("aria-hidden", "true").slideUp(300, "swing");
                    }
                }, 500);
            });           

        },

        "CheckElementPosition": function(element){ //CALL FOR SCROLL ANIMATIONS
            var template = this;

            //CHECK IF ELEMENT IS IN VIEWPORT
            var elementTop = $(element).offset().top;
            var elementLeft = $(element).offset().left;
            var elementBottom = elementTop + $(element).outerHeight();
            var viewportTop = $(window).scrollTop();
            var viewportBottom = viewportTop + $(window).height();
            var viewportWidth = $(window).width();

            return elementBottom > viewportTop && elementTop < viewportBottom && elementLeft < viewportWidth;

        },

        "ChannelBar": function() {
            $(".sw-channel-item").off('hover');
            $(".sw-channel-item").hover(function(){
                
                $("ul", this).stop(true, true);
                var subList = $(this).children('ul');
                if ($.trim(subList.html()) !== "") {
                    subList.slideDown(300, "swing");
                }
                $(this).addClass("hover");

                // const itemPosLeft = $(this).offset().left;
                // const itemPosRight = itemPosLeft + subList.outerWidth();
    
                // if( itemPosRight >= $(window).width() ){
                //     $('.sw-channel-dropdown', this)
                //         .addClass('overflowing')
                //         .css({
                //             'left': -(itemPosRight - $(window).width())
                //         });
                // }else if(itemPosLeft < 0){
                //     $('.sw-channel-dropdown', this)
                //         .addClass('overflowing')
                //         .css({
                //             'left': -(itemPosLeft)
                //         });
                // }else{
                //     $('.sw-channel-dropdown', this)
                //         .removeClass('overflowing')
                //         .css({
                //             'left': ''
                //         });
                // }

            }, function(){
                $(".sw-channel-dropdown").slideUp(300, "swing");
                $(this).removeClass("hover");
            });

        },
        "MegaMenu": function(){
            $("#sw-channel-list-container").megaChannelMenu({
                "numberOfChannels"					: 10, // integer 
                "maxSectionColumnsPerChannel"		: 3, // integer (this does NOT include the extra, non-section column)
                "extraColumn"						: true, // boolean
                "extraColumnTitle"					: true, //bolean
                "extraColumnDescription"			: true, //bolean
                "extraColumnImage"					: true, //bolean
                "extraColumnLink"					: false, //bolean
                "extraColumnPosition"				: "LEFT", // string, "ABOVE", "BELOW", "LEFT", "RIGHT"
                "sectionHeadings"					: true, // boolean
                "sectionHeadingLinks"				: false, // boolean
                "maxNumberSectionHeadingsPerColumn"	: 1, // integer 
                "megaMenuElements"					: [
                    { //CHANNEL 1
                        "channelName"			: '<SWCtrl controlname="Custom" props="Name:csmChannel1Name" />',
                        "channelTitle"			: '<SWCtrl controlname="Custom" props="Name:csmChannel1Title" />',
                        "channelDescription"	: '<SWCtrl controlname="Custom" props="Name:csmChannel1Description" />',
                        "channelImage"			: '<SWCtrl controlname="Custom" props="Name:csmChannel1Image" />',
                        "channelImageAlt"		: '<SWCtrl controlname="Custom" props="Name:csmChannel1ImageAlt" />',
                        "channelColumns"		: [
                            [ //COLUMN 1
                                { //SECTION 1
                                    "heading"	: [
                                        {
                                            "text"	: '<SWCtrl controlname="Custom" props="Name:csmChannel1Column1Heading1Text" />'
                                        }
                                    ],
                                    "sections"	: '<SWCtrl controlname="Custom" props="Name:csmChannel1Column1Heading1Sections" />'
                                }
                            ],
                            [ //COLUMN 2
                                { //SECTION 1
                                    "heading"	: [
                                        {
                                            "text"	: '<SWCtrl controlname="Custom" props="Name:csmChannel1Column2Heading1Text" />'
                                        }
                                    ],
                                    "sections"	: '<SWCtrl controlname="Custom" props="Name:csmChannel1Column2Heading1Sections" />'
                                }
                            ],
                            [ //COLUMN 3
                                { //SECTION 1
                                    "heading"	: [
                                        {
                                            "text"	: '<SWCtrl controlname="Custom" props="Name:csmChannel1Column3Heading1Text" />'
                                        }
                                    ],
                                    "sections"	: '<SWCtrl controlname="Custom" props="Name:csmChannel1Column3Heading1Sections" />'
                                }
                            ]
                        ]
                    },
                    { //CHANNEL 2
                        "channelName"			: '<SWCtrl controlname="Custom" props="Name:csmChannel2Name" />',
                        "channelTitle"			: '<SWCtrl controlname="Custom" props="Name:csmChannel2Title" />',
                        "channelDescription"	: '<SWCtrl controlname="Custom" props="Name:csmChannel2Description" />',
                        "channelImage"			: '<SWCtrl controlname="Custom" props="Name:csmChannel2Image" />',
                        "channelImageAlt"		: '<SWCtrl controlname="Custom" props="Name:csmChannel2ImageAlt" />',
                        "channelColumns"		: [
                            [ //COLUMN 1
                                { //SECTION 1
                                    "heading"	: [
                                        {
                                            "text"	: '<SWCtrl controlname="Custom" props="Name:csmChannel2Column1Heading1Text" />'
                                        }
                                    ],
                                    "sections"	: '<SWCtrl controlname="Custom" props="Name:csmChannel2Column1Heading1Sections" />'
                                }
                            ],
                            [ //COLUMN 2
                                { //SECTION 1
                                    "heading"	: [
                                        {
                                            "text"	: '<SWCtrl controlname="Custom" props="Name:csmChannel2Column2Heading1Text" />'
                                        }
                                    ],
                                    "sections"	: '<SWCtrl controlname="Custom" props="Name:csmChannel2Column2Heading1Sections" />'
                                }
                            ],
                            [ //COLUMN 3
                                { //SECTION 1
                                    "heading"	: [
                                        {
                                            "text"	: '<SWCtrl controlname="Custom" props="Name:csmChannel2Column3Heading1Text" />'
                                        }
                                    ],
                                    "sections"	: '<SWCtrl controlname="Custom" props="Name:csmChannel2Column3Heading1Sections" />'
                                }
                            ]
                        ]
                    },
                    { //CHANNEL 3
                        "channelName"			: '<SWCtrl controlname="Custom" props="Name:csmChannel3Name" />',
                        "channelTitle"			: '<SWCtrl controlname="Custom" props="Name:csmChannel3Title" />',
                        "channelDescription"	: '<SWCtrl controlname="Custom" props="Name:csmChannel3Description" />',
                        "channelImage"			: '<SWCtrl controlname="Custom" props="Name:csmChannel3Image" />',
                        "channelImageAlt"		: '<SWCtrl controlname="Custom" props="Name:csmChannel3ImageAlt" />',
                        "channelColumns"		: [
                            [ //COLUMN 1
                                { //SECTION 1
                                    "heading"	: [
                                        {
                                            "text"	: '<SWCtrl controlname="Custom" props="Name:csmChannel3Column1Heading1Text" />'
                                        }
                                    ],
                                    "sections"	: '<SWCtrl controlname="Custom" props="Name:csmChannel3Column1Heading1Sections" />'
                                }
                            ],
                            [ //COLUMN 2
                                { //SECTION 1
                                    "heading"	: [
                                        {
                                            "text"	: '<SWCtrl controlname="Custom" props="Name:csmChannel3Column2Heading1Text" />'
                                        }
                                    ],
                                    "sections"	: '<SWCtrl controlname="Custom" props="Name:csmChannel3Column2Heading1Sections" />'
                                }
                            ],
                            [ //COLUMN 3
                                { //SECTION 1
                                    "heading"	: [
                                        {
                                            "text"	: '<SWCtrl controlname="Custom" props="Name:csmChannel3Column3Heading1Text" />'
                                        }
                                    ],
                                    "sections"	: '<SWCtrl controlname="Custom" props="Name:csmChannel3Column3Heading1Sections" />'
                                }
                            ]
                        ]
                    },
                    { //CHANNEL 4
                        "channelName"			: '<SWCtrl controlname="Custom" props="Name:csmChannel4Name" />',
                        "channelTitle"			: '<SWCtrl controlname="Custom" props="Name:csmChannel4Title" />',
                        "channelDescription"	: '<SWCtrl controlname="Custom" props="Name:csmChannel4Description" />',
                        "channelImage"			: '<SWCtrl controlname="Custom" props="Name:csmChannel4Image" />',
                        "channelImageAlt"		: '<SWCtrl controlname="Custom" props="Name:csmChannel4ImageAlt" />',
                        "channelColumns"		: [
                            [ //COLUMN 1
                                { //SECTION 1
                                    "heading"	: [
                                        {
                                            "text"	: '<SWCtrl controlname="Custom" props="Name:csmChannel4Column1Heading1Text" />'
                                        }
                                    ],
                                    "sections"	: '<SWCtrl controlname="Custom" props="Name:csmChannel4Column1Heading1Sections" />'
                                }
                            ],
                            [ //COLUMN 2
                                { //SECTION 1
                                    "heading"	: [
                                        {
                                            "text"	: '<SWCtrl controlname="Custom" props="Name:csmChannel4Column2Heading1Text" />'
                                        }
                                    ],
                                    "sections"	: '<SWCtrl controlname="Custom" props="Name:csmChannel4Column2Heading1Sections" />'
                                }
                            ],
                            [ //COLUMN 3
                                { //SECTION 1
                                    "heading"	: [
                                        {
                                            "text"	: '<SWCtrl controlname="Custom" props="Name:csmChannel4Column3Heading1Text" />'
                                        }
                                    ],
                                    "sections"	: '<SWCtrl controlname="Custom" props="Name:csmChannel4Column3Heading1Sections" />'
                                }
                            ]
                        ]
                    },
                    { //CHANNEL 5
                        "channelName"			: '<SWCtrl controlname="Custom" props="Name:csmChannel5Name" />',
                        "channelTitle"			: '<SWCtrl controlname="Custom" props="Name:csmChannel5Title" />',
                        "channelDescription"	: '<SWCtrl controlname="Custom" props="Name:csmChannel5Description" />',
                        "channelImage"			: '<SWCtrl controlname="Custom" props="Name:csmChannel5Image" />',
                        "channelImageAlt"		: '<SWCtrl controlname="Custom" props="Name:csmChannel5ImageAlt" />',
                        "channelColumns"		: [
                            [ //COLUMN 1
                                { //SECTION 1
                                    "heading"	: [
                                        {
                                            "text"	: '<SWCtrl controlname="Custom" props="Name:csmChannel5Column1Heading1Text" />'
                                        }
                                    ],
                                    "sections"	: '<SWCtrl controlname="Custom" props="Name:csmChannel5Column1Heading1Sections" />'
                                }
                            ],
                            [ //COLUMN 2
                                { //SECTION 1
                                    "heading"	: [
                                        {
                                            "text"	: '<SWCtrl controlname="Custom" props="Name:csmChannel5Column2Heading1Text" />'
                                        }
                                    ],
                                    "sections"	: '<SWCtrl controlname="Custom" props="Name:csmChannel5Column2Heading1Sections" />'
                                }
                            ],
                            [ //COLUMN 3
                                { //SECTION 1
                                    "heading"	: [
                                        {
                                            "text"	: '<SWCtrl controlname="Custom" props="Name:csmChannel5Column3Heading1Text" />'
                                        }
                                    ],
                                    "sections"	: '<SWCtrl controlname="Custom" props="Name:csmChannel5Column3Heading1Sections" />'
                                }
                            ]
                        ]
                    },
                    { //CHANNEL 6
                        "channelName"			: '<SWCtrl controlname="Custom" props="Name:csmChannel6Name" />',
                        "channelTitle"			: '<SWCtrl controlname="Custom" props="Name:csmChannel6Title" />',
                        "channelDescription"	: '<SWCtrl controlname="Custom" props="Name:csmChannel6Description" />',
                        "channelImage"			: '<SWCtrl controlname="Custom" props="Name:csmChannel6Image" />',
                        "channelImageAlt"		: '<SWCtrl controlname="Custom" props="Name:csmChannel6ImageAlt" />',
                        "channelColumns"		: [
                            [ //COLUMN 1
                                { //SECTION 1
                                    "heading"	: [
                                        {
                                            "text"	: '<SWCtrl controlname="Custom" props="Name:csmChannel6Column1Heading1Text" />'
                                        }
                                    ],
                                    "sections"	: '<SWCtrl controlname="Custom" props="Name:csmChannel6Column1Heading1Sections" />'
                                }
                            ],
                            [ //COLUMN 2
                                { //SECTION 1
                                    "heading"	: [
                                        {
                                            "text"	: '<SWCtrl controlname="Custom" props="Name:csmChannel6Column2Heading1Text" />'
                                        }
                                    ],
                                    "sections"	: '<SWCtrl controlname="Custom" props="Name:csmChannel6Column2Heading1Sections" />'
                                }
                            ],
                            [ //COLUMN 3
                                { //SECTION 1
                                    "heading"	: [
                                        {
                                            "text"	: '<SWCtrl controlname="Custom" props="Name:csmChannel6Column3Heading1Text" />'
                                        }
                                    ],
                                    "sections"	: '<SWCtrl controlname="Custom" props="Name:csmChannel6Column3Heading1Sections" />'
                                }
                            ]
                        ]
                    },
                    { //CHANNEL 7
                        "channelName"			: '<SWCtrl controlname="Custom" props="Name:csmChannel7Name" />',
                        "channelTitle"			: '<SWCtrl controlname="Custom" props="Name:csmChannel7Title" />',
                        "channelDescription"	: '<SWCtrl controlname="Custom" props="Name:csmChannel7Description" />',
                        "channelImage"			: '<SWCtrl controlname="Custom" props="Name:csmChannel7Image" />',
                        "channelImageAlt"		: '<SWCtrl controlname="Custom" props="Name:csmChannel7ImageAlt" />',
                        "channelColumns"		: [
                            [ //COLUMN 1
                                { //SECTION 1
                                    "heading"	: [
                                        {
                                            "text"	: '<SWCtrl controlname="Custom" props="Name:csmChannel7Column1Heading1Text" />'
                                        }
                                    ],
                                    "sections"	: '<SWCtrl controlname="Custom" props="Name:csmChannel7Column1Heading1Sections" />'
                                }
                            ],
                            [ //COLUMN 2
                                { //SECTION 1
                                    "heading"	: [
                                        {
                                            "text"	: '<SWCtrl controlname="Custom" props="Name:csmChannel7Column2Heading1Text" />'
                                        }
                                    ],
                                    "sections"	: '<SWCtrl controlname="Custom" props="Name:csmChannel7Column2Heading1Sections" />'
                                }
                            ],
                            [ //COLUMN 3
                                { //SECTION 1
                                    "heading"	: [
                                        {
                                            "text"	: '<SWCtrl controlname="Custom" props="Name:csmChannel7Column3Heading1Text" />'
                                        }
                                    ],
                                    "sections"	: '<SWCtrl controlname="Custom" props="Name:csmChannel7Column3Heading1Sections" />'
                                }
                            ]
                        ]
                    },
                    { //CHANNEL 8
                        "channelName"			: '<SWCtrl controlname="Custom" props="Name:csmChannel8Name" />',
                        "channelTitle"			: '<SWCtrl controlname="Custom" props="Name:csmChannel8Title" />',
                        "channelDescription"	: '<SWCtrl controlname="Custom" props="Name:csmChannel8Description" />',
                        "channelImage"			: '<SWCtrl controlname="Custom" props="Name:csmChannel8Image" />',
                        "channelImageAlt"		: '<SWCtrl controlname="Custom" props="Name:csmChannel8ImageAlt" />',
                        "channelColumns"		: [
                            [ //COLUMN 1
                                { //SECTION 1
                                    "heading"	: [
                                        {
                                            "text"	: '<SWCtrl controlname="Custom" props="Name:csmChannel8Column1Heading1Text" />'
                                        }
                                    ],
                                    "sections"	: '<SWCtrl controlname="Custom" props="Name:csmChannel8Column1Heading1Sections" />'
                                }
                            ],
                            [ //COLUMN 2
                                { //SECTION 1
                                    "heading"	: [
                                        {
                                            "text"	: '<SWCtrl controlname="Custom" props="Name:csmChannel8Column2Heading1Text" />'
                                        }
                                    ],
                                    "sections"	: '<SWCtrl controlname="Custom" props="Name:csmChannel8Column2Heading1Sections" />'
                                }
                            ],
                            [ //COLUMN 3
                                { //SECTION 1
                                    "heading"	: [
                                        {
                                            "text"	: '<SWCtrl controlname="Custom" props="Name:csmChannel8Column3Heading1Text" />'
                                        }
                                    ],
                                    "sections"	: '<SWCtrl controlname="Custom" props="Name:csmChannel8Column3Heading1Sections" />'
                                }
                            ]
                        ]
                    },
                    { //CHANNEL 9
                        "channelName"			: '<SWCtrl controlname="Custom" props="Name:csmChannel9Name" />',
                        "channelTitle"			: '<SWCtrl controlname="Custom" props="Name:csmChannel9Title" />',
                        "channelDescription"	: '<SWCtrl controlname="Custom" props="Name:csmChannel9Description" />',
                        "channelImage"			: '<SWCtrl controlname="Custom" props="Name:csmChannel9Image" />',
                        "channelImageAlt"		: '<SWCtrl controlname="Custom" props="Name:csmChannel9ImageAlt" />',
                        "channelColumns"		: [
                            [ //COLUMN 1
                                { //SECTION 1
                                    "heading"	: [
                                        {
                                            "text"	: '<SWCtrl controlname="Custom" props="Name:csmChannel9Column1Heading1Text" />'
                                        }
                                    ],
                                    "sections"	: '<SWCtrl controlname="Custom" props="Name:csmChannel9Column1Heading1Sections" />'
                                }
                            ],
                            [ //COLUMN 2
                                { //SECTION 1
                                    "heading"	: [
                                        {
                                            "text"	: '<SWCtrl controlname="Custom" props="Name:csmChannel9Column2Heading1Text" />'
                                        }
                                    ],
                                    "sections"	: '<SWCtrl controlname="Custom" props="Name:csmChannel9Column2Heading1Sections" />'
                                }
                            ],
                            [ //COLUMN 3
                                { //SECTION 1
                                    "heading"	: [
                                        {
                                            "text"	: '<SWCtrl controlname="Custom" props="Name:csmChannel9Column3Heading1Text" />'
                                        }
                                    ],
                                    "sections"	: '<SWCtrl controlname="Custom" props="Name:csmChannel9Column3Heading1Sections" />'
                                }
                            ]
                        ]
                    },
                    { //CHANNEL 10
                        "channelName"			: '<SWCtrl controlname="Custom" props="Name:csmChannel10Name" />',
                        "channelTitle"			: '<SWCtrl controlname="Custom" props="Name:csmChannel10Title" />',
                        "channelDescription"	: '<SWCtrl controlname="Custom" props="Name:csmChannel10Description" />',
                        "channelImage"			: '<SWCtrl controlname="Custom" props="Name:csmChannel10Image" />',
                        "channelImageAlt"		: '<SWCtrl controlname="Custom" props="Name:csmChannel10ImageAlt" />',
                        "channelColumns"		: [
                            [ //COLUMN 1
                                { //SECTION 1
                                    "heading"	: [
                                        {
                                            "text"	: '<SWCtrl controlname="Custom" props="Name:csmChannel10Column1Heading1Text" />'
                                        }
                                    ],
                                    "sections"	: '<SWCtrl controlname="Custom" props="Name:csmChannel10Column1Heading1Sections" />'
                                }
                            ],
                            [ //COLUMN 2
                                { //SECTION 1
                                    "heading"	: [
                                        {
                                            "text"	: '<SWCtrl controlname="Custom" props="Name:csmChannel10Column2Heading1Text" />'
                                        }
                                    ],
                                    "sections"	: '<SWCtrl controlname="Custom" props="Name:csmChannel10Column2Heading1Sections" />'
                                }
                            ],
                            [ //COLUMN 3
                                { //SECTION 1
                                    "heading"	: [
                                        {
                                            "text"	: '<SWCtrl controlname="Custom" props="Name:csmChannel10Column3Heading1Text" />'
                                        }
                                    ],
                                    "sections"	: '<SWCtrl controlname="Custom" props="Name:csmChannel10Column3Heading1Sections" />'
                                }
                            ]
                        ]
                    }
                ],
                "allLoaded": function allLoaded() {}
            })
            $(".sw-channel-item.csmc").each(function(){
                var windowWidth = $(window).width();
                var dropdownWidth = $('ul.sw-channel-dropdown',this).width();
                var offset = (windowWidth - dropdownWidth);
                $('ul.sw-channel-dropdown',this).css("left",-offset);
            });
            $(window).resize(function(){ 
            	$(".sw-channel-item.csmc").each(function(){
                    var windowWidth = $(window).width();
                    var dropdownWidth = $('ul.sw-channel-dropdown',this).width();
                    var offset = (windowWidth - dropdownWidth);
                    $('ul.sw-channel-dropdown',this).css("left",-offset);
                });    
            });
            
            var counter=0;
            $(".sw-channel-item").each(function(){
            	if($(this).hasClass('csmc')){
                	counter++
                    $(this).addClass("mm-" + counter);
                }
                if($('.sw-channel-dropdown',this).length){
                	$(this).addClass('has-dd');
                }
            });


        
        },
        
        "Header": function() {
            // FOR SCOPE
            var template = this;
            
            //ADD SITE URL
            const homeLink = '<a style="display:inline-block" href="[$SiteAlias$]">';
            
            //LOGO DIMENSIONS
            var imgWidth = 320;
            var imgHeight = 247;
            
            //ADD LOGO TO HEADER
            var logoSrc = $.trim('<SWCtrl controlname="Custom" props="Name:schoolLogo" />');
            var srcSplit = logoSrc.split("/");
            var srcSplitLen = srcSplit.length;
            
            //CHECK IF LOGO IS EMPTY
            if((logoSrc == '') || (srcSplit[srcSplitLen - 1] == 'default-man.jpg')) {
                logoSrc = this.DefaultLogoSrc;
            }
            
            //ADD CUSTOM SCHOOL LOGO
            $('#gb-logo').append(homeLink + '<img width="' + imgWidth + '" height="' + imgHeight + '" src="' + logoSrc + '" alt="[$SiteName$] Logo"></a>');
            
            if('<SWCtrl controlname="Custom" props="Name:showSchoolName" />' == 'false'){ 
            	$('#gb-logo > a').wrap('<h1 />');
            }else{
            	$('#gb-logo-image').addClass('has-schoolname');
            }

        },

        "Homepage": function() {
            //FOR SCOPE
            var template = this;
            
            //LOOP EACH APP ON H1
            // $(".app").each(function(){
            //     //FOR SCOPE
            //     var thisApp = $(this);

            // });

            //COLUMN TWO CHECK
            if(!$.trim($('.hp-row.four .hp-column.two .region').html()).length ) {    
                $('.hp-row.four').find('.hp-column.two').removeClass('exists');
            } else {
                $('.hp-row.four').find('.hp-column.two').addClass('exists');
            }    
        },

        "MoveHeader": function() {
            switch(this.GetBreakPoint()) {
                case "desktop":
                    $('.header-logo').insertAfter($('#gb-channel-list')); 
                break;
                case "768":
                    $('.header-logo').insertAfter($('#gb-channel-bar')); 
                break;
                case "640":
                    $('.header-logo').insertAfter($('#gb-channel-bar')); 
                break;
                case "480":
                    $('.header-logo').insertAfter($('#gb-channel-bar')); 
                break;
                case "320":
                    $('.header-logo').insertAfter($('#gb-channel-bar')); 
                break;
            }
        },

        "MoveButtons": function() {
            //MOVE LINKS TO HEADERS
            $("[data-region='b'] .app").each(function(){
                //FOR SCOPE
                var thisApp = $(this);
                
                //MOVE CALENDAR LINK IF IT EXISTS
                if ($(thisApp).find(".view-calendar-link").length){
                    //IF IT ISNT WHERE IT SHOULD BE
                    if (!$(thisApp).find(".ui-widget-header .view-calendar-link").length) {
                        // move
                        $(thisApp).find(".view-calendar-link").insertAfter($(thisApp).find(".ui-widget-header h1"));
                    } //else do nothing
                }
                
                //MOVE MORE LINK IF IT EXISTS
                if ($(thisApp).find(".more-link").length){
                    //IF IT ISNT WHERE IT SHOULD BE
                    if (!$(thisApp).find(".ui-widget-header .more-link").length) {
                        // move
                        $(thisApp).find(".more-link").insertAfter($(thisApp).find(".ui-widget-header h1"));
                    } //else do nothing
                }
                //MOVE RSS LINK IF IT EXISTS
                if ($(thisApp).find(".app-level-social-rss").length){
                    if (!$(thisApp).find(".ui-widget-header .app-level-social-rss").length) {
                        // move
                        $(thisApp).find(".app-level-social-rss").insertAfter($(thisApp).find(".ui-widget-header h1"));
                    } //else do nothing
                }
            });
            
            $("[data-region='c'] .app, [data-region='d'] .app").each(function(appIndex, app){
                //FOR SCOPE
                var thisApp = $(this);
                
                //MOVE CALENDAR LINK IF IT EXISTS
                if ($(thisApp).find(".view-calendar-link").length){
                    //IF IT ISNT WHERE IT SHOULD BE
                    if (!$(thisApp).find(".ui-widget-header .view-calendar-link").length) {
                        // move
                        $(thisApp).find(".view-calendar-link").appendTo($(thisApp).find(".ui-widget-footer"));
                    } //else do nothing
                }
                
                //MOVE MORE LINK IF IT EXISTS
                if ($(thisApp).find(".more-link").length){
                    //IF IT ISNT WHERE IT SHOULD BE
                    if (!$(thisApp).find(".ui-widget-header .more-link").length) {
                        // move
                        $(thisApp).find(".more-link").appendTo($(thisApp).find(".ui-widget-footer"));
                    } //else do nothing
                }
                //MOVE RSS LINK IF IT EXISTS
                if ($(thisApp).find(".app-level-social-rss").length){
                    if (!$(thisApp).find(".ui-widget-header .app-level-social-rss").length) {
                        // move
                        $(thisApp).find(".app-level-social-rss").appendTo($(thisApp).find(".ui-widget-footer"));
                    } //else do nothing
                }

            //Hide empty footer
                if( $('.ui-widget-footer', app).children().not('.clear').length <= 0 ){
                    $('.ui-widget-footer', app).addClass('no-children').hide();
                }else{
                    $('.ui-widget-footer', app).addClass('has-children');
                    $('.ui-widget-footer .clear', app).remove();
                }
                });

        },
        
        "Spotlight": function() {
            //FOR SCOPE
            var template = this;

            var spotlightContent = [
                {
                    'show'          : <SWCtrl controlname="Custom" props="Name:igToggle1" />,
                    'text'          : '<SWCtrl controlname="Custom" props="Name:igTitle1" />',
                    'image'         : '<SWCtrl controlname="Custom" props="Name:igImg1" />',
                    'description'   : '<SWCtrl controlname="Custom" props="Name:igDescription1" />'
                },
                {
                    'show'          : <SWCtrl controlname="Custom" props="Name:igToggle2" />,
                    'text'          : '<SWCtrl controlname="Custom" props="Name:igTitle2" />',
                    'image'         : '<SWCtrl controlname="Custom" props="Name:igImg2" />',
                    'description'   : '<SWCtrl controlname="Custom" props="Name:igDescription2" />'
                },
                {
                    'show'          : <SWCtrl controlname="Custom" props="Name:igToggle3" />,
                    'text'          : '<SWCtrl controlname="Custom" props="Name:igTitle3" />',
                    'image'         : '<SWCtrl controlname="Custom" props="Name:igImg3" />',
                    'description'   : '<SWCtrl controlname="Custom" props="Name:igDescription3" />'
                },
                {
                    'show'          : <SWCtrl controlname="Custom" props="Name:igToggle4" />,
                    'text'          : '<SWCtrl controlname="Custom" props="Name:igTitle4" />',
                    'image'         : '<SWCtrl controlname="Custom" props="Name:igImg4" />',
                    'description'   : '<SWCtrl controlname="Custom" props="Name:igDescription4" />'
                },
                {
                    'show'          : <SWCtrl controlname="Custom" props="Name:igToggle5" />,
                    'text'          : '<SWCtrl controlname="Custom" props="Name:igTitle5" />',
                    'image'         : '<SWCtrl controlname="Custom" props="Name:igImg5" />',
                    'description'   : '<SWCtrl controlname="Custom" props="Name:igDescription5" />'
                },
                {
                    'show'          : <SWCtrl controlname="Custom" props="Name:igToggle6" />,
                    'text'          : '<SWCtrl controlname="Custom" props="Name:igTitle6" />',
                    'image'         : '<SWCtrl controlname="Custom" props="Name:igImg6" />',
                    'description'   : '<SWCtrl controlname="Custom" props="Name:igDescription6" />'
                }
            ];

            //CREATE VARIABLE FOR ITEMS
            var igTitles = '';
            var igDescrip = '';

            //LOOP THROUGH EACH ITEM
            $.each(spotlightContent, function(index, items) {

                //BUILD ITEMS
                if (items.show) { //IS TOGGLED ON --LOOK AT THIS CLOSER--

                    //FOR DESKTOP
                    if (index === 0) { //IS BUTTON 1
                        //BUILD TITLE
                        igTitles += ' <button id="title-'+index+'" class="tablink active" role="tab" aria-labelledby="title-'+index+'" aria-selected="true" tabindex="0" aria-controls="tabpanel-'+index+'"><p>'+items.text+'</p></button>'

                        //BUILD DESCRIPTION
                        igDescrip += ' <div id="tabpanel-'+index+'" class="spotlight-tagline show" aria-expanded="true" aria-labelledby="tabpanel-'+index+'"><div class="spotlight-inner-wrap"><p class="mobile-heading">'+items.text+'</p><div class="spotlight-img img-'+index+'"><img src="'+items.image+'" alt="'+items.text+'-icon"></div><h2 class="inner-title">'+items.text +'</h2><p>'+items.description+'</p></div></div>'

                    } else {
                        //BUILD TITLE
                        igTitles += ' <button id="title-'+index+'" class="tablink" role="tab" aria-selected="false" tabindex="-1" aria-labelledby="title-'+index+'" aria-controls="tabpanel-'+index+'"><p>'+items.text+'</p></button>'

                        //BUILD DESCRIPTION
                        igDescrip += ' <div id="tabpanel-'+index+'" class="spotlight-tagline no-show" aria-expanded="true" aria-labelledby="tabpanel-'+index+'"><div class="spotlight-inner-wrap"><p class="mobile-heading">'+items.text+'</p><div class="spotlight-img img-'+index+'"><img src="'+items.image+'" alt="'+items.text+'-icon"></div><h2 class="inner-title">'+items.text +'</h2><p>'+items.description+'</p></div></div>'
                    }
                }
            });

            //IF ITEMS EXIST, PUSH
            if (igTitles.length && igDescrip.length) {
                $('.spotlight-tabs').prepend(igTitles);
                $('.spotlight-tabpanels').prepend(igDescrip);

                //ADD NAV
                $('.spotlight-tabs').prepend('<div class="cs-ig-nav-controls"><button class="cs-ig-nav-control back" aria-label="View the content of the previous tab"></button><button class="cs-ig-nav-control next" aria-label="View the content of the next tab"></button></div>');
            }

            template.IGNav();

            //SPOTLIGHT TABS INTERACTION
            $(".tablink").on('focus click', function(e){
                var currentTab = $(this); //curent tab
                var currentTabPanel = $('#' + $(this).attr('aria-controls')); //corresponding tabpanel

                $(".tablink").removeClass('active'); //remove active from all tabs
                $(".tablink").attr({
                    'aria-selected' : 'false',
                    'tabindex'     : '-1'
                }); //update all tabs to selected false

                currentTab.addClass('active'); //find current tab, make active
                currentTab.attr({
                    'aria-selected': 'true',
                    'tabindex': '0',
                }); //find current tab, make selected

                $(".spotlight-tagline").addClass('no-show'); //add no-show to all tabpanels
                $(".spotlight-tagline").removeClass('show'); //remove show from all tabpanels
                $(".spotlight-tagline").attr('aria-expanded', 'false'); //update all tabpanels to selected false
                currentTabPanel.removeClass('no-show'); //remove no-show from current panel
                currentTabPanel.addClass('show'); //add show to current panel
                currentTabPanel.attr('aria-expanded', 'true'); //update all tabpanels to selected false
            });
            //SPOTLIGHT TAB INTERACTION ONCE FOCUSED
            $(".tablink").on("keydown", function(e){
                var currentTab = $(this); //curent tab
                var currentTabPanel = $('#' + $(this).attr('aria-controls')); //corresponding tabpanel

                //ARROW KEY MOVEMENTS
                switch(e.key) {
    
                    //CONSUME DOWN ARROW KEY
                    case template.KeyCodes.right:
                    case template.KeyCodes.down:
                        e.preventDefault();
                        if($(currentTab).is(":last-child")) { //IF WE ARE AT BOTTOM OF LIST
                            $(".tablink:first-child").focus(); //GO TO FIRST
                        } else {
                            $(currentTab).next().focus(); //IF NOT GO TO NEXT DOWN
                        } 
                    break;

                    //CONSUME UP ARROW KEY
                    case template.KeyCodes.left:
                    case template.KeyCodes.up:
                        e.preventDefault();
                        if($(currentTab).is(":first-child")) { //IF WE ARE AT TOP OF LIST
                            $(".tablink:last-child").focus(); //GO TO FIRST
                        } else {
                            $(currentTab).prev().focus(); //ESLE GO
                        }
                    break;
                }
            });

        },

        "IGNav": function IGNav() {
            var template = this;
        
            function updateAriaPolite() {
              var humanTab = $(".tablink.active").index() + 1;
              var humanNumTabs = $(".tablink").length;
              var updatedText = "Now viewing tab " + humanTab + " of " + humanNumTabs;
              $(".cs-ig-nav-notifications").html(updatedText);
            }
        
            updateAriaPolite(); //NAV CONTROLS
        
            $(".cs-ig-nav-control").on('click keydown', function (event) {
              if (template.AllyClick(event) === true) {
                var resetAttributes = function resetAttributes() { //RESET TABS
                  $(".tablink").attr({
                    "aria-selected": "false",
                    "tabindex": "-1"
                  }).removeClass("active");
                  $(".spotlight-tagline").removeClass("active").removeClass('show').addClass('no-show').attr("aria-hidden", "true");
                };
        
                var activateTabPanel = function activateTabPanel() { //CHANGE TAB
                  var tabPanel = $(".tablink.active").attr("aria-controls");
                  $("#" + tabPanel).addClass("active").addClass('show').removeClass('no-show').attr("aria-hidden", "false");
                };
        
                //DONT LET THE PAGE JUMP ON KEYDOWN
                event.preventDefault();
                var numTabs = $(".tablink").length - 1;
                var currentTab = $(".tablink.active").index() -1; //SUBTRACT ONE BECAUSE IT SKIPPED TWO
        
                if ($(this).hasClass("next")) {
                  if (currentTab < numTabs) {
                    //MOVE TO THE NEXT TAB ITEM
                    resetAttributes();
                    $(".tablink").eq(currentTab).next().attr({
                      "aria-selected": "true",
                      "tabindex": "0"
                    }).addClass("active");
                    activateTabPanel();
                    updateAriaPolite();
                  } else {
                    //MOVE TO THE FIRST TAB ITEM
                    resetAttributes();
                    $(".tablink").eq(0).attr({
                      "aria-selected": "true",
                      "tabindex": "0"
                    }).addClass("active");
                    activateTabPanel();
                    updateAriaPolite();
                  }
                } else {
                  if (currentTab > 0) {
                    //MOVE TO THE PREVIOUS TAB ITEM
                    resetAttributes();
                    $(".tablink").eq(currentTab).prev().attr({
                      "aria-selected": "true",
                      "tabindex": "0"
                    }).addClass("active");
                    activateTabPanel();
                    updateAriaPolite();
                  } else {
                    //MOVE TO THE LAST TAB ITEM
                    resetAttributes();
                    $(".tablink").eq(numTabs).attr({
                      "aria-selected": "true",
                      "tabindex": "0"
                    }).addClass("active");
                    activateTabPanel();
                    updateAriaPolite();
                  }
                }
              }
            }); 

            //TAB FOCUS
            $(".tablink").on("focus", function () {
              if (!$(this).hasClass("active")) {
                $(".tablink").attr({
                  "aria-selected": "false",
                  "tabindex": "-1"
                }).removeClass("active");
                var tabPanel = $(this).attr("aria-controls");
                $(this).attr({
                  "aria-selected": "true",
                  "tabindex": "0"
                }).addClass("active");
                $(".spotlight-tagline").removeClass("active").removeClass('show').attr("aria-hidden", "true");
                $("#" + tabPanel).addClass("active").addClass('show').attr("aria-hidden", "false");
                updateAriaPolite();
              }
            }); //KEYBOARD NAVIGATION
        
            $(".tablink").keydown(function (e) {
              // CAPTURE KEY CODE
              switch (e.key) {
                // CONSUME LEFT AND UP ARROWS
                case template.KeyCodes.left:
                case template.KeyCodes.up:
                  e.preventDefault(); // IS FIRST ITEM
        
                  if ($(this).is(":first-child")) {
                    // FOCUS DROPDOWN BUTTON
                    $(".tablink:last-child").focus();
                  } else {
                    // FOCUS PREVIOUS ITEM
                    $(this).prev().focus();
                  }
        
                  break;
                // CONSUME RIGHT AND DOWN ARROWS
        
                case template.KeyCodes.right:
                case template.KeyCodes.down:
                  e.preventDefault(); // IS LAST ITEM
        
                  if ($(this).is(":last-child")) {
                    // FOCUS FIRST ITEM
                    $(".tablink:first-child").focus();
                  } else {
                    // FOCUS NEXT ITEM
                    $(this).next().focus();
                  }
        
                  break;
                // CONSUME HOME KEY
        
                case template.KeyCodes.home:
                  e.preventDefault(); // FOCUS FIRST ITEM
        
                  $(".tablink:first-child").focus();
                  break;
                // CONSUME END KEY
        
                case template.KeyCodes.end:
                  e.preventDefault(); // FOCUS LAST ITEM
        
                  $(".tablink:last-child").focus();
                  break;
              }
            });
          },

        "Body": function() {
            // FOR SCOPE
            var template = this;
            
            //AUTO FOUCS SIGN IN FIELD
            $("swsignin-txt-username").focus();

            // APPLY RESPONSIVE DIMENSIONS TO CONTENT IMAGES
            $('div.ui-widget.app .ui-widget-detail img')
                .not($('div.ui-widget.app.cs-rs-multimedia-rotator .ui-widget-detail img'))
                .not($('div.ui-widget.app.gallery.json .ui-widget-detail img'))
                .each(function() {
                    if ($(this).attr('width') !== undefined && $(this).attr('height') !== undefined) { // IMAGE HAS INLINE DIMENSIONS
                        $(this).css({'display': 'inline-block', 'width': '100%', 'max-width': $(this).attr('width') + 'px', 'height': 'auto', 'max-height': $(this).attr('height') + 'px'});
                    }
            });

            // ADJUST FIRST BREADCRUMB
            $('li.ui-breadcrumb-first > a > span').text('Home');

            // CHECK PAGELIST HEADER
            if($.trim($(".ui-widget.app.pagenavigation .ui-widget-header").text()) == "") {
                $(".ui-widget.app.pagenavigation .ui-widget-header").html("<h1>[$ChannelName$]</h1>");
            }
            
            //MOVE BUTTONS INTO UI-WIDGET-FOOTER
            $('.sp div.ui-widget.app, .spn div.ui-widget.app').each(function(appIndex, app){
                //more link
                if( $('.more-link', app).length > 0 ){
                    $('.more-link', app).prependTo( $('.ui-widget-footer', app) );
                    $('.more-link-under', app).parent().remove();
                }
                
                //view calendar link
                if( $('.view-calendar-link', app).length > 0 ){
                    $('.view-calendar-link', app).prependTo( $('.ui-widget-footer', app) );
                }
                
                if( $('.app-level-social-follow', app).length > 0 && $('.app-level-social-follow', app).children().length > 0 ){
                    $('.app-level-social-follow', app).prependTo( $('.ui-widget-footer', app) );
                }else{
                    $('.app-level-social-follow', app).remove();
                }
    
                //Hide empty footer
                if( $('.ui-widget-footer', app).children().not('.clear').length <= 0 ){
                    $('.ui-widget-footer', app).addClass('no-children').hide();
                }else{
                    $('.ui-widget-footer', app).addClass('has-children');
                    $('.ui-widget-footer .clear', app).remove();
                }

                //move button to header on accordion app
                if ($(app).hasClass("content-accordion")) {
                    const thisApp = $(app);
                    $('.content-accordion-toolbar', app).appendTo( $('.ui-widget-header', app) );
                }
                
            });

            $(".sp-column.one").csAppAccordion({
                "accordionBreakpoints" : [768, 640, 480, 320]
            });
        },

        "GlobalIcons": function() {
            $("#gb-icons, #gb-icons-sticky").creativeIcons({
                "iconNum"           : '<SWCtrl controlname="Custom" props="Name:numOfIcons" />',
                "defaultIconSrc"    : '',
                "icons"             : [
                    {
                        "image"     : '<SWCtrl controlname="Custom" props="Name:Icon1Image" />', 
                        "showText"  : true,
                        "text"      : '<SWCtrl controlname="Custom" props="Name:Icon1Text" />',
                        "url"       : '<SWCtrl controlname="Custom" props="Name:Icon1Link" />',
                        "target"    : '<SWCtrl controlname="Custom" props="Name:Icon1Target" />'
                    },
                    {
                        "image"     : '<SWCtrl controlname="Custom" props="Name:Icon2Image" />', 
                        "showText"  : true,
                        "text"      : '<SWCtrl controlname="Custom" props="Name:Icon2Text" />',
                        "url"       : '<SWCtrl controlname="Custom" props="Name:Icon2Link" />',
                        "target"    : '<SWCtrl controlname="Custom" props="Name:Icon2Target" />'
                    },
                    {
                        "image"     : '<SWCtrl controlname="Custom" props="Name:Icon3Image" />', 
                        "showText"  : true,
                        "text"      : '<SWCtrl controlname="Custom" props="Name:Icon3Text" />',
                        "url"       : '<SWCtrl controlname="Custom" props="Name:Icon3Link" />',
                        "target"    : '<SWCtrl controlname="Custom" props="Name:Icon3Target" />'
                    },
                    {
                        "image"     : '<SWCtrl controlname="Custom" props="Name:Icon4Image" />', 
                        "showText"  : true,
                        "text"      : '<SWCtrl controlname="Custom" props="Name:Icon4Text" />',
                        "url"       : '<SWCtrl controlname="Custom" props="Name:Icon4Link" />',
                        "target"    : '<SWCtrl controlname="Custom" props="Name:Icon4Target" />'
                    },
                    {
                        "image"     : '<SWCtrl controlname="Custom" props="Name:Icon5Image" />', 
                        "showText"  : true,
                        "text"      : '<SWCtrl controlname="Custom" props="Name:Icon5Text" />',
                        "url"       : '<SWCtrl controlname="Custom" props="Name:Icon5Link" />',
                        "target"    : '<SWCtrl controlname="Custom" props="Name:Icon5Target" />'
                    },
                    {
                        "image": '<SWCtrl controlname="Custom" props="Name:Icon6Image" />',
                        "showText": true,
                        "text": '<SWCtrl controlname="Custom" props="Name:Icon6Text" />',
                        "url": '<SWCtrl controlname="Custom" props="Name:Icon6Link" />',
                        "target": '<SWCtrl controlname="Custom" props="Name:Icon6Target" />'
                    },
                    {
                        "image": '<SWCtrl controlname="Custom" props="Name:Icon7Image" />',
                        "showText": true,
                        "text": '<SWCtrl controlname="Custom" props="Name:Icon7Text" />',
                        "url": '<SWCtrl controlname="Custom" props="Name:Icon7Link" />',
                        "target": '<SWCtrl controlname="Custom" props="Name:Icon7Target" />'
                    },
                    {
                        "image": '<SWCtrl controlname="Custom" props="Name:Icon8Image" />',
                        "showText": true,
                        "text": '<SWCtrl controlname="Custom" props="Name:Icon8Text" />',
                        "url": '<SWCtrl controlname="Custom" props="Name:Icon8Link" />',
                        "target": '<SWCtrl controlname="Custom" props="Name:Icon8Target" />'
                    },
                    {
                        "image": '<SWCtrl controlname="Custom" props="Name:Icon9Image" />',
                        "showText": true,
                        "text": '<SWCtrl controlname="Custom" props="Name:Icon9Text" />',
                        "url": '<SWCtrl controlname="Custom" props="Name:Icon9Link" />',
                        "target": '<SWCtrl controlname="Custom" props="Name:Icon9Target" />'
                    },
                    {
                        "image": '<SWCtrl controlname="Custom" props="Name:Icon10Image" />',
                        "showText": true,
                        "text": '<SWCtrl controlname="Custom" props="Name:Icon10Text" />',
                        "url": '<SWCtrl controlname="Custom" props="Name:Icon10Link" />',
                        "target": '<SWCtrl controlname="Custom" props="Name:Icon10Target" />'
                    },
                    {
                        "image": '<SWCtrl controlname="Custom" props="Name:Icon11Image" />',
                        "showText": true,
                        "text": '<SWCtrl controlname="Custom" props="Name:Icon11Text" />',
                        "url": '<SWCtrl controlname="Custom" props="Name:Icon11Link" />',
                        "target": '<SWCtrl controlname="Custom" props="Name:Icon11Target" />'
                    },
                    {
                        "image": '<SWCtrl controlname="Custom" props="Name:Icon12Image" />',
                        "showText": true,
                        "text": '<SWCtrl controlname="Custom" props="Name:Icon12Text" />',
                        "url": '<SWCtrl controlname="Custom" props="Name:Icon12Link" />',
                        "target": '<SWCtrl controlname="Custom" props="Name:Icon12Target" />'
                    }
                ],
                    "siteID"        : "[$SiteID$]",
                    "siteAlias"     : "[$SiteAlias$]",
                    "calendarLink"  : "[$SiteCalendarLink$]",
                    "contactEmail"  : "[$SiteContactEmail$]",
                    "allLoaded"     : function(){ }
            });

        },

        "SocialIcons": function() {
            var socialIcons = [{
                "show":'<SWCtrl controlname="Custom" props="Name:showFacebook" />',
                "label": "Facebook",
                "class": "facebook",
                "url":'<SWCtrl controlname="Custom" props="Name:FacebookUrl" />',
                "target":'<SWCtrl controlname="Custom" props="Name:FacebookTarget" />'
              }, 
              {
                "show":'<SWCtrl controlname="Custom" props="Name:showTwitter" />',
                "label": "Twitter",
                "class": "twitter",
                "url":'<SWCtrl controlname="Custom" props="Name:TwitterUrl" />',
                "target":'<SWCtrl controlname="Custom" props="Name:TwitterTarget" />'
              },
              {
                "show":'<SWCtrl controlname="Custom" props="Name:showYouTube" />',
                "label": "YouTube",
                "class": "youtube",
                "url":'<SWCtrl controlname="Custom" props="Name:YouTubeUrl" />',
                "target":'<SWCtrl controlname="Custom" props="Name:YouTubeTarget" />'
              }, 
              {
                "show":'<SWCtrl controlname="Custom" props="Name:showInstagram" />',
                "label": "Instagram",
                "class": "instagram",
                "url":'<SWCtrl controlname="Custom" props="Name:InstagramUrl" />',
                "target":'<SWCtrl controlname="Custom" props="Name:InstagramTarget" />'
              }, 
              {
                "show":'<SWCtrl controlname="Custom" props="Name:showLinkedIn" />',
                "label": "LinkedIn",
                "class": "linkedin",
                "url":'<SWCtrl controlname="Custom" props="Name:LinkedInUrl" />',
                "target":'<SWCtrl controlname="Custom" props="Name:LinkedInTarget" />'
              }, 
              {
                "show":'<SWCtrl controlname="Custom" props="Name:showVimeo" />',
                "label": "Vimeo",
                "class": "vimeo",
                "url":'<SWCtrl controlname="Custom" props="Name:VimeoUrl" />',
                "target":'<SWCtrl controlname="Custom" props="Name:VimeoTarget" />'
              }, 
              {
                "show":'<SWCtrl controlname="Custom" props="Name:showFlickr" />',
                "label": "Flickr",
                "class": "flickr",
                "url":'<SWCtrl controlname="Custom" props="Name:FlickrUrl" />',
                "target":'<SWCtrl controlname="Custom" props="Name:FlickrTarget" />'
              }, 
              {
                "show":'<SWCtrl controlname="Custom" props="Name:showPinterest" />',
                "label": "Pinterest",
                "class": "pinterest",
                "url":'<SWCtrl controlname="Custom" props="Name:PinterestUrl" />',
                "target":'<SWCtrl controlname="Custom" props="Name:PinterestTarget" />'
              }, 
              {
                "show":'<SWCtrl controlname="Custom" props="Name:showPeachjar" />',
                "label": "Peachjar",
                "class": "peachjar",
                "url":'<SWCtrl controlname="Custom" props="Name:PeachjarUrl" />',
                "target":'<SWCtrl controlname="Custom" props="Name:PeachjarTarget" />'
              },
              {
                "show":'<SWCtrl controlname="Custom" props="Name:showRss" />',
                "label": "Rss",
                "class": "rss",
                "url":'<SWCtrl controlname="Custom" props="Name:RssUrl" />',
                "target":'<SWCtrl controlname="Custom" props="Name:RssTarget" />'
              }];

              //CREATE ICON VARIABLE
              var icons = "";

              //LOOP THROUGH EACH AND BUILD
              $.each(socialIcons, function(index, icon) {
                if(icon.show) {
                    icons += '<li class="gb-social-icon" data-toggle=' + icon.show + '><a class="gb-social-icons ' + icon.class + ' district" href="' + icon.url + '" target="' + icon.target + '" aria-label="' + icon.label + '"></a></li>';
                }
              });

            //AFTER LOOP PUSH ICONS TO DOM
            if(icons.length) {
                $("#gb-socials").prepend(icons); 
            }

        },

        "MMGPlugin": function MMGPlugin() {
            // FOR SCOPE
            var template = this;
            
            if($("#sw-content-container10 .ui-widget.app.multimedia-gallery").length) {
                var mmg = eval("multimediaGallery" + $("#sw-content-container10 .ui-widget.app.multimedia-gallery:first").attr("data-pmi"));
                mmg.props.defaultGallery = false;
                
                $("#sw-content-container10 .ui-widget.app.multimedia-gallery:first").csMultimediaGallery({
                    "efficientLoad" : true,
                    "imageWidth" : 1500,
                    "imageHeight" : 993,
                    "mobileDescriptionContainer": [960, 768, 640, 480, 320], // [960, 768, 640, 480, 320]
                    "galleryOverlay" : false,
                    "linkedElement" : [], // ["image", "title", "overlay"]
                    "playPauseControl" : true,
                    "backNextControls" : true,
                    "bullets" : false,
                    "thumbnails" : false,
                    "thumbnailViewerNum": [4, 4, 3, 3, 2], // NUMERICAL - [960 view, 768 view, 640 view, 480 view, 320 view]
                    "autoRotate" : false,
                    "hoverPause" : true,
                    "transitionType" : "slide", // fade, slide, custom
                    "transitionSpeed" : 1.5,
                    "transitionDelay" : 4,
                    "fullScreenRotator" : false,
                    "fullScreenBreakpoints" : [960], // NUMERICAL - [960, 768, 640, 480, 320]
                    "onImageLoad" : function(props) {}, // props.element, props.recordIndex, props.mmgRecords
                    "allImagesLoaded" : function(props) {}, // props.element, props.mmgRecords
                    "onTransitionStart" : function(props) {}, // props.element, props.currentRecordIndex, props.currentGalleryIndex, props.nextRecordIndex, props.nextGalleryIndex, props.mmgRecords
                    "onTransitionEnd" : function(props) {}, // props.element, props.currentRecordIndex, props.currentGalleryIndex, props.mmgRecords
                    "allLoaded" : function(props) {

                        //WRAP DESCRIPTION IN WRAPPER
                        $('.mmg-description-outer', props.element).wrap('<div class="mmg-description-wrapper flex-cont fd-col jc-sb" />');

                        //IF CONTROLS EXIST, THEN WRAP THEM
                        if( $('.mmg-control', props.element).length > 0 ){
                            $('.mmg-description-outer', props.element).append('<div class="mmg-controls flex-cont ai-c jc-c"></div>');
    
                            $('.mmg-controls', props.element).append($('#hp-slideshow .mmg-control.back'));
                            $('.mmg-controls', props.element).append($('#hp-slideshow .mmg-control.play-pause'));
                            $('.mmg-controls', props.element).append($('#hp-slideshow .mmg-control.next'));
                        }

                    }, // props.element, props.mmgRecords
                    "onWindowResize": function(props) {} // props.element, props.mmgRecords
                });
            }
        },

        "Slideshow": function Slideshow() {
            // FOR SCOPE
            var template = this;
        
              if('<SWCtrl controlname="Custom" props="Name:slideshowFeature" />' == "Multimedia Gallery;Streaming Video" || '<SWCtrl controlname="Custom" props="Name:slideshowFeature" />' == "Multimedia Gallery") {
                  if($("#sw-content-container10 .ui-widget.app.multimedia-gallery").length) {
                      this.MMGPlugin();
                  }
              } else {
                  this.StreamingVideo();
              }
          },

        "MMGGrid": function(){
        	
            //The MMG properties that we want to set for each photo grid
            var mmgGridSettings = {
                "efficientLoad" : false, //This must be false so all of the images will load.
                "imageWidth" : 300,
                "imageHeight" : 350,
                "mobileDescriptionContainer": [768, 640, 480, 320], // [960, 768, 640, 480, 320] //needs to be left blank
                "linkedElement" : ["image"], // ["image", "title", "overlay"]
                "playPauseControl" : true,
                "backNextControls" : true,
                "autoRotate" : false,
                "onImageLoad" : function(props) {}, // props.element, props.recordIndex, props.mmgRecords
                "allImagesLoaded" : function(props) {}, // props.element, props.mmgRecords
                "allLoaded" : function(props) {
                    if( props.mmgRecords.length > 1 ){
                        // pause the MMG if it autorotate param fails to stop rotation intiially
                        $('.mmg-control.play-pause', props.element).click();
                         
                        //wrap all of the descriptions to make overflow styling easier and more visually appealing
                        $('.mmg-description', props.element).wrap('<div class="mmg-description-wrapper" />');
                    }
                    
                    //reset are aria attributes.
                    $('.mmg-slide', props.element)
                        .attr('tabindex', 0)
                        .attr('aria-hidden', 'false');
                    
                    //check each slide for a description, if there isn't one then remove the tabindex because there isn't anything to show.
                    $('.mmg-slide', props.element).each(function(slideIndex, slide){
                        if(!$('.mmg-description', slide).length){
                            $(this).attr('tabindex', '');
                        }
                    });

                    //IF CONTROLS EXIST, THEN WRAP THEM
                    if( $('.mmg-control', props.element).length > 0 ){
                        $('.mmg-slides-outer', props.element).append('<div class="mmg-controls"></div>');

                        $('.mmg-controls', props.element).append($('#mmg-photo-grid .mmg-control.back'));
                        $('.mmg-controls', props.element).append($('#mmg-photo-grid .mmg-control.play-pause'));
                        $('.mmg-controls', props.element).append($('#mmg-photo-grid .mmg-control.next'));
                    }
                }, // props.element, props.mmgRecords
                "onWindowResize": function(props) {} // props.element, props.mmgRecords
            };

            // Go through each grid area
            $("#mmg-photo-grid").each(function(photoGridIndex, photoGrid){
                // Loop through each mmg app in that grid area
                if( $(".ui-widget.app.multimedia-gallery", photoGrid).length ){
                    $('#pmi-' + $(".ui-widget.app.multimedia-gallery", photoGrid).attr('data-pmi')).next().addClass('app-after-grid');
                    //mark the row if there happens to be an MMG at the beginning or end of the content in this row
                    if( $(".region > div:first .ui-widget.app.multimedia-gallery", photoGrid).length ) $(photoGrid).addClass('grid-is-first'); //remove rows top-padding
                    if( $(".region > div:last .ui-widget.app.multimedia-gallery", photoGrid).length ) $(photoGrid).addClass('grid-is-last'); //remove rows bottom-padding
                    
                    //build out the MMG's within this grid area with our settings 
                    $(".ui-widget.app.multimedia-gallery", photoGrid).each(function(appIndex, app){
                        var mmgGrid = eval("multimediaGallery" + $(app).attr("data-pmi"));
                        mmgGrid.props.defaultGallery = false;

                        $(app).csMultimediaGallery(mmgGridSettings);

                        // if ($(window).width() < 640) {
                        //     alert('Less than 960');
                        //  }
                        //  else {
                        //     alert('More than 960');
                        //  }
                    });
                }
         });
        
         
            $('#mmg-photo-grid .mmg-slide a').focus(function(e){
                $(this).closest('.mmg-slide').addClass('focus');
            })
            .blur(function(e){
                $(this).closest('.mmg-slide').removeClass('focus');
            });
            
            $('#mmg-photo-grid.mmg-slide').on('click keydown', '.mmg-description-link', function(e){
                if(e.handleObj.type == "click" || e.keyCode == 13){
                    var activeRecordIndex = $(this).closest('li.mmg-slide').attr('data-record-index');
                    $(this).closest('li.mmg-slide').addClass('active');
                    $(this).closest('div.mmg-container').attr('data-active-record-index', activeRecordIndex);
                }
            });
            
            
            //if the close button is clicked then return focus to classed watch video btn and remove that class
            $('#mmg-photo-grid').on('keydown', '.mmg-video-close', function(e){
                if(e.keyCode == 13){
                    setTimeout(function(e){
                        $('#mmg-photo-grid .mmg-slide.active').removeClass('active');
                    }, 1000);
                }
            });
        },

        "StreamingVideo": function StreamingVideo() {
        // FOR SCOPE
        var template = this;
        var videoVendor ='<SWCtrl controlname="Custom" props="Name:streamingVideoVendor" />';
        var mobilePhoto ='<SWCtrl controlname="Custom" props="Name:mobileBackgroundPhoto" />';
    
        if ($.trim(mobilePhoto) == "" || mobilePhoto.indexOf("default-man.jpg") > -1) {
            mobilePhoto = "/cms/lib/SWCS000001/Centricity/template/103/defaults/streaming-video-default.jpg";
        }
    
        $("#hp-slideshow").csStreamingVideo({
            // USER CONFIGURATIONS
            "videoSource": videoVendor,
            // OPTIONS ARE: YouTube, Vimeo, MyVRSpot
            "videoID":'<SWCtrl controlname="Custom" props="Name:streamingVideoID" />',
            // DEFAULT IS BLANK TO AUTOFILL THE PROPER ID FOR THE VENDOR
            "myVRSpotVideoWidth": "853.5",
            "myVRSpotVideoHeight": "480",
            "fullWidthBreakpoints": [],
            // OPTIONS ARE: ["Desktop", 1024, 768, 640, 480, 320]
            "fullWidthCSSPosition": "",
            // OPITONS ARE: static, relative, fixed, absolute, sticky
            "fullScreenBreakpoints": [],
            // OPTIONS ARE: ["Desktop", 1024, 768, 640, 480, 320]
            "fullScreenCSSPosition": "",
            // OPITONS ARE: static, relative, fixed, absolute, sticky
            "showVideoTitle":<SWCtrl controlname="Custom" props="Name:showVideoTitle" />,
            "videoTitleText":'<SWCtrl controlname="Custom" props="Name:videoTitle" />',
            "showVideoDescription":<SWCtrl controlname="Custom" props="Name:showVideoCaption" />,
            "videoDescriptionText":'<SWCtrl controlname="Custom" props="Name:videoCaption" />',
            "showVideoLinks":<SWCtrl controlname="Custom" props="Name:showVideoLink" />,
            "videoLinks": [{
            "text":'<SWCtrl controlname="Custom" props="Name:videoLinkText" />',
            "link":'<SWCtrl controlname="Custom" props="Name:videoLinkUrl" />',
            "target":'<SWCtrl controlname="Custom" props="Name:videoLinkTarget" />'
            }],
            //{ "text": "Video Text", "link": "Video Link", "target": "Video Target"  }
            "showAudioButtons": true,
            "showWatchVideoButton": true,
            "useBackgroundPhoto": true,
            "backgroundPhoto": mobilePhoto,
            "backgroundPhotoBreakpoints": [1024, 768, 640, 480, 320],
            // OPTIONS ARE: ["Desktop", 1024, 768, 640, 480, 320]
            "backgroundPhotoCSSPosition": "",
            // OPITONS ARE: static, relative, fixed, absolute, sticky
            "useCustomResize": false,
            "extendResize": function extendResize(props) {
            switch (template.GetBreakPoint()) {
                case "desktop":
                $(".cs-streaming-video-controls", props.element).insertAfter(".cs-streaming-video-description", props.element);
                break;
    
                case '768':
                case "640":
                case "480":
                case "320":
                $(".cs-streaming-video-controls", props.element).appendTo(".cs-streaming-video-description", props.element);
                break;
            }
            },
            //element, videoHeight, videoWidth
            "onReady": function onReady(props) {},
            // IF NOEMBED REQUEST RETURNS VIDEO DATA, PROPS WILL BE THE RETURNED VIDEO DATA OBJECT. OTHERWISE, PROPS WILL BE props.width AND props.height FOR VIDEO WIDTH AND HEIGHT
            "allLoaded": function allLoaded(props) {
            switch (template.GetBreakPoint()) {
                case "desktop":
                $(".cs-streaming-video-controls", props.element).insertAfter(".cs-streaming-video-description", props.element);
                break;
    
                case '768':
                case "640":
                case "480":
                case "320":
                $(".cs-streaming-video-controls", props.element).appendTo(".cs-streaming-video-description", props.element);
                break;
            }
            
            } // props.element
    
        });
        },          

          "ModEvents": function() {
            // FOR SCOPE
            var template = this;

            $(".ui-widget.app.upcomingevents").modEvents({
                columns     : "yes",
                monthLong   : "no",
                dayWeek     : "yes"
            });

            eventsByDay(".upcomingevents .ui-articles");

            function eventsByDay(container) {
                $(".ui-article", container).each(function(){
                    if (!$(this).find("h1.ui-article-title.sw-calendar-block-date").size()){
                        var moveArticle = $(this).html();
                        $(this).parent().prev().children().children().next().append(moveArticle);
                        $(this).parent().remove();
                    };
                }); 

                $(".ui-article", container).each(function(){
                    var newDateTime = $('.upcoming-column.left h1', this).html().toLowerCase().split("</span>");
                    var newDate = '';
                    var dateIndex = ((newDateTime.length > 2) ? 2 : 1); //if the dayWeek is set to yes we need to account for that in our array indexing
                    
                    //if we have the day of the week make sure to include it.
                    if( dateIndex > 1 ){ 
                        newDate += newDateTime[0] + "</span>";
                    }
                    
                    //add in the month
                    newDate += newDateTime[dateIndex - 1] + '</span>'; //the month is always the in the left array position to the date
                    
                    //wrap the date in a new tag
                    newDate += '<span class="jeremy-date">' + newDateTime[dateIndex] + '</span>';
                    
                    //append the date and month back into their columns
                    $('.upcoming-column.left h1', this).html(newDate);  
                    
                    
                    //add an ALL DAY label if no time was given
                    $('.upcoming-column.right .ui-article-description', this).each(function(){
                        if( $('.sw-calendar-block-time', this).length < 1 ){ //if it doesnt exist add it
                            $(this).prepend('<span class="sw-calendar-block-time">ALL DAY</span>');
                        }
                        var text = $(".sw-calendar-block-title", this).text();
                        var text = text.split("@");
                        if(text.length == 2) {
                            $(".sw-calendar-block-title a", this).text(text[0]);
                            $(this).append("<span class='sw-calendar-block-location'>"+text[1]+"</span>");
                        }
                        $(".sw-calendar-block-time", this).appendTo($(this));
                    });
                    
                    //WRAP DATE AND MONTH IN A CONTAINER
                    $(".jeremy-date, .joel-month", this).wrapAll("<span class='adam-hug'></span>");

                    var eventDay = $.trim($(this).find(".joel-day").html().toLowerCase());
                    var eventMonth = $.trim($(this).find(".joel-month").text().toLowerCase());
                    var eventNumberDay = $.trim($(this).find(".jeremy-date").text());
                    var ariaMonth = "";
                    var ariaDate = "";

					//ADD MORE EVENTS LINK
                    if ($(this).find(".ui-article-description").length > 2) {
                      var moreEventsLink = $(this).find(".ui-article-description:nth-child(1) a").attr("href");
                      moreEventsLink = moreEventsLink.split("/").slice(0, -2).join("/");
                      moreEventsLink = moreEventsLink + "/day";
                      $(this).find(".upcoming-column.right").append("<a href='" + moreEventsLink + "' target='blank' aria-label='View all events for " + eventDay + ", " + ariaMonth + " " + ariaDate + ".' class='all-events-link'></a>");
                    }
                    
                    switch (eventMonth) {
                      case "jan":
                        ariaMonth = "January";
                        break;

                      case "feb":
                        ariaMonth = "February";
                        break;

                      case "mar":
                        ariaMonth = "March";
                        break;

                      case "apr":
                        ariaMonth = "April";
                        break;

                      case "may":
                        ariaMonth = "May";
                        break;

                      case "jun":
                        ariaMonth = "June";
                        break;

                      case "jul":
                        ariaMonth = "July";
                        break;

                      case "aug":
                        ariaMonth = "August";
                        break;

                      case "sep":
                        ariaMonth = "September";
                        break;

                      case "oct":
                        ariaMonth = "October";
                        break;

                      case "nov":
                        ariaMonth = "November";
                        break;

                      case "dec":
                        ariaMonth = "December";
                        break;

                      default:
                        ariaMonth = eventMonth;
                        break;
                    }

                    switch (eventNumberDay) {
                      case "1":
                        ariaDate = "First";
                        break;

                      case "2":
                        ariaDate = "Second";
                        break;

                      case "3":
                        ariaDate = "Third";
                        break;

                      case "4":
                        ariaDate = "Fourth";
                        break;

                      case "5":
                        ariaDate = "Fifth";
                        break;

                      case "6":
                        ariaDate = "Sixth";
                        break;

                      case "7":
                        ariaDate = "Seventh";
                        break;

                      case "8":
                        ariaDate = "Eighth";
                        break;

                      case "9":
                        ariaDate = "Ninth";
                        break;

                      case "10":
                        ariaDate = "Tenth";
                        break;

                      case "11":
                        ariaDate = "Eleventh";
                        break;

                      case "12":
                        ariaDate = "Twelveth";
                        break;

                      case "13":
                        ariaDate = "Thirteenth";
                        break;

                      case "14":
                        ariaDate = "Fourteenth";
                        break;

                      case "15":
                        ariaDate = "Fifteenth";
                        break;

                      case "16":
                        ariaDate = "Sixteenth";
                        break;

                      case "17":
                        ariaDate = "Seventeenth";
                        break;

                      case "18":
                        ariaDate = "Eighteenth";
                        break;

                      case "19":
                        ariaDate = "Nineteenth";
                        break;

                      case "20":
                        ariaDate = "Twentieth";
                        break;

                      case "21":
                        ariaDate = "Twenty-first";
                        break;

                      case "22":
                        ariaDate = "Twenty-second";
                        break;

                      case "23":
                        ariaDate = "Twenty-third";
                        break;

                      case "24":
                        ariaDate = "Twenty-fourth"

                      case "25":
                        ariaDate = "Twenty-fifth";
                        break;

                      case "26":
                        ariaDate = "Twenty-sixth";
                        break;

                      case "27":
                        ariaDate = "Twenty-seventh";
                        break;

                      case "28":
                        ariaDate = "Twenty-eighth";
                        break;

                      case "29":
                        ariaDate = "Twenty-ninth";
                        break;

                      case "30":
                        ariaDate = "Thirtieth";
                        break;

                      case "31":
                        ariaDate = "Thirty-first";
                        break;

                      default:
                        ariaDate = eventNumberDay;
                        break;
                    }

                }) 
            }

            //ADD NO EVENTS TEXT
            $(".upcomingevents").each(function(){
                if(!$(this).find(".ui-article").length){
                    $("<li><p>There are no upcoming events to display.</p></l1>").appendTo($(this).find(".ui-articles"));
                }
            });
    
        },


        "RsMenu": function() {
            //FOR SCOPE
            var template = this;
            var usedSearchLinks = [];
            var searchLinks = [];

            searchLinks.forEach(checkIfUsed);

            var extraOptions = {};
            //ARE THEY USED, IF SO ASSIGN THEM
            if (usedSearchLinks.length > 0){
                Object.assign(extraOptions, {"Quick Links": usedSearchLinks});
            };

            //CHECK IF ASSIGNED
            function checkIfUsed(item) {
                //TRIM
                if ($.trim(item.text) != "") {
                    var itemToPush = {
                        "text"      :   item.text,
                        "url"       :   item.url,
                        "target"    :   item.target                       
                    };

                    //PLACE USED LINKS
                    usedSearchLinks.push(itemToPush);
                }
            }
            $.csRsMenu({
                "breakPoint" : 768, // SYSTEM BREAK POINTS - 768, 640, 480, 320
                "slideDirection" : "left-to-right", // OPTIONS - left-to-right, right-to-left
                "menuButtonParent" : "#gb-header-mobile",
                "menuBtnText" : "Menu",
                "colors": {
                    "pageOverlay": '#000000',
                    "menuBackground": '#FFFFFF',
                    "menuText": '#333333',
                    "menuTextAccent": '#333333',
                    "dividerLines": '#E6E6E6',
                    "buttonBackground": '#E6E6E6',
                    "buttonText": '#333333'
                },
                "showDistrictHome": template.ShowDistrictHome,
                "districtHomeText": "District",
                "showSchools" : template.ShowSchoolList,
                "schoolMenuText": "Schools",
                "showTranslate" : true,
                "translateMenuText": "Translate",
                "translateVersion": 2, // 1 = FRAMESET, 2 = BRANDED
                "translateId" : "",
                "translateLanguages": [ // ["ENGLISH LANGUAGE NAME", "TRANSLATED LANGUAGE NAME", "LANGUAGE CODE"]
                    ["Afrikaans", "Afrikaans", "af"],
                    ["Albanian", "shqiptar", "sq"],
                    ["Amharic", "አማርኛ", "am"],
                    ["Arabic", "العربية", "ar"],
                    ["Armenian", "հայերեն", "hy"],
                    ["Azerbaijani", "Azərbaycan", "az"],
                    ["Basque", "Euskal", "eu"],
                    ["Belarusian", "Беларуская", "be"],
                    ["Bengali", "বাঙালি", "bn"],
                    ["Bosnian", "bosanski", "bs"],
                    ["Bulgarian", "български", "bg"],
                    ["Burmese", "မြန်မာ", "my"],
                    ["Catalan", "català", "ca"],
                    ["Cebuano", "Cebuano", "ceb"],
                    ["Chichewa", "Chichewa", "ny"],
                    ["Chinese Simplified", "简体中文", "zh-CN"],
                    ["Chinese Traditional", "中國傳統的", "zh-TW"],
                    ["Corsican", "Corsu", "co"],
                    ["Croatian", "hrvatski", "hr"],
                    ["Czech", "čeština", "cs"],
                    ["Danish", "dansk", "da"],
                    ["Dutch", "Nederlands", "nl"],
                    ["Esperanto", "esperanto", "eo"],
                    ["Estonian", "eesti", "et"],
                    ["Filipino", "Pilipino", "tl"],
                    ["Finnish", "suomalainen", "fi"],
                    ["French", "français", "fr"],
                    ["Galician", "galego", "gl"],
                    ["Georgian", "ქართული", "ka"],
                    ["German", "Deutsche", "de"],
                    ["Greek", "ελληνικά", "el"],
                    ["Gujarati", "ગુજરાતી", "gu"],
                    ["Haitian Creole", "kreyòl ayisyen", "ht"],
                    ["Hausa", "Hausa", "ha"],
                    ["Hawaiian", "ʻŌlelo Hawaiʻi", "haw"],
                    ["Hebrew", "עִברִית", "iw"],
                    ["Hindi", "हिंदी", "hi"],
                    ["Hmong", "Hmong", "hmn"],
                    ["Hungarian", "Magyar", "hu"],
                    ["Icelandic", "Íslenska", "is"],
                    ["Igbo", "Igbo", "ig"],
                    ["Indonesian", "bahasa Indonesia", "id"],
                    ["Irish", "Gaeilge", "ga"],
                    ["Italian", "italiano", "it"],
                    ["Japanese", "日本語", "ja"],
                    ["Javanese", "Jawa", "jw"],
                    ["Kannada", "ಕನ್ನಡ", "kn"],
                    ["Kazakh", "Қазақ", "kk"],
                    ["Khmer", "ភាសាខ្មែរ", "km"],
                    ["Korean", "한국어", "ko"],
                    ["Kurdish", "Kurdî", "ku"],
                    ["Kyrgyz", "Кыргызча", "ky"],
                    ["Lao", "ລາວ", "lo"],
                    ["Latin", "Latinae", "la"],
                    ["Latvian", "Latvijas", "lv"],
                    ["Lithuanian", "Lietuvos", "lt"],
                    ["Luxembourgish", "lëtzebuergesch", "lb"],
                    ["Macedonian", "Македонски", "mk"],
                    ["Malagasy", "Malagasy", "mg"],
                    ["Malay", "Malay", "ms"],
                    ["Malayalam", "മലയാളം", "ml"],
                    ["Maltese", "Malti", "mt"],
                    ["Maori", "Maori", "mi"],
                    ["Marathi", "मराठी", "mr"],
                    ["Mongolian", "Монгол", "mn"],
                    ["Myanmar", "မြန်မာ", "my"],
                    ["Nepali", "नेपाली", "ne"],
                    ["Norwegian", "norsk", "no"],
                    ["Nyanja", "madambwe", "ny"],
                    ["Pashto", "پښتو", "ps"],
                    ["Persian", "فارسی", "fa"],
                    ["Polish", "Polskie", "pl"],
                    ["Portuguese", "português", "pt"],
                    ["Punjabi", "ਪੰਜਾਬੀ ਦੇ", "pa"],
                    ["Romanian", "Română", "ro"],
                    ["Russian", "русский", "ru"],
                    ["Samoan", "Samoa", "sm"],
                    ["Scottish Gaelic", "Gàidhlig na h-Alba", "gd"],
                    ["Serbian", "Српски", "sr"],
                    ["Sesotho", "Sesotho", "st"],
                    ["Shona", "Shona", "sn"],
                    ["Sindhi", "سنڌي", "sd"],
                    ["Sinhala", "සිංහල", "si"],
                    ["Slovak", "slovenský", "sk"],
                    ["Slovenian", "slovenski", "sl"],
                    ["Somali", "Soomaali", "so"],
                    ["Spanish", "Español", "es"],
                    ["Sundanese", "Sunda", "su"],
                    ["Swahili", "Kiswahili", "sw"],
                    ["Swedish", "svenska", "sv"],
                    ["Tajik", "Тоҷикистон", "tg"],
                    ["Tamil", "தமிழ்", "ta"],
                    ["Telugu", "తెలుగు", "te"],
                    ["Thai", "ไทย", "th"],
                    ["Turkish", "Türk", "tr"],
                    ["Ukrainian", "український", "uk"],
                    ["Urdu", "اردو", "ur"],
                    ["Uzbek", "O'zbekiston", "uz"],
                    ["Vietnamese", "Tiếng Việt", "vi"],
                    ["Welsh", "Cymraeg", "cy"],
                    ["Western Frisian", "Western Frysk", "fy"],
                    ["Xhosa", "isiXhosa", "xh"],
                    ["Yiddish", "ייִדיש", "yi"],
                    ["Yoruba", "yorùbá", "yo"],
                    ["Zulu", "Zulu", "zu"]
                ],
                "showAccount": true,
                "accountMenuText": "User Options",
                "usePageListNavigation": false,
                "extraMenuOptions": extraOptions,
                "siteID": "[$siteID$]",
                "allLoaded": function(){}
            });
        },

        "Translate": function() {
            $('#translate-dropdown').creativeTranslate({
                'type': 2, // 1 = FRAMESET, 2 = BRANDED, 3 = API
                'advancedOptions': {
                    'addMethod': 'append',
                    'removeBrandedDefaultStyling': false,
                    'useTranslatedNames': true, // ONLY FOR FRAMESET AND API VERSIONS - true = TRANSLATED LANGUAGE NAME, false = ENGLISH LANGUAGE NAME
                },
                'translateLoaded': function() {} // CALLBACK TO RUN YOUR CUSTOM CODE AFTER EVERYTHING IS READY               
            });
        },

        "Search": function() {
            var template = this;

            const defSearchText = 'Search...';
        
            const searchBox = $('#gb-search-input');
            
            searchBox
                .attr('placeholder', '')
                .val(defSearchText)
                .focus(function() {
                    if($(this).val() == defSearchText) {
                        $(this).val('');
                    }
                })
                .blur(function() {
                    if($(this).val() == '') {
                        $(this).val(defSearchText);
                    }
                });
                
            $('#gb-search-submit').submit(function(e){
                e.preventDefault();
        
                if($('#gb-search-input').val() != '' && $('#gb-search-input').val() != defSearchText) {
                    window.location.href = '/site/Default.aspx?PageType=6&SiteID=[$SiteID$]&SearchString=' + $('#gb-search-input').val();
                }
            });

            $(".search-selector").on('click keydown', function (event) {
  
                if (template.AllyClick(event) === true) {
                  //DONT LET THE PAGE JUMP ON KEYDOWN
                  event.preventDefault();
          
                  if (!$('#search-form-wrapper').hasClass("open")) {
                    openSearch();
                  } else {
                    closeSearch();
                  }
                }
              });

            $(".search-selector").on("focusout", function () {
                setTimeout(function () {
                  if (!$(".gb-search-input").hasClass("focus")) {
                    closeSearch();
                  }
                }, 300);
              });
              
            function closeSearch() {
            $(".search-selector").removeClass("open");
            $("#search-form-wrapper").removeClass("open").attr("aria-expanded", "false");
            $("#search-form-wrapper").slideUp().removeClass("open").attr("aria-hidden", "true"); //WHAT OPENS
        
            $(".search-selector").attr("tabindex", "-1");
            $("#gb-search-input").attr("tabindex", "-1");
            }
        
            function openSearch() {
            $(".search-selector").addClass("open").attr("aria-expanded", "true");
            $("#search-form-wrapper").addClass("open");
            $("#search-form-wrapper").slideDown().addClass("open").attr("aria-hidden", "false"); //WHAT OPENS
        
            $("#gb-search-input").focus().attr("tabindex", "0");
            $(".search-selector").attr("tabindex", "0");
            }

            $("#gb-search-submit").on("keydown", function (e) {
            // CAPTURE KEY CODE
            switch (e.key) {
                case template.KeyCodes.tab:
                if (!e.shiftKey) {
                    e.preventDefault();
                    $(".search-selector").focus();
                }
        
                break;
            }
            });
        },
        
        "Footer": function() {
            //FOR SCOPE
            var template = this;
            
            //ADD LOGO
            const homeLink = '<a style="display:inline-block" href="[$SiteAlias$]">'; //SITE URL
            
            //LOGO DIMENSIONS
            var imgWidth = 255;
            var imgHeight = 165;
            
            //ADD LOGO TO HEADER
            var logoSrc = $.trim('<SWCtrl controlname="Custom" props="Name:schoolLogo" />');
            var srcSplit = logoSrc.split("/");
            var srcSplitLen = srcSplit.length;
            
            //CHECK IF LOGO IS EMPTY
            if((logoSrc == '') || (srcSplit[srcSplitLen - 1] == 'default-man.jpg')) {
                logoSrc = this.DefaultLogoSrc;
            }
            
            //ADD CUSTOM SCHOOL LOGO
            $('.gb-logo-footer').append(homeLink + '<img width="' + imgWidth + '" height="' + imgHeight + '" src="' + logoSrc + '" alt="[$SiteName$] Logo"></a>');
            
            //ADD BLACKBOARD FOOTER
            $(document).csBbFooter({
                'footerContainer'   : '#disclaimer-row',
                'useDisclaimer'     : false,
                'disclaimerText'    : ''
            });
        },       
        
        "AllyClick": function(event) {
            if(event.type == 'click') {
                return true;
            } else if(event.type == 'keydown' && (event.key == this.KeyCodes.space || event.key == this.KeyCodes.enter)) {
                return true;
            } else {
                return false;
            }
        },

        "GetBreakPoint": function() {
            return window.getComputedStyle(document.querySelector("body"), ":before").getPropertyValue("content").replace(/"|'/g, "");/*"*/
        },
    }