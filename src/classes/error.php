<?php

class ErrorPage {
 
    function __construct() {
        
    }

    public function PrintError($message) {
        $error = '
            <!DOCTYPE html>
            <html lang="en">
                <head>
                    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
                    <style>
                        body {
                            background: #36454f;
                            color: #efefef;
                            text-align: center;
                            padding: 35px;
                        }
                        main {
                            max-width: 750px;
                            margin: 0 auto;
                        }
                    </style>
                </head>
                <body>
                    <header>
                        <h1>Hold Up!</h1>
                    </header>
                    <main>
                        ' . $message . '
                    </main>
                </body>
            </html>
        ';

        return $error;
    }

}

?>