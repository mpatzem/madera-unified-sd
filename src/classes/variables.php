<?php

// SETTING UP THIS WAY HELPS KEEP THINGS OUT OF THE GLOBAL SPACE WHILE HAVING GLOBAL ACCESS
class Variables {

    function __construct() {
        $this->variables = array(
            "siteName" => "Madera Unified School District",
            "siteDomain" => "https://mpatzem.schoolwires.net",
            "homepageUrl" => "/Page/1",
            "subpageUrl" => "/Page/273",
            "subpageNoNavUrl" => "/Page/556",
            "siteAlias" => "dev1",
            "siteAddress" => "1902 Howard Rd",
            "siteCity" => "Madera",
            "siteState" => "CA",
            "siteZip" => "93637",
            "sitePhone" => "559.675.4500",
            "siteFax" => "555.555.5555",
        );
    }

    public function Get($variable) {
        if(array_key_exists($variable, $this->variables)) {
            return $this->variables[$variable];
        } else {
            return null;
        }
    }

}

?>