
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Frameset//EN" "http://www.w3.org/TR/html4/frameset.dtd">

<html lang="en">
<head>
    <title>Our Programs / Lorem Ipsum</title>
    <!--
    <PageMap>
    <DataObject type="document">
    <Attribute name="siteid">4</Attribute>
    </DataObject>
    </PageMap>
    -->
    

<script>
(function(apiKey){
    (function(p,e,n,d,o){var v,w,x,y,z;o=p[d]=p[d]||{};o._q=[];
    v=['initialize','identify','updateOptions','pageLoad'];for(w=0,x=v.length;w<x;++w)(function(m){
        o[m]=o[m]||function(){o._q[m===v[0]?'unshift':'push']([m].concat([].slice.call(arguments,0)));};})(v[w]);
        y=e.createElement(n);y.async=!0;y.src='https://cdn.pendo.io/agent/static/'+apiKey+'/pendo.js';
        z=e.getElementsByTagName(n)[0];z.parentNode.insertBefore(y,z);})(window,document,'script','pendo');

        // Call this whenever information about your visitors becomes available
        // Please use Strings, Numbers, or Bools for value types.
        pendo.initialize({
            visitor: {
                id: 'SWCS000010-cngv0iEqRbHQc+9Am84vTQ==',   // Required if user is logged in
                role: 'Anonymous',
                isPartOfGroup: 'False',
                // email:        // Optional
                // You can add any additional visitor level key-values here,
                // as long as it's not one of the above reserved names.
            },

            account: {
                id: 'SWCS000010', // Highly recommended
                version: '2.70',
                name:         'mpatzem.schoolwires.net'
                // planLevel:    // Optional
                // planPrice:    // Optional
                // creationDate: // Optional

                // You can add any additional account level key-values here,
                // as long as it's not one of the above reserved names.
            }
        });
})('ca0f531d-af61-45a7-7c9a-079f24d9128a');
</script>

    
    <meta property="og:type" content="website" />
<meta property="fb:app_id" content="411584262324304" />
<meta property="og:url" content="http%3A%2F%2Fmpatzem.schoolwires.net%2Fsite%2Fdefault.aspx%3FPageID%3D273" />
<meta property="og:title" content="Our Programs / Lorem Ipsum" />
<meta name="twitter:card" value="summary" />
<meta name="twitter:title" content="Our Programs / Lorem Ipsum" />
<meta itemprop="name" content="Our Programs / Lorem Ipsum" />

    <!-- Begin swuc.GlobalJS -->
<script type="text/javascript">
 staticURL = "https://mpatzem.schoolwires.net/Static/";
 SessionTimeout = "50";
 BBHelpURL = "";
</script>
<!-- End swuc.GlobalJS -->

    <script src='https://mpatzem.schoolwires.net/Static/GlobalAssets/Scripts/min/sri-failover.min.js' type='text/javascript'></script>

    <!-- Stylesheets -->
    <link rel="Stylesheet" type="text/css" href="https://mpatzem.schoolwires.net/Static/GlobalAssets/webfonts/OpenSans-Light.css" />
    <link rel="Stylesheet" type="text/css" href="https://mpatzem.schoolwires.net/Static/GlobalAssets/webfonts/OpenSans-Italic.css" />
    <link rel="Stylesheet" type="text/css" href="https://mpatzem.schoolwires.net/Static/GlobalAssets/webfonts/OpenSans-Regular.css" />
    <link rel="Stylesheet" type="text/css" href="https://mpatzem.schoolwires.net/Static/GlobalAssets/webfonts/OpenSans-SemiBold.css" />
    <link rel="Stylesheet" type="text/css" href="https://mpatzem.schoolwires.net/Static/GlobalAssets/Scripts/ThirdParty/shepherd/shepherd-theme-default.css" />
    <link rel="Stylesheet" type="text/css" href="https://mpatzem.schoolwires.net/Static/App_Themes/SW/jquery.jgrowl.css" />
    <link rel="Stylesheet" type="text/css" href="https://mpatzem.schoolwires.net/Static//site/assets/styles/system_2660.css" />
    <link rel="Stylesheet" type="text/css" href="https://mpatzem.schoolwires.net/Static//site/assets/styles/apps_2590.css" />
    <link rel="Stylesheet" type="text/css" href="https://mpatzem.schoolwires.net/Static/App_Themes/SW/jQueryUI.css" />
    <link rel="Stylesheet" type="text/css" href="https://mpatzem.schoolwires.net/Static/GlobalAssets/webfonts/SchoolwiresMobile_2320.css" />
    
    <link rel="Stylesheet" type="text/css" href="https://mpatzem.schoolwires.net/Static/GlobalAssets/Styles/Grid.css" />

    <!-- Scripts -->
    <script src="https://mpatzem.schoolwires.net/Static/GlobalAssets/WCM-2680/WCM.js" type="text/javascript"></script>
    <script src="https://mpatzem.schoolwires.net/Static/GlobalAssets/WCM-2680/API.js" type="text/javascript"></script>
    <script src='https://mpatzem.schoolwires.net/Static/GlobalAssets/Scripts/min/jquery-3.0.0.min.js' type='text/javascript'></script>
    <script src='https://mpatzem.schoolwires.net/Static//GlobalAssets/Scripts/min/jquery-migrate-1.4.1.min.js' type='text/javascript'></script
    <script src='https://ajax.googleapis.com/ajax/libs/swfobject/2.2/swfobject.js' type='text/javascript'
        integrity='sha384-JO4qIitDJfdsiD2P0i3fG6TmhkLKkiTfL4oVLkVFhGs5frz71Reviytvya4wIdDW' crossorigin='anonymous'
        data-sri-failover='https://mpatzem.schoolwires.net/Static/GlobalAssets/Scripts/min/swfobject.js'></script>
    <script src='https://mpatzem.schoolwires.net/Static/GlobalAssets/Scripts/ThirdParty/tether/tether.min.js' type='text/javascript'></script>
    <script src='https://mpatzem.schoolwires.net/Static/GlobalAssets/Scripts/ThirdParty/shepherd/shepherd.min.js' type='text/javascript'></script>
   
    <script type="text/javascript">
        $(document).ready(function () {
            SetCookie('SWScreenWidth', screen.width);
            SetCookie('SWClientWidth', document.body.clientWidth);
            
            $("div.ui-article:last").addClass("last-article");
            $("div.region .app:last").addClass("last-app");

            // get on screen alerts
            var isAnyActiveOSA = 'False';
            var onscreenAlertCookie = GetCookie('Alerts');

            if (onscreenAlertCookie == '' || onscreenAlertCookie == undefined) {
                onscreenAlertCookie = "";
            }
            if (isAnyActiveOSA == 'True') {
                GetContent(homeURL + "/cms/Tools/OnScreenAlerts/UserControls/OnScreenAlertDialogListWrapper.aspx?OnScreenAlertCookie=" + onscreenAlertCookie + "&SiteID=4", "onscreenalert-holder", 2, "OnScreenAlertCheckListItem();");
            }            

        });

    // ADA SKIP NAV
    $(document).ready(function () {
        $(document).on('focus', '#skipLink', function () {
            $("div.sw-skipnav-outerbar").animate({
                marginTop: "0px"
            }, 500);
        });

        $(document).on('blur', '#skipLink', function () {
            $("div.sw-skipnav-outerbar").animate({
                marginTop: "-30px"
            }, 500);
        });
    });

    // ADA MYSTART
    $(document).ready(function () {
        var top_level_nav = $('.sw-mystart-nav');

        // Set tabIndex to -1 so that top_level_links can't receive focus until menu is open
        // school dropdown
        $(top_level_nav).find('ul').find('a').attr('tabIndex', -1);

        // my account dropdown
        $(top_level_nav).next('ul').find('a').attr('tabIndex', -1);

        var openNavCallback = function(e, element) {
             // hide open menus
            hideMyStartBarMenu();

            // show school dropdown
            if ($(element).find('ul').length > 0) {
                $(element).find('.sw-dropdown').css('display', 'block').attr('aria-hidden', 'false');
                $(element).find('.sw-dropdown').find('li:first-child a').focus()
            }

            // show my account dropdown
            if ($(element).next('ul').length > 0) {
                $(element).next('.sw-dropdown').css('display', 'block').attr('aria-hidden', 'false');
                $(element).next('.sw-dropdown').find('li:first-child a').focus();
                $('#sw-mystart-account').addClass("clicked-state");
            }
        }

        $(top_level_nav).click(function (e) {
            openNavCallback(e, this);
        });

        $('.sw-dropdown-list li').click(function(e) {
            e.stopImmediatePropagation();
            $(this).focus();
        });
        
        // Bind arrow keys for navigation
        $(top_level_nav).keydown(function (e) {
            if (e.keyCode == 37) { //key left
                e.preventDefault();

                // This is the first item
                if ($(this).prev('.sw-mystart-nav').length == 0) {
                    $(this).parents('div').find('.sw-mystart-nav').last().focus();
                } else {
                    $(this).prev('.sw-mystart-nav').focus();
                }
            } else if (e.keyCode == 38) { //key up
                e.preventDefault();

                // show school dropdown
                if ($(this).find('ul').length > 0) {
                    $(this).find('div.sw-dropdown').css('display', 'block').find('ul').attr('aria-hidden', 'false').find('a').attr('tabIndex', 0).last().focus();
                }

                // show my account dropdown
                if ($(this).find('ul').length > 0) {
                    $(this).find('ul.sw-dropdown').css('display', 'block').attr('aria-hidden', 'false').find('a').attr('tabIndex', 0).last().focus();
                }
            } else if (e.keyCode == 39) { //key right
                e.preventDefault();

                // This is the last item
                if ($(this).next('.sw-mystart-nav').length == 0) {
                    $(this).parents('div').find('.sw-mystart-nav').first().focus();
                } else {
                    $(this).next('.sw-mystart-nav').focus();
                }
            } else if (e.keyCode == 40) { //key down
                e.preventDefault();

                // show school dropdown
                if ($(this).find('ul').length > 0) {
                    $(this).find('div.sw-dropdown').css('display', 'block').find('ul').attr('aria-hidden', 'false').find('a').attr('tabIndex', 0).first().focus();
                }

                // show my account dropdown
                if ($(this).next('ul').length > 0) {
                    $(this).next('ul.sw-dropdown').css('display', 'block').attr('aria-hidden', 'false').find('a').attr('tabIndex', 0).first().focus();
                }
            } else if (e.keyCode == 13 || e.keyCode == 32) { //enter key
                // If submenu is hidden, open it
                e.preventDefault();

                
                openNavCallback(e, this);
                $(this).parent('li').find('ul[aria-hidden=true]').attr('aria-hidden', 'false').find('a').attr('tabIndex', 0).first().focus();
            } else if (e.keyCode == 27) { //escape key
                e.preventDefault();
                hideMyStartBarMenu();
            } else {
                $(this).parent('.sw-mystart-nav').find('ul[aria-hidden=false] a').each(function () {
                    if (typeof keyCodeMap != "undefined" && $(this).text().substring(0, 1).toLowerCase() == keyCodeMap[e.keyCode]) {
                        $(this).focus();
                        return false;
                    }
                });
            }
        });

        // school dropdown
        var startbarlinks = $(top_level_nav).find('ul').find('a');
        bindMyStartBarLinks(startbarlinks);

        // my account dropdown
        var myaccountlinks = $(top_level_nav).next('ul').find('a');
        bindMyStartBarLinks(myaccountlinks);

        function bindMyStartBarLinks(links) {
            $(links).keydown(function (e) {
                e.stopPropagation();

                if (e.keyCode == 38) { //key up
                    e.preventDefault();

                    // This is the first item
                    if ($(this).parent('li').prev('li').length == 0) {
                        if ($(this).parents('ul').parents('.sw-mystart-nav').length > 0) {
                            $(this).parents('ul').parents('.sw-mystart-nav').focus();
                        } else {
                            $(this).parents('ul').prev('.sw-mystart-nav').focus();
                        }
                    } else {
                        $(this).parent('li').prev('li').find('a').first().focus();
                    }
                } else if (e.keyCode == 40) { //key down
                    e.preventDefault();

                    if ($(this).parent('li').next('li').length == 0) {
                        if ($(this).parents('ul').parents('.sw-mystart-nav').length > 0) {
                            $(this).parents('ul').parents('.sw-mystart-nav').focus();
                        } else {
                            $(this).parents('ul').prev('.sw-mystart-nav').focus();
                        }
                    } else {
                        $(this).parent('li').next('li').find('a').first().attr('tabIndex', 0);
                        $(this).parent('li').next('li').find('a').first().focus();
                    }
                } else if (e.keyCode == 27 || e.keyCode == 37) { // escape key or key left
                    e.preventDefault();
                    hideMyStartBarMenu();
                } else if (e.keyCode == 32) { //enter key
                    e.preventDefault();
                    window.location = $(this).attr('href');
                } else {
                    var found = false;

                    $(this).parent('div').nextAll('li').find('a').each(function () {
                        if (typeof keyCodeMap != "undefined" && $(this).text().substring(0, 1).toLowerCase() == keyCodeMap[e.keyCode]) {
                            $(this).focus();
                            found = true;
                            return false;
                        }
                    });

                    if (!found) {
                        $(this).parent('div').prevAll('li').find('a').each(function () {
                            if (typeof keyCodeMap != "undefined" && $(this).text().substring(0, 1).toLowerCase() == keyCodeMap[e.keyCode]) {
                                $(this).focus();
                                return false;
                            }
                        });
                    }
                }
            });
        }
        
        // Hide menu if click or focus occurs outside of navigation
        $('#sw-mystart-inner').find('.sw-mystart-nav').last().keydown(function (e) {
            if (e.keyCode == 9) {
                // If the user tabs out of the navigation hide all menus
                hideMyStartBarMenu();
            }
        });

        /*$(document).click(function() { 
            hideMyStartBarMenu();
        });*/

        // try to capture as many custom MyStart bars as possible
        $('.sw-mystart-button').find('a').focus(function () {
            hideMyStartBarMenu();
        });

        $('#sw-mystart-inner').click(function (e) {
            e.stopPropagation();
        });

        $('ul.sw-dropdown-list').blur(function () {
            hideMyStartBarMenu();
        });

        $('#ui-btn-mypasskey').focus(function () {
            hideMyStartBarMenu();
        });

        $('#ui-btn-sitemanager').focus(function () {
            hideMyStartBarMenu();
        });

        $('#ui-btn-myview').focus(function () {
            hideMyStartBarMenu();
        });

        $('#ui-btn-signin').focus(function () {
            hideMyStartBarMenu();
        });

        $('#ui-btn-register').focus(function () {
            hideMyStartBarMenu();
        });

        // button click events
        $('div.sw-mystart-button.home a').keydown(function (e) {
            e.stopImmediatePropagation();

            if (e.keyCode == 13) {
                $(this).click();
            }
        });

        $('div.sw-mystart-button.pw a').keydown(function (e) {
            e.stopImmediatePropagation();

            if (e.keyCode == 13) {
                $(this).click();
            }
        });

        $('div.sw-mystart-button.manage a').keydown(function (e) {
            e.stopImmediatePropagation();

            if (e.keyCode == 13) {
                $(this).click();
            }
        });

        $('#sw-mystart-account').keydown(function (e) {
            e.stopImmediatePropagation();

            if (e.keyCode == 13) {
                $(this).addClass('clicked-state');
                $('#sw-myaccount-list').show();
            }
        });

        $('#sw-mystart-mypasskey a').keydown(function (e) {
            e.stopImmediatePropagation();

            if (e.keyCode == 13) {
                $(this).click();
            }
        });

        $('div.sw-mystart-button.signin a').keydown(function (e) {
            e.stopImmediatePropagation();

            if (e.keyCode == 13) {
                $(this).click();
            }
        });

        $('div.sw-mystart-button.register a').keydown(function (e) {
            e.stopImmediatePropagation();

            if (e.keyCode == 13) {
                $(this).click();
            }
        });
    });

    function hideMyStartBarMenu() {
        $('.sw-dropdown').attr('aria-hidden', 'true').css('display', 'none');
        $('#sw-mystart-account').removeClass("clicked-state");
    }

    // ADA CHANNEL NAV
    $(document).ready(function() {
        var channelCount;
        var channelIndex = 1;
        var settings = {
            menuHoverClass: 'hover'
        };

        // Add ARIA roles to menubar and menu items
        $('[id="channel-navigation"]').attr('role', 'menubar').find('li a').attr('role', 'menuitem').attr('tabindex', '0');

        var top_level_links = $('[id="channel-navigation"]').find('> li > a');
        channelCount = $(top_level_links).length;


        $(top_level_links).each(function() {
            $(this).attr('aria-posinset', channelIndex).attr('aria-setsize', channelCount);
            $(this).next('ul').attr({ 'aria-hidden': 'true', 'role': 'menu' });

            if ($(this).parent('li.sw-channel-item').children('ul').length > 0) {
                $(this).attr('aria-haspopup', 'true');
            }

            var sectionCount = $(this).next('ul').find('a').length;
            var sectionIndex = 1;
            $(this).next('ul').find('a').each(function() {
                $(this).attr('tabIndex', -1).attr('aria-posinset', sectionIndex).attr('aria-setsize', sectionCount);
                sectionIndex++;
            });
            channelIndex++;

        });

        $(top_level_links).focus(function () {
            //hide open menus
            hideChannelMenu();

            if ($(this).parent('li').find('ul').length > 0) {
                $(this).parent('li').addClass(settings.menuHoverClass).find('ul').attr('aria-hidden', 'false').css('display', 'block');
            }
        });

        // Bind arrow keys for navigation
        $(top_level_links).keydown(function (e) {
            if (e.keyCode == 37) { //key left
                e.preventDefault();

                // This is the first item
                if ($(this).parent('li').prev('li').length == 0) {
                    $(this).parents('ul').find('> li').last().find('a').first().focus();
                } else {
                    $(this).parent('li').prev('li').find('a').first().focus();
                }
            } else if (e.keyCode == 38) { //key up
                e.preventDefault();

                if ($(this).parent('li').find('ul').length > 0) {
                    $(this).parent('li').addClass(settings.menuHoverClass).find('ul').css('display', 'block').attr('aria-hidden', 'false').find('a').attr('tabIndex', 0).last().focus();
                }
            } else if (e.keyCode == 39) { //key right
                e.preventDefault();

                // This is the last item
                if ($(this).parent('li').next('li').length == 0) {
                    $(this).parents('ul').find('> li').first().find('a').first().focus();
                } else {
                    $(this).parent('li').next('li').find('a').first().focus();
                }
            } else if (e.keyCode == 40) { //key down
                e.preventDefault();

                if ($(this).parent('li').find('ul').length > 0) {
                    $(this).parent('li')
                         .addClass(settings.menuHoverClass)
                         .find('ul.sw-channel-dropdown').css('display', 'block')
                         .attr('aria-hidden', 'false')
                         .find('a').attr('tabIndex', 0)
                         .first().focus();
                }
            } else if (e.keyCode == 13 || e.keyCode == 32) { //enter key
                // If submenu is hidden, open it
                e.preventDefault();

                $(this).parent('li').find('ul[aria-hidden=true]').attr('aria-hidden', 'false').addClass(settings.menuHoverClass).find('a').attr('tabIndex', 0).first().focus();
            } else if (e.keyCode == 27) { //escape key
                e.preventDefault();
                hideChannelMenu();
            } else {
                $(this).parent('li').find('ul[aria-hidden=false] a').each(function () {
                    if (typeof keyCodeMap != "undefined" && $(this).text().substring(0, 1).toLowerCase() == keyCodeMap[e.keyCode]) {
                        $(this).focus();
                        return false;
                    }
                });
            }
        });

        var links = $(top_level_links).parent('li').find('ul').find('a');

        $(links).keydown(function (e) {
            if (e.keyCode == 38) {
                e.preventDefault();

                // This is the first item
                if ($(this).parent('li').prev('li').length == 0) {
                    $(this).parents('ul').parents('li').find('a').first().focus();
                } else {
                    $(this).parent('li').prev('li').find('a').first().focus();
                }
            } else if (e.keyCode == 40) {
                e.preventDefault();

                if ($(this).parent('li').next('li').length == 0) {
                    $(this).parents('ul').parents('li').find('a').first().focus();
                } else {
                    $(this).parent('li').next('li').find('a').first().focus();
                }
            } else if (e.keyCode == 27 || e.keyCode == 37) {
                e.preventDefault();
                $(this).parents('ul').first().prev('a').focus().parents('ul').first().find('.' + settings.menuHoverClass).removeClass(settings.menuHoverClass);
            } else if (e.keyCode == 32) {
                e.preventDefault();
                window.location = $(this).attr('href');
            } else {
                var found = false;

                $(this).parent('li').nextAll('li').find('a').each(function () {
                    if (typeof keyCodeMap != "undefined" && $(this).text().substring(0, 1).toLowerCase() == keyCodeMap[e.keyCode]) {
                        $(this).focus();
                        found = true;
                        return false;
                    }
                });

                if (!found) {
                    $(this).parent('li').prevAll('li').find('a').each(function () {
                        if (typeof keyCodeMap != "undefined" && $(this).text().substring(0, 1).toLowerCase() == keyCodeMap[e.keyCode]) {
                            $(this).focus();
                            return false;
                        }
                    });
                }
            }
        });

        function hideChannelMenu() {
            $('li.sw-channel-item.' + settings.menuHoverClass).removeClass(settings.menuHoverClass).find('ul').attr('aria-hidden', 'true').css('display', 'none').find('a').attr('tabIndex', -1);
        }
        
        // Hide menu if click or focus occurs outside of navigation
        $('[id="channel-navigation"]').find('a').last().keydown(function (e) {
            if (e.keyCode == 9) {
                // If the user tabs out of the navigation hide all menus
                hideChannelMenu();
            }
        });

        $('[id="channel-navigation"]').find('a').first().keydown(function (e) {
            if (e.keyCode == 9) {
                // hide open MyStart Bar menus
                hideMyStartBarMenu();
            }
        });

        /*$(document).click(function() {
            hideChannelMenu();
        });*/

        $('[id="channel-navigation"]').click(function (e) {
            e.stopPropagation();
        });
    });

    $(document).ready(function() {
        $('input.required').each(function() {
            if ($('label[for="' + $(this).attr('id') + '"]').length > 0) {
                if ($('label[for="' + $(this).attr('id') + '"]').html().indexOf('recStar') < 0) {
                    $('label[for="' + $(this).attr('id') + '"]').prepend('<span class="recStar" aria-label="required item">*</span> ');
                }
            }
        });

        $(document).ajaxComplete(function() {
            $('input.required').each(function() {
                if ($('label[for="' + $(this).attr('id') + '"]').length > 0) {
                    if ($('label[for="' + $(this).attr('id') + '"]').html().indexOf('recStar') < 0) {
                        $('label[for="' + $(this).attr('id') + '"]').prepend('<span class="recStar" aria-label="required item">*</span> ');
                    }
                }
            });
        });
    });
    </script>

    <!-- Page -->
    
    <style type="text/css">/* MedaiBegin Standard *//* GroupBegin EditorStyles */
html {
	font-size: 16px;
	line-height: 1.75;
}
/* GroupBegin EditorStyles */
body {
	font-family: $bodyFont;
	font-weight: 400;
	color: #000000;
	text-rendering: optimizeLegibility !important;
	-webkit-font-smoothing: antialiased !important;
	-moz-font-smoothing: antialiased !important;
	font-smoothing: antialiased !important;
	-moz-osx-font-smoothing: grayscale;
}
h1 {
	font-size: 1.625rem;
	font-weight: 700;
}
h2 {
	color:#094f84;
	font-weight: 700;
	font-size: 1.5rem;
}
h3 {
	color:#094f84;
	font-weight: 700;
	font-size: 1.3125rem;
}
h4 {
	color:#094f84;
	font-weight: 700;
	font-size: 1.125rem;
}

.Box_Title {
	font-size: 1.25rem;
	font-weight: 500;
	display: inline-block;
	margin-bottom: 6px;
}
.Border_Box {
	border:1px solid #d5d5d5;
	padding:27px 30px;
	display:block;
}
.Primary_Box {
	background:#084F84;
	color:#fff;
	padding:27px 30px;
	display:block;
}
.Secondary_Box {
	background:#1072BF;
	color:#fff;
	padding:27px 30px;
	display:block;
}
.Featured_Button {
	background:#1072BF;
	color:#fff;
	padding:27px 30px;
	display:block;
}
/* GroupEnd */

.flexpage .ui-article .Primary_Box a,
.flexpage .ui-article .Secondary_Box a {
	color:#fff;
}
 /* GroupEnd *//* MediaEnd */</style>
    

    <!-- App Preview -->
    


    <style type="text/css">
        /* HOMEPAGE EDIT THUMBNAIL STYLES */

        div.region {
            ;
        }

            div.region span.homepage-thumb-region-number {
                font: bold 100px verdana;
                color: #fff;
            }

        div.homepage-thumb-region {
            background: #264867; /*dark blue*/
            border: 5px solid #fff;
            text-align: center;
            padding: 40px 0 40px 0;
            display: block;
        }

            div.homepage-thumb-region.region-1 {
                background: #264867; /*dark blue*/
            }

            div.homepage-thumb-region.region-2 {
                background: #5C1700; /*dark red*/
            }

            div.homepage-thumb-region.region-3 {
                background: #335905; /*dark green*/
            }

            div.homepage-thumb-region.region-4 {
                background: #B45014; /*dark orange*/
            }

            div.homepage-thumb-region.region-5 {
                background: #51445F; /*dark purple*/
            }

            div.homepage-thumb-region.region-6 {
                background: #3B70A0; /*lighter blue*/
            }

            div.homepage-thumb-region.region-7 {
                background: #862200; /*lighter red*/
            }

            div.homepage-thumb-region.region-8 {
                background: #417206; /*lighter green*/
            }

            div.homepage-thumb-region.region-9 {
                background: #D36929; /*lighter orange*/
            }

            div.homepage-thumb-region.region-10 {
                background: #6E5C80; /*lighter purple*/
            }

        /* END HOMEPAGE EDIT THUMBNAIL STYLES */
    </style>

    <style type="text/css" media="print">
        .noprint {
            display: none !important;
        }
    </style>

    <style type ="text/css">
        .ui-txt-validate {
            display: none;
        }
    </style>

    

<!-- Begin Schoolwires Traffic Code --> 

<script type="text/javascript">

    (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
            (i[r].q = i[r].q || []).push(arguments)
        }, i[r].l = 1 * new Date(); a = s.createElement(o),
        m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
    })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

    ga('create', 'UA-5173826-6', 'auto', 'BBTracker' );
    ga('BBTracker.set', 'dimension1', 'AWS');
    ga('BBTracker.set', 'dimension2', 'False');
    ga('BBTracker.set', 'dimension3', 'SWCS000010');
    ga('BBTracker.set', 'dimension4', '1184');
    ga('BBTracker.set', 'dimension5', '3');
    ga('BBTracker.set', 'dimension6', '273');

    ga('BBTracker.send', 'pageview');

</script>

<!-- End Schoolwires Traffic Code --> 

    <!-- Ally Alternative Formats Loader    START   -->
    
    <!-- Ally Alternative Formats Loader    END     -->

</head>

        <body data-translate="google">
    

    <input type="hidden" id="hidFullPath" value="https://mpatzem.schoolwires.net/" />
    <input type="hidden" id="hidActiveChannelNavType" value="0" />
    <input type="hidden" id="hidActiveChannel" value ="1173" />
    <input type="hidden" id="hidActiveSection" value="1184" />

    <!-- OnScreen Alert Dialog Start -->
    <div id="onscreenalert-holder"></div>
    <!-- OnScreen Alert Dialog End -->

    <!-- ADA Skip Nav -->
    <div class="sw-skipnav-outerbar">
        <a href="#sw-maincontent" id="skipLink" class="sw-skipnav" tabindex="0">Skip to Main Content</a>
    </div>

    <!-- DashBoard SideBar Start -->
    
    <!-- DashBoard SideBar End -->

    <!-- off-canvas menu enabled-->
    

    

<style type="text/css">
	/* SPECIAL MODE BAR */
	div.sw-special-mode-bar {
		background: #FBC243 url('https://mpatzem.schoolwires.net/Static//GlobalAssets/Images/special-mode-bar-background.png') no-repeat;
		height: 30px;
		text-align: left;
		font-size: 12px;
		position: relative;
		z-index: 10000;
	}
	div.sw-special-mode-bar > div {
		padding: 8px 0 0 55px;
		font-weight: bold;
	}
	div.sw-special-mode-bar > div > a {
		margin-left: 20px;
		background: #A0803D;
		-moz-border-radius: 5px;
		-webkit-border-radius: 5px;
		color: #fff;
		padding: 4px 6px 4px 6px;
		font-size: 11px;
	}

	/* END SPECIAL MODE BAR */
</style>

<script type="text/javascript">
	
	function SWEndPreviewMode() { 
		var data = "{}";
		var success = "window.location='';";
		var failure = "CallControllerFailure(result[0].errormessage);";
		CallController("https://mpatzem.schoolwires.net/site/SiteController.aspx/EndPreviewMode", data, success, failure);
	}
	
    function SWEndEmulationMode() {
        var data = "{}";
        var success = "DeleteCookie('SourceEmulationUserID');DeleteCookie('SidebarIsClosed');window.location='https://mpatzem.schoolwires.net/ums/Users/Users.aspx';";
        var failure = "CallControllerFailure(result[0].errormessage);";
        CallController("https://mpatzem.schoolwires.net/site/SiteController.aspx/EndEmulationMode", data, success, failure);
	}

	function SWEndPreviewConfigMode() {
	    var data = "{}";
	    var success = "window.location='';";
	    var failure = "CallControllerFailure(result[0].errormessage);";
	    CallController("https://mpatzem.schoolwires.net/site/SiteController.aspx/EndPreviewConfigMode", data, success, failure);
	}
</script>
            

    <!-- BEGIN - MYSTART BAR -->
<div id='sw-mystart-outer' class='noprint'>
<div id='sw-mystart-inner'>
<div id='sw-mystart-left'>
<div class='sw-mystart-nav sw-mystart-button home'><a tabindex="0" href="https://mpatzem.schoolwires.net/Domain/4" alt="District Home" title="Return to the homepage on the district site."><span>District Home<div id='sw-home-icon'></div>
</span></a></div>
<div class='sw-mystart-nav sw-mystart-dropdown schoollist' tabindex='0' aria-label='Select a School' role='navigation'>
<div class='selector' aria-hidden='true'>Select a School...</div>
<div class='sw-dropdown' aria-hidden='false'>
<div class='sw-dropdown-selected' aria-hidden='true'>Select a School</div>
<ul class='sw-dropdown-list' aria-hidden='false' aria-label='Schools'>
<li><a href="https://mpatzem.schoolwires.net/Domain/1503">Humble ISD</a></li>
<li><a href="https://mpatzem.schoolwires.net/Domain/1580">Fletch_Test_2</a></li>
<li><a href="https://mpatzem.schoolwires.net/Domain/1634">Grosse Pointe</a></li>
</ul>
</div>
<div class='sw-dropdown-arrow' aria-hidden='true'></div>
</div>
</div>
<div id='sw-mystart-right'>
<div id='ui-btn-signin' class='sw-mystart-button signin'><a href="https://mpatzem.schoolwires.net/site/Default.aspx?PageType=7&SiteID=4&IgnoreRedirect=true"><span>Sign In</span></a></div>
<div id='ui-btn-register' class='sw-mystart-button register'><a href="https://mpatzem.schoolwires.net/site/Default.aspx?PageType=10&SiteID=4"><span>Register</span></a></div>
<div id='sw-mystart-search' class='sw-mystart-nav'>
<script type="text/javascript">
$(document).ready(function() {
    $('#sw-search-input').keyup(function(e) {        if (e.keyCode == 13) {
            SWGoToSearchResultsPageswsearchinput();
        }
    });
    $('#sw-search-input').val($('#swsearch-hid-word').val())});
function SWGoToSearchResultsPageswsearchinput() {
window.location.href="https://mpatzem.schoolwires.net/site/Default.aspx?PageType=6&SiteID=4&SearchString=" + $('#sw-search-input').val();
}
</script>
<label for="sw-search-input" class="hidden-label">Search Our Site</label>
<input id="sw-search-input" type="text" title="Search Term" aria-label="Search Our Site" placeholder="Search this Site..." />
<a tabindex="0" id="sw-search-button" title="Search" href="javascript:;" role="button" aria-label="Submit Site Search" onclick="SWGoToSearchResultsPageswsearchinput();"><span><img src="https://mpatzem.schoolwires.net/Static//globalassets/images/sw-mystart-search.png" alt="Search" /></span></a><script type="text/javascript">
$(document).ready(function() {
    $('#sw-search-button').keyup(function(e) {        if (e.keyCode == 13) {
            SWGoToSearchResultsPageswsearchinput();        }
    });
});
</script>

</div>
<div class='clear'></div>
</div>
</div>
</div>
<!-- END - MYSTART BAR -->
<div><div id="sw-channel-list-container" role="navigation">
<ul id="channel-navigation" class="sw-channel-list" role="menubar">
<li id="navc-HP" class="sw-channel-item" ><a href="https://mpatzem.schoolwires.net/Page/1" aria-label="Home"><span>Home</span></a></li>
<li id="navc-1173" class="sw-channel-item">
<a href="https://mpatzem.schoolwires.net/domain/1184"">
<span>Discover</span></a>
<div class="hidden-sections"><ul>"

</ul></div>
<ul class="sw-channel-dropdown">
<li id="navs-1184"><a href="https://mpatzem.schoolwires.net/domain/1184"><span>Our Programs</span></a></li>
<li id="navs-1185"><a href="https://mpatzem.schoolwires.net/domain/1185"><span>Classes and Schedules</span></a></li>
<li id="navs-1186"><a href="https://mpatzem.schoolwires.net/domain/1186"><span>Locations</span></a></li>
<li id="navs-1187"><a href="https://mpatzem.schoolwires.net/domain/1187"><span>Teacher Information</span></a></li>
<li id="navs-1188"><a href="https://mpatzem.schoolwires.net/domain/1188"><span>School Locator</span></a></li>
<li id="navs-1189"><a href="https://mpatzem.schoolwires.net/domain/1189"><span>Discover Why</span></a></li>
<li id="navs-1207"><a href="https://mpatzem.schoolwires.net/domain/1207"><span>Subpage</span></a></li>
<li id="navs-1654"><a href="https://mpatzem.schoolwires.net/domain/1654"><span>Section 8</span></a></li>
<li id="navs-1655"><a href="https://mpatzem.schoolwires.net/domain/1655"><span>Section 9</span></a></li>
<li id="navs-1656"><a href="https://mpatzem.schoolwires.net/domain/1656"><span>Section 10</span></a></li>
<li id="navs-1657"><a href="https://mpatzem.schoolwires.net/domain/1657"><span>Section 11</span></a></li>
<li id="navs-1658"><a href="https://mpatzem.schoolwires.net/domain/1658"><span>Section 12</span></a></li>
<li id="navs-1659"><a href="https://mpatzem.schoolwires.net/domain/1659"><span>Section 13</span></a></li>
<li id="navs-1660"><a href="https://mpatzem.schoolwires.net/domain/1660"><span>Section 14</span></a></li>
<li id="navs-1661"><a href="https://mpatzem.schoolwires.net/domain/1661"><span>Section 15</span></a></li>
<li id="navs-1662"><a href="https://mpatzem.schoolwires.net/domain/1662"><span>Section 16</span></a></li>
<li id="navs-1663"><a href="https://mpatzem.schoolwires.net/domain/1663"><span>Section 17</span></a></li>
<li id="navs-1664"><a href="https://mpatzem.schoolwires.net/domain/1664"><span>Section 18</span></a></li>
<li id="navs-1665"><a href="https://mpatzem.schoolwires.net/domain/1665"><span>Section 19</span></a></li>
<li id="navs-1666"><a href="https://mpatzem.schoolwires.net/domain/1666"><span>Section 20</span></a></li>
<li id="navs-1667"><a href="https://mpatzem.schoolwires.net/domain/1667"><span>Section 21</span></a></li>
</ul>
</li><li id="navc-1174" class="sw-channel-item">
<a href="https://mpatzem.schoolwires.net/domain/1190"">
<span>Our Schools</span></a>
<div class="hidden-sections"><ul>"

</ul></div>
<ul class="sw-channel-dropdown">
<li id="navs-1190"><a href="https://mpatzem.schoolwires.net/domain/1190"><span>Section Title</span></a></li>
<li id="navs-1191"><a href="https://mpatzem.schoolwires.net/domain/1191"><span>Headlines and Features</span></a></li>
<li id="navs-1192"><a href="https://mpatzem.schoolwires.net/domain/1192"><span>Human Resources</span></a></li>
<li id="navs-1193"><a href="https://mpatzem.schoolwires.net/domain/1193"><span>Employment Opportunities</span></a></li>
</ul>
</li><li id="navc-1175" class="sw-channel-item">
<a href="https://mpatzem.schoolwires.net/domain/1194"">
<span>Family Resources</span></a>
<div class="hidden-sections"><ul>"

</ul></div>
<ul class="sw-channel-dropdown">
<li id="navs-1194"><a href="https://mpatzem.schoolwires.net/domain/1194"><span>Our Programs</span></a></li>
<li id="navs-1195"><a href="https://mpatzem.schoolwires.net/domain/1195"><span>Classes and Schedules</span></a></li>
<li id="navs-1196"><a href="https://mpatzem.schoolwires.net/domain/1196"><span>Locations</span></a></li>
<li id="navs-1197"><a href="https://mpatzem.schoolwires.net/domain/1197"><span>Teacher Information</span></a></li>
<li id="navs-1198"><a href="https://mpatzem.schoolwires.net/domain/1198"><span>School Locator</span></a></li>
<li id="navs-1199"><a href="https://mpatzem.schoolwires.net/domain/1199"><span>Discover Why</span></a></li>
</ul>
</li><li id="navc-1176" class="sw-channel-item">
<a href="https://mpatzem.schoolwires.net/domain/1200"">
<span>Clubs</span></a>
<div class="hidden-sections"><ul>"

</ul></div>
<ul class="sw-channel-dropdown">
<li id="navs-1200"><a href="https://mpatzem.schoolwires.net/domain/1200"><span>Our Programs</span></a></li>
<li id="navs-1201"><a href="https://mpatzem.schoolwires.net/domain/1201"><span>Classes and Schedules</span></a></li>
<li id="navs-1202"><a href="https://mpatzem.schoolwires.net/domain/1202"><span>Locations</span></a></li>
<li id="navs-1203"><a href="https://mpatzem.schoolwires.net/domain/1203"><span>Teacher Information</span></a></li>
<li id="navs-1204"><a href="https://mpatzem.schoolwires.net/domain/1204"><span>School Locator</span></a></li>
<li id="navs-1205"><a href="https://mpatzem.schoolwires.net/domain/1205"><span>Discover Why</span></a></li>
</ul>
</li><li id="navc-1288" class="hidden-channel">
<a href="https://mpatzem.schoolwires.net/domain/1288">
<span>Classrooms</span></a>
<div class="hidden-sections"><ul>"

</ul></div>
</li><li id="navc-1434" class="sw-channel-item">
<a href="https://mpatzem.schoolwires.net/domain/1435"">
<span>Technology</span></a>
<div class="hidden-sections"><ul>"

</ul></div>
<ul class="dropdown-hidden">
<li id="navs-1435"><a href="https://mpatzem.schoolwires.net/domain/1435"><span>Technology</span></a></li>
</ul>
</li></ul><div class='clear'></div>
</div>



<script type="text/javascript">
    $(document).ready(function() {
        channelHoverIE();
        channelTouch();
        closeMenuByPressingKey();
    });

    function channelTouch() {
        // this will change the dropdown behavior when it is touched vs clicked.
        // channels will be clickable on second click. first click simply opens the menu.
        $('#channel-navigation > .sw-channel-item > a').on({
            'touchstart': function (e) {
                
                // see if has menu
                if ($(this).siblings('ul.sw-channel-dropdown').children('li').length > 0)  {
                    var button = $(this);

                    // add href as property if not already set
                    // then remove href attribute
                    if (!button.prop('linkHref')) {
                        button.prop('linkHref', button.attr('href'));
                        button.removeAttr('href');
                    }

                    // check to see if menu is already open
                    if ($(this).siblings('ul.sw-channel-dropdown').is(':visible')) {
                        // if open go to link
                        window.location.href = button.prop('linkHref');
                    } 

                } 
            }
        });
    }


    
    function channelHoverIE(){
		// set z-index for IE7
		var parentZindex = $('#channel-navigation').parents('div:first').css('z-index');
		var zindex = (parentZindex > 0 ? parentZindex : 8000);
		$(".sw-channel-item").each(function(ind) {
			$(this).css('z-index', zindex - ind);
			zindex --;
		});
	    $(".sw-channel-item").hover(function(){
	        var subList = $(this).children('ul');
	        if ($.trim(subList.html()) !== "") {
	            subList.show();
	            subList.attr("aria-hidden", "false").attr("aria-expanded", "true");
		    }
		    $(this).addClass("hover");
	    }, function() {
	        $(".sw-channel-dropdown").hide();
	        $(this).removeClass("hover");
	        var subList = $(this).children('ul');
	        if ($.trim(subList.html()) !== "") {
	            subList.attr("aria-hidden", "true").attr("aria-expanded", "false");
	        }
	    });
    }

    function closeMenuByPressingKey() {
        $(".sw-channel-item").each(function(ind) {
            $(this).keyup(function (event) {
                if (event.keyCode == 27) { // ESC
                    $(".sw-channel-dropdown").hide();
                    $(this).removeClass("hover");
                    var subList = $(this).children('ul');
                    if ($.trim(subList.html()) !== "") {
                        subList.attr("aria-hidden", "true").attr("aria-expanded", "false");
                    }
                }
                if (event.keyCode == 13 || event.keyCode == 32) { //enter or space
                    $(this).find('a').get(0).click();
                }
            }); 
        });

        $(".sw-channel-item a").each(function (ind) {
            $(this).parents('.sw-channel-item').keydown(function (e) {
                if (e.keyCode == 9) { // TAB
                    $(".sw-channel-dropdown").hide();
                    $(this).removeClass("hover");
                    var subList = $(this).children('ul');
                    if ($.trim(subList.html()) !== "") {
                        subList.attr("aria-hidden", "true").attr("aria-expanded", "false");
                    }
                }
            });
        });

        $(".sw-channel-dropdown li").each(function(ind) {
            $(this).keydown(function (event) {
                if (event.keyCode == 9) { // TAB
                    $(".sw-channel-dropdown").hide();
                    var parentMenuItem = $(this).parent().closest('li');
                    parentMenuItem.removeClass("hover");
                    var subList = parentMenuItem.children('ul');
                    if ($.trim(subList.html()) !== "") {
                        subList.attr("aria-hidden", "true").attr("aria-expanded", "false");
                    }
                    parentMenuItem.next().find('a:first').focus();
                    event.preventDefault();
                    event.stopPropagation();
                }

                if (event.keyCode == 37 || // left arrow
                    event.keyCode == 39) { // right arrow
                    $(".sw-channel-dropdown").hide();
                    var parentMenuItem = $(this).parent().closest('li');
                    parentMenuItem.removeClass("hover");
                    var subList = parentMenuItem.children('ul');
                    if ($.trim(subList.html()) !== "") {
                        subList.attr("aria-hidden", "true").attr("aria-expanded", "false");
                    }
                    if (event.keyCode == 37) {
                        parentMenuItem.prev().find('a:first').focus();
                    } else {
                        parentMenuItem.next().find('a:first').focus();
                    }
                    event.preventDefault();
                    event.stopPropagation();
                }
            });
        });
    }

</script>

</div>
<div><ul class="ui-breadcrumbs" role="navigation" aria-label="Breadcrumbs">
<li class="ui-breadcrumb-first"><a href="https://mpatzem.schoolwires.net/Domain/4"><span>Fletch_Test</span></a></li>

<li data-bccID="1173"><a href=""><span></span></a></li>

<li class="ui-breadcrumb-last" data-bcsID="1184"><a href=""><span></span></a></li>

<li class="ui-breadcrumb-last""><span>Lorem Ipsum</span></li>

</ul>
</div>
<div><div class='ui-widget app navigation pagenavigation'>
<div class="ui-widget-header">
<h1>
Our Programs</h1>
</div>
<div class='ui-widget-detail'>
<script type="text/javascript" src="https://mpatzem.schoolwires.net/Static/GlobalAssets/Scripts/sw-ada.js"></script><script type="text/javascript">$(document).ready(function () {WCM.ADA.tree = new WCM.ADA.Tree();});</script><ul class="page-navigation">
<li id="pagenavigation-273" class="active">
<a href="https://mpatzem.schoolwires.net/Page/273"
><span>Lorem Ipsum</span>
</a>
</li>
<li id="pagenavigation-274" class="">
<a href="https://mpatzem.schoolwires.net/Page/274"
><span>Resources Here</span>
</a>
</li>
<li id="pagenavigation-275" class="">
<a href="https://mpatzem.schoolwires.net/Page/275"
><span>Discover New Info</span>
</a>
</li>
<li id="pagenavigation-276" class="">
<a href="https://mpatzem.schoolwires.net/Page/276"
><span>Partnerships</span>
</a>
</li>
<li id="pagenavigation-277" class="">
<a href="https://mpatzem.schoolwires.net/Page/277"
><span>Hierarchy Expanded</span>
</a>
<ul>
<li id="pagenavigation-278" class="">
<a href="https://mpatzem.schoolwires.net/Page/278"
><span>Nested Item</span>
</a>
</li>
<li id="pagenavigation-279" class="">
<a href="https://mpatzem.schoolwires.net/Page/279"
><span>Another Nested Item</span>
</a>
</li>
<li id="pagenavigation-280" class="">
<a href="https://mpatzem.schoolwires.net/Page/280"
><span>Information Here</span>
</a>
</li></ul>
</li>
<li id="pagenavigation-281" class="">
<a href="https://mpatzem.schoolwires.net/Page/281"
><span>Discover New Info</span>
</a>
</li>
<li id="pagenavigation-282" class="">
<a href="https://mpatzem.schoolwires.net/Page/282"
><span>Partnerships</span>
</a>
</li>
<li id="pagenavigation-554" class="">
<a href="https://mpatzem.schoolwires.net/Page/554"
><span>TestTest</span>
</a>
</li>
<li id="pagenavigation-555" class="">
<a href="https://mpatzem.schoolwires.net/Page/555"
><span>Humble Feature App</span>
</a>
</li>
<li id="pagenavigation-557" class="">
<a href="https://mpatzem.schoolwires.net/Page/557"
><span>Not Lorem Ipsum</span>
</a>
</li></ul>
</div>
<div class="ui-widget-footer">
</div>
</div>
</div>
<div><!-- Start Centricity Content Area --><div id="sw-content-layout-wrapper" class="ui-sp ui-print" role="main"><a id="sw-maincontent" name="sw-maincontent" tabindex="-1"></a><div id="sw-content-layout7"><div id="sw-content-container1" class="ui-column-one region"><div id='pmi-1709'>



<div id='sw-module-12550'>
    <script type="text/javascript">
        $(document).ready(function() {
            var DomainID = '1184';
            var PageID = '273';
            var RenderLoc = '0';
            var MIID = '1255';

            //added to check to make sure moderated content doesn't bleed through the dialog
            if ($('#dialog-overlay-WindowLargeModal-body.moderateContent').length > 0) {
                $("#module-content-" + MIID).find(".ui-widget-detail").find(".ui-article").append("<p>&nbsp;</p>");
            }
        });
    </script>

    <script type="text/javascript">$(document).ready(function() {CheckScript('ModuleView');CheckScript('Mustache');
 });</script>
    
    <div id="module-content-1255" >
<div class="ui-widget app flexpage">
	<div class="ui-widget-header">
		<h1>Content</h1>
	</div>
	
	<div class="ui-widget-detail">
		<ul class="ui-articles">
<li>
	<div class="ui-article">
		<div class="ui-article-description">
        	<span><span ><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis tempor vulputate tellus, ut elementum neque sodales ac. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Duis sit amet neque id nisi efficitur gravida ac quis elit. Cras et semper justo, et dignissim arcu. Quisque ac nibh eu urna consequat dapibus. Nullam enim orci luctus eu molestie in, tristique ac leo. Morbi consectetur ligula et nibh bibendum, a tincidunt sem tristique. Cras egestas odio tellus, id tincidunt tortor suscipit non. Pellentesque imperdiet ex arcu, et rhoncus sem lacinia vitae.</p></span></span>
        </div>
		<div class="clear"></div>
	</div>
</li>
</ul>
</div>
	<div class="ui-widget-footer">
		
			
		
		<div class="clear"></div>
	</div>
</div>
<script type="text/javascript">
$(document).ready(function (){
$(".tag-list li a").keypress(function(e) { if(e.which == 13) { $(this).click();   }});
});
function LoadGroupedData(container, MIID, PMI, groupYear, groupMonth, groupBy, tag) {
  //ViewToUse looks at the hidden Sidebar List View defined in the Builder View.
  var viewToUse = "";
    if($('#hid-'+MIID+'-SidebarListView').length > 0){
      viewToUse = $("#hid-" + MIID + "-SidebarListView").val();
    };
  GetContent('https://mpatzem.schoolwires.net//cms/UserControls/ModuleView/ModuleViewRendererWrapper.aspx?DomainID=1184&PageID=273&ModuleInstanceID=' + MIID + '&PageModuleInstanceID=' + PMI + '&RenderLoc=0&FromRenderLoc=0&GroupYear=' + groupYear + '&GroupMonth=' + groupMonth + '&GroupByField=' + groupBy + '&EnableQuirksMode=0&ViewID=' + viewToUse + '&Tag=' + tag, container, 2, 'chkSidebar();');
}
function LoadData(container, MIID, PMI, flexDataID, groupYear, groupMonth, groupBy, targetView, tag) {
  if(targetView !== undefined || targetView.Length() == 0){
  //targetView looks at the hidden Detail View defined in the Builder View.
      targetView = $('#hid-'+MIID+'-DetailView').val();
   }
  GetContent('https://mpatzem.schoolwires.net//cms/UserControls/ModuleView/ModuleViewRendererWrapper.aspx?DomainID=1184&PageID=273&ModuleInstanceID=' + MIID + '&PageModuleInstanceID=' + PMI + '&FlexDataID=' + flexDataID + '&GroupYear=' + groupYear + '&GroupMonth=' + groupMonth + '&GroupByField=' + groupBy + '&RenderLoc=0&FromRenderLoc=0&EnableQuirksMode=0&Tag=' + tag + '&ViewID=' + targetView, container, 2, 'chkSidebar();');
}
function LoadTaggedData(container, MIID, PMI, tag) {
  //ViewToUse looks at the hidden Sidebar List View defined in the Builder View.
  var viewToUse = "";
    if($('#hid-'+MIID+'-SidebarListView').length > 0){
      viewToUse = $("#hid-" + MIID + "-SidebarListView").val();
    };
  GetContent('https://mpatzem.schoolwires.net//cms/UserControls/ModuleView/ModuleViewRendererWrapper.aspx?DomainID=1184&PageID=273&ModuleInstanceID=' + MIID + '&PageModuleInstanceID=' + PMI + '&RenderLoc=0&FromRenderLoc=0&Tag=' + tag + '&EnableQuirksMode=0&ViewID='+viewToUse, container, 2, 'chkSidebar();');
  setTimeout(function(){ $("#module-content-"+ MIID +"").find('[tabindex]:first').focus(); }, 200);
}
</script>
</div>

</div></div>
<div id='pmi-1743'>



<div id='sw-module-15920'>
    <script type="text/javascript">
        $(document).ready(function() {
            var DomainID = '1184';
            var PageID = '273';
            var RenderLoc = '0';
            var MIID = '1592';

            //added to check to make sure moderated content doesn't bleed through the dialog
            if ($('#dialog-overlay-WindowLargeModal-body.moderateContent').length > 0) {
                $("#module-content-" + MIID).find(".ui-widget-detail").find(".ui-article").append("<p>&nbsp;</p>");
            }
        });
    </script>

    <script type="text/javascript">$(document).ready(function() {CheckScript('ModuleView');CheckScript('Mustache');
 });</script>
    
    <div id="module-content-1592" >
<div class="ui-widget app tabbed-content">
	<div class="ui-widget-header">
		<h1>Tabbed Content</h1>
	</div>
	
	<div class="ui-widget-detail">
    	<div class="content-accordion-toolbar ui-helper-hidden">
        	<a href="#" class="content-accordion-toggle" data-toggle-status="expand" role="button" aria-label="Expand All Accordion Items">Expand All</a>
        </div>
		<ul class="ui-articles">
<li>  
                <div class="ui-article tabbed-content-panel-wrapper" id="tabbed-content-panel-wrapper-1743-1482" aria-hidden="true" aria-labelledby="tabbed-content-tab-1743-1482" role="tabpanel" tabindex="0"> 
                    <button class="ui-article-title content-accordion-button ui-helper-hidden" id="content-accordion-button-1743-1482" aria-expanded="false" aria-controls="tabbed-content-panel-1743-1482"><span>About</span></button>
                    <div class="content-accordion-panel" id="content-accordion-panel-1743-1482" aria-hidden="false">
                        <p>The Tabbed Content app is a responsive and accessible app that displays articles in a condensed way. This functionality is highly sought after for managing and organizing a high volume of articles and data.&nbsp;</p>
                    </div>
                </div> 
            </li>
<li>  
                <div class="ui-article tabbed-content-panel-wrapper" id="tabbed-content-panel-wrapper-1743-1483" aria-hidden="true" aria-labelledby="tabbed-content-tab-1743-1483" role="tabpanel" tabindex="0"> 
                    <button class="ui-article-title content-accordion-button ui-helper-hidden" id="content-accordion-button-1743-1483" aria-expanded="false" aria-controls="tabbed-content-panel-1743-1483"><span>Download</span></button>
                    <div class="content-accordion-panel" id="content-accordion-panel-1743-1483" aria-hidden="false">
                        <p>The Download section is a completely unresponsive and inaccessible app that doesn't display articles in a condensed way.</p>
                    </div>
                </div> 
            </li>
</ul>
	</div>

	<div class="ui-widget-footer ui-clear"></div>
</div>




<link rel="stylesheet" type="text/css" href="//extend.schoolwires.com/creative/module_library/tabbed-content/css/tabbed-content.min.css" />
<script type="text/javascript" src="//extend.schoolwires.com/creative/module_library/tabbed-content/js/tabbed-content.min.js"></script>

<script type="text/javascript">
    $(function() {
        CsTabbedContentApp.Init(1743);
    });
    
</script>
<script type="text/javascript">
$(document).ready(function (){
$(".tag-list li a").keypress(function(e) { if(e.which == 13) { $(this).click();   }});
});
function LoadGroupedData(container, MIID, PMI, groupYear, groupMonth, groupBy, tag) {
  //ViewToUse looks at the hidden Sidebar List View defined in the Builder View.
  var viewToUse = "";
    if($('#hid-'+MIID+'-SidebarListView').length > 0){
      viewToUse = $("#hid-" + MIID + "-SidebarListView").val();
    };
  GetContent('https://mpatzem.schoolwires.net//cms/UserControls/ModuleView/ModuleViewRendererWrapper.aspx?DomainID=1184&PageID=273&ModuleInstanceID=' + MIID + '&PageModuleInstanceID=' + PMI + '&RenderLoc=0&FromRenderLoc=0&GroupYear=' + groupYear + '&GroupMonth=' + groupMonth + '&GroupByField=' + groupBy + '&EnableQuirksMode=0&ViewID=' + viewToUse + '&Tag=' + tag, container, 2, 'chkSidebar();');
}
function LoadData(container, MIID, PMI, flexDataID, groupYear, groupMonth, groupBy, targetView, tag) {
  if(targetView !== undefined || targetView.Length() == 0){
  //targetView looks at the hidden Detail View defined in the Builder View.
      targetView = $('#hid-'+MIID+'-DetailView').val();
   }
  GetContent('https://mpatzem.schoolwires.net//cms/UserControls/ModuleView/ModuleViewRendererWrapper.aspx?DomainID=1184&PageID=273&ModuleInstanceID=' + MIID + '&PageModuleInstanceID=' + PMI + '&FlexDataID=' + flexDataID + '&GroupYear=' + groupYear + '&GroupMonth=' + groupMonth + '&GroupByField=' + groupBy + '&RenderLoc=0&FromRenderLoc=0&EnableQuirksMode=0&Tag=' + tag + '&ViewID=' + targetView, container, 2, 'chkSidebar();');
}
function LoadTaggedData(container, MIID, PMI, tag) {
  //ViewToUse looks at the hidden Sidebar List View defined in the Builder View.
  var viewToUse = "";
    if($('#hid-'+MIID+'-SidebarListView').length > 0){
      viewToUse = $("#hid-" + MIID + "-SidebarListView").val();
    };
  GetContent('https://mpatzem.schoolwires.net//cms/UserControls/ModuleView/ModuleViewRendererWrapper.aspx?DomainID=1184&PageID=273&ModuleInstanceID=' + MIID + '&PageModuleInstanceID=' + PMI + '&RenderLoc=0&FromRenderLoc=0&Tag=' + tag + '&EnableQuirksMode=0&ViewID='+viewToUse, container, 2, 'chkSidebar();');
  setTimeout(function(){ $("#module-content-"+ MIID +"").find('[tabindex]:first').focus(); }, 200);
}
</script>
</div>

</div></div>
</div><div id="sw-content-container2" class="ui-column-one-half region" ><div id='pmi-1742'>



<div id='sw-module-15910'>
    <script type="text/javascript">
        $(document).ready(function() {
            var DomainID = '1184';
            var PageID = '273';
            var RenderLoc = '0';
            var MIID = '1591';

            //added to check to make sure moderated content doesn't bleed through the dialog
            if ($('#dialog-overlay-WindowLargeModal-body.moderateContent').length > 0) {
                $("#module-content-" + MIID).find(".ui-widget-detail").find(".ui-article").append("<p>&nbsp;</p>");
            }
        });
    </script>

    <script type="text/javascript">$(document).ready(function() {CheckScript('ModuleView');CheckScript('Mustache');
 });</script>
    
    <div id="module-content-1591" >
<div class="ui-widget app multimedia-gallery" data-pmi="1742" data-mi="1591">
	<div class="ui-widget-header">
		<h1>Multimedia Gallery</h1>
	</div>
    <div class="ui-widget-detail">
    	<div id="mmg-container-1742" class="mmg-container" data-gallery-type="default" data-transition="fade" data-record-num="0" data-is-animating="false" data-play-state="playing" data-is-hovering="false" data-has-focus="false" data-is-touching="false" data-active-record-index="0" data-active-gallery-index="0">
            <div class="mmg-viewer">
                <div class="mmg-slides-outer">
                    <ul class="mmg-slides"></ul>
                </div>
                <div class="mmg-live-feedback mmg-ally-hidden" aria-live="polite" aria-atomic="true"></div>
            </div>
        </div>
        
        <script type="text/javascript">
            var multimediaGallery1742 = {
            	"props": {
                	"defaultGallery": true,
                    "imageWidth" : 960,
                	"imageHeight" : 500,
                	"pageModuleInstanceId": "1742",
                    "moduleInstanceId": "1591",
                    "virtualFolderPath": "https://mpatzem.schoolwires.net/cms/lib/SWCS000010/Centricity/ModuleInstance/1591/"
                },
				"records": [
{
                    "flexDataId": "1481",
                    "hideTitle": "True",
                    "title": "School... again?",
                    "hideCaption": "False",
                    "caption": "",
                    "imageSrc": "https://mpatzem.schoolwires.net/cms/lib/SWCS000010/Centricity/Domain/1184/MMG-3.png",
                    "imageWidth": "999",
                    "imageHeight": "594",
                    "imageAltText": "mountain again",
                    "isLinked": "False",
                    "linkUrl": "",
                    "linkText": "",
                    "openLinkInNewWindow": "False",
                    "videoLinkText": "",
                    "videoIsEmbedded": "False",
                    "videoIframe": ""
                },
{
                    "flexDataId": "1480",
                    "hideTitle": "False",
                    "title": "It&#39;s school time",
                    "hideCaption": "False",
                    "caption": "Caption caption",
                    "imageSrc": "https://mpatzem.schoolwires.net/cms/lib/SWCS000010/Centricity/Domain/1184/MMG-2.png",
                    "imageWidth": "999",
                    "imageHeight": "594",
                    "imageAltText": "mountains",
                    "isLinked": "False",
                    "linkUrl": "",
                    "linkText": "",
                    "openLinkInNewWindow": "False",
                    "videoLinkText": "",
                    "videoIsEmbedded": "False",
                    "videoIframe": ""
                },
{
                    "flexDataId": "1479",
                    "hideTitle": "False",
                    "title": "Back to School",
                    "hideCaption": "False",
                    "caption": "",
                    "imageSrc": "https://mpatzem.schoolwires.net/cms/lib/SWCS000010/Centricity/Domain/1184/MMG-1.png",
                    "imageWidth": "999",
                    "imageHeight": "594",
                    "imageAltText": "Mountain",
                    "isLinked": "False",
                    "linkUrl": "",
                    "linkText": "",
                    "openLinkInNewWindow": "False",
                    "videoLinkText": "",
                    "videoIsEmbedded": "False",
                    "videoIframe": ""
                },
{}]
			};
		</script>
	</div>
	<div class="ui-widget-footer ui-clear"></div>
</div>

<script type="text/javascript" src="//extend.schoolwires.com/creative/scripts/creative/rotate/multimedia-gallery/cs.multimedia.gallery.min.js"></script>
<script type="text/javascript">
	$(function() {
    	if(multimediaGallery1742.props.defaultGallery) {
        	$("#pmi-1742 .ui-widget.app.multimedia-gallery").csMultimediaGallery({
                "pageModuleInstanceId": 1742,
                "moduleInstanceId": 1591,
                "imageWidth" : multimediaGallery1742.props.imageWidth,
                "imageHeight" : multimediaGallery1742.props.imageHeight,
                "playPauseControl" : true,
                "bullets" : true
            });
        }
    });
</script>

<style type="text/css">
#pmi-1742:before {
    content: "960";
    display: none;
}
@media (max-width: 1023px) {
    #pmi-1742:before {
        content: "768";
    }
}
@media (max-width: 767px) {
    #pmi-1742:before {
        content: "640";
    }
}
@media (max-width: 639px) {
    #pmi-1742:before {
        content: "480";
    }
}
@media (max-width: 479px) {
    #pmi-1742:before {
        content: "320";
    }
}
</style>
<script type="text/javascript">
$(document).ready(function (){
$(".tag-list li a").keypress(function(e) { if(e.which == 13) { $(this).click();   }});
});
function LoadGroupedData(container, MIID, PMI, groupYear, groupMonth, groupBy, tag) {
  //ViewToUse looks at the hidden Sidebar List View defined in the Builder View.
  var viewToUse = "";
    if($('#hid-'+MIID+'-SidebarListView').length > 0){
      viewToUse = $("#hid-" + MIID + "-SidebarListView").val();
    };
  GetContent('https://mpatzem.schoolwires.net//cms/UserControls/ModuleView/ModuleViewRendererWrapper.aspx?DomainID=1184&PageID=273&ModuleInstanceID=' + MIID + '&PageModuleInstanceID=' + PMI + '&RenderLoc=0&FromRenderLoc=0&GroupYear=' + groupYear + '&GroupMonth=' + groupMonth + '&GroupByField=' + groupBy + '&EnableQuirksMode=0&ViewID=' + viewToUse + '&Tag=' + tag, container, 2, 'chkSidebar();');
}
function LoadData(container, MIID, PMI, flexDataID, groupYear, groupMonth, groupBy, targetView, tag) {
  if(targetView !== undefined || targetView.Length() == 0){
  //targetView looks at the hidden Detail View defined in the Builder View.
      targetView = $('#hid-'+MIID+'-DetailView').val();
   }
  GetContent('https://mpatzem.schoolwires.net//cms/UserControls/ModuleView/ModuleViewRendererWrapper.aspx?DomainID=1184&PageID=273&ModuleInstanceID=' + MIID + '&PageModuleInstanceID=' + PMI + '&FlexDataID=' + flexDataID + '&GroupYear=' + groupYear + '&GroupMonth=' + groupMonth + '&GroupByField=' + groupBy + '&RenderLoc=0&FromRenderLoc=0&EnableQuirksMode=0&Tag=' + tag + '&ViewID=' + targetView, container, 2, 'chkSidebar();');
}
function LoadTaggedData(container, MIID, PMI, tag) {
  //ViewToUse looks at the hidden Sidebar List View defined in the Builder View.
  var viewToUse = "";
    if($('#hid-'+MIID+'-SidebarListView').length > 0){
      viewToUse = $("#hid-" + MIID + "-SidebarListView").val();
    };
  GetContent('https://mpatzem.schoolwires.net//cms/UserControls/ModuleView/ModuleViewRendererWrapper.aspx?DomainID=1184&PageID=273&ModuleInstanceID=' + MIID + '&PageModuleInstanceID=' + PMI + '&RenderLoc=0&FromRenderLoc=0&Tag=' + tag + '&EnableQuirksMode=0&ViewID='+viewToUse, container, 2, 'chkSidebar();');
  setTimeout(function(){ $("#module-content-"+ MIID +"").find('[tabindex]:first').focus(); }, 200);
}
</script>
</div>

</div></div>
<div id='pmi-1744'>



<div id='sw-module-15930'>
    <script type="text/javascript">
        $(document).ready(function() {
            var DomainID = '1184';
            var PageID = '273';
            var RenderLoc = '0';
            var MIID = '1593';

            //added to check to make sure moderated content doesn't bleed through the dialog
            if ($('#dialog-overlay-WindowLargeModal-body.moderateContent').length > 0) {
                $("#module-content-" + MIID).find(".ui-widget-detail").find(".ui-article").append("<p>&nbsp;</p>");
            }
        });
    </script>

    <script type="text/javascript">$(document).ready(function() {CheckScript('ModuleView');CheckScript('Mustache');
 });</script>
    
    <div id="module-content-1593" >
<div class="ui-widget app content-accordion">
	<div class="ui-widget-header">
		<h1>C.  Accordion</h1>
	</div>
	
	<div class="ui-widget-detail">
    	<div class="content-accordion-toolbar">
        	<a href="#" class="content-accordion-toggle" data-toggle-status="expand" role="button" aria-label="Expand All Accordion Items">Expand All</a>
        </div>
		<ul class="ui-articles">
<li>  
                <div class="ui-article" data-auto-expand="False"> 
                    <h1 class="ui-article-title content-accordion-button" id="content-accordion-button-1744-1486" role="button" tabindex="0" aria-expanded="false" aria-controls="content-accordion-panel-1744-1486" aria-label="2020-21 New Student Registration"><span>2020-21 New Student Registration</span></h1>
                    <div class="content-accordion-panel ui-clear" id="content-accordion-panel-1744-1486" aria-hidden="true">
                        
                    </div>
                </div> 
            </li>
<li>  
                <div class="ui-article" data-auto-expand="False"> 
                    <h1 class="ui-article-title content-accordion-button" id="content-accordion-button-1744-1485" role="button" tabindex="0" aria-expanded="false" aria-controls="content-accordion-panel-1744-1485" aria-label="When should I enroll my student?"><span>When should I enroll my student?</span></h1>
                    <div class="content-accordion-panel ui-clear" id="content-accordion-panel-1744-1485" aria-hidden="true">
                        
                    </div>
                </div> 
            </li>
<li>  
                <div class="ui-article" data-auto-expand="False"> 
                    <h1 class="ui-article-title content-accordion-button" id="content-accordion-button-1744-1484" role="button" tabindex="0" aria-expanded="false" aria-controls="content-accordion-panel-1744-1484" aria-label="Attendance Boundaries & Feeder Schools"><span>Attendance Boundaries & Feeder Schools</span></h1>
                    <div class="content-accordion-panel ui-clear" id="content-accordion-panel-1744-1484" aria-hidden="true">
                        
                    </div>
                </div> 
            </li>
</ul>
	</div>

	<div class="ui-widget-footer ui-clear"></div>
</div>

<link rel="stylesheet" type="text/css" href="//extend.schoolwires.com/creative/module_library/content-accordion/css/cs.content.accordion.app.css" />
<script type="text/javascript" src="//extend.schoolwires.com/creative/module_library/content-accordion/js/cs.content.accordion.app.min.js"></script>
<script type="text/javascript">
    $(function() {
    	CsContentAccordionApp.Init(1744);
    });
</script>
<script type="text/javascript">
$(document).ready(function (){
$(".tag-list li a").keypress(function(e) { if(e.which == 13) { $(this).click();   }});
});
function LoadGroupedData(container, MIID, PMI, groupYear, groupMonth, groupBy, tag) {
  //ViewToUse looks at the hidden Sidebar List View defined in the Builder View.
  var viewToUse = "";
    if($('#hid-'+MIID+'-SidebarListView').length > 0){
      viewToUse = $("#hid-" + MIID + "-SidebarListView").val();
    };
  GetContent('https://mpatzem.schoolwires.net//cms/UserControls/ModuleView/ModuleViewRendererWrapper.aspx?DomainID=1184&PageID=273&ModuleInstanceID=' + MIID + '&PageModuleInstanceID=' + PMI + '&RenderLoc=0&FromRenderLoc=0&GroupYear=' + groupYear + '&GroupMonth=' + groupMonth + '&GroupByField=' + groupBy + '&EnableQuirksMode=0&ViewID=' + viewToUse + '&Tag=' + tag, container, 2, 'chkSidebar();');
}
function LoadData(container, MIID, PMI, flexDataID, groupYear, groupMonth, groupBy, targetView, tag) {
  if(targetView !== undefined || targetView.Length() == 0){
  //targetView looks at the hidden Detail View defined in the Builder View.
      targetView = $('#hid-'+MIID+'-DetailView').val();
   }
  GetContent('https://mpatzem.schoolwires.net//cms/UserControls/ModuleView/ModuleViewRendererWrapper.aspx?DomainID=1184&PageID=273&ModuleInstanceID=' + MIID + '&PageModuleInstanceID=' + PMI + '&FlexDataID=' + flexDataID + '&GroupYear=' + groupYear + '&GroupMonth=' + groupMonth + '&GroupByField=' + groupBy + '&RenderLoc=0&FromRenderLoc=0&EnableQuirksMode=0&Tag=' + tag + '&ViewID=' + targetView, container, 2, 'chkSidebar();');
}
function LoadTaggedData(container, MIID, PMI, tag) {
  //ViewToUse looks at the hidden Sidebar List View defined in the Builder View.
  var viewToUse = "";
    if($('#hid-'+MIID+'-SidebarListView').length > 0){
      viewToUse = $("#hid-" + MIID + "-SidebarListView").val();
    };
  GetContent('https://mpatzem.schoolwires.net//cms/UserControls/ModuleView/ModuleViewRendererWrapper.aspx?DomainID=1184&PageID=273&ModuleInstanceID=' + MIID + '&PageModuleInstanceID=' + PMI + '&RenderLoc=0&FromRenderLoc=0&Tag=' + tag + '&EnableQuirksMode=0&ViewID='+viewToUse, container, 2, 'chkSidebar();');
  setTimeout(function(){ $("#module-content-"+ MIID +"").find('[tabindex]:first').focus(); }, 200);
}
</script>
</div>

</div></div>
<div id='pmi-1969'>



<div id='sw-module-15780'>
    <script type="text/javascript">
        $(document).ready(function() {
            var DomainID = '1184';
            var PageID = '273';
            var RenderLoc = '0';
            var MIID = '1578';

            //added to check to make sure moderated content doesn't bleed through the dialog
            if ($('#dialog-overlay-WindowLargeModal-body.moderateContent').length > 0) {
                $("#module-content-" + MIID).find(".ui-widget-detail").find(".ui-article").append("<p>&nbsp;</p>");
            }
        });
    </script>

    <script type="text/javascript">$(document).ready(function() {CheckScript('ModuleView');CheckScript('Mustache');
 });</script>
    
    <div id="module-content-1578" >
<div class="ui-widget app headlines">
	
	<div class="ui-widget-header">
		<h1>Latest News</h1>
	</div>
	
	<div class="ui-widget-detail" id="sw-app-headlines-1578">
		<ul class="ui-articles">
<li>  
    <div  class="ui-article">   
        <span class="ui-article-thumb" >
            <span class="img">
                <img alt=" test" height="140" width="210" src="https://mpatzem.schoolwires.net/..//cms/lib/SWCS000010/Centricity/Domain/1184/music-1.png" />
            </span>
        </span>   
        
        <h1 class="ui-article-title">
        	<a href="https://mpatzem.schoolwires.net/../site/default.aspx?PageType=3&DomainID=1184&ModuleInstanceID=1578&ViewID=6446EE88-D30C-497E-9316-3F8874B3E108&RenderLoc=0&FlexDataID=1471&PageID=273"><span>Headline Number</span></a>
            </h1> 
        <p class="ui-article-description">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat.</p>   
        <div class="ui-article-controls">                                    
            <a class="sub-link" title="Go to comments for Headline Number" href="https://mpatzem.schoolwires.net/../site/default.aspx?PageType=3&DomainID=1184&ModuleInstanceID=1578&ViewID=6446EE88-D30C-497E-9316-3F8874B3E108&RenderLoc=0&FlexDataID=1471&PageID=273&Comments=true"><span>Comments (-1)</span></a>
               
        </div>   
        <div class="clear"></div>  
    </div>
</li>
<li>  
    <div  class="ui-article">   
        <span class="ui-article-thumb" >
            <span class="img">
                <img alt=" test 2" height="140" width="210" src="https://mpatzem.schoolwires.net/..//cms/lib/SWCS000010/Centricity/Domain/1184/music-2.png" />
            </span>
        </span>   
        
        <h1 class="ui-article-title">
        	<a href="https://mpatzem.schoolwires.net/../site/default.aspx?PageType=3&DomainID=1184&ModuleInstanceID=1578&ViewID=6446EE88-D30C-497E-9316-3F8874B3E108&RenderLoc=0&FlexDataID=1469&PageID=273"><span>Motion Sickness</span></a>
            </h1> 
        <p class="ui-article-description">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit.</p>   
        <div class="ui-article-controls">                                    
            <a class="sub-link" title="Go to comments for Motion Sickness" href="https://mpatzem.schoolwires.net/../site/default.aspx?PageType=3&DomainID=1184&ModuleInstanceID=1578&ViewID=6446EE88-D30C-497E-9316-3F8874B3E108&RenderLoc=0&FlexDataID=1469&PageID=273&Comments=true"><span>Comments (-1)</span></a>
               
        </div>   
        <div class="clear"></div>  
    </div>
</li>
<li>  
    <div  class="ui-article">   
        <span class="ui-article-thumb" >
            <span class="img">
                <img alt=" test 3" height="140" width="210" src="https://mpatzem.schoolwires.net/..//cms/lib/SWCS000010/Centricity/Domain/1184/music-3.png" />
            </span>
        </span>   
        
        <h1 class="ui-article-title">
        	<a href="https://mpatzem.schoolwires.net/../site/default.aspx?PageType=3&DomainID=1184&ModuleInstanceID=1578&ViewID=6446EE88-D30C-497E-9316-3F8874B3E108&RenderLoc=0&FlexDataID=1468&PageID=273"><span>Smoke Signals</span></a>
            </h1> 
        <p class="ui-article-description">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit.</p>   
        <div class="ui-article-controls">                                    
            <a class="sub-link" title="Go to comments for Smoke Signals" href="https://mpatzem.schoolwires.net/../site/default.aspx?PageType=3&DomainID=1184&ModuleInstanceID=1578&ViewID=6446EE88-D30C-497E-9316-3F8874B3E108&RenderLoc=0&FlexDataID=1468&PageID=273&Comments=true"><span>Comments (-1)</span></a>
               
        </div>   
        <div class="clear"></div>  
    </div>
</li>
<li>  
    <div  class="ui-article">   
        <span class="ui-article-thumb" >
            <span class="img">
                <img alt="" height="" width=""  />
            </span>
        </span>   
        
        <h1 class="ui-article-title">
        	<a href="https://mpatzem.schoolwires.net/../site/default.aspx?PageType=3&DomainID=1184&ModuleInstanceID=1578&ViewID=6446EE88-D30C-497E-9316-3F8874B3E108&RenderLoc=0&FlexDataID=1533&PageID=273"><span>No Image</span></a>
            </h1> 
        <p class="ui-article-description">Lorem Ipsum dolor sit amet, consetetur sadipscing elitr, sesd diam. </p>   
        <div class="ui-article-controls">                                    
            <a class="sub-link" title="Go to comments for No Image" href="https://mpatzem.schoolwires.net/../site/default.aspx?PageType=3&DomainID=1184&ModuleInstanceID=1578&ViewID=6446EE88-D30C-497E-9316-3F8874B3E108&RenderLoc=0&FlexDataID=1533&PageID=273&Comments=true"><span>Comments (-1)</span></a>
               
        </div>   
        <div class="clear"></div>  
    </div>
</li>
</ul><div class="ui-read-more"><a id='MoreLinkButton1578' class='more-link' aria-label='Go to more records' onclick='MoreViewClick(this);' href='https://mpatzem.schoolwires.net/site/default.aspx?PageType=14&DomainID=1184&PageID=273&ModuleInstanceID=1578&ViewID=c83d46ac-74fe-4857-8c9a-5922a80225e2&IsMoreExpandedView=True'><span>more</span></a><div class='more-link-under'>&nbsp;</div></div>
</ul>
	</div>

	<div class="ui-widget-footer">
		
		
		<div class="clear"></div>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function() {
        /*$(document).on('click', 'a.ui-article-thumb', function() {
        	window.location = $(this).attr('href');
    	});*/
    		
		$('#sw-app-headlines-1578').find('img').each(function() {
			if ($.trim(this.src) == '' ) {
				$(this).parent().parent().remove();
			}
		});
        
        // Jason Smith - 12/9/2014 - Removed due to bandwidth implications
		
	});

</script>
<script type="text/javascript">
$(document).ready(function (){
$(".tag-list li a").keypress(function(e) { if(e.which == 13) { $(this).click();   }});
});
function LoadGroupedData(container, MIID, PMI, groupYear, groupMonth, groupBy, tag) {
  //ViewToUse looks at the hidden Sidebar List View defined in the Builder View.
  var viewToUse = "";
    if($('#hid-'+MIID+'-SidebarListView').length > 0){
      viewToUse = $("#hid-" + MIID + "-SidebarListView").val();
    };
  GetContent('https://mpatzem.schoolwires.net//cms/UserControls/ModuleView/ModuleViewRendererWrapper.aspx?DomainID=1184&PageID=273&ModuleInstanceID=' + MIID + '&PageModuleInstanceID=' + PMI + '&RenderLoc=0&FromRenderLoc=0&GroupYear=' + groupYear + '&GroupMonth=' + groupMonth + '&GroupByField=' + groupBy + '&EnableQuirksMode=0&ViewID=' + viewToUse + '&Tag=' + tag, container, 2, 'chkSidebar();');
}
function LoadData(container, MIID, PMI, flexDataID, groupYear, groupMonth, groupBy, targetView, tag) {
  if(targetView !== undefined || targetView.Length() == 0){
  //targetView looks at the hidden Detail View defined in the Builder View.
      targetView = $('#hid-'+MIID+'-DetailView').val();
   }
  GetContent('https://mpatzem.schoolwires.net//cms/UserControls/ModuleView/ModuleViewRendererWrapper.aspx?DomainID=1184&PageID=273&ModuleInstanceID=' + MIID + '&PageModuleInstanceID=' + PMI + '&FlexDataID=' + flexDataID + '&GroupYear=' + groupYear + '&GroupMonth=' + groupMonth + '&GroupByField=' + groupBy + '&RenderLoc=0&FromRenderLoc=0&EnableQuirksMode=0&Tag=' + tag + '&ViewID=' + targetView, container, 2, 'chkSidebar();');
}
function LoadTaggedData(container, MIID, PMI, tag) {
  //ViewToUse looks at the hidden Sidebar List View defined in the Builder View.
  var viewToUse = "";
    if($('#hid-'+MIID+'-SidebarListView').length > 0){
      viewToUse = $("#hid-" + MIID + "-SidebarListView").val();
    };
  GetContent('https://mpatzem.schoolwires.net//cms/UserControls/ModuleView/ModuleViewRendererWrapper.aspx?DomainID=1184&PageID=273&ModuleInstanceID=' + MIID + '&PageModuleInstanceID=' + PMI + '&RenderLoc=0&FromRenderLoc=0&Tag=' + tag + '&EnableQuirksMode=0&ViewID='+viewToUse, container, 2, 'chkSidebar();');
  setTimeout(function(){ $("#module-content-"+ MIID +"").find('[tabindex]:first').focus(); }, 200);
}
</script>
</div>

</div></div>
</div><div id="sw-content-container3" class="ui-column-one-half region"><div id='pmi-1710'>
<div id="module-content-1576" >
<div class="ui-widget app navigation  siteshortcuts">
	<div class="ui-widget-header"><h1>Site Shortcuts</h1></div>
	<div class="ui-widget-detail">
		<ul class="site-shortcuts">
<li id="siteshortcut-274" class="navigationgroup"><a >Punisher</a>
<ul>
<li id="siteshortcut-275" class="navigationgroup"><a >Moon Song</a>
</li></ul>
</li>
<li id="siteshortcut-269" class="navigationgroup"><a >KEXP</a>
</li>
<li id="siteshortcut-268" class="navigationgroup"><a >Stranger in the Alps</a>
</li>
<li id="siteshortcut-267" class="navigationgroup"><a >Side B</a>
<ul>
<li id="siteshortcut-273" class="navigationgroup"><a >Motion Sickness</a>
</li>
<li id="siteshortcut-272" class="navigationgroup"><a >Georgia</a>
</li></ul>
</li>
<li id="siteshortcut-266" class="navigationgroup"><a >Side A</a>
<ul>
<li id="siteshortcut-271" class="navigationgroup"><a >Demi Moore</a>
</li>
<li id="siteshortcut-270" class="navigationgroup"><a >Scott Street</a>
</li></ul>
</li></ul>
<div class="app-level-social-follow"></div>
	</div>
	<div class="ui-widget-footer">
	</div>
</div></div>
</div>
<div id='pmi-1711'>



<div id='sw-module-15770'>
    <script type="text/javascript">
        $(document).ready(function() {
            var DomainID = '1184';
            var PageID = '273';
            var RenderLoc = '0';
            var MIID = '1577';

            //added to check to make sure moderated content doesn't bleed through the dialog
            if ($('#dialog-overlay-WindowLargeModal-body.moderateContent').length > 0) {
                $("#module-content-" + MIID).find(".ui-widget-detail").find(".ui-article").append("<p>&nbsp;</p>");
            }
        });
    </script>

    <script type="text/javascript">$(document).ready(function() {CheckScript('ModuleView');CheckScript('Mustache');
 });</script>
    
    <div id="module-content-1577" >
<div class="ui-widget app announcements">
	<div class="ui-widget-header">
		<h1>Announcements</h1>
	</div>
	
	<div class="ui-widget-detail">
		<ul class="ui-articles">
<li>
    <div class="ui-article">
		<div class="ui-article-description">
			<span><p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit.</p></span>
		</div>
		<div class="ui-article-controls">
        	<a class="sub-link" title="Go to comments for Title 2" href="https://mpatzem.schoolwires.net/../site/default.aspx?PageType=3&ModuleInstanceID=1577&ViewID=ed695a1c-ef13-4546-b4eb-4fefcdd4f389&RenderLoc=0&FlexDataID=1466&PageID=273&Comments=true"><span>Comments (-1)</span></a>
			
		</div>
		<div class="clear"></div>
	</div>
</li>
<li>
    <div class="ui-article">
		<div class="ui-article-description">
			<span><p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit.</p></span>
		</div>
		<div class="ui-article-controls">
        	<a class="sub-link" title="Go to comments for Titles for Announcements" href="https://mpatzem.schoolwires.net/../site/default.aspx?PageType=3&ModuleInstanceID=1577&ViewID=ed695a1c-ef13-4546-b4eb-4fefcdd4f389&RenderLoc=0&FlexDataID=1465&PageID=273&Comments=true"><span>Comments (-1)</span></a>
			
		</div>
		<div class="clear"></div>
	</div>
</li>
</ul>
</div>
	<div class="ui-widget-footer">
		
		<div class="app-level-social-follow"></div> 
		<div class="clear"></div>
	</div>
</div>
<script type="text/javascript">
$(document).ready(function (){
$(".tag-list li a").keypress(function(e) { if(e.which == 13) { $(this).click();   }});
});
function LoadGroupedData(container, MIID, PMI, groupYear, groupMonth, groupBy, tag) {
  //ViewToUse looks at the hidden Sidebar List View defined in the Builder View.
  var viewToUse = "";
    if($('#hid-'+MIID+'-SidebarListView').length > 0){
      viewToUse = $("#hid-" + MIID + "-SidebarListView").val();
    };
  GetContent('https://mpatzem.schoolwires.net//cms/UserControls/ModuleView/ModuleViewRendererWrapper.aspx?DomainID=1184&PageID=273&ModuleInstanceID=' + MIID + '&PageModuleInstanceID=' + PMI + '&RenderLoc=0&FromRenderLoc=0&GroupYear=' + groupYear + '&GroupMonth=' + groupMonth + '&GroupByField=' + groupBy + '&EnableQuirksMode=0&ViewID=' + viewToUse + '&Tag=' + tag, container, 2, 'chkSidebar();');
}
function LoadData(container, MIID, PMI, flexDataID, groupYear, groupMonth, groupBy, targetView, tag) {
  if(targetView !== undefined || targetView.Length() == 0){
  //targetView looks at the hidden Detail View defined in the Builder View.
      targetView = $('#hid-'+MIID+'-DetailView').val();
   }
  GetContent('https://mpatzem.schoolwires.net//cms/UserControls/ModuleView/ModuleViewRendererWrapper.aspx?DomainID=1184&PageID=273&ModuleInstanceID=' + MIID + '&PageModuleInstanceID=' + PMI + '&FlexDataID=' + flexDataID + '&GroupYear=' + groupYear + '&GroupMonth=' + groupMonth + '&GroupByField=' + groupBy + '&RenderLoc=0&FromRenderLoc=0&EnableQuirksMode=0&Tag=' + tag + '&ViewID=' + targetView, container, 2, 'chkSidebar();');
}
function LoadTaggedData(container, MIID, PMI, tag) {
  //ViewToUse looks at the hidden Sidebar List View defined in the Builder View.
  var viewToUse = "";
    if($('#hid-'+MIID+'-SidebarListView').length > 0){
      viewToUse = $("#hid-" + MIID + "-SidebarListView").val();
    };
  GetContent('https://mpatzem.schoolwires.net//cms/UserControls/ModuleView/ModuleViewRendererWrapper.aspx?DomainID=1184&PageID=273&ModuleInstanceID=' + MIID + '&PageModuleInstanceID=' + PMI + '&RenderLoc=0&FromRenderLoc=0&Tag=' + tag + '&EnableQuirksMode=0&ViewID='+viewToUse, container, 2, 'chkSidebar();');
  setTimeout(function(){ $("#module-content-"+ MIID +"").find('[tabindex]:first').focus(); }, 200);
}
</script>
</div>

</div></div>
<div id='pmi-1956'>
<div id="module-content-1581" ><div class="ui-widget app upcomingevents">
 <div class="ui-widget-header">
     <h1>Upcoming Events</h1>
 </div>
 <div class="ui-widget-detail">
		<ul class="ui-articles">
<li>
<div class="ui-article">
     <h1 class="ui-article-title sw-calendar-block-date">Today</h1>
     <p class="ui-article-description">
         <span class="sw-calendar-block-time">8:00 AM - 9:00 AM</span>
         <span class="sw-calendar-block-title"><a href="
https://mpatzem.schoolwires.net/site/Default.aspx?PageID=273&DomainID=1184#calendar1798/20230405/event/2907">Test 1</a></span>
     </p>
</div>
</li>
<li>
<div class="ui-article">
     <p class="ui-article-description">
         <span class="sw-calendar-block-time">11:00 AM - 12:00 PM</span>
         <span class="sw-calendar-block-title"><a href="
https://mpatzem.schoolwires.net/site/Default.aspx?PageID=273&DomainID=1184#calendar1798/20230405/event/2913">Event 1</a></span>
     </p>
</div>
</li>
<li>
<div class="ui-article">
     <h1 class="ui-article-title sw-calendar-block-date">Tomorrow</h1>
     <p class="ui-article-description">
         <span class="sw-calendar-block-time">8:00 AM - 9:00 AM</span>
         <span class="sw-calendar-block-title"><a href="
https://mpatzem.schoolwires.net/site/Default.aspx?PageID=273&DomainID=1184#calendar1798/20230406/event/2907">Test 1</a></span>
     </p>
</div>
</li>
<li>
<div class="ui-article">
     <p class="ui-article-description">
         <span class="sw-calendar-block-time">11:00 AM - 12:00 PM</span>
         <span class="sw-calendar-block-title"><a href="
https://mpatzem.schoolwires.net/site/Default.aspx?PageID=273&DomainID=1184#calendar1798/20230406/event/2914">Event 1</a></span>
     </p>
</div>
</li>
<li>
<div class="ui-article">
     <h1 class="ui-article-title sw-calendar-block-date">Friday</h1>
     <p class="ui-article-description">
         <span class="sw-calendar-block-time">8:00 AM - 9:00 AM</span>
         <span class="sw-calendar-block-title"><a href="
https://mpatzem.schoolwires.net/site/Default.aspx?PageID=273&DomainID=1184#calendar1798/20230407/event/2907">Test 1</a></span>
     </p>
</div>
</li>
<li>
<div class="ui-article">
     <p class="ui-article-description">
         <span class="sw-calendar-block-time">11:00 AM - 12:00 PM</span>
         <span class="sw-calendar-block-title"><a href="
https://mpatzem.schoolwires.net/site/Default.aspx?PageID=273&DomainID=1184#calendar1798/20230407/event/2915">Event 1</a></span>
     </p>
</div>
</li>
<li>
<div class="ui-article">
     <h1 class="ui-article-title sw-calendar-block-date">Saturday</h1>
     <p class="ui-article-description">
         <span class="sw-calendar-block-time">8:00 AM - 9:00 AM</span>
         <span class="sw-calendar-block-title"><a href="
https://mpatzem.schoolwires.net/site/Default.aspx?PageID=273&DomainID=1184#calendar1798/20230408/event/2907">Test 1</a></span>
     </p>
</div>
</li>
<li>
<div class="ui-article">
     <p class="ui-article-description">
         <span class="sw-calendar-block-time">11:00 AM - 12:00 PM</span>
         <span class="sw-calendar-block-title"><a href="
https://mpatzem.schoolwires.net/site/Default.aspx?PageID=273&DomainID=1184#calendar1798/20230408/event/2916">Event 1</a></span>
     </p>
</div>
</li>
<li>
<div class="ui-article">
     <h1 class="ui-article-title sw-calendar-block-date">Sunday</h1>
     <p class="ui-article-description">
         <span class="sw-calendar-block-time">8:00 AM - 9:00 AM</span>
         <span class="sw-calendar-block-title"><a href="
https://mpatzem.schoolwires.net/site/Default.aspx?PageID=273&DomainID=1184#calendar1798/20230409/event/2907">Test 1</a></span>
     </p>
</div>
</li>
<li>
<div class="ui-article">
     <p class="ui-article-description">
         <span class="sw-calendar-block-time">11:00 AM - 12:00 PM</span>
         <span class="sw-calendar-block-title"><a href="
https://mpatzem.schoolwires.net/site/Default.aspx?PageID=273&DomainID=1184#calendar1798/20230409/event/2917">Event 1</a></span>
     </p>
</div>
</li>
	</ul>
<a class='view-calendar-link' href="https://mpatzem.schoolwires.net/Page/273"><span>View Calendar</span></a>
 </div>
 <div class="ui-widget-footer">
 </div>
</div>
</div>
</div>
<div id='pmi-1715'>



<div id='sw-module-15800'>
    <script type="text/javascript">
        $(document).ready(function() {
            var DomainID = '1184';
            var PageID = '273';
            var RenderLoc = '0';
            var MIID = '1580';

            //added to check to make sure moderated content doesn't bleed through the dialog
            if ($('#dialog-overlay-WindowLargeModal-body.moderateContent').length > 0) {
                $("#module-content-" + MIID).find(".ui-widget-detail").find(".ui-article").append("<p>&nbsp;</p>");
            }
        });
    </script>

    <script type="text/javascript">$(document).ready(function() {CheckScript('ModuleView');CheckScript('Mustache');
 });</script>
    
    <div id="module-content-1580" >
<div class="ui-widget app flexpage">
	<div class="ui-widget-header ui-helper-hidden">
		
	</div>
	
	<div class="ui-widget-detail">
		<ul class="ui-articles">
<li>
	<div class="ui-article">
		<div class="ui-article-description">
        	<span><span ><p><span class="Border_Box">Background color and text are set using the Primary Editor Style Background Color and Primary Editor Style Text Color options in the TCW.&nbsp;<a href="https://kfilko.schoolwires.net/#" target="_parent">Link&nbsp;colors are set in the TCW.</a>&nbsp;Choose Primary Editor Style Link Color located under Global Template Colors.<br /></span></p>
<p><span class="Primary_Box">Background color and text are set using the Primary Editor Style Background Color and Primary Editor Style Text Color options in the TCW.&nbsp;<a href="https://kfilko.schoolwires.net/#" target="_parent">Link&nbsp;colors are set in the TCW.</a>&nbsp;Choose Primary Editor Style Link Color located under Global Template Colors.<br /></span></p>
<p><span class="Secondary_Box">Background color and text are set using the&nbsp;Secondary Editor Style Background Color and&nbsp;Secondary Editor Style Text Color options in the TCW.&nbsp;<a href="https://kfilko.schoolwires.net/#" target="_parent">Link&nbsp;colors are set in the TCW.</a>&nbsp;Choose&nbsp;Primary Editor Style Link Color located under Global Template Colors.</span></p>
<p><span class="Call_To_Action_Button"><a href="http://www.google.com">Featured Content Button Style</a></span></p></span></span>
        </div>
		<div class="clear"></div>
	</div>
</li>
</ul>
</div>
	<div class="ui-widget-footer">
		
			
		
		<div class="clear"></div>
	</div>
</div>
<script type="text/javascript">
$(document).ready(function (){
$(".tag-list li a").keypress(function(e) { if(e.which == 13) { $(this).click();   }});
});
function LoadGroupedData(container, MIID, PMI, groupYear, groupMonth, groupBy, tag) {
  //ViewToUse looks at the hidden Sidebar List View defined in the Builder View.
  var viewToUse = "";
    if($('#hid-'+MIID+'-SidebarListView').length > 0){
      viewToUse = $("#hid-" + MIID + "-SidebarListView").val();
    };
  GetContent('https://mpatzem.schoolwires.net//cms/UserControls/ModuleView/ModuleViewRendererWrapper.aspx?DomainID=1184&PageID=273&ModuleInstanceID=' + MIID + '&PageModuleInstanceID=' + PMI + '&RenderLoc=0&FromRenderLoc=0&GroupYear=' + groupYear + '&GroupMonth=' + groupMonth + '&GroupByField=' + groupBy + '&EnableQuirksMode=0&ViewID=' + viewToUse + '&Tag=' + tag, container, 2, 'chkSidebar();');
}
function LoadData(container, MIID, PMI, flexDataID, groupYear, groupMonth, groupBy, targetView, tag) {
  if(targetView !== undefined || targetView.Length() == 0){
  //targetView looks at the hidden Detail View defined in the Builder View.
      targetView = $('#hid-'+MIID+'-DetailView').val();
   }
  GetContent('https://mpatzem.schoolwires.net//cms/UserControls/ModuleView/ModuleViewRendererWrapper.aspx?DomainID=1184&PageID=273&ModuleInstanceID=' + MIID + '&PageModuleInstanceID=' + PMI + '&FlexDataID=' + flexDataID + '&GroupYear=' + groupYear + '&GroupMonth=' + groupMonth + '&GroupByField=' + groupBy + '&RenderLoc=0&FromRenderLoc=0&EnableQuirksMode=0&Tag=' + tag + '&ViewID=' + targetView, container, 2, 'chkSidebar();');
}
function LoadTaggedData(container, MIID, PMI, tag) {
  //ViewToUse looks at the hidden Sidebar List View defined in the Builder View.
  var viewToUse = "";
    if($('#hid-'+MIID+'-SidebarListView').length > 0){
      viewToUse = $("#hid-" + MIID + "-SidebarListView").val();
    };
  GetContent('https://mpatzem.schoolwires.net//cms/UserControls/ModuleView/ModuleViewRendererWrapper.aspx?DomainID=1184&PageID=273&ModuleInstanceID=' + MIID + '&PageModuleInstanceID=' + PMI + '&RenderLoc=0&FromRenderLoc=0&Tag=' + tag + '&EnableQuirksMode=0&ViewID='+viewToUse, container, 2, 'chkSidebar();');
  setTimeout(function(){ $("#module-content-"+ MIID +"").find('[tabindex]:first').focus(); }, 200);
}
</script>
</div>

</div></div>
<div id='pmi-1721'>



<div id='sw-module-15830'>
    <script type="text/javascript">
        $(document).ready(function() {
            var DomainID = '1184';
            var PageID = '273';
            var RenderLoc = '0';
            var MIID = '1583';

            //added to check to make sure moderated content doesn't bleed through the dialog
            if ($('#dialog-overlay-WindowLargeModal-body.moderateContent').length > 0) {
                $("#module-content-" + MIID).find(".ui-widget-detail").find(".ui-article").append("<p>&nbsp;</p>");
            }
        });
    </script>

    <script type="text/javascript">$(document).ready(function() {CheckScript('ModuleView');CheckScript('Mustache');
 });</script>
    
    <div id="module-content-1583" >
<div class="ui-widget app flexpage">
	<div class="ui-widget-header ui-helper-hidden">
		
	</div>
	
	<div class="ui-widget-detail">
		<ul class="ui-articles">
<li>
	<div class="ui-article">
		<div class="ui-article-description">
        	<span><span ><h1 class="ui-widget-header">H1 Style</h1>
<div class="ui-widget-detail">
<div class="ui-article last-article">
<div class="ui-article-description">
<h2>H2 Style</h2>
<h3>H3 Style</h3>
<h4>H4 Style</h4>
<p>Body Copy - Lorem ipsum dolor sit amet, conse ctetur adipiscing elit. Ut ultricies molestis <a href="https://mpatzem.schoolwires.net/site/Default.aspx?PageID=1" target="">This is a link</a> adipiscing elit. Ut ultricies molestie lacinia.Lorem ipsum dolor sit amet, conse ctetlit. Ut ultricies molestie lacinia</p>
</div>
</div>
</div></span></span>
        </div>
		<div class="clear"></div>
	</div>
</li>
</ul>
</div>
	<div class="ui-widget-footer">
		
			
		
		<div class="clear"></div>
	</div>
</div>
<script type="text/javascript">
$(document).ready(function (){
$(".tag-list li a").keypress(function(e) { if(e.which == 13) { $(this).click();   }});
});
function LoadGroupedData(container, MIID, PMI, groupYear, groupMonth, groupBy, tag) {
  //ViewToUse looks at the hidden Sidebar List View defined in the Builder View.
  var viewToUse = "";
    if($('#hid-'+MIID+'-SidebarListView').length > 0){
      viewToUse = $("#hid-" + MIID + "-SidebarListView").val();
    };
  GetContent('https://mpatzem.schoolwires.net//cms/UserControls/ModuleView/ModuleViewRendererWrapper.aspx?DomainID=1184&PageID=273&ModuleInstanceID=' + MIID + '&PageModuleInstanceID=' + PMI + '&RenderLoc=0&FromRenderLoc=0&GroupYear=' + groupYear + '&GroupMonth=' + groupMonth + '&GroupByField=' + groupBy + '&EnableQuirksMode=0&ViewID=' + viewToUse + '&Tag=' + tag, container, 2, 'chkSidebar();');
}
function LoadData(container, MIID, PMI, flexDataID, groupYear, groupMonth, groupBy, targetView, tag) {
  if(targetView !== undefined || targetView.Length() == 0){
  //targetView looks at the hidden Detail View defined in the Builder View.
      targetView = $('#hid-'+MIID+'-DetailView').val();
   }
  GetContent('https://mpatzem.schoolwires.net//cms/UserControls/ModuleView/ModuleViewRendererWrapper.aspx?DomainID=1184&PageID=273&ModuleInstanceID=' + MIID + '&PageModuleInstanceID=' + PMI + '&FlexDataID=' + flexDataID + '&GroupYear=' + groupYear + '&GroupMonth=' + groupMonth + '&GroupByField=' + groupBy + '&RenderLoc=0&FromRenderLoc=0&EnableQuirksMode=0&Tag=' + tag + '&ViewID=' + targetView, container, 2, 'chkSidebar();');
}
function LoadTaggedData(container, MIID, PMI, tag) {
  //ViewToUse looks at the hidden Sidebar List View defined in the Builder View.
  var viewToUse = "";
    if($('#hid-'+MIID+'-SidebarListView').length > 0){
      viewToUse = $("#hid-" + MIID + "-SidebarListView").val();
    };
  GetContent('https://mpatzem.schoolwires.net//cms/UserControls/ModuleView/ModuleViewRendererWrapper.aspx?DomainID=1184&PageID=273&ModuleInstanceID=' + MIID + '&PageModuleInstanceID=' + PMI + '&RenderLoc=0&FromRenderLoc=0&Tag=' + tag + '&EnableQuirksMode=0&ViewID='+viewToUse, container, 2, 'chkSidebar();');
  setTimeout(function(){ $("#module-content-"+ MIID +"").find('[tabindex]:first').focus(); }, 200);
}
</script>
</div>

</div></div>
<div id='pmi-1974'>
<div id="module-content-1798" >

<div class="ui-widget app calendar" style="overflow: hidden;">
<div class="ui-widget-header ui-helper-hidden">
    
</div>


	
<div class="ui-widget-detail" style="overflow: hidden;">
<div id="calendar-pnl-calendarcontainer-1798">
    
    <div id="calendar-newplaceholder-1798"></div>

    
    
</div>
</div>

<div class="ui-widget-footer">
<div class="clear"></div>
</div>
    </div>


<script type='text/javascript'>
    if ('False' === 'True') {
    eventDateID = 0;
    currentMonth = 0;
    currentYear = 0;

    $(document).ready(function() {		   
        // ADA click events
        $("#calendar-pnl-filter-toggle a").keydown(function(e) {
            e.stopImmediatePropagation();

            if (e.keyCode == 13) {
                $(this).click();
            }
        });

        $("#calendar-btn-print").keydown(function(e) {
            e.stopImmediatePropagation();

            if (e.keyCode == 13) {
                $(this).click();
            }
        });

        $("#A1").keydown(function(e) {
            e.stopImmediatePropagation();

            if (e.keyCode == 13) {
                $(this).click();
            }
        });

        $("#calendar-btn-calendarfollow").keydown(function(e) {
            e.stopImmediatePropagation();

            if (e.keyCode == 13) {
                $(this).click();
            }
        });

        $("#calendar-btn-icalfed").keydown(function(e) {
            e.stopImmediatePropagation();

            if (e.keyCode == 13) {
                $(this).click();
            }
        });

        $("#calendar-btn-change-today").keydown(function(e) {
            e.stopImmediatePropagation();

            if (e.keyCode == 13) {
                $(this).click();
            }
        });

        $("#calendar-btn-change-day").keydown(function(e) {
            e.stopImmediatePropagation();

            if (e.keyCode == 13) {
                $(this).click();
            }
        });

        $("#calendar-btn-change-week").keydown(function(e) {
            e.stopImmediatePropagation();

            if (e.keyCode == 13) {
                $(this).click();
            }
        });

        $("#calendar-btn-change-month").keydown(function(e) {
            e.stopImmediatePropagation();

            if (e.keyCode == 13) {
                $(this).click();
            }
        });

        $("#ui-btn-calendarlist-list").keydown(function(e) {
            e.stopImmediatePropagation();

            if (e.keyCode == 13) {
                $(this).click();
            }
        });

        $("#ui-btn-calendarlist-back").keydown(function(e) {
            e.stopImmediatePropagation();

            if (e.keyCode == 13) {
                $(this).click();
            }
        });

        $("#ui-btn-calendarlist-forward").keydown(function(e) {
            e.stopImmediatePropagation();

            if (e.keyCode == 13) {
                $(this).click();
            }
        });

        $(".calendar-filter-btn").keydown(function(e) {
            e.stopImmediatePropagation();

            if (e.keyCode == 13) {
                $(this).click();
            }
        });

        $(".category-filter-btn").keydown(function(e) {
            e.stopImmediatePropagation();

            if (e.keyCode == 13) {
                $(this).click();
            }
        });

        $(document).on('click', '.ui-datepicker-calendar td a', function() {
            fixADA();
        });

        $(document).on('click', '.ui-datepicker-calendar td a', function() {
            fixADA();
        });

		// Don't show the sidebar if the width is not greater than 900 px.
	    var intCalendarWidth = $("#calendar-pnl-calendarcontainer").width();

	    if (intCalendarWidth < 900 ) {
			$("#calendar-pnl-smcalendar").addClass('ui-helper-hidden')
		}
			
		var thisYear = new Date().getFullYear();
	    
	    var thisMonth;
	    //if (eventDateID > 0) {
	    //	thisMonth = 3;
	    //} else {
	    	thisMonth = new Date().getMonth();
	    //}

	    $('#calendarlist-month').val(thisMonth + 1).change();

	    //function LoadEventPrint() {	
        //		var view = $('#calendar').fullCalendar('getView');
    //		var month = $('#calendar').fullCalendar('getmonth');
//		var date = $('#calendar').fullCalendar('getdate');
//		var year = $('#calendar').fullCalendar('getyear');
//		if(!$('#calendarlist-pnl-specialview').hasClass('ui-helper-hidden')){
//			view = "list";
//			month = $('#calendarlist-month').val() - 1;
//            year = $('#calendarlist-year').val();
//		} 
//		
//OpenWindow('https://mpatzem.schoolwires.net//site/UserControls/Calendar/CalendarPrint.aspx?ModuleInstanceID=1798&PageID=273&Date=' + date + '&Month=' + month + '&Year=' + year + '&View=' + view, 'PrintCalendar', 'width=' + (screen.width - 100) + ',height=' + (screen.height - 100) + ',menubar=yes');
	    // }

        //remove extra datepicker div that gets added into footer
	    if ($('#ui-datepicker-div').length){
	        $('#ui-datepicker-div').hide();
	    }

        $('#calendar').fullCalendar({
	        editable: false,
	        header: {
	            left: '',
	            center: 'title',
	            right: ''
	        },
            month: new Date().getMonth(),
            date: new Date().getDate(),
            year: new Date().getFullYear(),
            defaultView: 'month',
	        weekMode: 'liquid',
	        timeFormat: 'h:mm TT',
	        events: function(start, end, callback) {
                $.ajax({
                    type: "POST",
                    url: "https://mpatzem.schoolwires.net//site/UserControls/Calendar/CalendarController.aspx/GetEvents",
                    data: "{ModuleInstanceID: 1798, " +
                                "StartDate: '" + (parseInt(start.getMonth()) + 1) + "/" + start.getDate() + "/" + start.getFullYear() +  "', " +
                                "EndDate: '" + (parseInt(end.getMonth()) + 1) + "/" + end.getDate() + "/" + end.getFullYear() + "'}", 
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    success: function(result) { 
                        var calevents = eval(result.d);
	                    callback(calevents);
						$('.fc-event-title').addClass('ellipsis');
                    }
                });
	        }, 
	        eventClick: function(calEvent, jsEvent, view) {
	            if ($(this).hasClass('fc-event-more')) {
	                $('#calendar').find('td.fc-active-day').removeClass('fc-active-day');

	                // get weekday and date
	                var dayClass;
	                var dateClass = 'fc-day' + (calEvent.start.getDate() - 1);

	                switch (calEvent.start.getDay()) {
	                    case 0:
	                        dayClass = 'fc-sun';
	                        break;
	                    case 1:
	                        dayClass = 'fc-mon';
	                        break;
	                    case 2: 
	                        dayClass = 'fc-tue';
	                        break;
	                    case 3:
	                        dayClass = 'fc-wed';
	                        break;
	                    case 4:
	                        dayClass = 'fc-thu';
	                        break;
	                    case 5:
	                        dayClass = 'fc-fri';
	                        break;
	                    case 6:
	                        dayClass = 'fc-sat';
	                        break;
	                }

	                $('td.' + dayClass + '.' + dateClass).addClass('fc-active-day').attr('m', calEvent.start.getMonth()).attr('d', calEvent.start.getDate()).attr('y', calEvent.start.getFullYear());
	                ChangeView('basicDay', calEvent.start);
	            } else {
                    LoadEventDetail(calEvent.id);
	            }
	        },
	        eventMouseover: function(calEvent, jsEvent, view) {
	            ShowEventOverlay(this, calEvent.id, calEvent.eventid, calEvent.recurringevent, calEvent.title, calEvent.start, calEvent.end, calEvent.noendtime);
	        },
	        eventMouseout: function() {
	            $('#calendar-event-overlay').remove();
	        },
	        dayClick: function(dayDate, allDay, jsEvent, view) {
	            // highlight the day and set it to active
	            $('#calendar').find('td.fc-active-day').removeClass('fc-active-day');
	            $(this).addClass('fc-active-day').attr('m', dayDate.getMonth()).attr('d', dayDate.getDate()).attr('y', dayDate.getFullYear());
	        },
	        dayRender: function(date, cell) {
	            cell.bind('dblclick', function() {
	                $('#calendar').fullCalendar('changeView', 'basicDay');
	                $('#calendar').fullCalendar('gotoDate', date);
	            });
	        },
	        viewDisplay: function(view) {
	            var startDate = new Date();
	            var endDate = new Date();

	            startDate = view.start;
	            endDate = view.end;

	            var view = $('#calendar').fullCalendar('getView');

	            // if month view, save month
	            if (view.name == 'month') {
	                var d = $('#calendar').fullCalendar('getDate');
                    
	                currentMonth = $.fullCalendar.formatDate(d, 'M');
	                currentYear = $.fullCalendar.formatDate(d, 'yyyy');

	                currentMonth = currentMonth - 1;

	                /*currentMonth = $('#calendar').fullCalendar('getmonth');
	                currentYear = $('#calendar').fullCalendar('getyear');*/

	                $('#calendarlist-month').val(currentMonth + 1);
	                ChangeYearDropDown(currentYear);
                    $("#calendar-pnl-title h1").text(getFullMonth(currentMonth + 1) + " " + currentYear);
	            } else if (view.name == 'basicWeek') {
                    // select day
                    if ('' == 'basicWeek') {
                        var curdate = new Date(0, 0, 0);
                        var dateClass = 'fc-day' + curdate.getDay();
                        $('td.' + dateClass).addClass('fc-active-day').attr('m', curdate.getMonth()).attr('d', curdate.getDate()).attr('y', curdate.getFullYear());
                    }

	                // first see if week falls inside same month
	                if (startDate.getMonth() == endDate.getMonth()) {
	                    $('#calendarlist-month').val(startDate.getMonth() + 1);
	                    ChangeYearDropDown(startDate.getFullYear());
                        $("#calendar-pnl-title h1").text(getFullMonth(startDate.getMonth() + 1) + " " + startDate.getFullYear());
	                } else {
	                    // see if part of original month still shows
	                    if ((startDate.getMonth() == currentMonth && startDate.getFullYear() == currentYear) || (endDate.getMonth() == currentMonth && endDate.getFullYear() == currentYear)) {
	                         $('#calendarlist-month').val(currentMonth + 1);
	                         ChangeYearDropDown(currentYear);
                             $("#calendar-pnl-title h1").text(getFullMonth(currentMonth + 1) + " " + currentYear);
	                    } else {
	                        // moved away from that month, see which month covers more area
	                        if (endDate.Date < 3) {
	                            $('#calendarlist-month').val(endDate.getMonth() + 1);
	                            ChangeYearDropDown(endDate.getFullYear());
                                $("#calendar-pnl-title h1").text(getFullMonth(endDate.getMonth() + 1) + " " + endDate.getFullYear());
	                        } else {
	                            $('#calendarlist-month').val(startDate.getMonth() + 1);
	                            ChangeYearDropDown(startDate.getFullYear());
                                $("#calendar-pnl-title h1").text(getFullMonth(startDate.getMonth() + 1) + " " + startDate.getFullYear());
	                        }
	                    }
	                }
	            } else if (view.name == 'basicDay') {
	                $('#calendarlist-month').val(startDate.getMonth() + 1);
	                ChangeYearDropDown(startDate.getFullYear());
                    $("#calendar-pnl-title h1").text(getFullMonth(startDate.getMonth() + 1) + " " + startDate.getFullYear());
	            }
	            
	           //alert('finished - dropdown says ' + $('#calendarlist-month').val() + ', calendar says ' + $('#calendar').fullCalendar('getmonth'));
	            
				if ($('#calendar-pnl-smcalendar').is(':visible')) {
				    var d = $('#calendar').fullCalendar('getDate');
                    
				    var myMonth = $.fullCalendar.formatDate(d, 'M');
				    var myYear = $.fullCalendar.formatDate(d, 'yyyy');
                    var myDay = $.fullCalendar.formatDate(d, 'd');

				    myMonth = myMonth - 1;

                    $('#small-calendar-1').datepicker('setDate', new Date(myYear, myMonth));
				}

				// set toggle button classes
				$('#calendarview-toggle a').removeClass('active');

				switch (view.name) {
					case 'month':
						$('#calendarview-toggle a.month').addClass('active');
						break;
					case 'basicWeek':
						$('#calendarview-toggle a.week').addClass('active');
						break;
					case 'basicDay':
						$('#calendarview-toggle a.day').addClass('active');
						break;
				}
	        },
            eventRender: function(event, eventElement) {
                var view = $('#calendar').fullCalendar('getView');

                eventElement.attr('tabIndex', 0);

                eventElement.css("backgroundColor", event.CategoryColor);
                eventElement.css("borderColor", event.CategoryColor);

                eventElement.keydown(function(e) {
                    e.stopImmediatePropagation();

                    if (e.keyCode == 13) {
                        LoadEventDetail(event.id);
                    }
                });

                adjustColour(eventElement);

                if (event.nativeevent == 0) {
                    eventElement.addClass('calendarevent-locked');
                } else if (event.registration.toLowerCase() == "true") {
                    eventElement.addClass('calendarevent-register');
                } else if (event.mandatory.toLowerCase() == "true") {
                    eventElement.addClass('calendarevent-mandatory');
                }

                // check for dates to display that are spanning and in current month only
                // hide all single day events that don't fall in current month
                if (view.name == "month") {
                    var d = $('#calendar').fullCalendar('getDate');
                    var currentMonth = $.fullCalendar.formatDate(d, 'M');

                    var eventStartYear = $.fullCalendar.formatDate(event.start, "yyyy");
                    var eventEndYear = $.fullCalendar.formatDate(event.end, "yyyy");

                    var eventStartMonth = $.fullCalendar.formatDate(event.start, "M");
                    var eventEndMonth = $.fullCalendar.formatDate(event.end, "M");

                    if (eventEndYear == eventStartYear) {
                        if (eventStartMonth != currentMonth) {
                            /*if (event.start.getDate() - event.end.getDate() == 0) {
                                eventElement.addClass("ui-helper-hidden");
                            }*/

                            if ((parseInt(eventStartMonth, 10) > parseInt(currentMonth, 10)) || (parseInt(eventEndMonth, 10) < parseInt(currentMonth, 10))) {
                                eventElement.addClass("ui-helper-hidden");
                            }
                        }
                    }
                }
	        }
        });

        // set up small calendars
        $('#small-calendar-1').datepicker({ 
            dayNamesMin: ['', '', '', '', '', '', ''],
            numberOfMonths: [3, 1],
            monthNames: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
            onSelect: function(dateText, inst) { 
                var myDate = new Date(dateText);

                $('#calendar').fullCalendar('changeView', 'basicDay');
                $('#calendar').fullCalendar('gotoDate', myDate);
            }
        });
        
        $(document).on('click', '#calendar-tbl-calendars .ui-datepicker-title', function() {
            var tempMonth = $.trim($(this).text()).substring(0, 3);
            var myMonth = 0;
            var myYear = $.trim($(this).text()).substring(3);

            switch (tempMonth.toLowerCase()) {
                case 'jan':
                    myMonth = 0;
                    break;
                 case 'feb':
                    myMonth = 1;
                    break;
                 case 'mar':
                    myMonth = 2;
                    break;
                 case 'apr':
                    myMonth = 3;
                    break;
                 case 'may':
                    myMonth = 4;
                    break;
                 case 'jun':
                    myMonth = 5;
                    break;
                 case 'jul':
                    myMonth = 6;
                    break;
                 case 'aug':
                    myMonth = 7;
                    break;
                 case 'sep':
                    myMonth = 8;
                    break;
                 case 'oct':
                    myMonth = 9;
                    break;
                 case 'nov':
                    myMonth = 10;
                    break;
                 case 'dec':
                    myMonth = 11;
                    break;
            }

            var myDate = new Date(parseInt(myYear), parseInt(myMonth));// parseInt necessary for safari

            $('#calendar').fullCalendar('changeView', 'month');
            $('#calendar').fullCalendar('gotoDate', myDate);
            
            if (!$('#calendarlist-pnl-specialview').hasClass('ui-helper-hidden')) {
                myMonth = myMonth + 1;
                                
                $('#calendarlist-month').val(myMonth).change();
                $('#calendarlist-year').val($.trim(myYear)).change();
                
                if (!$('#calendarlist-pnl-specialview').hasClass('ui-helper-hidden')) {
                    LoadEventListView(1798, myMonth, myYear);
                }

                if ($('#calendar-pnl-smcalendar').is(':visible')) {
			        $('#small-calendar-1').datepicker('setDate', new Date(myYear, myMonth - 1));
				}
			}
        });
        
	    $(document).on('click', '#calendar-btn-myevents', function() {
            OpenDialogOverlay("WindowLargeModal", { LoadType: "U", LoadURL: "https://mpatzem.schoolwires.net//site/UserControls/Calendar/MyEventsWrapper.aspx?ModuleInstanceID=1798" });
        });

	    $(document).on('click', '#calendar-btn-legend', function() {
            OpenDialogOverlay("WindowMediumModal", { LoadType: "U", LoadURL: "https://mpatzem.schoolwires.net//site/UserControls/Calendar/CategoryLegendWrapper.aspx?ModuleInstanceID=1798" });
        });
        
	    $(document).on('click', '#eventexport-btn-save', function() {
		   $('#calendarexport-pnl-detail').validate({ onSuccess: EventDetailExportByDateRange });
		});
       
        if ('' == 'list') {
            LoadEventListView(1798, 0, 0);
        }
        
	    $(document).on('click', '#myeventslist-btn-filter', function() {
            $('#myeventslist-pnl-toolbar').validate({ onSuccess: SearchMyEvents });
        });
        
	    $(document).on('change', '#calendarlist-month, #calendarlist-year', function() {
            $("#calendar-pnl-title h1").text(getFullMonth($('#calendarlist-month').val()) + " " + $('#calendarlist-year').val());
	    });

	    $('#calendar').fullCalendar('render');

        // remove table header for ADA compliance
        // add tabindex for table cells
	    fixADA();
   }); // end document ready
   
    function fixADA() {
        $(".ui-datepicker-calendar thead").remove();
        $(".ui-datepicker-calendar td").attr("tabindex", 0);
    }

    function ChangeYearDropDown(year) {
        var addedYear = false;
        if ( year < parseInt($('#sel-calendarlist-year li:first').attr('itemid'))) {
            $('<li itemid="' + year + '">' + year + '</li>').insertBefore("#sel-calendarlist-year li:first");
            addedYear = true;
        } else if (year > parseInt($('#sel-calendarlist-year li:last').attr('itemid'))) {
            $("#sel-calendarlist-year").append('<li itemid="' + year + '">' + year + '</li>');
            addedYear = true;
        }

        $('#calendarlist-year').val(year);

        Month = $('#calendarlist-month').val();
        Year = $('#calendarlist-year').val();

        if($('#sel-calendarlist-month li.selected').attr('itemid') != Month) {
            $('#sel-calendarlist-month li.selected').attr('class', '');
            $('#sel-calendarlist-month li[itemid="'+Month+'"]').addClass('selected');
            $('#selectedsel-calendarlist-month').children('span').text($('#sel-calendarlist-month li.selected').text());
        }

        if($('#sel-calendarlist-month li.selected').text() != $('#selectedsel-calendarlist-month').children('span').text()) {
            $('#selectedsel-calendarlist-month').children('span').text($('#sel-calendarlist-month li.selected').text());
        }

        if(addedYear) {
            attachClickEvents('sel-calendarlist-year', $('#sel-calendarlist-year').find('li'), $('#calendarlist-year'), $('#selectedsel-calendarlist-year'));

            $('#sel-calendarlist-year li[itemid="'+Year+'"]').not('.noselect').click(function(e) {
                ChangeYear();
                $('#selectedsel-calendarlist-year').children('span').text(Year);
            });
        }

        if($('#sel-calendarlist-year li.selected').attr('itemid') != Year) {
           $('#sel-calendarlist-year li.selected').attr('class', '');
           $('#sel-calendarlist-year li[itemid="'+Year+'"]').addClass('selected');
           $('#selectedsel-calendarlist-year').children('span').text($('#sel-calendarlist-year li.selected').text());
        }
    }

	function ChangeView(type, eDate) {
        if (!$('#calendarlist-pnl-specialview').hasClass('ui-helper-hidden')) {                    
            $('#calendarlist-pnl-specialview').addClass('ui-helper-hidden');
            $('#calendar').removeClass('ui-helper-hidden');
            setTimeout("$('#calendar').fullCalendar('refetchEvents');", 500);
        }

        if (eDate !== undefined) { // clicked 'more' link
            var gotoDate = new Date(eDate.getFullYear(), eDate.getMonth(), eDate.getDate());

            $('#calendar').fullCalendar('gotoDate', gotoDate);
            $('#calendar').fullCalendar('changeView', type);
        } else if ($('#calendar').find('.fc-active-day').length > 0) { // active day
			var activeDay = $('td.fc-active-day');
			var gotoDate = new Date(activeDay.attr('y'), activeDay.attr('m'), activeDay.attr('d'));

			$('#calendar').fullCalendar('gotoDate', gotoDate);
            $('#calendar').fullCalendar('changeView', type);
        } else { // go to view w/o changing
            var gotoDate = new Date($('#calendarlist-year').val(), ($('#calendarlist-month').val() - 1), 1);
            $('#calendar').fullCalendar('gotoDate', gotoDate);
            $('#calendar').fullCalendar('changeView', type);
        }

	    // remove table header for ADA compliance
	    // add tabindex for table cells
        fixADA();
    }
	
	function LoadEventDetail(eventDateID, userRegID, isEdit) {
	    (userRegID === undefined ? userRegID = 0 : '');
	    (isEdit === undefined ? isEdit = false : '');
	   
        var isListView = !$('#calendarlist-pnl-specialview').hasClass('ui-helper-hidden')
        var currentView = $('#calendar').fullCalendar('getView');

        if (!$('#calendarlist-pnl-specialview').hasClass('ui-helper-hidden')) {
            currentView = 'list';
        } else {
            currentView = currentView.name;
        }

        /*var currentDate = $('#calendar').fullCalendar('getdate');
        var currentMonth = $('#calendar').fullCalendar('getmonth');
        var currentYear = $('#calendar').fullCalendar('getyear');*/

        var currentDate = $('#calendar').fullCalendar('getDate');
        var currentMonth = $.fullCalendar.formatDate(currentDate, 'M');
        var currentYear = $.fullCalendar.formatDate(currentDate, 'yyyy');

        currentMonth = currentMonth - 1;

        parent.location.href = "https://mpatzem.schoolwires.net//site/Default.aspx?PageID=273&PageType=17&DomainID=1184&ModuleInstanceID=1798&EventDateID=" + eventDateID + "&CurrentView=" + currentView; //"&UserRegID=" + userRegID + "&IsEdit=" + isEdit;
    }
	
	function ShowEventOverlay(obj, eventDateID, eventID, recurring, title, start, end, noEndTime) {
        $this = $(obj);
        $('#calendar-event-overlay').remove();

        // get date for display
		var startDate = (start.getMonth() + 1) + '/' + start.getDate() + '/' + start.getFullYear();
		var endDate = "";
	    //if (end !== null) {
		    endDate = (end.getMonth() + 1) + '/' + end.getDate() + '/' + end.getFullYear();
        //} else endDate = startDate;
		
		var strDate = "";
		var allDay = false;
		//var noEndTime = false;
		
		// get times
		var startHour = start.getHours().toString();
		var startMinute = start.getMinutes().toString();

		var startTime = (startHour < 12 ? (startHour.length == 2 ? startHour : (startHour == "0" ? "12" : "0" + startHour)) + ":" + (startMinute.length == 2 ? startMinute : "0" + startMinute) + " AM" : ((startHour - 12).toString().length == 2 ? startHour - 12 : (startHour - 12 == 0 ? "12" : "0" + (startHour - 12))) + ":" + (startMinute.length == 2 ? startMinute : "0" + startMinute) + " PM");
		
		var endHour = "";
		var endMinute = "";

	    if (end !== null) {
		    endHour = end.getHours().toString();
		    endMinute = end.getMinutes().toString();
	    } else {
		    endHour = "00";
		    endMinute = "00";
	    }

        var endTime = (endHour < 12 ? (endHour.length == 2 ? endHour : "0" + endHour) + ":" + (endMinute.length == 2 ? endMinute : "0" + endMinute) + " AM" : ((endHour - 12).toString().length == 2 ? endHour - 12 : (endHour - 12 == 0 ? "12" : "0" + (endHour - 12))) + ":" + (endMinute.length == 2 ? endMinute : "0" + endMinute) + " PM");
		
        // check if all day
		if (startTime == "12:00 AM" && endTime == "11:59 PM") {
			allDay = true;
		}
		
		// check no end time for migrated data
	    //if (endTime == "00:00 am") {
		//    noEndTime = true;
	    //}
	
		// set start date
		strDate += startDate;

		// add time if not all day
		if (allDay == false) {
			strDate += ' ' + startTime;
		}

		// if not same day, add end date
		if (startDate != endDate) {
			strDate += ' to ' + endDate;
		}

		// add end time if not all day
		if (allDay == false && noEndTime == false) {
			if (startDate != endDate) {
				strDate += ' ' + endTime;
			} else {
				strDate += ' to  ' + endTime;
			}
		}

		// check title length and add ellipsis if necessary
		if (title.length > 70) {
		    title = title.substring(0, 69) + '...';
		}

	    $('body').append('<div id="calendar-event-overlay"><h1>' + title.replace(/&/g, '&amp').replace(/\"/g, '&quot;').replace(/</g, '&lt;').replace(/>/g, '&gt;')  + "</h1><h2> " + strDate + '</h2><div class="arrow"></div></div>');
        
        $(document).on('click', function() {
            $('#calendar-event-overlay').remove();
        });

        $('#calendar-event-overlay').css({
            'left': ($this.offset().left - ($('#calendar-event-overlay').outerWidth() / 2) + 
                ($this.outerWidth() / 2)) + 'px',
            'top': ($this.offset().top - $('#calendar-event-overlay').outerHeight() - 18) + 'px'
        }).bind('mouseleave', function() {
            $(this).remove();
        });
	}

    function EditMyEventRegistration(eventDateID, userRegID) {
        //LoadEventRegistrationEdit(userRegID);
        LoadEventDetail(eventDateID, userRegID, true)
    }
    
    function RemoveMyEventRegistration(userRegID,EventDateID) {
        //var data = "{userRegID:" + userRegID + ", EventDateID:" + EventDateID + "}";
        swalert('Are you sure you want to remove this registration?','Remove Registration','critical','yesno',YesRemoveMyEventRegistration,'','',{userRegID:userRegID,EventDateID:EventDateID});
    }
    
    function YesRemoveMyEventRegistration(userData) {
        var data = "{UserRegistrationID: " + userData.userRegID  + ", EventDateID: " + userData.EventDateID + "}";
        var StartDate;
        var EndDate;

        if ($('#myeventslist-txt-startdate').val() == '') {
            StartDate = '1/1/1900';
        } else {
            StartDate = $('#myeventslist-txt-startdate').val();
        }

        if ($('#myeventslist-txt-enddate').val() == '') {
            EndDate = '1/1/1900';
        } else {
            EndDate = $('#myeventslist-txt-enddate').val();
        }       
              
        if (StartDate != '1/1/1900') {
            StartDate += ' 0:00:00';
        }

        if (EndDate != '1/1/1900') {
            EndDate += ' 23:59:59';
        }

        var success = 'GetContent("https://mpatzem.schoolwires.net//site/UserControls/Calendar/MyEventsListWrapper.aspx?ModuleInstanceID=1798&StartDate=' + StartDate + '&EndDate=' + EndDate + '", "myeventslist-pnl-detail", 2, ""); ShowNotification("Success! You have deleted your registration for this event.");';
        var failure = 'CallControllerFailure(result[0].errormessage);';
        
        CallController("https://mpatzem.schoolwires.net//site/UserControls/Calendar/CalendarController.aspx/DeleteRegistration", data, success, failure);
    } 
       
    function LoadEventListView(ModuleInstanceID, Month, Year) {
		$('#calendarview-toggle a').removeClass('active');
		$('#calendarview-toggle a.list').addClass('active');

        var itemid = $('#calendarlist-month').val();
        var gotoDate = new Date($('#calendarlist-year').val(), (itemid - 1), 1);
        $('#calendar-pnl-title h1').text(gotoDate.toLocaleString("en-us", { month: "long" }) + ' ' + $('#calendarlist-year').val());

        $('#calendarlist-pnl-specialview').removeClass('ui-helper-hidden');
        $('#calendar').addClass('ui-helper-hidden');

        Month = $('#calendarlist-month').val();
        Year = $('#calendarlist-year').val();

        GetContent("https://mpatzem.schoolwires.net//site/UserControls/Calendar/EventListViewWrapper.aspx?ModuleInstanceID=" + ModuleInstanceID + "&Month=" + Month + "&Year=" + Year, "calendarlist-pnl-specialview", 2, '');
    }

    function ChangeMonth() { 
        var itemid = $('#calendarlist-month').val();
        $('#calendar').find('td.fc-active-day').removeClass('fc-active-day');
        var gotoDate = new Date($('#calendarlist-year').val(), (itemid - 1), 1);
        $('#calendar').fullCalendar('gotoDate', gotoDate);

        if (!$('#calendarlist-pnl-specialview').hasClass('ui-helper-hidden')) {
            LoadEventListView(1798, $('#calendar').fullCalendar('getmonth'), $('#calendar').fullCalendar('getyear'));
        }
        
        if ($('#calendar-pnl-smcalendar').is(':visible')) {
	        $('#small-calendar-1').datepicker('setDate', new Date($('#calendarlist-year').val(), parseInt($('#calendarlist-month').val()-1)));
        }    
        
        ChangeYearDropDown($('#calendarlist-year').val());

        // remove table header for ADA compliance
        // add tabindex for table cells
        fixADA();
    }

    function ChangeYear(itemid) { 
		var itemid = $('#calendarlist-year').val();
        $('#calendar').find('td.fc-active-day').removeClass('fc-active-day');
        var gotoDate = new Date(itemid, ($('#calendarlist-month').val() - 1), 1);
        $('#calendar').fullCalendar('gotoDate', gotoDate);

        if (!$('#calendarlist-pnl-specialview').hasClass('ui-helper-hidden')) {
            LoadEventListView(1798, $('#calendar').fullCalendar('getmonth'), $('#calendar').fullCalendar('getyear'));
        }
        
        if ($('#calendar-pnl-smcalendar').is(':visible')) {
		    $('#small-calendar-1').datepicker('setDate', new Date($('#calendarlist-year').val(), parseInt($('#calendarlist-month').val()-1)));
        }

        ChangeYearDropDown($('#calendarlist-year').val());
        // remove table header for ADA compliance
        // add tabindex for table cells
        fixADA();
    }

    function GoToNext() {
        $('#calendar').find('td.fc-active-day').removeClass('fc-active-day');
        $('#calendar').fullCalendar('next');

        if (!$('#calendarlist-pnl-specialview').hasClass('ui-helper-hidden')) {
            var month = parseInt($('#calendarlist-month').val()) + 1;
            var year = parseInt($('#calendarlist-year').val());

            if (month > 12) {
                month = 1;
                year = year + 1;
            }

            $('#calendarlist-month').val(month).change();
            ChangeYearDropDown(year);
            $('#calendarlist-year').val(year).change();
            
            if (!$('#calendarlist-pnl-specialview').hasClass('ui-helper-hidden')) {
                LoadEventListView(1798, $('#calendar').fullCalendar('getmonth'), $('#calendar').fullCalendar('getyear'));
            }

            if ($('#calendar-pnl-smcalendar').is(':visible')) {
				$('#small-calendar-1').datepicker('setDate', new Date($('#calendarlist-year').val(), parseInt($('#calendarlist-month').val())-1));
			}
        }

        // remove table header for ADA compliance
        // add tabindex for table cells
        fixADA();
    }
    
    function GoToPrevious() {
        $('#calendar').find('td.fc-active-day').removeClass('fc-active-day');
        $('#calendar').fullCalendar('prev');

        if (!$('#calendarlist-pnl-specialview').hasClass('ui-helper-hidden')) {
            var month = $('#calendarlist-month').val() - 1;
            var year = $('#calendarlist-year').val();

            if (month < 1) {
                month = 12;
                year = year - 1;
            }

            $('#calendarlist-month').val(month).change();
            ChangeYearDropDown(year);
            $('#calendarlist-year').val(year).change();
            
            if (!$('#calendarlist-pnl-specialview').hasClass('ui-helper-hidden')) {
                LoadEventListView(1798, $('#calendar').fullCalendar('getmonth'), $('#calendar').fullCalendar('getyear'));
            }

            if ($('#calendar-pnl-smcalendar').is(':visible')) {
				$('#small-calendar-1').datepicker('setDate', new Date($('#calendarlist-year').val(), parseInt($('#calendarlist-month').val())-1));
			}
        }

        // remove table header for ADA compliance
        // add tabindex for table cells
        fixADA();
    }
    
    function GoToToday() {
        $('#calendar').find('td.fc-active-day').removeClass('fc-active-day');
        $('#calendar').fullCalendar('today');
        if (!$('#calendarlist-pnl-specialview').hasClass('ui-helper-hidden')) {
            LoadEventListView(1798, $('#calendar').fullCalendar('getmonth'), $('#calendar').fullCalendar('getyear'));
        }

        // remove table header for ADA compliance
        // add tabindex for table cells
        fixADA();
    }

    function LoadEventPrint() {
        var view = $('#calendar').fullCalendar('getView');
        view = view.name;

		/*var month = $('#calendar').fullCalendar('getmonth');
		var date = $('#calendar').fullCalendar('getdate');
		var year = $('#calendar').fullCalendar('getyear');*/

        var date = $('#calendar').fullCalendar('getDate');
        var month = $.fullCalendar.formatDate(date, 'M');
        var day = $.fullCalendar.formatDate(date, 'd');
        var year = $.fullCalendar.formatDate(date, 'yyyy');

        month = month - 1;

		if (!$('#calendarlist-pnl-specialview').hasClass('ui-helper-hidden')) {
			view = "list";
			month = $('#calendarlist-month').val() - 1;
            year = $('#calendarlist-year').val();
		} 
		
		OpenWindow('https://mpatzem.schoolwires.net//site/UserControls/Calendar/CalendarPrint.aspx?ModuleInstanceID=1798&PageID=273&DomainID=1184&Date=' + day + '&Month=' + month + '&Year=' + year + '&View=' + view, 'PrintCalendar', 'width=' + (screen.width - 100) + ',height=' + (screen.height - 100) + ',menubar=yes,scrollbars=yes,top=0,left=0,resizable=yes');
    }
	
    function ToggleEventListDetail() {
        if ($('#eventlist-chk-toggledetail').is(':checked')) {
            $('.ui-eventlistview-detail').removeClass('ui-helper-hidden');
        } else {
            $('.ui-eventlistview-detail').addClass('ui-helper-hidden');
        }
    }

    function LoadEventExportByDateRange() {
        OpenDialogOverlay("WindowMediumModal", { LoadType: "U", LoadURL: "https://mpatzem.schoolwires.net//site/UserControls/Calendar/EventExportByDateRangeWrapper.aspx" });
    }

    function EventDetailExportByDateRange() {
        var eventDate = new Date($('#eventexport-txt-startdate').val());
        var endDate = new Date($('#eventexport-txt-enddate').val());
                
        if (endDate < eventDate) {
            swalert('The start date must come before the end date.', 'Attention', 'warning', 'ok');
            return false;
        }

        document.location.href = "https://mpatzem.schoolwires.net//site/UserControls/Calendar/EventExport.aspx?ModuleInstanceID=1798&StartDate=" + $('#eventexport-txt-startdate').val() + "&EndDate=" + $('#eventexport-txt-enddate').val();
        CloseDialogOverlay('WindowMediumModal');
    }

    function getFullMonth(monthValue) {
        monthValue = parseInt(monthValue);
        var fullMonth = "";

        switch(monthValue) {
            case 1:
                fullMonth = "January";
                break;
            case 2:
                fullMonth = "February";
                break;
            case 3:
                fullMonth = "March";
                break;
            case 4:
                fullMonth = "April";
                break;
            case 5:
                fullMonth = "May";
                break;
            case 6:
                fullMonth = "June";
                break;
            case 7:
                fullMonth = "July";
                break;
            case 8:
                fullMonth = "August";
                break;
            case 9:
                fullMonth = "September";
                break;
            case 10:
                fullMonth = "October";
                break;
            case 11:
                fullMonth = "November";
                break;
            case 12:
                fullMonth = "December";
                break;
        }

        return fullMonth;
    }
    
    function SearchMyEvents() {
        var StartDate;
        var EndDate;

        if ($('#myeventslist-txt-startdate').val() == '') {
            StartDate = '1/1/1900';
        } else {
            StartDate = $('#myeventslist-txt-startdate').val();
        }

        if ($('#myeventslist-txt-enddate').val() == '') {
            EndDate = '1/1/1900';
        } else {
            EndDate = $('#myeventslist-txt-enddate').val();
        }
        
        if ((StartDate != '1/1/1900') && (EndDate != '1/1/1900')) {
            if (new Date(StartDate) > new Date(EndDate)) {
                swalert('The start date must come before the end date.', 'Attention', 'warning', 'ok', '', '', '');
                return false;
            }
        } else if ((EndDate != '1/1/1900') && (StartDate == '1/1/1900')) {
            var CurrentDate = new Date();

            StartDate = (parseInt(CurrentDate.getMonth()) + 1) + "/" + CurrentDate.getDate() + "/" + CurrentDate.getFullYear();

            if (new Date(StartDate) > new Date(EndDate)) {
                swalert('The end date must come after the current date.', 'Attention', 'warning', 'ok', '', '', '');
                return false;
            }
        }
              
        if (StartDate != '1/1/1900') {
            StartDate += ' 0:00:00';
        }

        if (EndDate != '1/1/1900') {
            EndDate += ' 23:59:59';
        }

        GetContent("https://mpatzem.schoolwires.net//site/UserControls/Calendar/MyEventsListWrapper.aspx?ModuleInstanceID=1798&StartDate=" + StartDate + "&EndDate=" + EndDate, "myeventslist-pnl-detail", 2, '');
    }

    function RefreshCalendarFilter() {
        if ($('#calendar-pnl-calendarfilter-closed').is(':visible')) {
            GetContent("https://mpatzem.schoolwires.net//site/UserControls/Calendar/CalendarFilterListWrapper.aspx?ModuleInstanceID=" + "1798" + "&Expanded=" + false, "calendar-pnl-calendarfilter-closed", 2);
        } else {
            GetContent("https://mpatzem.schoolwires.net//site/UserControls/Calendar/CalendarFilterListWrapper.aspx?ModuleInstanceID=" + "1798" + "&Expanded=" + true, "calendar-pnl-calendarfilter-open", 2);             
        }
    }
    
    function ToggleCalendarFilter(expanded) {
        if (expanded === undefined) {
            expanded = $('#calendar-pnl-calendarfilter-closed').is(':visible');
        }

        if (expanded === true) {
            var callback = "$('#calendar-pnl-calendarfilter-closed').fadeOut('fast',function() { $('#calendar-pnl-calendarfilter-open').fadeIn('fast');});";
            GetContent("https://mpatzem.schoolwires.net//site/UserControls/Calendar/CalendarFilterListWrapper.aspx?ModuleInstanceID=" + "1798" + "&Expanded=" + true, "calendar-pnl-calendarfilter-open", 3, callback);
        } else {
            var callback = "$('#calendar-pnl-calendarfilter-open').fadeOut('fast',function() { $('#calendar-pnl-calendarfilter-closed').fadeIn('fast');});";
            GetContent("https://mpatzem.schoolwires.net//site/UserControls/Calendar/CalendarFilterListWrapper.aspx?ModuleInstanceID=" + "1798" + "&Expanded=" + false, "calendar-pnl-calendarfilter-closed", 3, callback); 
        }
    }

    function OpenCalendarFilter() {
        OpenDialogOverlay("WindowLargeModal", { LoadType: "U", LoadURL: "https://mpatzem.schoolwires.net//site/UserControls/Calendar/CalendarFilterNewWrapper.aspx?DomainID=4&ModuleInstanceID=1798" });
    }

    function RefreshCategoryFilter() {
        if ($('#calendar-pnl-categoryfilter-closed').is(':visible')) {
            GetContent("https://mpatzem.schoolwires.net//site/UserControls/Calendar/CategoryFilterListWrapper.aspx?ModuleInstanceID=" + "1798" + "&Expanded=" + false, "calendar-pnl-categoryfilter-closed", 2);
        } else {
            GetContent("https://mpatzem.schoolwires.net//site/UserControls/Calendar/CategoryFilterListWrapper.aspx?ModuleInstanceID=" + "1798" + "&Expanded=" + true, "calendar-pnl-categoryfilter-open", 2);             
        }
    }

    function ToggleCategoryFilter(expanded) {
        if (expanded === undefined) {
            expanded = $('#calendar-pnl-categoryfilter-closed').is(':visible');
        }

        if (expanded === true) {
            var callback = "$('#calendar-pnl-categoryfilter-closed').fadeOut('fast',function() { $('#calendar-pnl-categoryfilter-open').fadeIn('fast');});";
            GetContent("https://mpatzem.schoolwires.net//site/UserControls/Calendar/CategoryFilterListWrapper.aspx?ModuleInstanceID=" + "1798" + "&Expanded=" + true, "calendar-pnl-categoryfilter-open", 3, callback);
        } else {
             var callback = "$('#calendar-pnl-categoryfilter-open').fadeOut('fast',function() { $('#calendar-pnl-categoryfilter-closed').fadeIn('fast');});";
             GetContent("https://mpatzem.schoolwires.net//site/UserControls/Calendar/CategoryFilterListWrapper.aspx?ModuleInstanceID=" + "1798" + "&Expanded=" + false, "calendar-pnl-categoryfilter-closed", 3, callback); 
         }
     }

    function OpenCategoryFilter() {
        OpenDialogOverlay("WindowLargeModal", { LoadType: "U", LoadURL: "https://mpatzem.schoolwires.net//site/UserControls/Calendar/CategoryFilterNewWrapper.aspx?DomainID=4&ModuleInstanceID=1798" });
    }

    function RemoveCalendarFilter(ModuleInstanceID) {
        $('#calendarfilter-ul-list li[itemid=' + ModuleInstanceID + ']').remove();

        var strCalendarFilter = GetCookie('CalendarFilter');
        strCalendarFilter = strCalendarFilter.replace(ModuleInstanceID + '-', '');

        SetCookie('CalendarFilter', strCalendarFilter, 365);

        if (!$('#calendarlist-pnl-specialview').hasClass('ui-helper-hidden')) {
            LoadEventListView(1798, $('#calendar').fullCalendar('getmonth'), $('#calendar').fullCalendar('getyear'));
        } else {
            $('#calendar').fullCalendar('refetchEvents');
        }
    }

    function AddCategoryFilter(EventCategoryMasterID) {
        $('#categoryfilter-ul-list li[itemid=' + EventCategoryMasterID + ']').remove();

        var strCategoryFilter = GetCookie('CategoryFilter');
        strCategoryFilter = strCategoryFilter + EventCategoryMasterID + '-';

        SetCookie('CategoryFilter', strCategoryFilter, 365);

        if (!$('#calendarlist-pnl-specialview').hasClass('ui-helper-hidden')) {
            LoadEventListView(1798, $('#calendar').fullCalendar('getmonth'), $('#calendar').fullCalendar('getyear'));
        } else {
            $('#calendar').fullCalendar('refetchEvents');
        }
    }

    function UpdateCalendarFilter(id) {
        var strCalendarFilter = GetCookie('CalendarFilter');

        if ($('#calendarfilter-chk-calendar' + id).is(':checked')) {
            strCalendarFilter = strCalendarFilter + id + '-';
        } else {
            strCalendarFilter = strCalendarFilter.replace(id + '-', '');
        }

        SetCookie('CalendarFilter', strCalendarFilter, 365);
    }  

    function LoadCalendarChannelList() {
        GetContent("https://mpatzem.schoolwires.net//site/UserControls/Calendar/CalendarChannelListWrapper.aspx?DomainID=" + $('#calendarfilter-hid-siteid').val(), "calendarfilter-pnl-channellist", 2);
        LoadCalendarSearchList();
    }  

    function SelectFilteredCalendars() {
        var arrValues = GetCookie('CalendarFilter').split('-');
        
        $.each(arrValues, function() {
            $('#calendarfilter-chk-calendar' + this).attr('checked', true);
        });
    }   

    function LoadCalendarSearchList() {
        var DomainID = $('#calendarfilter-hid-channelid').val();

        if (DomainID == 0) {
            DomainID = $('#calendarfilter-hid-siteid').val();
        }

        GetContent("https://mpatzem.schoolwires.net//site/UserControls/Calendar/CalendarSearchListWrapper.aspx?DomainID=" + DomainID + "&ModuleInstanceID=" + "1798" + "&SearchFor=" + $('#calendarfilternew-txt-search').val(), "calendarfilter-pnl-results", 2, '');
    } 

    function CloseCalendarFilter() {
        CloseDialogOverlay('WindowLargeModal');

        if (!$('#calendarlist-pnl-specialview').hasClass('ui-helper-hidden')) {
            LoadEventListView(1798, $('#calendar').fullCalendar('getmonth'), $('#calendar').fullCalendar('getyear'));
        } else {
            $('#calendar').fullCalendar('refetchEvents');
        }

        RefreshCalendarFilter();
    }

    function CloseCategoryFilter() {
        CloseDialogOverlay('WindowLargeModal');

        if (!$('#calendarlist-pnl-specialview').hasClass('ui-helper-hidden')) {
            LoadEventListView(1798, $('#calendar').fullCalendar('getmonth'), $('#calendar').fullCalendar('getyear'));
        } else {
            $('#calendar').fullCalendar('refetchEvents');
        }

        RefreshCategoryFilter();
    }

    function SelectFilteredCategories() {
        var arrValues = GetCookie('CategoryFilter').split('-');

        $.each(arrValues, function() {
            $('#categoryfilter-chk-category' + this).attr('checked', false);
        });
    }

    function UpdateCategoryFilter(id) {
        var strCategoryFilter = GetCookie('CategoryFilter');

        if ($('#categoryfilter-chk-category' + id).is(':checked')) {
            strCategoryFilter = strCategoryFilter.replace(id + '-', '');
        } else {
            strCategoryFilter = strCategoryFilter + id + '-';
        }

        SetCookie('CategoryFilter', strCategoryFilter, 365);
    }

    function toggleFilters() {
        if ($("#calendar-filter-wrapper").hasClass("open")) {
            $("#calendar-filter-wrapper").removeClass("open").addClass("closed");
            SlideUpControl('calendar-filter-wrapper');
            $("#calendar-pnl-filter-toggle-chevron").attr("src", "https://mpatzem.schoolwires.net/Static//GlobalAssets/Images/Icons/16/toggle-closed.png");
            $("#calendar-pnl-filter-toggle-chevron").attr("alt","Open Customized Calendar View");
            $("#calendar-pnl-filter-toggle-chevron").attr("aria-expanded", "false");
        } else {
            $("#calendar-filter-wrapper").removeClass("closed").addClass("open");
            SlideDownControl('calendar-filter-wrapper');
            $("#calendar-pnl-filter-toggle-chevron").attr("src", "https://mpatzem.schoolwires.net/Static//GlobalAssets/Images/Icons/16/toggle-open.png");
            $("#calendar-pnl-filter-toggle-chevron").attr("alt", "Close Customized Calendar View");
            $("#calendar-pnl-filter-toggle-chevron").attr("aria-expanded", "true");
        }
    }

    function UpdateCalendarFollow() {
        var data = "{ModuleInstanceID: 1798}";
        var success = 'ShowNotification("Success! You are now following this calendar in MyView.");';
        //var failure = 'CallControllerFailure(result[0].errormessage);';
        var failure = 'ShowNotification(result[0].errormessage);';
        
        CallController("https://mpatzem.schoolwires.net//site/UserControls/Calendar/CalendarController.aspx/UpdateCalendarFollow", data, success, failure);
    }
    } else {
        function showHideComments(view) {
            $("#calendar-pnl-calendarcontainer-1798 .app-level-social-commenting")[view === 'event' ? 'hide' : 'show']();
            $("#calendar-pnl-calendarcontainer-1798 .app-level-social-rating")[view === 'event' ? 'hide' : 'show']();
        }

        $("#calendar-pnl-calendarcontainer-1798 .app-level-social-commenting, #calendar-pnl-calendarcontainer-1798 .app-level-social-rating").hide();

        Bb.WCM.UI.Calendar('1798', { attachTo: document.getElementById('calendar-newplaceholder-1798'), 
            name: '', customized: true, pageID: '273', domainID: '1184', isUserLogged: false, showRssBtn: false})
        .then (
            function (cal) {
                showHideComments(cal.view);
                cal.on("viewChange", function (view) {
                    showHideComments(view);
                });
                cal.on("interaction", function (category, action, label) {
                    if (!window.AddAnalyticsEvent) {
                        var loadAttempts = 0;
                        var checker = setInterval( function () {
                            loadAttempts++;
                            if (window.AddAnalyticsEvent) {
                                AddAnalyticsEvent(category, action, label);
                                clearInterval(checker);
                            }
                            if (loadAttempts >= 10) {
                                clearInterval(checker);
                            }
                        }, 300);
                    } else {
                        AddAnalyticsEvent(category, action, label);
                    }
                });
            }
        );
    }
</script>
</div>
</div>
</div><div id="sw-content-container4" class="ui-column-one region"></div><div class="clear"></div></div></div><!-- End Centricity Content Area --></div><!-- BEGIN - STANDARD FOOTER -->
<div id='sw-footer-outer'>
<div id='sw-footer-inner'>
<div id='sw-footer-left'></div>
<div id='sw-footer-right'>
<div id='sw-footer-links'>
<ul>
<li><a title='Click to email the primary contact' href='mailto:noreply@schoolwires.com'>Questions or Feedback?</a> | </li>
<li><a href='https://www.blackboard.com/blackboard-web-community-manager-privacy-statement' target="_blank">Blackboard Web Community Manager Privacy Policy (Updated)</a> | </li>
<li><a href='https://help.blackboard.com/Terms_of_Use' target="_blank">Terms of Use</a></li>
</ul>
</div>
<div id='sw-footer-copyright'>Copyright &copy; 2002-2023 Blackboard, Inc. All rights reserved.</div>
<div id='sw-footer-logo'><a href='http://www.blackboard.com' title="Blackboard, Inc. All rights reserved.">
<img src='https://mpatzem.schoolwires.net/Static//GlobalAssets/Images/Navbar/blackboard_logo.png'
 alt="Blackboard, Inc. All rights reserved."/>
</a></div>
</div>
</div>
</div>
<!-- END - STANDARD FOOTER -->
<script type="text/javascript">
   $(document).ready(function(){
      var beaconURL='https://analytics.schoolwires.com/analytics.asmx/Insert?AccountNumber=6boZxfPfyUa2SJk0fiN5BA%3d%3d&SessionID=8d81054d-c2d7-477a-98c1-1fd74da987fc&SiteID=4&ChannelID=1173&SectionID=1184&PageID=273&HitDate=4%2f5%2f2023+3%3a14%3a26+PM&Browser=Chrome+87.0&OS=Unknown&IPAddress=10.61.92.252';
      try {
         $.getJSON(beaconURL + '&jsonp=?', function(myData) {});
      } catch(err) { 
         // prevent site error for analytics
      }
   });
</script>

    <input type="hidden" id="hid-pageid" value="273" />

    

    <div id='dialog-overlay-WindowMedium-base' class='ui-dialog-overlay-base' ><div id='WindowMedium' role='dialog' tabindex='-1'  class='ui-dialog-overlay medium' ><div class='ui-dialog-overlay-title-bar' id='dialog-overlay-WindowMedium-title-bar' ></div><div class='ui-dialog-overlay-body' id='dialog-overlay-WindowMedium-body' ></div><button class='ui-dialog-overlay-close' title='Close' aria-label='Close' onclick='CloseDialogOverlay("WindowMedium");' ></button><div class='ui-dialog-overlay-footer' id='dialog-overlay-WindowMedium-footer' ></div></div></div>

<script type="text/javascript">
    $(document).ready(function() {
        $('#dialog-overlay-WindowMedium-base').appendTo('body');
        
        $('body').on('keydown', '.ui-dialog-overlay-base-modal, .ui-dialog-overlay, .ui-sw-alert', function (e) {
            var swAlertOpen = $(".ui-sw-alert").length;
            if (swAlertOpen > 1) {
                if (e.keyCode == 27) {//escape key
                    e.stopImmediatePropagation();
                    e.preventDefault();
                    //get id of open alert
                    var alertboxid = $('.ui-sw-alert').attr('id');
                    //click ok or no
                    if ($('#' + alertboxid + 'ok').length > 0) {
                        $('#' + alertboxid + 'ok').click();
                    } else {
                        $('#' + alertboxid + 'no').click();
                    }
                }
            } else {
                if (e.keyCode == 27) {//escape key
                    e.stopImmediatePropagation();
                    e.preventDefault();
                    // Check if ESC was pressed on DatePicker
                    var raisedByDatepicker = e.target.classList.contains("datepicker");
                    if (!raisedByDatepicker) {
                        e.stopImmediatePropagation();
                        e.preventDefault();
                        // Close Dialog
                        $('.ui-dialog-overlay-close.modal:visible').last().click();
                    } else {
                        // Remove focus
                        e.target.blur();
                        e.target.classList.remove("focus");
                    }
                }
            }
        });
 		
        

    });
</script>
    <div id='dialog-overlay-WindowSmall-base' class='ui-dialog-overlay-base' ><div id='WindowSmall' role='dialog' tabindex='-1'  class='ui-dialog-overlay small' ><div class='ui-dialog-overlay-title-bar' id='dialog-overlay-WindowSmall-title-bar' ></div><div class='ui-dialog-overlay-body' id='dialog-overlay-WindowSmall-body' ></div><button class='ui-dialog-overlay-close' title='Close' aria-label='Close' onclick='CloseDialogOverlay("WindowSmall");' ></button><div class='ui-dialog-overlay-footer' id='dialog-overlay-WindowSmall-footer' ></div></div></div>

<script type="text/javascript">
    $(document).ready(function() {
        $('#dialog-overlay-WindowSmall-base').appendTo('body');
        
        $('body').on('keydown', '.ui-dialog-overlay-base-modal, .ui-dialog-overlay, .ui-sw-alert', function (e) {
            var swAlertOpen = $(".ui-sw-alert").length;
            if (swAlertOpen > 1) {
                if (e.keyCode == 27) {//escape key
                    e.stopImmediatePropagation();
                    e.preventDefault();
                    //get id of open alert
                    var alertboxid = $('.ui-sw-alert').attr('id');
                    //click ok or no
                    if ($('#' + alertboxid + 'ok').length > 0) {
                        $('#' + alertboxid + 'ok').click();
                    } else {
                        $('#' + alertboxid + 'no').click();
                    }
                }
            } else {
                if (e.keyCode == 27) {//escape key
                    e.stopImmediatePropagation();
                    e.preventDefault();
                    // Check if ESC was pressed on DatePicker
                    var raisedByDatepicker = e.target.classList.contains("datepicker");
                    if (!raisedByDatepicker) {
                        e.stopImmediatePropagation();
                        e.preventDefault();
                        // Close Dialog
                        $('.ui-dialog-overlay-close.modal:visible').last().click();
                    } else {
                        // Remove focus
                        e.target.blur();
                        e.target.classList.remove("focus");
                    }
                }
            }
        });
 		
        

    });
</script>
    <div id='dialog-overlay-WindowLarge-base' class='ui-dialog-overlay-base' ><div id='WindowLarge' role='dialog' tabindex='-1'  class='ui-dialog-overlay large' ><div class='ui-dialog-overlay-title-bar' id='dialog-overlay-WindowLarge-title-bar' ></div><div class='ui-dialog-overlay-body' id='dialog-overlay-WindowLarge-body' ></div><button class='ui-dialog-overlay-close' title='Close' aria-label='Close' onclick='CloseDialogOverlay("WindowLarge");' ></button><div class='ui-dialog-overlay-footer' id='dialog-overlay-WindowLarge-footer' ></div></div></div>

<script type="text/javascript">
    $(document).ready(function() {
        $('#dialog-overlay-WindowLarge-base').appendTo('body');
        
        $('body').on('keydown', '.ui-dialog-overlay-base-modal, .ui-dialog-overlay, .ui-sw-alert', function (e) {
            var swAlertOpen = $(".ui-sw-alert").length;
            if (swAlertOpen > 1) {
                if (e.keyCode == 27) {//escape key
                    e.stopImmediatePropagation();
                    e.preventDefault();
                    //get id of open alert
                    var alertboxid = $('.ui-sw-alert').attr('id');
                    //click ok or no
                    if ($('#' + alertboxid + 'ok').length > 0) {
                        $('#' + alertboxid + 'ok').click();
                    } else {
                        $('#' + alertboxid + 'no').click();
                    }
                }
            } else {
                if (e.keyCode == 27) {//escape key
                    e.stopImmediatePropagation();
                    e.preventDefault();
                    // Check if ESC was pressed on DatePicker
                    var raisedByDatepicker = e.target.classList.contains("datepicker");
                    if (!raisedByDatepicker) {
                        e.stopImmediatePropagation();
                        e.preventDefault();
                        // Close Dialog
                        $('.ui-dialog-overlay-close.modal:visible').last().click();
                    } else {
                        // Remove focus
                        e.target.blur();
                        e.target.classList.remove("focus");
                    }
                }
            }
        });
 		
        

    });
</script>

    <div id='dialog-overlay-WindowMediumModal-base' class='ui-dialog-overlay-base-modal' ><div id='WindowMediumModal' role='dialog' tabindex='-1'  class='ui-dialog-overlay medium' ><div class='ui-dialog-overlay-title-bar' id='dialog-overlay-WindowMediumModal-title-bar' ></div><div class='ui-dialog-overlay-body' id='dialog-overlay-WindowMediumModal-body' ></div><button class='ui-dialog-overlay-close modal' title='Close' aria-label='Close' onclick='CloseDialogOverlay("WindowMediumModal");' ></button><div class='ui-dialog-overlay-footer' id='dialog-overlay-WindowMediumModal-footer' ></div></div></div>

<script type="text/javascript">
    $(document).ready(function() {
        $('#dialog-overlay-WindowMediumModal-base').appendTo('body');
        
        $('body').on('keydown', '.ui-dialog-overlay-base-modal, .ui-dialog-overlay, .ui-sw-alert', function (e) {
            var swAlertOpen = $(".ui-sw-alert").length;
            if (swAlertOpen > 1) {
                if (e.keyCode == 27) {//escape key
                    e.stopImmediatePropagation();
                    e.preventDefault();
                    //get id of open alert
                    var alertboxid = $('.ui-sw-alert').attr('id');
                    //click ok or no
                    if ($('#' + alertboxid + 'ok').length > 0) {
                        $('#' + alertboxid + 'ok').click();
                    } else {
                        $('#' + alertboxid + 'no').click();
                    }
                }
            } else {
                if (e.keyCode == 27) {//escape key
                    e.stopImmediatePropagation();
                    e.preventDefault();
                    // Check if ESC was pressed on DatePicker
                    var raisedByDatepicker = e.target.classList.contains("datepicker");
                    if (!raisedByDatepicker) {
                        e.stopImmediatePropagation();
                        e.preventDefault();
                        // Close Dialog
                        $('.ui-dialog-overlay-close.modal:visible').last().click();
                    } else {
                        // Remove focus
                        e.target.blur();
                        e.target.classList.remove("focus");
                    }
                }
            }
        });
 		
        

    });
</script>
    <div id='dialog-overlay-WindowSmallModal-base' class='ui-dialog-overlay-base-modal' ><div id='WindowSmallModal' role='dialog' tabindex='-1'  class='ui-dialog-overlay small' ><div class='ui-dialog-overlay-title-bar' id='dialog-overlay-WindowSmallModal-title-bar' ></div><div class='ui-dialog-overlay-body' id='dialog-overlay-WindowSmallModal-body' ></div><button class='ui-dialog-overlay-close modal' title='Close' aria-label='Close' onclick='CloseDialogOverlay("WindowSmallModal");' ></button><div class='ui-dialog-overlay-footer' id='dialog-overlay-WindowSmallModal-footer' ></div></div></div>

<script type="text/javascript">
    $(document).ready(function() {
        $('#dialog-overlay-WindowSmallModal-base').appendTo('body');
        
        $('body').on('keydown', '.ui-dialog-overlay-base-modal, .ui-dialog-overlay, .ui-sw-alert', function (e) {
            var swAlertOpen = $(".ui-sw-alert").length;
            if (swAlertOpen > 1) {
                if (e.keyCode == 27) {//escape key
                    e.stopImmediatePropagation();
                    e.preventDefault();
                    //get id of open alert
                    var alertboxid = $('.ui-sw-alert').attr('id');
                    //click ok or no
                    if ($('#' + alertboxid + 'ok').length > 0) {
                        $('#' + alertboxid + 'ok').click();
                    } else {
                        $('#' + alertboxid + 'no').click();
                    }
                }
            } else {
                if (e.keyCode == 27) {//escape key
                    e.stopImmediatePropagation();
                    e.preventDefault();
                    // Check if ESC was pressed on DatePicker
                    var raisedByDatepicker = e.target.classList.contains("datepicker");
                    if (!raisedByDatepicker) {
                        e.stopImmediatePropagation();
                        e.preventDefault();
                        // Close Dialog
                        $('.ui-dialog-overlay-close.modal:visible').last().click();
                    } else {
                        // Remove focus
                        e.target.blur();
                        e.target.classList.remove("focus");
                    }
                }
            }
        });
 		
        

    });
</script>
    <div id='dialog-overlay-WindowLargeModal-base' class='ui-dialog-overlay-base-modal' ><div id='WindowLargeModal' role='dialog' tabindex='-1'  class='ui-dialog-overlay large' ><div class='ui-dialog-overlay-title-bar' id='dialog-overlay-WindowLargeModal-title-bar' ></div><div class='ui-dialog-overlay-body' id='dialog-overlay-WindowLargeModal-body' ></div><button class='ui-dialog-overlay-close modal' title='Close' aria-label='Close' onclick='CloseDialogOverlay("WindowLargeModal");' ></button><div class='ui-dialog-overlay-footer' id='dialog-overlay-WindowLargeModal-footer' ></div></div></div>

<script type="text/javascript">
    $(document).ready(function() {
        $('#dialog-overlay-WindowLargeModal-base').appendTo('body');
        
        $('body').on('keydown', '.ui-dialog-overlay-base-modal, .ui-dialog-overlay, .ui-sw-alert', function (e) {
            var swAlertOpen = $(".ui-sw-alert").length;
            if (swAlertOpen > 1) {
                if (e.keyCode == 27) {//escape key
                    e.stopImmediatePropagation();
                    e.preventDefault();
                    //get id of open alert
                    var alertboxid = $('.ui-sw-alert').attr('id');
                    //click ok or no
                    if ($('#' + alertboxid + 'ok').length > 0) {
                        $('#' + alertboxid + 'ok').click();
                    } else {
                        $('#' + alertboxid + 'no').click();
                    }
                }
            } else {
                if (e.keyCode == 27) {//escape key
                    e.stopImmediatePropagation();
                    e.preventDefault();
                    // Check if ESC was pressed on DatePicker
                    var raisedByDatepicker = e.target.classList.contains("datepicker");
                    if (!raisedByDatepicker) {
                        e.stopImmediatePropagation();
                        e.preventDefault();
                        // Close Dialog
                        $('.ui-dialog-overlay-close.modal:visible').last().click();
                    } else {
                        // Remove focus
                        e.target.blur();
                        e.target.classList.remove("focus");
                    }
                }
            }
        });
 		
        

    });
</script>
    <div id='dialog-overlay-WindowXLargeModal-base' class='ui-dialog-overlay-base-modal' ><div id='WindowXLargeModal' role='dialog' tabindex='-1'  class='ui-dialog-overlay xlarge' ><div class='ui-dialog-overlay-title-bar' id='dialog-overlay-WindowXLargeModal-title-bar' ></div><div class='ui-dialog-overlay-body' id='dialog-overlay-WindowXLargeModal-body' ></div><button class='ui-dialog-overlay-close modal' title='Close' aria-label='Close' onclick='CloseDialogOverlay("WindowXLargeModal");' ></button><div class='ui-dialog-overlay-footer' id='dialog-overlay-WindowXLargeModal-footer' ></div></div></div>

<script type="text/javascript">
    $(document).ready(function() {
        $('#dialog-overlay-WindowXLargeModal-base').appendTo('body');
        
        $('body').on('keydown', '.ui-dialog-overlay-base-modal, .ui-dialog-overlay, .ui-sw-alert', function (e) {
            var swAlertOpen = $(".ui-sw-alert").length;
            if (swAlertOpen > 1) {
                if (e.keyCode == 27) {//escape key
                    e.stopImmediatePropagation();
                    e.preventDefault();
                    //get id of open alert
                    var alertboxid = $('.ui-sw-alert').attr('id');
                    //click ok or no
                    if ($('#' + alertboxid + 'ok').length > 0) {
                        $('#' + alertboxid + 'ok').click();
                    } else {
                        $('#' + alertboxid + 'no').click();
                    }
                }
            } else {
                if (e.keyCode == 27) {//escape key
                    e.stopImmediatePropagation();
                    e.preventDefault();
                    // Check if ESC was pressed on DatePicker
                    var raisedByDatepicker = e.target.classList.contains("datepicker");
                    if (!raisedByDatepicker) {
                        e.stopImmediatePropagation();
                        e.preventDefault();
                        // Close Dialog
                        $('.ui-dialog-overlay-close.modal:visible').last().click();
                    } else {
                        // Remove focus
                        e.target.blur();
                        e.target.classList.remove("focus");
                    }
                }
            }
        });
 		
        

    });
</script>

    <div id='dialog-overlay-MyAccountSubscriptionOverlay-base' class='ui-dialog-overlay-base-modal' ><div id='MyAccountSubscriptionOverlay' role='dialog' tabindex='-1'  class='ui-dialog-overlay medium' ><div class='ui-dialog-overlay-title-bar' id='dialog-overlay-MyAccountSubscriptionOverlay-title-bar' ></div><div class='ui-dialog-overlay-body' id='dialog-overlay-MyAccountSubscriptionOverlay-body' ></div><button class='ui-dialog-overlay-close modal' title='Close' aria-label='Close' onclick='CloseDialogOverlay("MyAccountSubscriptionOverlay");' ></button><div class='ui-dialog-overlay-footer' id='dialog-overlay-MyAccountSubscriptionOverlay-footer' ></div></div></div>

<script type="text/javascript">
    $(document).ready(function() {
        $('#dialog-overlay-MyAccountSubscriptionOverlay-base').appendTo('body');
        
        $('body').on('keydown', '.ui-dialog-overlay-base-modal, .ui-dialog-overlay, .ui-sw-alert', function (e) {
            var swAlertOpen = $(".ui-sw-alert").length;
            if (swAlertOpen > 1) {
                if (e.keyCode == 27) {//escape key
                    e.stopImmediatePropagation();
                    e.preventDefault();
                    //get id of open alert
                    var alertboxid = $('.ui-sw-alert').attr('id');
                    //click ok or no
                    if ($('#' + alertboxid + 'ok').length > 0) {
                        $('#' + alertboxid + 'ok').click();
                    } else {
                        $('#' + alertboxid + 'no').click();
                    }
                }
            } else {
                if (e.keyCode == 27) {//escape key
                    e.stopImmediatePropagation();
                    e.preventDefault();
                    // Check if ESC was pressed on DatePicker
                    var raisedByDatepicker = e.target.classList.contains("datepicker");
                    if (!raisedByDatepicker) {
                        e.stopImmediatePropagation();
                        e.preventDefault();
                        // Close Dialog
                        $('.ui-dialog-overlay-close.modal:visible').last().click();
                    } else {
                        // Remove focus
                        e.target.blur();
                        e.target.classList.remove("focus");
                    }
                }
            }
        });
 		
        

    });
</script>

    <div id='dialog-overlay-InsertOverlay-base' class='ui-dialog-overlay-base-modal' ><div id='InsertOverlay' role='dialog' tabindex='-1'  class='ui-dialog-overlay large' ><div class='ui-dialog-overlay-title-bar' id='dialog-overlay-InsertOverlay-title-bar' ></div><div class='ui-dialog-overlay-body' id='dialog-overlay-InsertOverlay-body' ></div><button class='ui-dialog-overlay-close modal' title='Close' aria-label='Close' onclick='CloseDialogOverlay("InsertOverlay");' ></button><div class='ui-dialog-overlay-footer' id='dialog-overlay-InsertOverlay-footer' ></div></div></div>

<script type="text/javascript">
    $(document).ready(function() {
        $('#dialog-overlay-InsertOverlay-base').appendTo('body');
        
        $('body').on('keydown', '.ui-dialog-overlay-base-modal, .ui-dialog-overlay, .ui-sw-alert', function (e) {
            var swAlertOpen = $(".ui-sw-alert").length;
            if (swAlertOpen > 1) {
                if (e.keyCode == 27) {//escape key
                    e.stopImmediatePropagation();
                    e.preventDefault();
                    //get id of open alert
                    var alertboxid = $('.ui-sw-alert').attr('id');
                    //click ok or no
                    if ($('#' + alertboxid + 'ok').length > 0) {
                        $('#' + alertboxid + 'ok').click();
                    } else {
                        $('#' + alertboxid + 'no').click();
                    }
                }
            } else {
                if (e.keyCode == 27) {//escape key
                    e.stopImmediatePropagation();
                    e.preventDefault();
                    // Check if ESC was pressed on DatePicker
                    var raisedByDatepicker = e.target.classList.contains("datepicker");
                    if (!raisedByDatepicker) {
                        e.stopImmediatePropagation();
                        e.preventDefault();
                        // Close Dialog
                        $('.ui-dialog-overlay-close.modal:visible').last().click();
                    } else {
                        // Remove focus
                        e.target.blur();
                        e.target.classList.remove("focus");
                    }
                }
            }
        });
 		
        

    });
</script>
    <div id='dialog-overlay-InsertOverlay2-base' class='ui-dialog-overlay-base-modal' ><div id='InsertOverlay2' role='dialog' tabindex='-1'  class='ui-dialog-overlay large' ><div class='ui-dialog-overlay-title-bar' id='dialog-overlay-InsertOverlay2-title-bar' ></div><div class='ui-dialog-overlay-body' id='dialog-overlay-InsertOverlay2-body' ></div><button class='ui-dialog-overlay-close modal' title='Close' aria-label='Close' onclick='CloseDialogOverlay("InsertOverlay2");' ></button><div class='ui-dialog-overlay-footer' id='dialog-overlay-InsertOverlay2-footer' ></div></div></div>

<script type="text/javascript">
    $(document).ready(function() {
        $('#dialog-overlay-InsertOverlay2-base').appendTo('body');
        
        $('body').on('keydown', '.ui-dialog-overlay-base-modal, .ui-dialog-overlay, .ui-sw-alert', function (e) {
            var swAlertOpen = $(".ui-sw-alert").length;
            if (swAlertOpen > 1) {
                if (e.keyCode == 27) {//escape key
                    e.stopImmediatePropagation();
                    e.preventDefault();
                    //get id of open alert
                    var alertboxid = $('.ui-sw-alert').attr('id');
                    //click ok or no
                    if ($('#' + alertboxid + 'ok').length > 0) {
                        $('#' + alertboxid + 'ok').click();
                    } else {
                        $('#' + alertboxid + 'no').click();
                    }
                }
            } else {
                if (e.keyCode == 27) {//escape key
                    e.stopImmediatePropagation();
                    e.preventDefault();
                    // Check if ESC was pressed on DatePicker
                    var raisedByDatepicker = e.target.classList.contains("datepicker");
                    if (!raisedByDatepicker) {
                        e.stopImmediatePropagation();
                        e.preventDefault();
                        // Close Dialog
                        $('.ui-dialog-overlay-close.modal:visible').last().click();
                    } else {
                        // Remove focus
                        e.target.blur();
                        e.target.classList.remove("focus");
                    }
                }
            }
        });
 		
        

    });
</script>
    
    <div id="videowrapper" class="ui-helper-hidden">
        <div id="videodialog" role="application">
            <a id="videodialog-close" role="button" href="javascript:;" aria-label="Close Overlay" class="close-btn" onclick="closeVideoDialog();">CLOSE</a>
            <div id="videodialog-video" ></div>
            <div id="videodialog-foot" tabindex="0"></div>
        </div>
    </div>
    <div id="attachmentwrapper" class="ui-helper-hidden">
        <div id="attachmentdialog" role="application">
            <a id="attachmentdialog-close" role="button" href="javascript:;" aria-label="Close Overlay" class="close-btn" onclick="closeAttachmentDialog();">CLOSE</a>
            <div id="attachmentdialog-container"></div>
        </div>
    </div>
    <script type="text/javascript">

        $(document).ready(function () {

            removeBrokenImages();
            checkSidebar();
            RemoveCookie();

            $('div.bullet').attr('tabindex', '');

            $('.navigation li.collapsible').each(function () {
                if ($(this).find('ul').length == 0) {
                    $(this).removeClass('collapsible');
                }
            });

            // find page nav state cookie and add open chevron
            var arrValues = GetCookie('SWPageNavState').split('~');

            $.each(arrValues, function () {
                if (this != '') {
                    $('#' + this).addClass('collapsible').prepend("<div class='bullet collapsible' aria-label='Close Page Submenu'/>");
                }
            });

            // find remaining sub menus and add closed chevron and close menu
            $('.navigation li > ul').each(function () {
                var list = $(this);

                if (list.parent().hasClass('active') && !list.parent().hasClass('collapsible')) {
                    // open sub for currently selected page                    
                    list.parent().addClass('collapsible').prepend("<div class='bullet collapsible'aria-label='Close Page Submenu' />");
                } else {
                    if (list.parent().hasClass('collapsible') && !list.siblings('div').hasClass('collapsible')) {
                        // open sub for page with auto expand
                        list.siblings('div.expandable').remove();
                        list.parent().prepend("<div class='bullet collapsible' aria-label='Close Page Submenu' />");
                    }
                }

                if (!list.siblings('div').hasClass('collapsible')) {
                    // keep all closed that aren't already set to open
                    list.parent().addClass('expandable').prepend("<div class='bullet expandable' aria-label='Open Page Submenu' />");
                    ClosePageSubMenu(list.parent());
                } else {
                    OpenPageSubMenu(list.parent());
                }
            });

            // remove bullet from hierarchy if no-bullet set
            $('.navigation li.collapsible').each(function () {
                if ($(this).hasClass('no-bullet')) {
                    if (!$(this).hasClass('navigationgroup')) { $(this).removeClass('collapsible'); }
                    $(this).children('div.collapsible').remove();
                }
            });

            $('.navigation li.expandable').each(function () {
                if ($(this).hasClass('no-bullet')) {
                    if (!$(this).hasClass('navigationgroup')) { $(this).removeClass('expandable'); }
                    $(this).children('div.expandable').remove();
                }
            });

            $('.navigation li:not(.collapsible,.expandable,.no-bullet)').each(function () {
                $(this).prepend("<div class='bullet'/>");
            });

            $('.navigation li.active').parents('ul').each(function () {
                if (!$(this).hasClass('page-navigation')) {
                    OpenPageSubMenu($(this).parent());
                }
            });

            // Set aria ttributes
            $('li.collapsible').each(function () {
                $(this).attr("aria-expanded", "true");
                $(this).find('div:first').attr('aria-pressed', 'true');
            });

            $('li.expandable').each(function () {
                $(this).attr("aria-expanded", "false");
                $(this).find('div:first').attr('aria-pressed', 'false');
            });

            $('div.bullet').each(function () {
                $(this).attr("aria-hidden", "true");
            });

            // set click event for chevron
            $(document).on('click', '.navigation div.collapsible', function () {
                ClosePageSubMenu($(this).parent());
            });

            $(document).on('click', '.navigation div.expandable', function () {
                OpenPageSubMenu($(this).parent());
            });

            // set navigation grouping links
            $(document).on('click', '.navigationgroup.collapsible > a', function () {
                ClosePageSubMenu($(this).parent());
            });

            $(document).on('click', '.navigationgroup.expandable > a', function () {
                OpenPageSubMenu($(this).parent());
            });

            //SW MYSTART DROPDOWNS
            $(document).on('click', '.sw-mystart-dropdown', function () {
                $(this).children(".sw-dropdown").css("display", "block");
            });

            $(".sw-mystart-dropdown").hover(function () { }, function () {
                $(this).children(".sw-dropdown").hide();
                $(this).blur();
            });

            //SW ACCOUNT DROPDOWN
            $(document).on('click', '#sw-mystart-account', function () {
                $(this).children("#sw-myaccount-list").show();
                $(this).addClass("clicked-state");
            });

            $("#sw-mystart-account, #sw-myaccount-list").hover(function () { }, function () {
                $(this).children("#sw-myaccount-list").hide();
                $(this).removeClass("clicked-state");
                $("#sw-myaccount").blur();
            });

            // set hover class for page and section navigation
            $('.ui-widget.app.pagenavigation, .ui-widget.app.sectionnavigation').find('li > a').hover(function () {
                $(this).addClass('hover');
            }, function () {
                $(this).removeClass('hover');
            });

            //set aria-label for home
            $('#navc-HP > a').attr('aria-label', 'Home');

            // set active class on channel and section
            var activeChannelNavType = $('input#hidActiveChannelNavType').val();
            if (activeChannelNavType == -1) {
                // homepage is active
                $('#navc-HP').addClass('active');
            } else if (activeChannelNavType == 1) {
                // calendar page is active
                $('#navc-CA').addClass('active');
            } else {
                // channel is active - set the active class on the channel
                var activeSelectorID = $('input#hidActiveChannel').val();
                $('#navc-' + activeSelectorID).addClass('active');

                // set the breadcrumb channel href to the channel nav href
                $('li[data-bccID=' + activeSelectorID + '] a').attr('href', $('#navc-' + activeSelectorID + ' a').attr('href'));
                $('li[data-bccID=' + activeSelectorID + '] a span').text($('#navc-' + activeSelectorID + ' a span').first().text());

                // set the active class on the section
                activeSelectorID = $('input#hidActiveSection').val();
                $('#navs-' + activeSelectorID).addClass('active');

                // set the breadcrumb section href to the channel nav href
                $('li[data-bcsID=' + activeSelectorID + '] a').attr('href', $('#navs-' + activeSelectorID + ' a').attr('href'));
                if ($('#navs-' + activeSelectorID + ' a').attr('target') !== undefined) {
                    $('li[data-bcsID=' + activeSelectorID + '] a').attr('target', $('#navs-' + activeSelectorID + ' a').attr('target'));
                }
                $('li[data-bcsID=' + activeSelectorID + '] span').text($('#navs-' + activeSelectorID + ' a span').text());

                if ($('.sw-directory-columns').length > 0) {
                    $('ul.ui-breadcrumbs li:last-child').remove();
                    $('ul.ui-breadcrumbs li:last-child a').replaceWith(function() { return $('span', this); });
                    $('ul.ui-breadcrumbs li:last-child span').append(' Directory');
                }
            }
        }); // end document ready

        function OpenPageSubMenu(li) {
            if (li.prop('tagName').toLowerCase() == "li") {
                if (li.hasClass('expandable')) {
                    li.removeClass('expandable').addClass('collapsible');
                }
                if (li.find('div:first').hasClass('expandable')) {
                    li.find('div:first').removeClass('expandable').addClass('collapsible').attr('aria-pressed', 'true').attr('aria-label','Close Page Submenu');
                }
                li.find('ul:first').attr('aria-hidden', 'false').show();

                li.attr("aria-expanded", "true");

                PageNavigationStateCookie();
            }
        }

        function ClosePageSubMenu(li) {
            if (li.prop('tagName').toLowerCase() == "li") {
                li.removeClass('collapsible').addClass('expandable');
                li.find('div:first').removeClass('collapsible').addClass('expandable').attr('aria-pressed', 'false').attr('aria-label','Open Page Submenu');
                li.find('ul:first').attr('aria-hidden', 'true').hide();

                li.attr("aria-expanded", "false");

                PageNavigationStateCookie();
            }
        }

        function PageNavigationStateCookie() {
            var strCookie = "";

            $('.pagenavigation li > ul').each(function () {
                var item = $(this).parent('li');
                if (item.hasClass('collapsible') && !item.hasClass('no-bullet')) {
                    strCookie += $(this).parent().attr('id') + '~';
                }
            });

            SetCookie('SWPageNavState', strCookie);
        }

        function checkSidebar() {
            $(".ui-widget-sidebar").each(function () {
                if ($.trim($(this).html()) != "") {
                    $(this).show();
                    $(this).siblings(".ui-widget-detail").addClass("with-sidebar");
                }
            });
        }

        function removeBrokenImages() {
            //REMOVES ANY BROKEN IMAGES
            $("span.img img").each(function () {
                if ($(this).attr("src") !== undefined && $(this).attr("src") != '../../') {
                    $(this).parent().parent().show();
                    $(this).parent().parent().siblings().addClass("has-thumb");
                }
            });
        }

        function LoadEventDetailUE(moduleInstanceID, eventDateID, userRegID, isEdit) {
            (userRegID === undefined ? userRegID = 0 : '');
            (isEdit === undefined ? isEdit = false : '');
            OpenDialogOverlay("WindowMediumModal", { LoadType: "U", LoadURL: "https://mpatzem.schoolwires.net//site/UserControls/Calendar/EventDetailWrapper.aspx?ModuleInstanceID=" + moduleInstanceID + "&EventDateID=" + eventDateID + "&UserRegID=" + userRegID + "&IsEdit=" + isEdit });
        }

        function RemoveCookie() {
            // There are no sub page            
            if ($('.pagenavigation li li').length == 0) {
                //return false;
                PageNavigationStateCookie();
            }
        }
    </script>

    <script type="text/javascript">

        function AddOffCanvasMenuHeightForSiteNav() {
            var sitenavulHeight = 0;

            if ($('#sw-pg-sitenav-ul').length > 0) {
                sitenavulHeight = parseInt($("#sw-pg-sitenav-ul").height());
            }

            var swinnerwrapHeight = 0;

            if ($('#sw-inner-wrap').length > 0) {
                swinnerwrapHeight = parseInt($("#sw-inner-wrap").height());
            }

            // 360px is abount 5 li height
            if (sitenavulHeight + 360 >= swinnerwrapHeight) {
                $("#sw-inner-wrap").height(sitenavulHeight + 360);
            }
        }

        function AddOffCanvasMenuHeightForSelectSchool() {
            var selectschoolulHeight = 0;

            if ($('#sw-pg-selectschool-ul').length > 0) {
                selectschoolulHeight = parseInt($("#sw-pg-selectschool-ul").height());
            }

            var swinnerwrapHeight = 0;

            if ($('#sw-inner-wrap').length > 0) {
                swinnerwrapHeight = parseInt($("#sw-inner-wrap").height());
            }

            // 360px is abount 5 li height
            if (selectschoolulHeight + 360 >= swinnerwrapHeight) {
                $("#sw-inner-wrap").height(selectschoolulHeight + 360);
            }
        }

        $(document).ready(function () {
            if ($("#sw-pg-sitenav-a").length > 0) {
                $(document).on('click', '#sw-pg-sitenav-a', function () {
                    if ($("#sw-pg-sitenav-ul").hasClass('sw-pgmenu-closed')) {
                        AddOffCanvasMenuHeightForSiteNav();

                        $("ul.sw-pgmenu-toplevel").removeClass('sw-pgmenu-open').addClass('sw-pgmenu-closed');
                        $("#sw-pg-sitenav-ul").removeClass('sw-pgmenu-closed');
                        $("#sw-pg-sitenav-ul").addClass('sw-pgmenu-open');
                    } else {
                        $("#sw-pg-sitenav-ul").removeClass('sw-pgmenu-open');
                        $("#sw-pg-sitenav-ul").addClass('sw-pgmenu-closed');
                    }
                });

                $(document).on('click', '#sw-pg-selectschool-a', function () {
                    if ($("#sw-pg-selectschool-ul").hasClass('sw-pgmenu-closed')) {
                        AddOffCanvasMenuHeightForSelectSchool();

                        $("ul.sw-pgmenu-toplevel").removeClass('sw-pgmenu-open').addClass('sw-pgmenu-closed');
                        $("#sw-pg-selectschool-ul").removeClass('sw-pgmenu-closed');
                        $("#sw-pg-selectschool-ul").addClass('sw-pgmenu-open');
                    } else {
                        $("#sw-pg-selectschool-ul").removeClass('sw-pgmenu-open');
                        $("#sw-pg-selectschool-ul").addClass('sw-pgmenu-closed');
                    }
                });

                $(document).on('click', '#sw-pg-myaccount-a', function () {
                    if ($("#sw-pg-myaccount-ul").hasClass('sw-pgmenu-closed')) {
                        $("ul.sw-pgmenu-toplevel").removeClass('sw-pgmenu-open').addClass('sw-pgmenu-closed');
                        $("#sw-pg-myaccount-ul").removeClass('sw-pgmenu-closed');
                        $("#sw-pg-myaccount-ul").addClass('sw-pgmenu-open');
                    } else {
                        $("#sw-pg-myaccount-ul").removeClass('sw-pgmenu-open');
                        $("#sw-pg-myaccount-ul").addClass('sw-pgmenu-closed');
                    }
                });

                $(document).on('click', '.pg-list-bullet', function () {
                    $(this).prev().toggle();

                    if ($(this).hasClass('closed')) {
                        AddOffCanvasMenuHeightForSiteNav();

                        $(this).removeClass('closed');
                        $(this).addClass('open');
                    } else {
                        $(this).removeClass('open');
                        $(this).addClass('closed');
                    }
                });

                $(document).on('mouseover', '#sw-pg-selectschool', function () {
                    $("#sw-pg-selectschool-firstli").removeClass('sw-pg-selectschool-firstli-mouseout').addClass('sw-pg-selectschool-firstli-mouseover');
                    $("#sw-pg-selectschool-firstli a").addClass('sw-pg-selectschool-firstli-a-mouseover').removeClass('sw-pg-selectschool-firstli-a-mouseout');
                });

                $(document).on('mouseout', '#sw-pg-selectschool', function () {
                    $("#sw-pg-selectschool-firstli").removeClass('sw-pg-selectschool-firstli-mouseover').addClass('sw-pg-selectschool-firstli-mouseout');
                    $("#sw-pg-selectschool-firstli a").addClass('sw-pg-selectschool-firstli-a-mouseout').removeClass('sw-pg-selectschool-firstli-a-mouseover');
                });

                $(document).on('mouseover', '#sw-pg-myaccount', function () {
                    $("#sw-pg-myaccount-firstli").removeClass('sw-pg-myaccount-firstli-mouseout').addClass('sw-pg-myaccount-firstli-mouseover');
                    $("#sw-pg-myaccount-firstli a").addClass('sw-pg-myaccount-firstli-a-mouseover').removeClass('sw-pg-myaccount-firstli-a-mouseout');
                });

                $(document).on('mouseout', '#sw-pg-myaccount', function () {
                    $("#sw-pg-myaccount-firstli").removeClass('sw-pg-myaccount-firstli-mouseover').addClass('sw-pg-myaccount-firstli-mouseout');
                    $("#sw-pg-myaccount-firstli a").addClass('sw-pg-myaccount-firstli-a-mouseout').removeClass('sw-pg-myaccount-firstli-a-mouseover');
                });

                $(document).on('mouseover', '#sw-pg-sitenav', function () {
                    $("#sw-pg-sitenav-firstli").removeClass('sw-pg-sitenav-firstli-mouseout').addClass('sw-pg-sitenav-firstli-mouseover');
                    $("#sw-pg-sitenav-firstli a").addClass('sw-pg-sitenav-firstli-a-mouseover').removeClass('sw-pg-sitenav-firstli-a-mouseout');
                });

                $(document).on('mouseout', '#sw-pg-sitenav', function () {
                    $("#sw-pg-sitenav-firstli").removeClass('sw-pg-sitenav-firstli-mouseover').addClass('sw-pg-sitenav-firstli-mouseout');
                    $("#sw-pg-sitenav-firstli a").addClass('sw-pg-sitenav-firstli-a-mouseout').removeClass('sw-pg-sitenav-firstli-a-mouseover');
                });

                $(document).on('mouseover', '#sw-pg-district', function () {
                    $("#sw-pg-district-firstli").removeClass('sw-pg-district-firstli-mouseout').addClass('sw-pg-district-firstli-mouseover');
                    $("#sw-pg-district-firstli a").addClass('sw-pg-district-firstli-a-mouseover').removeClass('sw-pg-district-firstli-a-mouseout');
                });

                $(document).on('mouseout', '#sw-pg-district', function () {
                    $("#sw-pg-district-firstli").removeClass('sw-pg-district-firstli-mouseover').addClass('sw-pg-district-firstli-mouseout');
                    $("#sw-pg-district-firstli a").addClass('sw-pg-district-firstli-a-mouseout').removeClass('sw-pg-district-firstli-a-mouseover');
                });
            }
        });


    </script>
    <script src='https://mpatzem.schoolwires.net/Static//GlobalAssets/Scripts/min/jquery-ui-1.12.0.min.js' type='text/javascript'></script>
    <script src="https://mpatzem.schoolwires.net/Static/GlobalAssets/Scripts/min/SW-UI_2680.min.js" type='text/javascript'></script>
    <script src="https://mpatzem.schoolwires.net/Static/GlobalAssets/Scripts/jquery.sectionlayer.js" type='text/javascript'></script>
    
    <script src="https://mpatzem.schoolwires.net/Static/GlobalAssets/Scripts/min/swfobject.min.js" type="text/javascript"></script>
    <script src="https://mpatzem.schoolwires.net/Static/GlobalAssets/Scripts/min/jquery.ajaxupload_2440.min.js" type="text/javascript"></script>

    <!-- Begin swuc.CheckScript -->
  <script type="text/javascript" src="https://mpatzem.schoolwires.net/Static/GlobalAssets/Scripts/ThirdParty/json2.js"></script>
  
<script>var homeURL = location.protocol + "//" + window.location.hostname;

function parseXML(xml) {
    if (window.ActiveXObject && window.GetObject) {
        var dom = new ActiveXObject('Microsoft.XMLDOM');
        dom.loadXML(xml);
        return dom;
    }

    if (window.DOMParser) {
        return new DOMParser().parseFromString(xml, 'text/xml');
    } else {
        throw new Error('No XML parser available');
    }
}

function GetContent(URL, TargetClientID, Loadingtype, SuccessCallback, FailureCallback, IsOverlay, Append) {
    (Loadingtype === undefined ? LoadingType = 3 : '');
    (SuccessCallback === undefined ? SuccessCallback = '' : '');
    (FailureCallback === undefined ? FailureCallback = '' : '');
    (IsOverlay === undefined ? IsOverlay = '0' : '');
    (Append === undefined ? Append = false : '');

    var LoadingHTML;
    var Selector;

    switch (Loadingtype) {
        //Small
        case 1:
            LoadingHTML = "SMALL LOADER HERE";
            break;
            //Large
        case 2:
            LoadingHTML = "<div class='ui-loading large' role='alert' aria-label='Loading content'></div>";
            break;
            // None
        case 3:
            LoadingHTML = "";
            break;
    }

    Selector = "#" + TargetClientID;

    ajaxCall = $.ajax({
        url: URL,
        cache: false,
        beforeSend: function () {
            if (Loadingtype != 3) { BlockUserInteraction(TargetClientID, LoadingHTML, Loadingtype, 0, IsOverlay); }
        },
        success: function (strhtml) {

            // check for calendar and empty div directly surrounding eventlist uc first 
            //      to avoid memory error in IE (Access violation reading location 0x00000018)
            //      need to figure out exactly why this is happening..
            //      Partially to do with this? http://support.microsoft.com/kb/927917/en-us
            //      The error/crash happens when .empty() is called on Selector 
            //          (.html() calls .empty().append() in jquery)
            //      * no one has come across this issue anywhere else in the product so far

            if ($(Selector).find('#calendar-pnl-calendarlist').length > 0) {
                $('#calendar-pnl-calendarlist').empty();
                $(Selector).html(strhtml);
            } else if (Append) {
                $(Selector).append(strhtml);
            }
            else {
                $(Selector).html(strhtml);
            }


            // check for tabindex 
            if ($(Selector).find(":input[tabindex='1']:first").length > 0) {
                $(Selector).find(":input[tabindex='1']:first").focus();
            } else {
                $(Selector).find(":input[type='text']:first").not('.nofocus').focus();
            }

            //if (CheckDirty(Selector) === true) { BindSetDirty(Selector); }
            //CheckDirty(Selector);
            BlockUserInteraction(TargetClientID, '', '', 1);
            (SuccessCallback != '' ? eval(SuccessCallback) : '');
        },
        failure: function () {
            BlockUserInteraction(TargetClientID, '', '', 1);
            (FailureCallback != '' ? eval(FailureCallback) : '');
        }
    });
}

function BlockUserInteraction(TargetClientID, LoadingHTML, Loadingtype, Unblock, IsOverlay) {
    if (LoadingHTML === undefined) {
        LoadingHTML = "<div class='ui-loading large'></div>";
    }

    if (Unblock == 1) {
        $('#' + TargetClientID).unblockinteraction();
    } else {
        if (IsOverlay == 1) {
            $('#' + TargetClientID).blockinteraction({ message: LoadingHTML, type: Loadingtype, isOverlay: true });
        } else {
            $('#' + TargetClientID).blockinteraction({ message: LoadingHTML, type: Loadingtype });
        }
    }
}

function OpenUltraDialogOverlay(OverlayClientID, options, Callback) {
    lastItemClicked = document.activeElement;
    var defaults = {
        LoadType: "U",
        LoadURL: "",
        TargetDivID: "",
        LoadContent: "",
        NoResize: false,
        ScrollTop: false,
        CloseCallback: undefined
    };

    jQuery.extend(defaults, options);

    // check what browser/version we're on
    var isIE = GetIEVersion();

    if (isIE == 0) {
        if ($.browser.mozilla) {
            //Firefox/Chrome
            $("body").css("overflow", "hidden");
        } else {
            //Safari
            $("html").css("overflow", "hidden");
        }
    } else {
        // IE
        $("html").css("overflow", "hidden");
    }

    var OverlaySelector;
    var BodyClientID;
    var TargetDivSelector;
    var CurrentScrollPosition;

    if (defaults.ScrollTop) {
        $.scrollTo(0, { duration: 0 });
    }

    OverlaySelector = "#dialog-ultra-overlay-" + OverlayClientID + "-base";
    BodyClientID = "dialog-ultra-overlay-" + OverlayClientID + "-holder";
    TargetDivSelector = "#" + defaults.TargetDivID;

    if ($.trim($("#dialog-ultra-overlay-" + OverlayClientID + "-holder").html()) != "") {
        CloseUltraDialogOverlay(OverlayClientID);
    }

    // U = URL
    // H - HTML
    // D - Divider

    var success = "";

    if (Callback !== undefined) {
        success += Callback;
    }

    // block user interaction
    BlockUserInteraction(BodyClientID, "<div class='ui-loading large'></div>", 2, 0, 1);

    switch (defaults.LoadType) {
        case 'U':
            GetContent(defaults.LoadURL, BodyClientID, 3, success);
            break;
        case 'H':
            $("#" + BodyClientID).html(defaults.LoadContent);
            break;
        case 'D':
            $("#" + BodyClientID).html($(TargetDivSelector).html());
            break;
    };

    // open the lateral panel
    var browserWidth = $(document).width();

    if (OverlayClientID == "UltraOverlayLarge") {
        browserWidth = browserWidth - 200;
    } else {
        browserWidth = browserWidth - 280;
    }

    $("#dialog-ultra-overlay-" + OverlayClientID + "-body").css("width", browserWidth);
    $(OverlaySelector).addClass("is-visible");

    // hide 'X' button if second window opened
    if (OverlayClientID == "UltraOverlayMedium") {
        $("#dialog-ultra-overlay-UltraOverlayLarge-close").hide();
    }
}

function CloseUltraDialogOverlay(OverlayClientID) {
    $("#dialog-ultra-overlay-" + OverlayClientID + "-base").removeClass("is-visible");

    // show 'X' button if second window closed
    if (OverlayClientID == "UltraOverlayMedium") {
        $("#dialog-ultra-overlay-UltraOverlayLarge-close").show();
    }

    if (OverlayClientID != "UltraOverlayMedium") {
        // check what browser/version we're on
        var isIE = GetIEVersion();

        if (isIE == 0) {
            if ($.browser.mozilla) {
                //Firefox/Chrome
                $("body").css("overflow", "auto");
            } else {
                //Safari
                $("html").css("overflow", "auto");
            }
        } else {
            // IE
            $("html").css("overflow", "auto");
        }
    }
    //focus back on the element clicked to open the dialog
    if (lastItemClicked !== undefined) {
        lastItemClicked.focus();
        lastItemClicked = undefined;
    }
}

//Save the last item clicked when opening a dialog overlay so the focus can go there upon close
var lastItemClicked;
function OpenDialogOverlay(OverlayClientID, options, Callback) {
    lastItemClicked = document.activeElement;
    var defaults = {
        LoadType: 'U',
        LoadURL: '',
        TargetDivID: '',
        LoadContent: '',
        NoResize: false,
        ScrollTop: false,
        CloseCallback: undefined
    };

    jQuery.extend(defaults, options);

    //check what browser/version we're on
    var isIE = GetIEVersion();

    //if (isIE == 0) {
    //    if ($.browser.mozilla) {
    //        //Firefox/Chrome
    //        $('body').css('overflow', 'hidden');
    //    } else {
    //        //Safari
    //        $('html').css('overflow', 'hidden');
    //    }
    //} else {
    //    // IE
    //    $('html').css('overflow', 'hidden');
    //}
    $('html').css('overflow', 'hidden');

    var OverlaySelector;
    var BodyClientID;
    var TargetDivSelector;

    if (defaults.ScrollTop) {
        $.scrollTo(0, { duration: 0 });
    }

    OverlaySelector = "#dialog-overlay-" + OverlayClientID + "-base";
    BodyClientID = "dialog-overlay-" + OverlayClientID + "-body";
    TargetDivSelector = "#" + defaults.TargetDivID;

    $(OverlaySelector).appendTo('body');

    $("#" + OverlayClientID).css("top", "5%");

    $("#" + BodyClientID).html("");

    if (isIEorEdge()) {
        $(OverlaySelector).show();
    } else {
        $(OverlaySelector).fadeIn();
    }

    if ($.trim($('#dialog-overlay-' + OverlayClientID + '-body').html()) != "") {
        CloseDialogOverlay(OverlayClientID);
    }

    // U = URL
    // H - HTML
    // D - Divider

    var success = "";

    if (Callback !== undefined) {
        success += Callback;
    }

    // Block user interaction
    $('#' + BodyClientID).css({ 'min-height': '100px' });
    BlockUserInteraction(BodyClientID, "<div class='ui-loading large'></div>", 2, 0, 1);

    switch (defaults.LoadType) {
        case 'U':
            GetContent(defaults.LoadURL, BodyClientID, 3, success);
            break;
        case 'H':
            $("#" + BodyClientID).html(defaults.LoadContent);
            break;
        case 'D':
            $("#" + BodyClientID).html($(TargetDivSelector).html());
            break;

    };

    if (defaults.CloseCallback !== undefined) {
        $(OverlaySelector + ' .ui-dialog-overlay-close').attr('onclick', 'CloseDialogOverlay(\'' + OverlayClientID + '\',' + defaults.CloseCallback + ')')
    }

    // check for tabindex 
    if ($("#" + BodyClientID).find(":input[tabindex='1']:first").length > 0) {
        $("#" + BodyClientID).find(":input[tabindex='1']:first").focus();
    } else if ($("#" + BodyClientID).find(":input[type='text']:first").length > 0) {
        $("#" + BodyClientID).find(":input[type='text']:first").focus();
    } else {
        $("#" + BodyClientID).parent().focus();
    }

    $('#dialog-overlay-' + OverlayClientID + '-base').css({'overflow': 'auto', 'top': '0', 'width': '100%', 'left': '0' });
}

function GetIEVersion() {
    var sAgent = window.navigator.userAgent;
    var Idx = sAgent.indexOf("MSIE");

    if (Idx > 0) {
        // If IE, return version number
        return parseInt(sAgent.substring(Idx + 5, sAgent.indexOf(".", Idx)));
    } else if (!!navigator.userAgent.match(/Trident\/7\./)) {
        // If IE 11 then look for Updated user agent string
        return 11;
    } else {
        //It is not IE
        return 0;
    }
}

function isIEorEdge() {
    var agent = window.navigator.userAgent;

    var ie = agent.indexOf('MSIE ');
    if (ie > 0) {
        // IE <= 10
        return parseInt(agent.substring(ie + 5, uaagentindexOf('.', ie)), 10);
    }

    var gum = agent.indexOf('Trident/');
    if (gum > 0) {
        // IE 11
        var camper = agent.indexOf('rv:');
        return parseInt(agent.substring(camper + 3, agent.indexOf('.', camper)), 10);
    }

    var linkinPark = agent.indexOf('Edge/');
    if (linkinPark > 0) {
        return parseInt(agent.substring(linkinPark + 5, agent.indexOf('.', linkinPark)), 10);
    }

    // other browser
    return false;
}

function SendEmail(to, from, subject, body, callback) {

    var data = "{ToEmailAddress: '" + to + "', " +
        "FromEmailAddress: '" + from + "', " +
        "Subject: '" + subject + "', " +
        "Body: '" + body + "'}";
    var url = homeURL + "/GlobalUserControls/SE/SEController.aspx/SE";

    $.ajax({
        type: "POST",
        url: url,
        data: data,
        async: false,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        cache: false,
        success: function (msg) {
            callback(msg.d);
        },
        headers: { 'X-Csrf-Token': GetCookie('CSRFToken') }
    });
}

// DETERMINE TEXT COLOR 

function rgbstringToTriplet(rgbstring) {
    var commadelim = rgbstring.substring(4, rgbstring.length - 1);
    var strings = commadelim.split(",");
    var numeric = [];

    for (var i = 0; i < 3; i++) {
        numeric[i] = parseInt(strings[i]);
    }

    return numeric;
}

function adjustColour(someelement) {
    var rgbstring = someelement.css('background-color');
    var triplet = [];
    var newtriplet = [];

    if (rgbstring != 'transparent') {
        if (/rgba\(0, 0, 0, 0\)/.exec(rgbstring)) {
            triplet = [255, 255, 255];
        } else {
            if (rgbstring.substring(0, 1).toLowerCase() != 'r') {
                CheckScript('RGBColor', staticURL + '/GlobalAssets/Scripts/ThirdParty/rgbcolor.js');
                // not rgb, convert it
                var color = new RGBColor(rgbstring);
                rgbstring = color.toRGB();
            }
            triplet = rgbstringToTriplet(rgbstring);
        }
    } else {
        triplet = [255, 255, 255];
    }

    // black or white:
    var total = 0; for (var i = 0; i < triplet.length; i++) { total += triplet[i]; }

    if (total > (3 * 256 / 2)) {
        newtriplet = [0, 0, 0];
    } else {
        newtriplet = [255, 255, 255];
    }

    var newstring = "rgb(" + newtriplet.join(",") + ")";

    someelement.css('color', newstring);
    someelement.find('*').css('color', newstring);

    return true;
}


// END DETERMINE TEXT color

function CheckScript2(ModuleName, ScriptSRC) {
    $.ajax({
        url: ScriptSRC,
        async: false,
        //context: document.body,
        success: function (html) {
            var script =
				document.createElement('script');
            document.getElementsByTagName('head')[0].appendChild(script);
            script.text = html;
        }
    });
}

// AREA / SCREEN CODES

function setCurrentScreenCode(screenCode) {
    SetCookie('currentScreenCode', screenCode);
    AddAnalyticsEvent(getCurrentAreaCode(), screenCode, 'Page View');
}

function getCurrentScreenCode() {
    var cookieValue = GetCookie('currentScreenCode');
    return (cookieValue != '' ? cookieValue : 0);
}

function setCurrentAreaCode(areaCode) {
    SetCookie('currentAreaCode', areaCode);
}

function getCurrentAreaCode() {
    var cookieValue = GetCookie('currentAreaCode');
    return (cookieValue != '' ? cookieValue : 0);
}

// END AREA / SCREEN CODES

// CLICK HOME TAB IN HEADER SECTION

function GoHome() {
    window.location.href = homeURL + "/cms/Workspace";
}

// END CLICK HOME TAB IN HEADER SECTION

// HELP PANEL

function OpenHelpPanel() {
    var URLScreenCode = getCurrentScreenCode();
    
    if (URLScreenCode == "" || URLScreenCode == 0) {
        URLScreenCode = getCurrentAreaCode();
        if (URLScreenCode == 0) {
            URLScreenCode = "";
        }
    } 

    AddAnalyticsEvent("Help", "How Do I...?", URLScreenCode);

    //help site url stored in webconfig, passed in to BBHelpURL from GlobalJSVar and GlobalJS.cs
    var HelpURL = BBHelpURL + URLScreenCode;
    window.open(HelpURL,"_blank");
}

// END HELP PANEL

// COOKIES
function SetCookie(name, value, days, ms) {
    var expires = "";

    if (ms) {
        var date = new Date();

        date.setMilliseconds(date.getMilliseconds() + ms);
        expires = "; expires=" + date.toGMTString();
    } else if (days) {
        var date = new Date();

        date.setDate(date.getDate() + days);
        expires = "; expires=" + date.toGMTString();
    }

    document.cookie = name + "=" + escape(value) + expires + "; path=/";
}

function GetCookie(name) {
    var value = "";

    if (document.cookie.length > 0) {
        var start = document.cookie.indexOf(name + "=");

        if (start != -1) {
            start = start + name.length + 1;

            var end = document.cookie.indexOf(";", start);

            if (end == -1) end = document.cookie.length;
            value = unescape(document.cookie.substring(start, end));
        }
    }

    return value;
}

function DeleteCookie(name) {
    SetCookie(name, '', -1);
}

function SetUnescapedCookie(name, value, days, ms) {
    var expires = "";

    if (ms) {
        var date = new Date();

        date.setMilliseconds(date.getMilliseconds() + ms);
        expires = "; expires=" + date.toGMTString();
    } else if (days) {
        var date = new Date();

        date.setDate(date.getDate() + days);
        expires = "; expires=" + date.toGMTString();
    }

    document.cookie = name + "=" + value + expires + "; path=/";
}
// END COOKIES



// IFRAME FUNCTIONS
function BindResizeFrame(FrameID) {
    $(document).on('load', "#" + FrameID, function () {
        var bodyHeight = $("#" + FrameID).contents().height() + 40;
        $("#" + FrameID).attr("height", bodyHeight + "px");
    });
}

function AdjustLinkTarget(FrameID) {
    $(document).on('load', "#" + FrameID, function () {
        $("#" + FrameID).contents().find("a").attr("target", "_parent");
    });
}

function ReloadDocViewer(moduleInstanceID, retryCount) {
    var iframe = document.getElementById('doc-viewer-' + moduleInstanceID);

    //if document failed to load and retry count is less or equal to 3, reload document and check again
    if (iframe.contentWindow.frames.length == 0 && retryCount <= 3) {
        retryCount = retryCount + 1;

        //reload the iFrame
        document.getElementById('doc-viewer-' + moduleInstanceID).src += '';

        //Check if document loaded again in 7.5 seconds
        setTimeout(ReloadDocViewer, (1000 * retryCount), moduleInstanceID, retryCount);

    } else if (iframe.contentWindow.frames.length == 0 && retryCount > 3) {
        $('#doc-viewer-' + moduleInstanceID).css('background', '');
        iframe.src = "/Errors/ReloadPage.aspx";
        iframe.height = 200;
    }

    if (iframe.contentWindow.frames.length == 1) {
        $('#doc-viewer-' + moduleInstanceID).css('background', '');
    }
}
// END IFRAME FUNCTIONS


// SCROLL TOP

function ScrollTop() {
    $.scrollTo(0, { duration: 1000 });
}

// END SCROLL TOP

// ANALYTICS TRACKING

function AddAnalyticsEvent(category, action, label) {
    ga('BBTracker.send', 'event', category, action, label);
}

// END ANYLYTICS TRACKING


// BEGIN INCLUDE DOC READY SCRIPTS

function IncludeDocReadyScripts() {
    var arrScripts = [
		staticURL + '/GlobalAssets/Scripts/min/external-combined.min.js',
        staticURL + '/GlobalAssets/Scripts/Utilities_2560.js'

    ];

    var script = document.createElement('script');
    script.type = 'text/javascript';
    $.each(arrScripts, function () {script.src = this;$('head').append(script);;
        
    });

}
// END INCLUDE DOC READY SCRIPTS

//BEGIN ONSCREEN ALERT SCRIPTS
function OnScreenAlertDialogInit() {
    $("#onscreenalert-message-dialog").hide();
    $(".titleMessage").hide();
    $("#onscreenalert-ctrl-msglist").hide();
    $(".onscreenalert-ctrl-footer").hide();

    $(".icon-icon_alert_round_message").addClass("noBorder");
    $('.onscreenalert-ctrl-modal').addClass('MoveIt2 MoveIt1 Smaller');
    $('.onscreenalert-ctrl-modal').css({ "top": "88%" });
    $("#onscreenalert-message-dialog").show();
    $('#onscreenalert-message-dialog-icon').focus();
}

function OnScreenAlertGotItDialog(serverTime) {
    var alertsID = '';
    alertsID = GetCookie("Alerts");
    var arr = [];

    if (alertsID.length > 0) {
        arr = alertsID.split(',')
    }

    $("#onscreenalertdialoglist li").each(function () {
        arr.push($(this).attr('id'));
    });

    arr.sort();
    var newarr = $.unique(arr);

    SetUnescapedCookie("Alerts", newarr.join(','), 365);
    OnScreenAlertMaskShow(false);

    $('.onscreenalert-ctrl-modal').removeClass("notransition");
    $(".onscreenalert-ctrl-footer").slideUp(200);
    $("#onscreenalert-ctrl-msglist").slideUp(200);

    // check what browser/version we're on
    // if IE 9 or less, don't do CSS 3 transitions (not supported)
    var isIE = GetIEVersion();

    if (isIE == 0) {
        // not IE
        hideOnScreenAlertTransitions();
    } else {
        // IE
        if (isIE == 8 || isIE == 9) {
            hideOnScreenAlertNoTransitions();
        } else if (isIE >= 10) {
            hideOnScreenAlertTransitions();
        }
    }
}

function OnScreenAlertOpenDialog(SiteID) {
    $('#sw-important-message-tooltip').hide();
    var onscreenAlertCookie = GetCookie('Alerts');

    if (onscreenAlertCookie != '' && onscreenAlertCookie != undefined) {
        GetContent(homeURL + "/cms/Tools/OnScreenAlerts/UserControls/OnScreenAlertDialogListWrapper.aspx?OnScreenAlertCookie=" + onscreenAlertCookie + "&SiteID=" + SiteID, "onscreenalert-ctrl-cookielist", 2, "OnScreenAlertCookieSuccess();");
        $('.onscreenalert-ctrl-content').focus();
    }
}

function OnScreenAlertCookieSuccess() {
    OnScreenAlertMaskShow(true);

    $('.onscreenalert-ctrl-modal').removeClass("notransition");
    $('.onscreenalert-ctrl-modal').removeClass("Smaller");

    // check what browser/version we're on
    // if IE 9 or less, don't do CSS 3 transitions (not supported)
    var isIE = GetIEVersion();

    if (isIE == 0) {
        // not IE
        showOnScreenAlertTransitions();
    } else {
        // IE
        if (isIE == 8 || isIE == 9) {
            showOnScreenAlertNoTransitions();
        } else if (isIE >= 10) {
            showOnScreenAlertTransitions();
        }
    }

    $('.onscreenalert-ctrl-modal').addClass('MoveIt3');
    $('.onscreenalert-ctrl-modal').attr('style', '');

    // move div to top of viewport
    $('.onscreenalert-ctrl-modal').one('transitionend', function (e) {
        var top = $('.onscreenalert-ctrl-modal').offset().top;

        $('.onscreenalert-ctrl-modal').addClass("notransition").removeClass('MoveIt1').addClass('MoveIt4');

        $('.onscreenalert-ctrl-modal').removeClass('MoveIt2');
    });
}

function showOnScreenAlertTransitions() {
    $('.onscreenalert-ctrl-modal').one('transitionend', function (e) {
        $(".icon-icon_alert_round_message").removeClass("noBorder");
        $(".titleMessage").show();
        $("#onscreenalert-ctrl-msglist").slideDown(200);
        $(".onscreenalert-ctrl-footer").slideDown(200);
    });
}

function showOnScreenAlertNoTransitions() {
    $(".icon-icon_alert_round_message").removeClass("noBorder");
    $(".titleMessage").show();
    $("#onscreenalert-ctrl-msglist").slideDown(200);
    $(".onscreenalert-ctrl-footer").slideDown(200);
}

function hideOnScreenAlertTransitions() {
    $(".titleMessage").fadeOut(200, function () {
        $('.onscreenalert-ctrl-modal').addClass("notransition").removeClass('MoveIt4').addClass('MoveIt1');
        $('.onscreenalert-ctrl-modal').attr('style', '');
        $('.onscreenalert-ctrl-modal').addClass("Smaller");
        $(".icon-icon_alert_round_message").addClass("noBorder");

        $('.onscreenalert-ctrl-modal').one('transitionend', function (e) {
            $('.onscreenalert-ctrl-modal').removeClass("notransition");
            $('.onscreenalert-ctrl-modal').removeClass('MoveIt3').addClass('MoveIt2');
            $('.onscreenalert-ctrl-modal').css({
                "top": "88%"
            });
        });
    });
}

function hideOnScreenAlertNoTransitions() {
    $(".titleMessage").fadeOut(200, function () {
        $('.onscreenalert-ctrl-modal').addClass("notransition").removeClass('MoveIt4').addClass('MoveIt1');
        $('.onscreenalert-ctrl-modal').attr('style', '');
        $('.onscreenalert-ctrl-modal').addClass("Smaller");
        $(".icon-icon_alert_round_message").addClass("noBorder");

        $('.onscreenalert-ctrl-modal').removeClass("notransition");
        $('.onscreenalert-ctrl-modal').removeClass('MoveIt3').addClass('MoveIt2');
        $('.onscreenalert-ctrl-modal').css({
            "top": "88%"
        });
    });
}

function OnScreenAlertCheckListItem() {
    var ShowOkGotIt = $("#onscreenalertdialoglist-hid-showokgotit").val();
    var ShowStickyBar = $("#onscreenalertdialoglist-hid-showstickybar").val();

    if (ShowOkGotIt == "True" && ShowStickyBar == "False") {
        OnScreenAlertShowCtrls(true, true, false);
    } else if (ShowOkGotIt == "False" && ShowStickyBar == "True") {
        OnScreenAlertShowCtrls(true, false, true);
    } else {
        OnScreenAlertShowCtrls(false, false, false);
    }
}

function OnScreenAlertShowCtrls(boolOkGotIt, boolMask, boolStickyBar) {
    if (boolOkGotIt == false) {
        $("#onscreenalert-message-dialog").hide()
    }

    (boolMask == true) ? (OnScreenAlertMaskShow(true)) : (OnScreenAlertMaskShow(false));

    if (boolStickyBar == true) {
        OnScreenAlertDialogInit();
    }
    else {
        $('.onscreenalert-ctrl-content').focus();
    }
    
}

function OnScreenAlertMaskShow(boolShow) {
    if (boolShow == true) {
        $("#onscreenalert-ctrl-mask").css({ "position": "absolute", "background": "#000", "filter": "alpha(opacity=70)", "-moz-opacity": "0.7", "-khtml-opacity": "0.7", "opacity": "0.7", "z-index": "9991", "top": "0", "left": "0" });
        $("#onscreenalert-ctrl-mask").css({ "height": function () { return $(document).height(); } });
        $("#onscreenalert-ctrl-mask").css({ "width": function () { return $(document).width(); } });
        $("#onscreenalert-ctrl-mask").show();
        $('body').css('overflow', 'hidden');
    } else {
        $("#onscreenalert-ctrl-mask").hide();
        $('body').css('overflow', 'scroll');
    }
}
// END ONSCREEN ALERT SCRIPTS

// Encoding - obfuscation
function swrot13(s) {
    return s.replace(/[a-zA-Z]/g, function(c) {return String.fromCharCode((c<="Z"?90:122)>=(c=c.charCodeAt(0)+13)?c:c-26);})
}

// START SIDEBAR TOUR SCRIPTS
/* 
 *  Wrapping sidebar tour in a function 
 *  to be called when needed.
 */

var hasPasskeys = false;
var hasStudents = false;
var isParentLinkStudent = false;
var hasNotifications = false;

function startSidebarTour() {
    // define tour
    var tour = new Shepherd.Tour({
        defaults: {
            classes: 'shepherd-theme-default'
        }
    });

    if ($('#dashboard-sidebar-student0').length > 0) {
        hasStudents = true;
    }

    if ($('#dashboard-sidebar-profile').length > 0) {
        isParentLinkStudent = true;
    }

    if ($("#dashboard-sidebar-passkeys").get(0)) {
        hasPasskeys = true;
    }

    if ($("#dashboard-sidebar-notification").get(0)) {
        hasNotifications = true;
    }


    // define steps

    tour.addStep('Intro', {
        //title: '',
        text: '<div id="tour-lightbulb-icon"></div><p>Introducing your new personalized dashboard. Would you like to take a quick tour?</p>',
        attachTo: 'body',
        classes: 'dashboard-sidebar-tour-firststep shepherd-theme-default',
        when: {
            show: function () {
                AddAnalyticsEvent('Dashboard', 'Tour', 'View');
            }
        },
        buttons: [
          {
              text: 'Yes! Let\'s go.',
              action: function () {
                  AddAnalyticsEvent('Dashboard', 'Tour', 'Continue');
                  tour.next();
                  TourADAFocus();
              },
              events: {
                  'keydown': function (e) {
                      TourButtonPress(e, tour);
                  }
              }
          },
          {
              text: 'Maybe later.',
              classes: 'shepherd-button-sec',
              action: function () {
                  AddAnalyticsEvent('Dashboard', 'Tour', 'Hide For Now');
                  tour.show('Later');
                  TourADAFocus();
              },
              events: {
                  'keydown': function (e) {
                      TourButtonPress(e, tour, 'Later');
                  }
              }
          },
          {
              text: 'No thanks. I\'ll explore on my own.',
              classes: 'shepherd-button-suboption',
              action: function () {
                  AddAnalyticsEvent('Dashboard', 'Tour', 'Hide Forever');
                  tour.show('Never');
                  TourADAFocus();
              },
              events: {
                  'keydown': function (e) {
                      TourButtonPress(e, tour, 'Never');
                  }
              }
          }
        ]
    }).addStep('Avatar', {
        text: 'Click on your user avatar to access and update your personal information and subscriptions.',
        attachTo: '#dashboard-sidebar-avatar-container right',
        when: {
            show: function() {
                $('h3.shepherd-title').attr('role', 'none'); //ADA compliance
            }
        },
        classes: 'shepherd-theme-default shepherd-element-custom',
        buttons: [
          {
              text: 'Continue',
              action: function () {
                tour.next();
                TourADAFocus();

              },
              events: {
                  'keydown': function (e) {
                      if (hasPasskeys) {
                          TourButtonPress(e, tour);
                      } else {
                          TourButtonPress(e, tour, 'Finish');
                      }
                  }
              }
          }
        ]
    }).addStep('Stream', {
        text: 'Go to your stream to see updates from your district and schools.',
        attachTo: '#dashboard-sidebar-stream right',
        buttons: [
          {
              text: 'Continue',
              action: function () {
                  if (hasPasskeys) {
                      tour.next();
                      TourADAFocus();
                  } else {
                      tour.show('Finish');
                      TourADAFocus();
                  }
              },
              events: {
                  'keydown': function (e) {
                      if(hasPasskeys){
                          TourButtonPress(e, tour);
                      } else {
                          TourButtonPress(e, tour, 'Finish');
                      }
                  }
              }
          }
        ]
    });

    if (hasPasskeys) {
        tour.addStep('Passkeys', {
            text: 'Use your passkeys to log into other district applications or websites.',
            attachTo: '#dashboard-sidebar-passkeys right',
            buttons: [
              {
                  text: 'Continue',
                  action: function () {
                      if (hasNotifications) {
                          tour.show('Notifications');
                          TourADAFocus();
                      }
                      else if (hasStudents) {
                          tour.show('Students');
                          TourADAFocus();
                      }
                      else if (isParentLinkStudent) {
                          tour.show('ParentLinkStudent');
                          TourADAFocus();
                      }
                      else {
                          tour.show('Finish');
                          TourADAFocus();
                      }
                  },
                  events: {
                      'keydown': function (e) {
                          if (hasStudents) {
                              TourButtonPress(e, tour, 'Notifications');
                          }
                          else {
                              TourButtonPress(e, tour, 'Finish');
                          }
                      }
                  }
              }
            ]
        })
    }

    if (hasNotifications) {
        tour.addStep('Notifications', {
            text: 'Open your notifications to review messages.',
            attachTo: '#dashboard-sidebar-notification right',
            buttons: [
              {
                  text: 'Continue',
                  action: function () {
                      if (hasStudents) {
                          tour.show('Students');
                          TourADAFocus();
                      }
                      else if (isParentLinkStudent) {
                          tour.show('ParentLinkStudent');
                          TourADAFocus();
                      }
                      else {
                          tour.show('Finish');
                          TourADAFocus();
                      }
                  },
                  events: {
                      'keydown': function (e) {
                          if (hasStudents) {
                              TourButtonPress(e, tour, 'Students');
                          }
                          else {
                              TourButtonPress(e, tour, 'Finish');
                          }
                      }
                  }
              }
            ]
        })
    }

    if (hasStudents) {
        tour.addStep('Students', {
            text: 'Select a student to view his or her information and records.',
            attachTo: '#dashboard-sidebar-student0 right',
            buttons: [
              {
                  text: 'Continue',
                  action: function () {
                      tour.show('Finish');
                      TourADAFocus();
                  },
                  events: {
                      'keydown': function (e) {
                          TourButtonPress(e, tour, 'Finish');
                      }
                  }
              }
            ]
        })
    }

    if (isParentLinkStudent) {
        tour.addStep('ParentLinkStudent', {
            text: 'View your information or other helpful resources.',
            attachTo: '#dashboard-sidebar-profile right',
            buttons: [
              {
                  text: 'Continue',
                  action: function () {
                      tour.show('Finish');
                      TourADAFocus();
                  },
                  events: {
                      'keydown': function (e) {
                          TourButtonPress(e, tour, 'Finish');
                      }
                  }
              }
            ]
        })
    }

    tour.addStep('Later', {
        text: 'Ok, we\'ll remind you later. You can access the tour here any time you want.',
        attachTo: '#dashboard-sidebar-help right',
        classes: 'shepherd-theme-default shepherd-element-custom-bottom',
        buttons: [
          {
              text: 'Finish',
              action: function () {
                  SetCookie("DashboardSidebarTour", "true");
                  tour.cancel();
              },
              events: {
                  'keydown': function (e) {
                      e.stopImmediatePropagation();
                      if (e.keyCode == 13) {
                          SetCookie("DashboardSidebarTour", "true");
                          tour.cancel();
                      }
                  }
              }
          }
        ]

    }).addStep('Never', {
        text: 'Ok, we won\'t ask you again. If you change your mind, you can access the tour here any time you want.',
        attachTo: '#dashboard-sidebar-help right',
        classes: 'shepherd-theme-default shepherd-element-custom-bottom',
        buttons: [
          {
              text: 'Finish',
              action: function () {
                  SetCookie("DashboardSidebarTour", "true");
                  DenyTour();
              },
              events: {
                  'keydown': function (e) {
                      e.stopImmediatePropagation();
                      if (e.keyCode == 13) {
                          SetCookie("DashboardSidebarTour", "true");
                          DenyTour();
                      }
                  }
              }
          }
        ]

    }).addStep('Finish', {
        text: 'All finished! Go here any time to view this tour again.',
        attachTo: '#dashboard-sidebar-help right',
        classes: 'shepherd-theme-default shepherd-element-custom-bottom',
        buttons: [
          {
              text: 'Finish',
              action: function () {
                  SetCookie("DashboardSidebarTour", "true");
                  FinishTour();
              },
              events: {
                  'keydown': function (e) {
                      e.stopImmediatePropagation();
                      if (e.keyCode == 13) {
                          SetCookie("DashboardSidebarTour", "true");
                          FinishTour();
                      }
                  }
              }
          }
        ]
    });

    // start tour
    tour.start();
    TourADA();
}

function TourADA() {
    $(".shepherd-content").attr("tabindex", "0");
    $(".shepherd-button").each(function () {
        var $this = $(this);
        $this.attr("title", $this.text());
        $this.attr("tabindex", "0");
    });
}

function TourADAFocus() {
    TourADA();
    $(".shepherd-content").focus();
}

function TourButtonPress(e, tour, next) {
    e.stopImmediatePropagation();
    if (e.keyCode == 13) {
        if (next == undefined) {
            tour.next();
        } else {
            tour.show(next);
        }
        TourADAFocus();
    }
}

function FinishTour() {
    var data = "";
    var success = 'ActiveTourCancel();';
    var failure = 'CallControllerFailure(result[0].errormessage);';
    CallController(homeURL + "/GlobalUserControls/DashBoardSideBar/DashBoardSideBarController.aspx/FinishTour", data, success, failure);
}

function DenyTour() {
    var data = "";
    var success = 'ActiveTourCancel();';
    var failure = 'CallControllerFailure(result[0].errormessage);';
    CallController(homeURL + "/GlobalUserControls/DashBoardSideBar/DashBoardSideBarController.aspx/DenyTour", data, success, failure);
}

function ActiveTourCancel() {
    Shepherd.activeTour.cancel();
}
// END SIDEBAR TOUR SCRIPTS

// BEGIN DOCUMENT READY
$(document).ready(function () {

    if ($('#sw-sidebar').height() > 1500) {
        $('#sw-page').css('min-height', $('#sw-sidebar').height() + 'px');
        $('#sw-inner').css('min-height', $('#sw-sidebar').height() + 'px');
    } else {
        $('#sw-page').css('min-height', $(document).height() + 'px');
        $('#sw-inner').css('min-height', $(document).height() + 'px');
    }

    $('#sw-footer').show();

    // add focus class to textboxes for IE
    $(document).on('focus', 'input', function () {
        $(this).addClass('focus');
    });

    $(document).on('blur', 'input', function () {
        $(this).removeClass('focus');
    });

    // default ajax setup
    $.ajaxSetup({
        cache: false,
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            //swalert(XMLHttpRequest.status + ' ' + textStatus + ': ' + errorThrown, 'Ajax Error', 'critical', 'ok');
            swalert("Something went wrong. We're sorry this happened. Please refresh your page and try again.", "Error", "critical", "ok");
        }
    });

    // make :Contains (case-insensitive version of :contains)
    jQuery.expr[':'].Contains = function (a, i, m) {
        return jQuery(a).text().toUpperCase().indexOf(m[3].toUpperCase()) >= 0;
    };

    // HIDE / SHOW DETAILS IN LIST SCREEN
    $(document).on('click', 'span.ui-show-detail', function () {
        var $this = $(this);

        if ($this.hasClass('open')) {
            // do nothing
        } else {
            $this.addClass('open').parent('div.ui-article-header').nextAll('div.ui-article-detail').slideDown(function () {
                // add close button
                $this.append("<span class='ui-article-detail-close'></span>");
            });
        }
    });

    $(document).on('click', 'span.ui-article-detail-close', function () {
        $this = $(this);
        $this.parents('.ui-article-header').nextAll('.ui-article-detail').slideUp(function () {
            $this.parent('.ui-show-detail').removeClass('open');
            // remove close button
            $this.remove();
        });
    });


    // LIST/EXPANDED VIEW ON LIST PAGES
    $(document).on('click', '#show-list-view', function () {
        $(this).addClass('ui-btn-toolbar-primary').removeClass('ui-btn-toolbar');
        $('#show-expanded-view').addClass('ui-btn-toolbar').removeClass('ui-btn-toolbar-primary');
        $('div.ui-article-detail').slideUp(function () {
            // remove close buttons
            $(this).prevAll('.ui-article-header').find('.ui-article-detail-close').remove();
            //$this.children('.ui-article-detail-close').remove();
        });
        $('span.ui-show-detail').removeClass('open');
    });

    $(document).on('click', '#show-expanded-view', function (i) {
        $(this).addClass('ui-btn-toolbar-primary').removeClass('ui-btn-toolbar');
        $('#show-list-view').addClass('ui-btn-toolbar').removeClass('ui-btn-toolbar-primary');
        $('div.ui-article-detail').slideDown('', function () {
            // add close buttons
            $(this).prevAll('.ui-article-header').children('.ui-show-detail').append("<span class='ui-article-detail-close'></span>");
        });
        $('span.ui-show-detail').addClass('open');
    });

    //Important Message Ok, got it tab=0
    $('#onscreenalert-ctrl-gotit').keydown(function (e) {
        e.stopImmediatePropagation();
        if (e.keyCode == 13) {
            $(this).click();
        }
    });

    $('#onscreenalert-message-dialog-icon').keydown(function (e) {
        e.stopImmediatePropagation();
        if (e.keyCode == 13) {
            $(this).click();
        }
    });
    
}); // end document ready

// load scripts after everything else
$(window).on('load',  IncludeDocReadyScripts);
﻿/// <reference path="../../../../Scripts/Slick/slick.min.js" />
/// <reference path="../../../../Scripts/Slick/slick.min.js" />
/// <reference path="../../../../Scripts/Slick/slick.min.js" />
/// <reference path="../../../../Scripts/Slick/slick.min.js" />
// Check for included script
function CheckScript(ModuleName, ScriptSRC, FunctionName) {

    var loadScriptFile = true;

    switch (ModuleName.toLowerCase()) {
        case 'sectionrobot':
            FunctionName = 'CheckSectionRobotScript';
            ScriptSRC = homeURL + '/cms/Tools/SectionRobot/SectionRobot.js';
            break;
        case 'assignments':
            FunctionName = 'CheckAssignmentsScript';
            ScriptSRC = homeURL + '/cms/Module/Assignments/Assignments.js';
            break;
        case 'spacedirectory':
            FunctionName = 'CheckSpaceDirectoryScript';
            ScriptSRC = homeURL + '/cms/Module/SpaceDirectory/SpaceDirectory.js';
            break;
        case 'calendar':
            FunctionName = 'CheckCalendarScript';
            ScriptSRC = homeURL + '/cms/Module/Calendar/Calendar.js';
            break;
        case 'fullcalendar':
            FunctionName = '$.fn.fullCalendar';
            ScriptSRC = staticURL + '/GlobalAssets/Scripts/ThirdParty/jquery.fullcalendar1.6.1.js';
            break;
        case 'links':
            FunctionName = 'CheckLinksScript';
            ScriptSRC = staticURL + '/cms/Module/Links/Links.js';
            break;
        case 'minibase':
            FunctionName = 'CheckMinibaseScript';
            ScriptSRC = homeURL + '/cms/Module/Minibase/Minibase.js';
            break;
        case 'moduleinstance':
            var randNum = Math.floor(Math.random() * (1000 - 10 + 1) + 1000);
            FunctionName = 'CheckModuleInstanceScript';
            ScriptSRC = homeURL + '/cms/Module/ModuleInstance/ModuleInstance.js?rand=' + randNum;
            break;
        case 'photogallery':
            FunctionName = 'CheckPhotoGalleryScript';
            ScriptSRC = staticURL + '/cms/Module/PhotoGallery/PhotoGallery_2520.js';
            break;
        case 'comments':
            FunctionName = 'CheckModerateCommentsScript';
            ScriptSRC = homeURL + '/cms/Tools/ModerateComments/ModerateComments.js';
            break;
        case 'postings':
            FunctionName = 'CheckModeratePostingsScript';
            ScriptSRC = homeURL + '/cms/Tools/ModerateContribution/ModerateContribution.js';
            break;
        case 'myaccount':
            FunctionName = 'CheckMyAccountScript';
            ScriptSRC = homeURL + '/cms/UserControls/MyAccount/MyAccount.js';
            break;
        case 'formsurvey':
            FunctionName = 'CheckFormSurveyScript';
            ScriptSRC = homeURL + '/cms/tools/FormsAndSurveys/Surveys.js';
            break;
        case 'alerts':
            FunctionName = 'CheckAlertsScript';
            ScriptSRC = homeURL + '/cms/tools/Alerts/Alerts.js';
            break;
        case 'onscreenalerts':
            FunctionName = 'CheckOnScreenalertsScript';
            ScriptSRC = homeURL + '/cms/tools/OnScreenAlerts/OnScreenAlerts.js';
            break;
        case 'workspace':
            FunctionName = 'CheckWorkspaceScript';
            ScriptSRC = staticURL + '/cms/Workspace/PageList_2680.js';
            break;
        case 'moduleview':
            FunctionName = 'CheckModuleViewScript';
            ScriptSRC = staticURL + '/GlobalAssets/Scripts/min/ModuleViewRenderer_2640.js';
            break;
        case 'pageeditingmoduleview':
            FunctionName = 'CheckPageEditingModuleViewScript';
            //ScriptSRC = staticURL + '/GlobalAssets/Scripts/min/PageEditingModuleViewRenderer.js';
            ScriptSRC = homeURL + '/cms/UserControls/ModuleView/PageEditingModuleViewRenderer_2640.js';
            break;
        case 'editarea':
            FunctionName = 'CheckEditAreaScript';
            ScriptSRC = homeURL + '/GlobalUserControls/EditArea/edit_area_full.js';
            break;
        case 'rating':
            FunctionName = '$.fn.rating';
            ScriptSRC = staticURL + '/GlobalAssets/Scripts/min/jquery.rating.min.js';
            break;
        case 'metadata':
            FunctionName = '$.fn.metadata';
            ScriptSRC = staticURL + '/GlobalAssets/Scripts/min/jquery.metadata.min.js';
            break;
        case 'pwcalendar':
            FunctionName = 'CheckPWCalendarScript';
            ScriptSRC = homeURL + '/myview/UserControls/Calendar/Calendar.js';
            break;
        case 'importwizard':
            FunctionName = 'CheckImportWizardScript';
            ScriptSRC = homeURL + '/cms/UserControls/ImportDialog/ImportWizard.js';
            break;
        case 'mustache':
            FunctionName = 'CheckMustacheScript';
            ScriptSRC = staticURL + '/GlobalAssets/Scripts/ThirdParty/mustache.js';
            break;
        case 'slick':
            FunctionName = 'CheckSlickScript';
            ScriptSRC = staticURL + '/GlobalAssets/Scripts/ThirdParty/Slick/slick.min.js';
            break;
        case 'galleria':
            FunctionName = 'CheckGalleriaScript';
            ScriptSRC = staticURL + '/GlobalAssets/Scripts/ThirdParty/galleria-custom-129_2520/galleria-1.2.9.min.js';
            break;
        case 'fine-uploader':
            FunctionName = 'CheckFineUploaderScript';
            ScriptSRC = staticURL + '/GlobalAssets/Scripts/ThirdParty/fine-uploader/fine-uploader.min.js';
            break;
        case 'tinymce':
            FunctionName = 'tinyMCE';
            ScriptSRC = homeURL + '/cms/Module/selectsurvey/ClientInclude/tinyMCE/jquery.tinymce.min.js';
            break;
        case 'attachmentview':
            FunctionName = 'CheckAttachmentScript';
            ScriptSRC = homeURL + '/GlobalUserControls/Attachment/AttachmentView.js';
            break;
        default:
            // module name not found
            if (ScriptSRC !== undefined && ScriptSRC !== null && ScriptSRC.length > 0) {
                // script src was specified in parameter
                if (FunctionName === undefined && ModuleName.length > 0) {
                    // default the function name for lookup from the module name
                    FunctionName = 'Check' + ModuleName + 'Script';
                }
            } else {
                // can't load a script file without at least a src and function name to test
                loadScriptFile = false;
            }
            break;
    }

    if (loadScriptFile === true) {
        try {
            if (eval("typeof " + FunctionName + " == 'function'")) {
                // do nothing, it's already included
            } else {
                var script = document.createElement('script');script.type = 'text/javascript';script.src = ScriptSRC;$('head').append(script);;
            }
        } catch (err) {
        }
    }
}
// End Check for included script
</script><!-- End swuc.CheckScript -->


    <!-- Server Load Time (04): 0.204216 Seconds -->

    

    <!-- off-canvas menu enabled-->
    

    <!-- Ally Alternative Formats Configure START   -->
    
    <!-- Ally Alternative Formats Configure END     -->

</body>
</html>
