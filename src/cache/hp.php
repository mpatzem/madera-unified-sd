
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Frameset//EN" "http://www.w3.org/TR/html4/frameset.dtd">

<html lang="en">
<head>
    <title>Fletch_Test / Homepage</title>
    <!--
    <PageMap>
    <DataObject type="document">
    <Attribute name="siteid">4</Attribute>
    </DataObject>
    </PageMap>
    -->
    

<script>
(function(apiKey){
    (function(p,e,n,d,o){var v,w,x,y,z;o=p[d]=p[d]||{};o._q=[];
    v=['initialize','identify','updateOptions','pageLoad'];for(w=0,x=v.length;w<x;++w)(function(m){
        o[m]=o[m]||function(){o._q[m===v[0]?'unshift':'push']([m].concat([].slice.call(arguments,0)));};})(v[w]);
        y=e.createElement(n);y.async=!0;y.src='https://cdn.pendo.io/agent/static/'+apiKey+'/pendo.js';
        z=e.getElementsByTagName(n)[0];z.parentNode.insertBefore(y,z);})(window,document,'script','pendo');

        // Call this whenever information about your visitors becomes available
        // Please use Strings, Numbers, or Bools for value types.
        pendo.initialize({
            visitor: {
                id: 'SWCS000010-cngv0iEqRbHQc+9Am84vTQ==',   // Required if user is logged in
                role: 'Anonymous',
                isPartOfGroup: 'False',
                // email:        // Optional
                // You can add any additional visitor level key-values here,
                // as long as it's not one of the above reserved names.
            },

            account: {
                id: 'SWCS000010', // Highly recommended
                version: '2.70',
                name:         'mpatzem.schoolwires.net'
                // planLevel:    // Optional
                // planPrice:    // Optional
                // creationDate: // Optional

                // You can add any additional account level key-values here,
                // as long as it's not one of the above reserved names.
            }
        });
})('ca0f531d-af61-45a7-7c9a-079f24d9128a');
</script>

    
    <meta property="og:type" content="website" />
<meta property="fb:app_id" content="411584262324304" />
<meta property="og:url" content="http%3A%2F%2Fmpatzem.schoolwires.net%2Fsite%2Fdefault.aspx%3FPageID%3D1" />
<meta property="og:title" content="Fletch_Test / Homepage" />
<meta name="twitter:card" value="summary" />
<meta name="twitter:title" content="Fletch_Test / Homepage" />
<meta itemprop="name" content="Fletch_Test / Homepage" />

    <!-- Begin swuc.GlobalJS -->
<script type="text/javascript">
 staticURL = "https://mpatzem.schoolwires.net/Static/";
 SessionTimeout = "50";
 BBHelpURL = "";
</script>
<!-- End swuc.GlobalJS -->

    <script src='https://mpatzem.schoolwires.net/Static/GlobalAssets/Scripts/min/sri-failover.min.js' type='text/javascript'></script>

    <!-- Stylesheets -->
    <link rel="Stylesheet" type="text/css" href="https://mpatzem.schoolwires.net/Static/GlobalAssets/webfonts/OpenSans-Light.css" />
    <link rel="Stylesheet" type="text/css" href="https://mpatzem.schoolwires.net/Static/GlobalAssets/webfonts/OpenSans-Italic.css" />
    <link rel="Stylesheet" type="text/css" href="https://mpatzem.schoolwires.net/Static/GlobalAssets/webfonts/OpenSans-Regular.css" />
    <link rel="Stylesheet" type="text/css" href="https://mpatzem.schoolwires.net/Static/GlobalAssets/webfonts/OpenSans-SemiBold.css" />
    <link rel="Stylesheet" type="text/css" href="https://mpatzem.schoolwires.net/Static/GlobalAssets/Scripts/ThirdParty/shepherd/shepherd-theme-default.css" />
    <link rel="Stylesheet" type="text/css" href="https://mpatzem.schoolwires.net/Static/App_Themes/SW/jquery.jgrowl.css" />
    <link rel="Stylesheet" type="text/css" href="https://mpatzem.schoolwires.net/Static//site/assets/styles/system_2660.css" />
    <link rel="Stylesheet" type="text/css" href="https://mpatzem.schoolwires.net/Static//site/assets/styles/apps_2590.css" />
    <link rel="Stylesheet" type="text/css" href="https://mpatzem.schoolwires.net/Static/App_Themes/SW/jQueryUI.css" />
    <link rel="Stylesheet" type="text/css" href="https://mpatzem.schoolwires.net/Static/GlobalAssets/webfonts/SchoolwiresMobile_2320.css" />
    
    <link rel="Stylesheet" type="text/css" href="https://mpatzem.schoolwires.net/Static/GlobalAssets/Styles/Grid.css" />

    <!-- Scripts -->
    <script src="https://mpatzem.schoolwires.net/Static/GlobalAssets/WCM-2680/WCM.js" type="text/javascript"></script>
    <script src="https://mpatzem.schoolwires.net/Static/GlobalAssets/WCM-2680/API.js" type="text/javascript"></script>
    <script src='https://mpatzem.schoolwires.net/Static/GlobalAssets/Scripts/min/jquery-3.0.0.min.js' type='text/javascript'></script>
    <script src='https://mpatzem.schoolwires.net/Static//GlobalAssets/Scripts/min/jquery-migrate-1.4.1.min.js' type='text/javascript'></script
    <script src='https://ajax.googleapis.com/ajax/libs/swfobject/2.2/swfobject.js' type='text/javascript'
        integrity='sha384-JO4qIitDJfdsiD2P0i3fG6TmhkLKkiTfL4oVLkVFhGs5frz71Reviytvya4wIdDW' crossorigin='anonymous'
        data-sri-failover='https://mpatzem.schoolwires.net/Static/GlobalAssets/Scripts/min/swfobject.js'></script>
    <script src='https://mpatzem.schoolwires.net/Static/GlobalAssets/Scripts/ThirdParty/tether/tether.min.js' type='text/javascript'></script>
    <script src='https://mpatzem.schoolwires.net/Static/GlobalAssets/Scripts/ThirdParty/shepherd/shepherd.min.js' type='text/javascript'></script>
   
    <script type="text/javascript">
        $(document).ready(function () {
            SetCookie('SWScreenWidth', screen.width);
            SetCookie('SWClientWidth', document.body.clientWidth);
            
            $("div.ui-article:last").addClass("last-article");
            $("div.region .app:last").addClass("last-app");

            // get on screen alerts
            var isAnyActiveOSA = 'False';
            var onscreenAlertCookie = GetCookie('Alerts');

            if (onscreenAlertCookie == '' || onscreenAlertCookie == undefined) {
                onscreenAlertCookie = "";
            }
            if (isAnyActiveOSA == 'True') {
                GetContent(homeURL + "/cms/Tools/OnScreenAlerts/UserControls/OnScreenAlertDialogListWrapper.aspx?OnScreenAlertCookie=" + onscreenAlertCookie + "&SiteID=4", "onscreenalert-holder", 2, "OnScreenAlertCheckListItem();");
            }            

        });

    // ADA SKIP NAV
    $(document).ready(function () {
        $(document).on('focus', '#skipLink', function () {
            $("div.sw-skipnav-outerbar").animate({
                marginTop: "0px"
            }, 500);
        });

        $(document).on('blur', '#skipLink', function () {
            $("div.sw-skipnav-outerbar").animate({
                marginTop: "-30px"
            }, 500);
        });
    });

    // ADA MYSTART
    $(document).ready(function () {
        var top_level_nav = $('.sw-mystart-nav');

        // Set tabIndex to -1 so that top_level_links can't receive focus until menu is open
        // school dropdown
        $(top_level_nav).find('ul').find('a').attr('tabIndex', -1);

        // my account dropdown
        $(top_level_nav).next('ul').find('a').attr('tabIndex', -1);

        var openNavCallback = function(e, element) {
             // hide open menus
            hideMyStartBarMenu();

            // show school dropdown
            if ($(element).find('ul').length > 0) {
                $(element).find('.sw-dropdown').css('display', 'block').attr('aria-hidden', 'false');
                $(element).find('.sw-dropdown').find('li:first-child a').focus()
            }

            // show my account dropdown
            if ($(element).next('ul').length > 0) {
                $(element).next('.sw-dropdown').css('display', 'block').attr('aria-hidden', 'false');
                $(element).next('.sw-dropdown').find('li:first-child a').focus();
                $('#sw-mystart-account').addClass("clicked-state");
            }
        }

        $(top_level_nav).click(function (e) {
            openNavCallback(e, this);
        });

        $('.sw-dropdown-list li').click(function(e) {
            e.stopImmediatePropagation();
            $(this).focus();
        });
        
        // Bind arrow keys for navigation
        $(top_level_nav).keydown(function (e) {
            if (e.keyCode == 37) { //key left
                e.preventDefault();

                // This is the first item
                if ($(this).prev('.sw-mystart-nav').length == 0) {
                    $(this).parents('div').find('.sw-mystart-nav').last().focus();
                } else {
                    $(this).prev('.sw-mystart-nav').focus();
                }
            } else if (e.keyCode == 38) { //key up
                e.preventDefault();

                // show school dropdown
                if ($(this).find('ul').length > 0) {
                    $(this).find('div.sw-dropdown').css('display', 'block').find('ul').attr('aria-hidden', 'false').find('a').attr('tabIndex', 0).last().focus();
                }

                // show my account dropdown
                if ($(this).find('ul').length > 0) {
                    $(this).find('ul.sw-dropdown').css('display', 'block').attr('aria-hidden', 'false').find('a').attr('tabIndex', 0).last().focus();
                }
            } else if (e.keyCode == 39) { //key right
                e.preventDefault();

                // This is the last item
                if ($(this).next('.sw-mystart-nav').length == 0) {
                    $(this).parents('div').find('.sw-mystart-nav').first().focus();
                } else {
                    $(this).next('.sw-mystart-nav').focus();
                }
            } else if (e.keyCode == 40) { //key down
                e.preventDefault();

                // show school dropdown
                if ($(this).find('ul').length > 0) {
                    $(this).find('div.sw-dropdown').css('display', 'block').find('ul').attr('aria-hidden', 'false').find('a').attr('tabIndex', 0).first().focus();
                }

                // show my account dropdown
                if ($(this).next('ul').length > 0) {
                    $(this).next('ul.sw-dropdown').css('display', 'block').attr('aria-hidden', 'false').find('a').attr('tabIndex', 0).first().focus();
                }
            } else if (e.keyCode == 13 || e.keyCode == 32) { //enter key
                // If submenu is hidden, open it
                e.preventDefault();

                
                openNavCallback(e, this);
                $(this).parent('li').find('ul[aria-hidden=true]').attr('aria-hidden', 'false').find('a').attr('tabIndex', 0).first().focus();
            } else if (e.keyCode == 27) { //escape key
                e.preventDefault();
                hideMyStartBarMenu();
            } else {
                $(this).parent('.sw-mystart-nav').find('ul[aria-hidden=false] a').each(function () {
                    if (typeof keyCodeMap != "undefined" && $(this).text().substring(0, 1).toLowerCase() == keyCodeMap[e.keyCode]) {
                        $(this).focus();
                        return false;
                    }
                });
            }
        });

        // school dropdown
        var startbarlinks = $(top_level_nav).find('ul').find('a');
        bindMyStartBarLinks(startbarlinks);

        // my account dropdown
        var myaccountlinks = $(top_level_nav).next('ul').find('a');
        bindMyStartBarLinks(myaccountlinks);

        function bindMyStartBarLinks(links) {
            $(links).keydown(function (e) {
                e.stopPropagation();

                if (e.keyCode == 38) { //key up
                    e.preventDefault();

                    // This is the first item
                    if ($(this).parent('li').prev('li').length == 0) {
                        if ($(this).parents('ul').parents('.sw-mystart-nav').length > 0) {
                            $(this).parents('ul').parents('.sw-mystart-nav').focus();
                        } else {
                            $(this).parents('ul').prev('.sw-mystart-nav').focus();
                        }
                    } else {
                        $(this).parent('li').prev('li').find('a').first().focus();
                    }
                } else if (e.keyCode == 40) { //key down
                    e.preventDefault();

                    if ($(this).parent('li').next('li').length == 0) {
                        if ($(this).parents('ul').parents('.sw-mystart-nav').length > 0) {
                            $(this).parents('ul').parents('.sw-mystart-nav').focus();
                        } else {
                            $(this).parents('ul').prev('.sw-mystart-nav').focus();
                        }
                    } else {
                        $(this).parent('li').next('li').find('a').first().attr('tabIndex', 0);
                        $(this).parent('li').next('li').find('a').first().focus();
                    }
                } else if (e.keyCode == 27 || e.keyCode == 37) { // escape key or key left
                    e.preventDefault();
                    hideMyStartBarMenu();
                } else if (e.keyCode == 32) { //enter key
                    e.preventDefault();
                    window.location = $(this).attr('href');
                } else {
                    var found = false;

                    $(this).parent('div').nextAll('li').find('a').each(function () {
                        if (typeof keyCodeMap != "undefined" && $(this).text().substring(0, 1).toLowerCase() == keyCodeMap[e.keyCode]) {
                            $(this).focus();
                            found = true;
                            return false;
                        }
                    });

                    if (!found) {
                        $(this).parent('div').prevAll('li').find('a').each(function () {
                            if (typeof keyCodeMap != "undefined" && $(this).text().substring(0, 1).toLowerCase() == keyCodeMap[e.keyCode]) {
                                $(this).focus();
                                return false;
                            }
                        });
                    }
                }
            });
        }
        
        // Hide menu if click or focus occurs outside of navigation
        $('#sw-mystart-inner').find('.sw-mystart-nav').last().keydown(function (e) {
            if (e.keyCode == 9) {
                // If the user tabs out of the navigation hide all menus
                hideMyStartBarMenu();
            }
        });

        /*$(document).click(function() { 
            hideMyStartBarMenu();
        });*/

        // try to capture as many custom MyStart bars as possible
        $('.sw-mystart-button').find('a').focus(function () {
            hideMyStartBarMenu();
        });

        $('#sw-mystart-inner').click(function (e) {
            e.stopPropagation();
        });

        $('ul.sw-dropdown-list').blur(function () {
            hideMyStartBarMenu();
        });

        $('#ui-btn-mypasskey').focus(function () {
            hideMyStartBarMenu();
        });

        $('#ui-btn-sitemanager').focus(function () {
            hideMyStartBarMenu();
        });

        $('#ui-btn-myview').focus(function () {
            hideMyStartBarMenu();
        });

        $('#ui-btn-signin').focus(function () {
            hideMyStartBarMenu();
        });

        $('#ui-btn-register').focus(function () {
            hideMyStartBarMenu();
        });

        // button click events
        $('div.sw-mystart-button.home a').keydown(function (e) {
            e.stopImmediatePropagation();

            if (e.keyCode == 13) {
                $(this).click();
            }
        });

        $('div.sw-mystart-button.pw a').keydown(function (e) {
            e.stopImmediatePropagation();

            if (e.keyCode == 13) {
                $(this).click();
            }
        });

        $('div.sw-mystart-button.manage a').keydown(function (e) {
            e.stopImmediatePropagation();

            if (e.keyCode == 13) {
                $(this).click();
            }
        });

        $('#sw-mystart-account').keydown(function (e) {
            e.stopImmediatePropagation();

            if (e.keyCode == 13) {
                $(this).addClass('clicked-state');
                $('#sw-myaccount-list').show();
            }
        });

        $('#sw-mystart-mypasskey a').keydown(function (e) {
            e.stopImmediatePropagation();

            if (e.keyCode == 13) {
                $(this).click();
            }
        });

        $('div.sw-mystart-button.signin a').keydown(function (e) {
            e.stopImmediatePropagation();

            if (e.keyCode == 13) {
                $(this).click();
            }
        });

        $('div.sw-mystart-button.register a').keydown(function (e) {
            e.stopImmediatePropagation();

            if (e.keyCode == 13) {
                $(this).click();
            }
        });
    });

    function hideMyStartBarMenu() {
        $('.sw-dropdown').attr('aria-hidden', 'true').css('display', 'none');
        $('#sw-mystart-account').removeClass("clicked-state");
    }

    // ADA CHANNEL NAV
    $(document).ready(function() {
        var channelCount;
        var channelIndex = 1;
        var settings = {
            menuHoverClass: 'hover'
        };

        // Add ARIA roles to menubar and menu items
        $('[id="channel-navigation"]').attr('role', 'menubar').find('li a').attr('role', 'menuitem').attr('tabindex', '0');

        var top_level_links = $('[id="channel-navigation"]').find('> li > a');
        channelCount = $(top_level_links).length;


        $(top_level_links).each(function() {
            $(this).attr('aria-posinset', channelIndex).attr('aria-setsize', channelCount);
            $(this).next('ul').attr({ 'aria-hidden': 'true', 'role': 'menu' });

            if ($(this).parent('li.sw-channel-item').children('ul').length > 0) {
                $(this).attr('aria-haspopup', 'true');
            }

            var sectionCount = $(this).next('ul').find('a').length;
            var sectionIndex = 1;
            $(this).next('ul').find('a').each(function() {
                $(this).attr('tabIndex', -1).attr('aria-posinset', sectionIndex).attr('aria-setsize', sectionCount);
                sectionIndex++;
            });
            channelIndex++;

        });

        $(top_level_links).focus(function () {
            //hide open menus
            hideChannelMenu();

            if ($(this).parent('li').find('ul').length > 0) {
                $(this).parent('li').addClass(settings.menuHoverClass).find('ul').attr('aria-hidden', 'false').css('display', 'block');
            }
        });

        // Bind arrow keys for navigation
        $(top_level_links).keydown(function (e) {
            if (e.keyCode == 37) { //key left
                e.preventDefault();

                // This is the first item
                if ($(this).parent('li').prev('li').length == 0) {
                    $(this).parents('ul').find('> li').last().find('a').first().focus();
                } else {
                    $(this).parent('li').prev('li').find('a').first().focus();
                }
            } else if (e.keyCode == 38) { //key up
                e.preventDefault();

                if ($(this).parent('li').find('ul').length > 0) {
                    $(this).parent('li').addClass(settings.menuHoverClass).find('ul').css('display', 'block').attr('aria-hidden', 'false').find('a').attr('tabIndex', 0).last().focus();
                }
            } else if (e.keyCode == 39) { //key right
                e.preventDefault();

                // This is the last item
                if ($(this).parent('li').next('li').length == 0) {
                    $(this).parents('ul').find('> li').first().find('a').first().focus();
                } else {
                    $(this).parent('li').next('li').find('a').first().focus();
                }
            } else if (e.keyCode == 40) { //key down
                e.preventDefault();

                if ($(this).parent('li').find('ul').length > 0) {
                    $(this).parent('li')
                         .addClass(settings.menuHoverClass)
                         .find('ul.sw-channel-dropdown').css('display', 'block')
                         .attr('aria-hidden', 'false')
                         .find('a').attr('tabIndex', 0)
                         .first().focus();
                }
            } else if (e.keyCode == 13 || e.keyCode == 32) { //enter key
                // If submenu is hidden, open it
                e.preventDefault();

                $(this).parent('li').find('ul[aria-hidden=true]').attr('aria-hidden', 'false').addClass(settings.menuHoverClass).find('a').attr('tabIndex', 0).first().focus();
            } else if (e.keyCode == 27) { //escape key
                e.preventDefault();
                hideChannelMenu();
            } else {
                $(this).parent('li').find('ul[aria-hidden=false] a').each(function () {
                    if (typeof keyCodeMap != "undefined" && $(this).text().substring(0, 1).toLowerCase() == keyCodeMap[e.keyCode]) {
                        $(this).focus();
                        return false;
                    }
                });
            }
        });

        var links = $(top_level_links).parent('li').find('ul').find('a');

        $(links).keydown(function (e) {
            if (e.keyCode == 38) {
                e.preventDefault();

                // This is the first item
                if ($(this).parent('li').prev('li').length == 0) {
                    $(this).parents('ul').parents('li').find('a').first().focus();
                } else {
                    $(this).parent('li').prev('li').find('a').first().focus();
                }
            } else if (e.keyCode == 40) {
                e.preventDefault();

                if ($(this).parent('li').next('li').length == 0) {
                    $(this).parents('ul').parents('li').find('a').first().focus();
                } else {
                    $(this).parent('li').next('li').find('a').first().focus();
                }
            } else if (e.keyCode == 27 || e.keyCode == 37) {
                e.preventDefault();
                $(this).parents('ul').first().prev('a').focus().parents('ul').first().find('.' + settings.menuHoverClass).removeClass(settings.menuHoverClass);
            } else if (e.keyCode == 32) {
                e.preventDefault();
                window.location = $(this).attr('href');
            } else {
                var found = false;

                $(this).parent('li').nextAll('li').find('a').each(function () {
                    if (typeof keyCodeMap != "undefined" && $(this).text().substring(0, 1).toLowerCase() == keyCodeMap[e.keyCode]) {
                        $(this).focus();
                        found = true;
                        return false;
                    }
                });

                if (!found) {
                    $(this).parent('li').prevAll('li').find('a').each(function () {
                        if (typeof keyCodeMap != "undefined" && $(this).text().substring(0, 1).toLowerCase() == keyCodeMap[e.keyCode]) {
                            $(this).focus();
                            return false;
                        }
                    });
                }
            }
        });

        function hideChannelMenu() {
            $('li.sw-channel-item.' + settings.menuHoverClass).removeClass(settings.menuHoverClass).find('ul').attr('aria-hidden', 'true').css('display', 'none').find('a').attr('tabIndex', -1);
        }
        
        // Hide menu if click or focus occurs outside of navigation
        $('[id="channel-navigation"]').find('a').last().keydown(function (e) {
            if (e.keyCode == 9) {
                // If the user tabs out of the navigation hide all menus
                hideChannelMenu();
            }
        });

        $('[id="channel-navigation"]').find('a').first().keydown(function (e) {
            if (e.keyCode == 9) {
                // hide open MyStart Bar menus
                hideMyStartBarMenu();
            }
        });

        /*$(document).click(function() {
            hideChannelMenu();
        });*/

        $('[id="channel-navigation"]').click(function (e) {
            e.stopPropagation();
        });
    });

    $(document).ready(function() {
        $('input.required').each(function() {
            if ($('label[for="' + $(this).attr('id') + '"]').length > 0) {
                if ($('label[for="' + $(this).attr('id') + '"]').html().indexOf('recStar') < 0) {
                    $('label[for="' + $(this).attr('id') + '"]').prepend('<span class="recStar" aria-label="required item">*</span> ');
                }
            }
        });

        $(document).ajaxComplete(function() {
            $('input.required').each(function() {
                if ($('label[for="' + $(this).attr('id') + '"]').length > 0) {
                    if ($('label[for="' + $(this).attr('id') + '"]').html().indexOf('recStar') < 0) {
                        $('label[for="' + $(this).attr('id') + '"]').prepend('<span class="recStar" aria-label="required item">*</span> ');
                    }
                }
            });
        });
    });
    </script>

    <!-- Page -->
    
    <style type="text/css">/* MedaiBegin Standard *//* GroupBegin EditorStyles */
html {
	font-size: 16px;
	line-height: 1.75;
}
/* GroupBegin EditorStyles */
body {
	font-family: $bodyFont;
	font-weight: 400;
	color: #000000;
	text-rendering: optimizeLegibility !important;
	-webkit-font-smoothing: antialiased !important;
	-moz-font-smoothing: antialiased !important;
	font-smoothing: antialiased !important;
	-moz-osx-font-smoothing: grayscale;
}
h1 {
	font-size: 1.625rem;
	font-weight: 700;
}
h2 {
	color:#094f84;
	font-weight: 700;
	font-size: 1.5rem;
}
h3 {
	color:#094f84;
	font-weight: 700;
	font-size: 1.3125rem;
}
h4 {
	color:#094f84;
	font-weight: 700;
	font-size: 1.125rem;
}

.Box_Title {
	font-size: 1.25rem;
	font-weight: 500;
	display: inline-block;
	margin-bottom: 6px;
}
.Border_Box {
	border:1px solid #d5d5d5;
	padding:27px 30px;
	display:block;
}
.Primary_Box {
	background:#084F84;
	color:#fff;
	padding:27px 30px;
	display:block;
}
.Secondary_Box {
	background:#1072BF;
	color:#fff;
	padding:27px 30px;
	display:block;
}
.Featured_Button {
	background:#1072BF;
	color:#fff;
	padding:27px 30px;
	display:block;
}
/* GroupEnd */

.flexpage .ui-article .Primary_Box a,
.flexpage .ui-article .Secondary_Box a {
	color:#fff;
}
 /* GroupEnd *//* MediaEnd */</style>
    

    <!-- App Preview -->
    


    <style type="text/css">
        /* HOMEPAGE EDIT THUMBNAIL STYLES */

        div.region {
            ;
        }

            div.region span.homepage-thumb-region-number {
                font: bold 100px verdana;
                color: #fff;
            }

        div.homepage-thumb-region {
            background: #264867; /*dark blue*/
            border: 5px solid #fff;
            text-align: center;
            padding: 40px 0 40px 0;
            display: block;
        }

            div.homepage-thumb-region.region-1 {
                background: #264867; /*dark blue*/
            }

            div.homepage-thumb-region.region-2 {
                background: #5C1700; /*dark red*/
            }

            div.homepage-thumb-region.region-3 {
                background: #335905; /*dark green*/
            }

            div.homepage-thumb-region.region-4 {
                background: #B45014; /*dark orange*/
            }

            div.homepage-thumb-region.region-5 {
                background: #51445F; /*dark purple*/
            }

            div.homepage-thumb-region.region-6 {
                background: #3B70A0; /*lighter blue*/
            }

            div.homepage-thumb-region.region-7 {
                background: #862200; /*lighter red*/
            }

            div.homepage-thumb-region.region-8 {
                background: #417206; /*lighter green*/
            }

            div.homepage-thumb-region.region-9 {
                background: #D36929; /*lighter orange*/
            }

            div.homepage-thumb-region.region-10 {
                background: #6E5C80; /*lighter purple*/
            }

        /* END HOMEPAGE EDIT THUMBNAIL STYLES */
    </style>

    <style type="text/css" media="print">
        .noprint {
            display: none !important;
        }
    </style>

    <style type ="text/css">
        .ui-txt-validate {
            display: none;
        }
    </style>

    

<!-- Begin Schoolwires Traffic Code --> 

<script type="text/javascript">

    (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
            (i[r].q = i[r].q || []).push(arguments)
        }, i[r].l = 1 * new Date(); a = s.createElement(o),
        m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
    })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

    ga('create', 'UA-5173826-6', 'auto', 'BBTracker' );
    ga('BBTracker.set', 'dimension1', 'AWS');
    ga('BBTracker.set', 'dimension2', 'False');
    ga('BBTracker.set', 'dimension3', 'SWCS000010');
    ga('BBTracker.set', 'dimension4', '4');
    ga('BBTracker.set', 'dimension5', '1');
    ga('BBTracker.set', 'dimension6', '1');

    ga('BBTracker.send', 'pageview');

</script>

<!-- End Schoolwires Traffic Code --> 

    <!-- Ally Alternative Formats Loader    START   -->
    
    <!-- Ally Alternative Formats Loader    END     -->

</head>

        <body data-translate="google">
    

    <input type="hidden" id="hidFullPath" value="https://mpatzem.schoolwires.net/" />
    <input type="hidden" id="hidActiveChannelNavType" value="-1" />
    <input type="hidden" id="hidActiveChannel" value ="0" />
    <input type="hidden" id="hidActiveSection" value="0" />

    <!-- OnScreen Alert Dialog Start -->
    <div id="onscreenalert-holder"></div>
    <!-- OnScreen Alert Dialog End -->

    <!-- ADA Skip Nav -->
    <div class="sw-skipnav-outerbar">
        <a href="#sw-maincontent" id="skipLink" class="sw-skipnav" tabindex="0">Skip to Main Content</a>
    </div>

    <!-- DashBoard SideBar Start -->
    
    <!-- DashBoard SideBar End -->

    <!-- off-canvas menu enabled-->
    

    

<style type="text/css">
	/* SPECIAL MODE BAR */
	div.sw-special-mode-bar {
		background: #FBC243 url('https://mpatzem.schoolwires.net/Static//GlobalAssets/Images/special-mode-bar-background.png') no-repeat;
		height: 30px;
		text-align: left;
		font-size: 12px;
		position: relative;
		z-index: 10000;
	}
	div.sw-special-mode-bar > div {
		padding: 8px 0 0 55px;
		font-weight: bold;
	}
	div.sw-special-mode-bar > div > a {
		margin-left: 20px;
		background: #A0803D;
		-moz-border-radius: 5px;
		-webkit-border-radius: 5px;
		color: #fff;
		padding: 4px 6px 4px 6px;
		font-size: 11px;
	}

	/* END SPECIAL MODE BAR */
</style>

<script type="text/javascript">
	
	function SWEndPreviewMode() { 
		var data = "{}";
		var success = "window.location='';";
		var failure = "CallControllerFailure(result[0].errormessage);";
		CallController("https://mpatzem.schoolwires.net/site/SiteController.aspx/EndPreviewMode", data, success, failure);
	}
	
    function SWEndEmulationMode() {
        var data = "{}";
        var success = "DeleteCookie('SourceEmulationUserID');DeleteCookie('SidebarIsClosed');window.location='https://mpatzem.schoolwires.net/ums/Users/Users.aspx';";
        var failure = "CallControllerFailure(result[0].errormessage);";
        CallController("https://mpatzem.schoolwires.net/site/SiteController.aspx/EndEmulationMode", data, success, failure);
	}

	function SWEndPreviewConfigMode() {
	    var data = "{}";
	    var success = "window.location='';";
	    var failure = "CallControllerFailure(result[0].errormessage);";
	    CallController("https://mpatzem.schoolwires.net/site/SiteController.aspx/EndPreviewConfigMode", data, success, failure);
	}
</script>
            

    <!-- BEGIN - MYSTART BAR -->
<div id='sw-mystart-outer' class='noprint'>
<div id='sw-mystart-inner'>
<div id='sw-mystart-left'>
<div class='sw-mystart-nav sw-mystart-button home'><a tabindex="0" href="https://mpatzem.schoolwires.net/Domain/4" alt="District Home" title="Return to the homepage on the district site."><span>District Home<div id='sw-home-icon'></div>
</span></a></div>
<div class='sw-mystart-nav sw-mystart-dropdown schoollist' tabindex='0' aria-label='Select a School' role='navigation'>
<div class='selector' aria-hidden='true'>Select a School...</div>
<div class='sw-dropdown' aria-hidden='false'>
<div class='sw-dropdown-selected' aria-hidden='true'>Select a School</div>
<ul class='sw-dropdown-list' aria-hidden='false' aria-label='Schools'>
<li><a href="https://mpatzem.schoolwires.net/Domain/1503">Humble ISD</a></li>
<li><a href="https://mpatzem.schoolwires.net/Domain/1580">Fletch_Test_2</a></li>
<li><a href="https://mpatzem.schoolwires.net/Domain/1634">Grosse Pointe</a></li>
</ul>
</div>
<div class='sw-dropdown-arrow' aria-hidden='true'></div>
</div>
</div>
<div id='sw-mystart-right'>
<div id='ui-btn-signin' class='sw-mystart-button signin'><a href="https://mpatzem.schoolwires.net/site/Default.aspx?PageType=7&SiteID=4&IgnoreRedirect=true"><span>Sign In</span></a></div>
<div id='ui-btn-register' class='sw-mystart-button register'><a href="https://mpatzem.schoolwires.net/site/Default.aspx?PageType=10&SiteID=4"><span>Register</span></a></div>
<div id='sw-mystart-search' class='sw-mystart-nav'>
<script type="text/javascript">
$(document).ready(function() {
    $('#sw-search-input').keyup(function(e) {        if (e.keyCode == 13) {
            SWGoToSearchResultsPageswsearchinput();
        }
    });
    $('#sw-search-input').val($('#swsearch-hid-word').val())});
function SWGoToSearchResultsPageswsearchinput() {
window.location.href="https://mpatzem.schoolwires.net/site/Default.aspx?PageType=6&SiteID=4&SearchString=" + $('#sw-search-input').val();
}
</script>
<label for="sw-search-input" class="hidden-label">Search Our Site</label>
<input id="sw-search-input" type="text" title="Search Term" aria-label="Search Our Site" placeholder="Search this Site..." />
<a tabindex="0" id="sw-search-button" title="Search" href="javascript:;" role="button" aria-label="Submit Site Search" onclick="SWGoToSearchResultsPageswsearchinput();"><span><img src="https://mpatzem.schoolwires.net/Static//globalassets/images/sw-mystart-search.png" alt="Search" /></span></a><script type="text/javascript">
$(document).ready(function() {
    $('#sw-search-button').keyup(function(e) {        if (e.keyCode == 13) {
            SWGoToSearchResultsPageswsearchinput();        }
    });
});
</script>

</div>
<div class='clear'></div>
</div>
</div>
</div>
<!-- END - MYSTART BAR -->
<div><div id="sw-channel-list-container" role="navigation">
<ul id="channel-navigation" class="sw-channel-list" role="menubar">
<li id="navc-HP" class="sw-channel-item" ><a href="https://mpatzem.schoolwires.net/Page/1" aria-label="Home"><span>Home</span></a></li>
<li id="navc-1173" class="sw-channel-item">
<a href="https://mpatzem.schoolwires.net/domain/1184"">
<span>Discover</span></a>
<div class="hidden-sections"><ul>"

</ul></div>
<ul class="sw-channel-dropdown">
<li id="navs-1184"><a href="https://mpatzem.schoolwires.net/domain/1184"><span>Our Programs</span></a></li>
<li id="navs-1185"><a href="https://mpatzem.schoolwires.net/domain/1185"><span>Classes and Schedules</span></a></li>
<li id="navs-1186"><a href="https://mpatzem.schoolwires.net/domain/1186"><span>Locations</span></a></li>
<li id="navs-1187"><a href="https://mpatzem.schoolwires.net/domain/1187"><span>Teacher Information</span></a></li>
<li id="navs-1188"><a href="https://mpatzem.schoolwires.net/domain/1188"><span>School Locator</span></a></li>
<li id="navs-1189"><a href="https://mpatzem.schoolwires.net/domain/1189"><span>Discover Why</span></a></li>
<li id="navs-1207"><a href="https://mpatzem.schoolwires.net/domain/1207"><span>Subpage</span></a></li>
<li id="navs-1654"><a href="https://mpatzem.schoolwires.net/domain/1654"><span>Section 8</span></a></li>
<li id="navs-1655"><a href="https://mpatzem.schoolwires.net/domain/1655"><span>Section 9</span></a></li>
<li id="navs-1656"><a href="https://mpatzem.schoolwires.net/domain/1656"><span>Section 10</span></a></li>
<li id="navs-1657"><a href="https://mpatzem.schoolwires.net/domain/1657"><span>Section 11</span></a></li>
<li id="navs-1658"><a href="https://mpatzem.schoolwires.net/domain/1658"><span>Section 12</span></a></li>
<li id="navs-1659"><a href="https://mpatzem.schoolwires.net/domain/1659"><span>Section 13</span></a></li>
<li id="navs-1660"><a href="https://mpatzem.schoolwires.net/domain/1660"><span>Section 14</span></a></li>
<li id="navs-1661"><a href="https://mpatzem.schoolwires.net/domain/1661"><span>Section 15</span></a></li>
<li id="navs-1662"><a href="https://mpatzem.schoolwires.net/domain/1662"><span>Section 16</span></a></li>
<li id="navs-1663"><a href="https://mpatzem.schoolwires.net/domain/1663"><span>Section 17</span></a></li>
<li id="navs-1664"><a href="https://mpatzem.schoolwires.net/domain/1664"><span>Section 18</span></a></li>
<li id="navs-1665"><a href="https://mpatzem.schoolwires.net/domain/1665"><span>Section 19</span></a></li>
<li id="navs-1666"><a href="https://mpatzem.schoolwires.net/domain/1666"><span>Section 20</span></a></li>
<li id="navs-1667"><a href="https://mpatzem.schoolwires.net/domain/1667"><span>Section 21</span></a></li>
</ul>
</li><li id="navc-1174" class="sw-channel-item">
<a href="https://mpatzem.schoolwires.net/domain/1190"">
<span>Our Schools</span></a>
<div class="hidden-sections"><ul>"

</ul></div>
<ul class="sw-channel-dropdown">
<li id="navs-1190"><a href="https://mpatzem.schoolwires.net/domain/1190"><span>Section Title</span></a></li>
<li id="navs-1191"><a href="https://mpatzem.schoolwires.net/domain/1191"><span>Headlines and Features</span></a></li>
<li id="navs-1192"><a href="https://mpatzem.schoolwires.net/domain/1192"><span>Human Resources</span></a></li>
<li id="navs-1193"><a href="https://mpatzem.schoolwires.net/domain/1193"><span>Employment Opportunities</span></a></li>
</ul>
</li><li id="navc-1175" class="sw-channel-item">
<a href="https://mpatzem.schoolwires.net/domain/1194"">
<span>Family Resources</span></a>
<div class="hidden-sections"><ul>"

</ul></div>
<ul class="sw-channel-dropdown">
<li id="navs-1194"><a href="https://mpatzem.schoolwires.net/domain/1194"><span>Our Programs</span></a></li>
<li id="navs-1195"><a href="https://mpatzem.schoolwires.net/domain/1195"><span>Classes and Schedules</span></a></li>
<li id="navs-1196"><a href="https://mpatzem.schoolwires.net/domain/1196"><span>Locations</span></a></li>
<li id="navs-1197"><a href="https://mpatzem.schoolwires.net/domain/1197"><span>Teacher Information</span></a></li>
<li id="navs-1198"><a href="https://mpatzem.schoolwires.net/domain/1198"><span>School Locator</span></a></li>
<li id="navs-1199"><a href="https://mpatzem.schoolwires.net/domain/1199"><span>Discover Why</span></a></li>
</ul>
</li><li id="navc-1176" class="sw-channel-item">
<a href="https://mpatzem.schoolwires.net/domain/1200"">
<span>Clubs</span></a>
<div class="hidden-sections"><ul>"

</ul></div>
<ul class="sw-channel-dropdown">
<li id="navs-1200"><a href="https://mpatzem.schoolwires.net/domain/1200"><span>Our Programs</span></a></li>
<li id="navs-1201"><a href="https://mpatzem.schoolwires.net/domain/1201"><span>Classes and Schedules</span></a></li>
<li id="navs-1202"><a href="https://mpatzem.schoolwires.net/domain/1202"><span>Locations</span></a></li>
<li id="navs-1203"><a href="https://mpatzem.schoolwires.net/domain/1203"><span>Teacher Information</span></a></li>
<li id="navs-1204"><a href="https://mpatzem.schoolwires.net/domain/1204"><span>School Locator</span></a></li>
<li id="navs-1205"><a href="https://mpatzem.schoolwires.net/domain/1205"><span>Discover Why</span></a></li>
</ul>
</li><li id="navc-1288" class="hidden-channel">
<a href="https://mpatzem.schoolwires.net/domain/1288">
<span>Classrooms</span></a>
<div class="hidden-sections"><ul>"

</ul></div>
</li><li id="navc-1434" class="sw-channel-item">
<a href="https://mpatzem.schoolwires.net/domain/1435"">
<span>Technology</span></a>
<div class="hidden-sections"><ul>"

</ul></div>
<ul class="dropdown-hidden">
<li id="navs-1435"><a href="https://mpatzem.schoolwires.net/domain/1435"><span>Technology</span></a></li>
</ul>
</li></ul><div class='clear'></div>
</div>



<script type="text/javascript">
    $(document).ready(function() {
        channelHoverIE();
        channelTouch();
        closeMenuByPressingKey();
    });

    function channelTouch() {
        // this will change the dropdown behavior when it is touched vs clicked.
        // channels will be clickable on second click. first click simply opens the menu.
        $('#channel-navigation > .sw-channel-item > a').on({
            'touchstart': function (e) {
                
                // see if has menu
                if ($(this).siblings('ul.sw-channel-dropdown').children('li').length > 0)  {
                    var button = $(this);

                    // add href as property if not already set
                    // then remove href attribute
                    if (!button.prop('linkHref')) {
                        button.prop('linkHref', button.attr('href'));
                        button.removeAttr('href');
                    }

                    // check to see if menu is already open
                    if ($(this).siblings('ul.sw-channel-dropdown').is(':visible')) {
                        // if open go to link
                        window.location.href = button.prop('linkHref');
                    } 

                } 
            }
        });
    }


    
    function channelHoverIE(){
		// set z-index for IE7
		var parentZindex = $('#channel-navigation').parents('div:first').css('z-index');
		var zindex = (parentZindex > 0 ? parentZindex : 8000);
		$(".sw-channel-item").each(function(ind) {
			$(this).css('z-index', zindex - ind);
			zindex --;
		});
	    $(".sw-channel-item").hover(function(){
	        var subList = $(this).children('ul');
	        if ($.trim(subList.html()) !== "") {
	            subList.show();
	            subList.attr("aria-hidden", "false").attr("aria-expanded", "true");
		    }
		    $(this).addClass("hover");
	    }, function() {
	        $(".sw-channel-dropdown").hide();
	        $(this).removeClass("hover");
	        var subList = $(this).children('ul');
	        if ($.trim(subList.html()) !== "") {
	            subList.attr("aria-hidden", "true").attr("aria-expanded", "false");
	        }
	    });
    }

    function closeMenuByPressingKey() {
        $(".sw-channel-item").each(function(ind) {
            $(this).keyup(function (event) {
                if (event.keyCode == 27) { // ESC
                    $(".sw-channel-dropdown").hide();
                    $(this).removeClass("hover");
                    var subList = $(this).children('ul');
                    if ($.trim(subList.html()) !== "") {
                        subList.attr("aria-hidden", "true").attr("aria-expanded", "false");
                    }
                }
                if (event.keyCode == 13 || event.keyCode == 32) { //enter or space
                    $(this).find('a').get(0).click();
                }
            }); 
        });

        $(".sw-channel-item a").each(function (ind) {
            $(this).parents('.sw-channel-item').keydown(function (e) {
                if (e.keyCode == 9) { // TAB
                    $(".sw-channel-dropdown").hide();
                    $(this).removeClass("hover");
                    var subList = $(this).children('ul');
                    if ($.trim(subList.html()) !== "") {
                        subList.attr("aria-hidden", "true").attr("aria-expanded", "false");
                    }
                }
            });
        });

        $(".sw-channel-dropdown li").each(function(ind) {
            $(this).keydown(function (event) {
                if (event.keyCode == 9) { // TAB
                    $(".sw-channel-dropdown").hide();
                    var parentMenuItem = $(this).parent().closest('li');
                    parentMenuItem.removeClass("hover");
                    var subList = parentMenuItem.children('ul');
                    if ($.trim(subList.html()) !== "") {
                        subList.attr("aria-hidden", "true").attr("aria-expanded", "false");
                    }
                    parentMenuItem.next().find('a:first').focus();
                    event.preventDefault();
                    event.stopPropagation();
                }

                if (event.keyCode == 37 || // left arrow
                    event.keyCode == 39) { // right arrow
                    $(".sw-channel-dropdown").hide();
                    var parentMenuItem = $(this).parent().closest('li');
                    parentMenuItem.removeClass("hover");
                    var subList = parentMenuItem.children('ul');
                    if ($.trim(subList.html()) !== "") {
                        subList.attr("aria-hidden", "true").attr("aria-expanded", "false");
                    }
                    if (event.keyCode == 37) {
                        parentMenuItem.prev().find('a:first').focus();
                    } else {
                        parentMenuItem.next().find('a:first').focus();
                    }
                    event.preventDefault();
                    event.stopPropagation();
                }
            });
        });
    }

</script>

</div>
<div style="border:5px solid black;margin:10px;"><a id="sw-maincontent" name="sw-maincontent" tabindex="-1"></a><div id="sw-content-container10" class="region ui-hp"><div id='pmi-1232'>



<div id='sw-module-12280'>
    <script type="text/javascript">
        $(document).ready(function() {
            var DomainID = '4';
            var PageID = '1';
            var RenderLoc = '0';
            var MIID = '1228';

            //added to check to make sure moderated content doesn't bleed through the dialog
            if ($('#dialog-overlay-WindowLargeModal-body.moderateContent').length > 0) {
                $("#module-content-" + MIID).find(".ui-widget-detail").find(".ui-article").append("<p>&nbsp;</p>");
            }
        });
    </script>

    <script type="text/javascript">$(document).ready(function() {CheckScript('ModuleView');CheckScript('Mustache');
 });</script>
    
    <div id="module-content-1228" >
<div class="ui-widget app multimedia-gallery" data-pmi="1232" data-mi="1228">
	<div class="ui-widget-header ui-helper-hidden">
		
	</div>
    <div class="ui-widget-detail">
    	<div id="mmg-container-1232" class="mmg-container" data-gallery-type="default" data-transition="fade" data-record-num="0" data-is-animating="false" data-play-state="playing" data-is-hovering="false" data-has-focus="false" data-is-touching="false" data-active-record-index="0" data-active-gallery-index="0">
            <div class="mmg-viewer">
                <div class="mmg-slides-outer">
                    <ul class="mmg-slides"></ul>
                </div>
                <div class="mmg-live-feedback mmg-ally-hidden" aria-live="polite" aria-atomic="true"></div>
            </div>
        </div>
        
        <script type="text/javascript">
            var multimediaGallery1232 = {
            	"props": {
                	"defaultGallery": true,
                    "imageWidth" : 960,
                	"imageHeight" : 500,
                	"pageModuleInstanceId": "1232",
                    "moduleInstanceId": "1228",
                    "virtualFolderPath": "https://mpatzem.schoolwires.net/cms/lib/SWCS000010/Centricity/ModuleInstance/1228/"
                },
				"records": [
{
                    "flexDataId": "1117",
                    "hideTitle": "False",
                    "title": "Student gear up for the 2022-2023 School Year",
                    "hideCaption": "False",
                    "caption": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean consequat nunc ac velit vehicula luctus. Dnibus dolor justo, vel suscipit magna rutrum.",
                    "imageSrc": "https://mpatzem.schoolwires.net/cms/lib/SWCS000010/Centricity/Domain/4/Untitled-1.jpg",
                    "imageWidth": "999",
                    "imageHeight": "647",
                    "imageAltText": "cyberpunk",
                    "isLinked": "True",
                    "linkUrl": "#",
                    "linkText": "See Calendar",
                    "openLinkInNewWindow": "False",
                    "videoLinkText": "",
                    "videoIsEmbedded": "False",
                    "videoIframe": ""
                },
{
                    "flexDataId": "1460",
                    "hideTitle": "False",
                    "title": "Lorem Ipsum",
                    "hideCaption": "False",
                    "caption": "Nunc sed augue lacus viverra vitae. Lectus magna fringilla urna porttitor rhoncus dolor purus.",
                    "imageSrc": "https://mpatzem.schoolwires.net/cms/lib/SWCS000010/Centricity/Domain/4/Untitled-2.jpg",
                    "imageWidth": "999",
                    "imageHeight": "647",
                    "imageAltText": "arcade",
                    "isLinked": "False",
                    "linkUrl": "",
                    "linkText": "",
                    "openLinkInNewWindow": "False",
                    "videoLinkText": "",
                    "videoIsEmbedded": "False",
                    "videoIframe": ""
                },
{
                    "flexDataId": "1459",
                    "hideTitle": "True",
                    "title": "Diam phasellus vestibulum",
                    "hideCaption": "False",
                    "caption": "",
                    "imageSrc": "https://mpatzem.schoolwires.net/cms/lib/SWCS000010/Centricity/Domain/4/Untitled-3.jpg",
                    "imageWidth": "999",
                    "imageHeight": "647",
                    "imageAltText": "arcade 1",
                    "isLinked": "False",
                    "linkUrl": "",
                    "linkText": "",
                    "openLinkInNewWindow": "False",
                    "videoLinkText": "",
                    "videoIsEmbedded": "False",
                    "videoIframe": ""
                },
{
                    "flexDataId": "1128",
                    "hideTitle": "False",
                    "title": "Quam vulputate dignissim suspendisse",
                    "hideCaption": "False",
                    "caption": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna.",
                    "imageSrc": "https://mpatzem.schoolwires.net/cms/lib/SWCS000010/Centricity/Domain/4/Untitled-4.jpg",
                    "imageWidth": "999",
                    "imageHeight": "647",
                    "imageAltText": "tokyo",
                    "isLinked": "True",
                    "linkUrl": "https://loremipsum.io/",
                    "linkText": "Link Text",
                    "openLinkInNewWindow": "True",
                    "videoLinkText": "Video Text",
                    "videoIsEmbedded": "True",
                    "videoIframe": ""
                },
{
                    "flexDataId": "1537",
                    "hideTitle": "False",
                    "title": "Quam Suspendisse",
                    "hideCaption": "False",
                    "caption": "Caption goes here!",
                    "imageSrc": "https://mpatzem.schoolwires.net/cms/lib/SWCS000010/Centricity/Domain/4/Untitled-5.jpg",
                    "imageWidth": "999",
                    "imageHeight": "647",
                    "imageAltText": "placeholder",
                    "isLinked": "False",
                    "linkUrl": "",
                    "linkText": "",
                    "openLinkInNewWindow": "False",
                    "videoLinkText": "",
                    "videoIsEmbedded": "False",
                    "videoIframe": ""
                },
{}]
			};
		</script>
	</div>
	<div class="ui-widget-footer ui-clear"></div>
</div>

<script type="text/javascript" src="//extend.schoolwires.com/creative/scripts/creative/rotate/multimedia-gallery/cs.multimedia.gallery.min.js"></script>
<script type="text/javascript">
	$(function() {
    	if(multimediaGallery1232.props.defaultGallery) {
        	$("#pmi-1232 .ui-widget.app.multimedia-gallery").csMultimediaGallery({
                "pageModuleInstanceId": 1232,
                "moduleInstanceId": 1228,
                "imageWidth" : multimediaGallery1232.props.imageWidth,
                "imageHeight" : multimediaGallery1232.props.imageHeight,
                "playPauseControl" : true,
                "bullets" : true
            });
        }
    });
</script>

<style type="text/css">
#pmi-1232:before {
    content: "960";
    display: none;
}
@media (max-width: 1023px) {
    #pmi-1232:before {
        content: "768";
    }
}
@media (max-width: 767px) {
    #pmi-1232:before {
        content: "640";
    }
}
@media (max-width: 639px) {
    #pmi-1232:before {
        content: "480";
    }
}
@media (max-width: 479px) {
    #pmi-1232:before {
        content: "320";
    }
}
</style>
<script type="text/javascript">
$(document).ready(function (){
$(".tag-list li a").keypress(function(e) { if(e.which == 13) { $(this).click();   }});
});
function LoadGroupedData(container, MIID, PMI, groupYear, groupMonth, groupBy, tag) {
  //ViewToUse looks at the hidden Sidebar List View defined in the Builder View.
  var viewToUse = "";
    if($('#hid-'+MIID+'-SidebarListView').length > 0){
      viewToUse = $("#hid-" + MIID + "-SidebarListView").val();
    };
  GetContent('https://mpatzem.schoolwires.net//cms/UserControls/ModuleView/ModuleViewRendererWrapper.aspx?DomainID=4&PageID=1&ModuleInstanceID=' + MIID + '&PageModuleInstanceID=' + PMI + '&RenderLoc=0&FromRenderLoc=0&GroupYear=' + groupYear + '&GroupMonth=' + groupMonth + '&GroupByField=' + groupBy + '&EnableQuirksMode=0&ViewID=' + viewToUse + '&Tag=' + tag, container, 2, 'chkSidebar();');
}
function LoadData(container, MIID, PMI, flexDataID, groupYear, groupMonth, groupBy, targetView, tag) {
  if(targetView !== undefined || targetView.Length() == 0){
  //targetView looks at the hidden Detail View defined in the Builder View.
      targetView = $('#hid-'+MIID+'-DetailView').val();
   }
  GetContent('https://mpatzem.schoolwires.net//cms/UserControls/ModuleView/ModuleViewRendererWrapper.aspx?DomainID=4&PageID=1&ModuleInstanceID=' + MIID + '&PageModuleInstanceID=' + PMI + '&FlexDataID=' + flexDataID + '&GroupYear=' + groupYear + '&GroupMonth=' + groupMonth + '&GroupByField=' + groupBy + '&RenderLoc=0&FromRenderLoc=0&EnableQuirksMode=0&Tag=' + tag + '&ViewID=' + targetView, container, 2, 'chkSidebar();');
}
function LoadTaggedData(container, MIID, PMI, tag) {
  //ViewToUse looks at the hidden Sidebar List View defined in the Builder View.
  var viewToUse = "";
    if($('#hid-'+MIID+'-SidebarListView').length > 0){
      viewToUse = $("#hid-" + MIID + "-SidebarListView").val();
    };
  GetContent('https://mpatzem.schoolwires.net//cms/UserControls/ModuleView/ModuleViewRendererWrapper.aspx?DomainID=4&PageID=1&ModuleInstanceID=' + MIID + '&PageModuleInstanceID=' + PMI + '&RenderLoc=0&FromRenderLoc=0&Tag=' + tag + '&EnableQuirksMode=0&ViewID='+viewToUse, container, 2, 'chkSidebar();');
  setTimeout(function(){ $("#module-content-"+ MIID +"").find('[tabindex]:first').focus(); }, 200);
}
</script>
</div>

</div></div>
</div></div>
<div style="border:5px solid black;margin:10px;"><div id="sw-content-container1" class="region ui-hp"><div id='pmi-1949'>
<div id="module-content-1787" ><div class="ui-widget app upcomingevents">
 <div class="ui-widget-header">
     <h1>Mark Your Calendars</h1>
 </div>
 <div class="ui-widget-detail">
		<ul class="ui-articles">
<li>
<div class="ui-article">
     <h1 class="ui-article-title sw-calendar-block-date">Today</h1>
     <p class="ui-article-description">
         <span class="sw-calendar-block-time">8:00 AM - 9:00 AM</span>
         <span class="sw-calendar-block-title"><a href="
https://mpatzem.schoolwires.net/site/Default.aspx?PageID=2&DomainID=4#calendar1/20230405/event/1172">La Luna</a></span>
     </p>
</div>
</li>
<li>
<div class="ui-article">
     <h1 class="ui-article-title sw-calendar-block-date">Friday</h1>
     <p class="ui-article-description">
         <span class="sw-calendar-block-title"><a href="
https://mpatzem.schoolwires.net/site/Default.aspx?PageID=2&DomainID=4#calendar1/20230407/event/607">Dad Joke Day</a></span>
     </p>
</div>
</li>
<li>
<div class="ui-article">
     <p class="ui-article-description">
         <span class="sw-calendar-block-time">8:00 AM - 9:00 AM</span>
         <span class="sw-calendar-block-title"><a href="
https://mpatzem.schoolwires.net/site/Default.aspx?PageID=2&DomainID=4#calendar1/20230407/event/1623">Okay Sure</a></span>
     </p>
</div>
</li>
<li>
<div class="ui-article">
     <p class="ui-article-description">
         <span class="sw-calendar-block-time">7:00 PM - 8:00 PM</span>
         <span class="sw-calendar-block-title"><a href="
https://mpatzem.schoolwires.net/site/Default.aspx?PageID=2&DomainID=4#calendar1/20230407/event/2823">Hidden Event</a></span>
     </p>
</div>
</li>
<li>
<div class="ui-article">
     <h1 class="ui-article-title sw-calendar-block-date">Monday</h1>
     <p class="ui-article-description">
         <span class="sw-calendar-block-time">8:00 AM - 9:00 AM</span>
         <span class="sw-calendar-block-title"><a href="
https://mpatzem.schoolwires.net/site/Default.aspx?PageID=2&DomainID=4#calendar1/20230410/event/1624">Okay Sure</a></span>
     </p>
</div>
</li>
<li>
<div class="ui-article">
     <p class="ui-article-description">
         <span class="sw-calendar-block-time">2:00 PM - 3:00 PM</span>
         <span class="sw-calendar-block-title"><a href="
https://mpatzem.schoolwires.net/site/Default.aspx?PageID=2&DomainID=4#calendar1/20230410/event/298">Sure, Why Not?</a></span>
     </p>
</div>
</li>
<li>
<div class="ui-article">
     <p class="ui-article-description">
         <span class="sw-calendar-block-time">7:00 PM - 8:00 PM</span>
         <span class="sw-calendar-block-title"><a href="
https://mpatzem.schoolwires.net/site/Default.aspx?PageID=2&DomainID=4#calendar1/20230410/event/2824">Hidden Event</a></span>
     </p>
</div>
</li>
<li>
<div class="ui-article">
     <h1 class="ui-article-title sw-calendar-block-date">April 12, 2023</h1>
     <p class="ui-article-description">
         <span class="sw-calendar-block-time">8:00 AM - 9:00 AM</span>
         <span class="sw-calendar-block-title"><a href="
https://mpatzem.schoolwires.net/site/Default.aspx?PageID=2&DomainID=4#calendar1/20230412/event/1173">La Luna</a></span>
     </p>
</div>
</li>
<li>
<div class="ui-article">
     <h1 class="ui-article-title sw-calendar-block-date">April 14, 2023</h1>
     <p class="ui-article-description">
         <span class="sw-calendar-block-title"><a href="
https://mpatzem.schoolwires.net/site/Default.aspx?PageID=2&DomainID=4#calendar1/20230414/event/608">Dad Joke Day</a></span>
     </p>
</div>
</li>
<li>
<div class="ui-article">
     <p class="ui-article-description">
         <span class="sw-calendar-block-time">8:00 AM - 9:00 AM</span>
         <span class="sw-calendar-block-title"><a href="
https://mpatzem.schoolwires.net/site/Default.aspx?PageID=2&DomainID=4#calendar1/20230414/event/1625">Okay Sure</a></span>
     </p>
</div>
</li>
<li>
<div class="ui-article">
     <p class="ui-article-description">
         <span class="sw-calendar-block-time">7:00 PM - 8:00 PM</span>
         <span class="sw-calendar-block-title"><a href="
https://mpatzem.schoolwires.net/site/Default.aspx?PageID=2&DomainID=4#calendar1/20230414/event/2825">Hidden Event</a></span>
     </p>
</div>
</li>
<li>
<div class="ui-article">
     <h1 class="ui-article-title sw-calendar-block-date">April 17, 2023</h1>
     <p class="ui-article-description">
         <span class="sw-calendar-block-time">8:00 AM - 9:00 AM</span>
         <span class="sw-calendar-block-title"><a href="
https://mpatzem.schoolwires.net/site/Default.aspx?PageID=2&DomainID=4#calendar1/20230417/event/1626">Okay Sure</a></span>
     </p>
</div>
</li>
<li>
<div class="ui-article">
     <p class="ui-article-description">
         <span class="sw-calendar-block-time">2:00 PM - 3:00 PM</span>
         <span class="sw-calendar-block-title"><a href="
https://mpatzem.schoolwires.net/site/Default.aspx?PageID=2&DomainID=4#calendar1/20230417/event/299">Sure, Why Not?</a></span>
     </p>
</div>
</li>
<li>
<div class="ui-article">
     <p class="ui-article-description">
         <span class="sw-calendar-block-time">7:00 PM - 8:00 PM</span>
         <span class="sw-calendar-block-title"><a href="
https://mpatzem.schoolwires.net/site/Default.aspx?PageID=2&DomainID=4#calendar1/20230417/event/2826">Hidden Event</a></span>
     </p>
</div>
</li>
<li>
<div class="ui-article">
     <h1 class="ui-article-title sw-calendar-block-date">April 19, 2023</h1>
     <p class="ui-article-description">
         <span class="sw-calendar-block-time">8:00 AM - 9:00 AM</span>
         <span class="sw-calendar-block-title"><a href="
https://mpatzem.schoolwires.net/site/Default.aspx?PageID=2&DomainID=4#calendar1/20230419/event/1174">La Luna</a></span>
     </p>
</div>
</li>
	</ul>
<a class='view-calendar-link' href="https://mpatzem.schoolwires.net/Page/2"><span>View Calendar</span></a>
 </div>
 <div class="ui-widget-footer">
 </div>
</div>
</div>
</div>
</div></div>
<div style="border:5px solid black;margin:10px;"><div id="sw-content-container2" class="region ui-hp"><div id='pmi-1968'>



<div id='sw-module-17960'>
    <script type="text/javascript">
        $(document).ready(function() {
            var DomainID = '4';
            var PageID = '1';
            var RenderLoc = '0';
            var MIID = '1796';

            //added to check to make sure moderated content doesn't bleed through the dialog
            if ($('#dialog-overlay-WindowLargeModal-body.moderateContent').length > 0) {
                $("#module-content-" + MIID).find(".ui-widget-detail").find(".ui-article").append("<p>&nbsp;</p>");
            }
        });
    </script>

    <script type="text/javascript">$(document).ready(function() {CheckScript('ModuleView');CheckScript('Mustache');
 });</script>
    
    <div id="module-content-1796" >
<div class="ui-widget app headlines">
	
	<div class="ui-widget-header">
		<h1>Headlines &amp; Features</h1>
	</div>
	
	<div class="ui-widget-detail" id="sw-app-headlines-1796">
		<ul class="ui-articles">
<li>  
    <div  class="ui-article">   
        <span class="ui-article-thumb" >
            <span class="img">
                <img alt=" records" height="140" width="210" src="https://mpatzem.schoolwires.net/..//cms/lib/SWCS000010/Centricity/Domain/4/music-1.png" />
            </span>
        </span>   
        
        <h1 class="ui-article-title">
        	<a href="https://mpatzem.schoolwires.net/../site/default.aspx?PageType=3&DomainID=4&ModuleInstanceID=1796&ViewID=6446EE88-D30C-497E-9316-3F8874B3E108&RenderLoc=0&FlexDataID=1550&PageID=1"><span>Teacher Recognized</span></a>
            </h1> 
        <p class="ui-article-description">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras fringilla interdum sem. Lorem ipsum dolor sit amet.</p>   
        <div class="ui-article-controls">                                    
            <a class="sub-link" title="Go to comments for Teacher Recognized" href="https://mpatzem.schoolwires.net/../site/default.aspx?PageType=3&DomainID=4&ModuleInstanceID=1796&ViewID=6446EE88-D30C-497E-9316-3F8874B3E108&RenderLoc=0&FlexDataID=1550&PageID=1&Comments=true"><span>Comments (-1)</span></a>
               
        </div>   
        <div class="clear"></div>  
    </div>
</li>
<li>  
    <div  class="ui-article">   
        <span class="ui-article-thumb" >
            <span class="img">
                <img alt=" recordssssssss" height="140" width="210" src="https://mpatzem.schoolwires.net/..//cms/lib/SWCS000010/Centricity/Domain/4/music-2.png" />
            </span>
        </span>   
        
        <h1 class="ui-article-title">
        	<a href="https://mpatzem.schoolwires.net/../site/default.aspx?PageType=3&DomainID=4&ModuleInstanceID=1796&ViewID=6446EE88-D30C-497E-9316-3F8874B3E108&RenderLoc=0&FlexDataID=1551&PageID=1"><span>Student Focus on Art in New Ways</span></a>
            </h1> 
        <p class="ui-article-description">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras fringilla interdum. Loremor sit amet, consectetur adipiscing elit. Cras fringilla interdum.</p>   
        <div class="ui-article-controls">                                    
            <a class="sub-link" title="Go to comments for Student Focus on Art in New Ways" href="https://mpatzem.schoolwires.net/../site/default.aspx?PageType=3&DomainID=4&ModuleInstanceID=1796&ViewID=6446EE88-D30C-497E-9316-3F8874B3E108&RenderLoc=0&FlexDataID=1551&PageID=1&Comments=true"><span>Comments (-1)</span></a>
               
        </div>   
        <div class="clear"></div>  
    </div>
</li>
<li>  
    <div  class="ui-article">   
        <span class="ui-article-thumb" >
            <span class="img">
                <img alt="wall " height="140" width="210" src="https://mpatzem.schoolwires.net/..//cms/lib/SWCS000010/Centricity/Domain/4/music-3.png" />
            </span>
        </span>   
        
        <h1 class="ui-article-title">
        	<a href="https://mpatzem.schoolwires.net/../site/default.aspx?PageType=3&DomainID=4&ModuleInstanceID=1796&ViewID=6446EE88-D30C-497E-9316-3F8874B3E108&RenderLoc=0&FlexDataID=1552&PageID=1"><span>After School Programs Schedule</span></a>
            </h1> 
        <p class="ui-article-description">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras fringilla interdum sem. Lorem ipsum dolor sit amet, consectetur adipi.</p>   
        <div class="ui-article-controls">                                    
            <a class="sub-link" title="Go to comments for After School Programs Schedule" href="https://mpatzem.schoolwires.net/../site/default.aspx?PageType=3&DomainID=4&ModuleInstanceID=1796&ViewID=6446EE88-D30C-497E-9316-3F8874B3E108&RenderLoc=0&FlexDataID=1552&PageID=1&Comments=true"><span>Comments (-1)</span></a>
               
        </div>   
        <div class="clear"></div>  
    </div>
</li>
</ul><div class="ui-read-more"><a id='MoreLinkButton1796' class='more-link' aria-label='Go to more records' onclick='MoreViewClick(this);' href='https://mpatzem.schoolwires.net/site/default.aspx?PageType=14&DomainID=4&PageID=1&ModuleInstanceID=1796&ViewID=c83d46ac-74fe-4857-8c9a-5922a80225e2&IsMoreExpandedView=True'><span>more</span></a><div class='more-link-under'>&nbsp;</div></div>
</ul>
	</div>

	<div class="ui-widget-footer">
		
		
		<div class="clear"></div>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function() {
        /*$(document).on('click', 'a.ui-article-thumb', function() {
        	window.location = $(this).attr('href');
    	});*/
    		
		$('#sw-app-headlines-1796').find('img').each(function() {
			if ($.trim(this.src) == '' ) {
				$(this).parent().parent().remove();
			}
		});
        
        // Jason Smith - 12/9/2014 - Removed due to bandwidth implications
		
	});

</script>
<script type="text/javascript">
$(document).ready(function (){
$(".tag-list li a").keypress(function(e) { if(e.which == 13) { $(this).click();   }});
});
function LoadGroupedData(container, MIID, PMI, groupYear, groupMonth, groupBy, tag) {
  //ViewToUse looks at the hidden Sidebar List View defined in the Builder View.
  var viewToUse = "";
    if($('#hid-'+MIID+'-SidebarListView').length > 0){
      viewToUse = $("#hid-" + MIID + "-SidebarListView").val();
    };
  GetContent('https://mpatzem.schoolwires.net//cms/UserControls/ModuleView/ModuleViewRendererWrapper.aspx?DomainID=4&PageID=1&ModuleInstanceID=' + MIID + '&PageModuleInstanceID=' + PMI + '&RenderLoc=0&FromRenderLoc=0&GroupYear=' + groupYear + '&GroupMonth=' + groupMonth + '&GroupByField=' + groupBy + '&EnableQuirksMode=0&ViewID=' + viewToUse + '&Tag=' + tag, container, 2, 'chkSidebar();');
}
function LoadData(container, MIID, PMI, flexDataID, groupYear, groupMonth, groupBy, targetView, tag) {
  if(targetView !== undefined || targetView.Length() == 0){
  //targetView looks at the hidden Detail View defined in the Builder View.
      targetView = $('#hid-'+MIID+'-DetailView').val();
   }
  GetContent('https://mpatzem.schoolwires.net//cms/UserControls/ModuleView/ModuleViewRendererWrapper.aspx?DomainID=4&PageID=1&ModuleInstanceID=' + MIID + '&PageModuleInstanceID=' + PMI + '&FlexDataID=' + flexDataID + '&GroupYear=' + groupYear + '&GroupMonth=' + groupMonth + '&GroupByField=' + groupBy + '&RenderLoc=0&FromRenderLoc=0&EnableQuirksMode=0&Tag=' + tag + '&ViewID=' + targetView, container, 2, 'chkSidebar();');
}
function LoadTaggedData(container, MIID, PMI, tag) {
  //ViewToUse looks at the hidden Sidebar List View defined in the Builder View.
  var viewToUse = "";
    if($('#hid-'+MIID+'-SidebarListView').length > 0){
      viewToUse = $("#hid-" + MIID + "-SidebarListView").val();
    };
  GetContent('https://mpatzem.schoolwires.net//cms/UserControls/ModuleView/ModuleViewRendererWrapper.aspx?DomainID=4&PageID=1&ModuleInstanceID=' + MIID + '&PageModuleInstanceID=' + PMI + '&RenderLoc=0&FromRenderLoc=0&Tag=' + tag + '&EnableQuirksMode=0&ViewID='+viewToUse, container, 2, 'chkSidebar();');
  setTimeout(function(){ $("#module-content-"+ MIID +"").find('[tabindex]:first').focus(); }, 200);
}
</script>
</div>

</div></div>
</div></div>
<div style="border:5px solid black;margin:10px;"><div id="sw-content-container3" class="region ui-hp"><div id='pmi-1962'>
<div id="module-content-1587" >
<div class="ui-widget app navigation  siteshortcuts">
	<div class="ui-widget-header"><h1>Quick Links</h1></div>
	<div class="ui-widget-detail">
		<ul class="site-shortcuts">
<li id="siteshortcut-276" class=""><a href="https://mpatzem.schoolwires.net/" target="_parent" >ESSERS III Report</a>
</li>
<li id="siteshortcut-277" class=""><a href="https://mpatzem.schoolwires.net/" target="_parent" >Submit a School Safety Tip via Safe2Say Something</a>
</li>
<li id="siteshortcut-278" class=""><a href="https://mpatzem.schoolwires.net/" target="_parent" >Student Assistance Program (SAP)</a>
</li>
<li id="siteshortcut-279" class=""><a href="https://mpatzem.schoolwires.net/" target="_parent" >Daily COVID Report</a>
</li>
<li id="siteshortcut-280" class=""><a href="https://mpatzem.schoolwires.net/" target="_parent" >ESASD Anonymous Fraud Hotline</a>
</li>
<li id="siteshortcut-281" class=""><a href="https://mpatzem.schoolwires.net/" target="_parent" >2021-2022 SY Calendar </a>
</li>
<li id="siteshortcut-282" class=""><a href="https://mpatzem.schoolwires.net/" target="_parent" >Change Password</a>
</li>
<li id="siteshortcut-295" class=""><a href="https://mpatzem.schoolwires.net/" target="_parent" >Clever</a>
</li>
<li id="siteshortcut-296" class=""><a href="https://mpatzem.schoolwires.net/" target="_parent" >Work Requests</a>
</li>
<li id="siteshortcut-297" class=""><a href="https://mpatzem.schoolwires.net/" target="_parent" >Login Mobile Device</a>
</li>
<li id="siteshortcut-298" class=""><a href="https://mpatzem.schoolwires.net/" target="_parent" >Login Mobile Device</a>
</li></ul>
<div class="app-level-social-follow"></div>
	</div>
	<div class="ui-widget-footer">
	</div>
</div></div>
</div>
</div></div>
<div style="border:5px solid black;margin:10px;"><div id="sw-content-container4" class="region ui-hp"><div id='pmi-1967'>



<div id='sw-module-17950'>
    <script type="text/javascript">
        $(document).ready(function() {
            var DomainID = '4';
            var PageID = '1';
            var RenderLoc = '0';
            var MIID = '1795';

            //added to check to make sure moderated content doesn't bleed through the dialog
            if ($('#dialog-overlay-WindowLargeModal-body.moderateContent').length > 0) {
                $("#module-content-" + MIID).find(".ui-widget-detail").find(".ui-article").append("<p>&nbsp;</p>");
            }
        });
    </script>

    <script type="text/javascript">$(document).ready(function() {CheckScript('ModuleView');CheckScript('Mustache');
 });</script>
    
    <div id="module-content-1795" >
<div class="ui-widget app multimedia-gallery" data-pmi="1967" data-mi="1795">
	<div class="ui-widget-header">
		<h1>Image Collage Title</h1>
	</div>
    <div class="ui-widget-detail">
    	<div id="mmg-container-1967" class="mmg-container" data-gallery-type="default" data-transition="fade" data-record-num="0" data-is-animating="false" data-play-state="playing" data-is-hovering="false" data-has-focus="false" data-is-touching="false" data-active-record-index="0" data-active-gallery-index="0">
            <div class="mmg-viewer">
                <div class="mmg-slides-outer">
                    <ul class="mmg-slides"></ul>
                </div>
                <div class="mmg-live-feedback mmg-ally-hidden" aria-live="polite" aria-atomic="true"></div>
            </div>
        </div>
        
        <script type="text/javascript">
            var multimediaGallery1967 = {
            	"props": {
                	"defaultGallery": true,
                    "imageWidth" : 960,
                	"imageHeight" : 500,
                	"pageModuleInstanceId": "1967",
                    "moduleInstanceId": "1795",
                    "virtualFolderPath": "https://mpatzem.schoolwires.net/cms/lib/SWCS000010/Centricity/ModuleInstance/1795/"
                },
				"records": [
{
                    "flexDataId": "1540",
                    "hideTitle": "False",
                    "title": "Photo Title",
                    "hideCaption": "False",
                    "caption": "A a a tellus parturient habitasse primis himenaeos consectetur dis parturient pharetra diam fermentum accumsan vivamus vestibulum sem. Sociis a vestibulum curabitur iaculis vestibulum auctor lorem.",
                    "imageSrc": "https://mpatzem.schoolwires.net/cms/lib/SWCS000010/Centricity/Domain/4/crab.jpg",
                    "imageWidth": "300",
                    "imageHeight": "350",
                    "imageAltText": "crabs",
                    "isLinked": "True",
                    "linkUrl": "#",
                    "linkText": "See Calendar",
                    "openLinkInNewWindow": "False",
                    "videoLinkText": "Watch Video",
                    "videoIsEmbedded": "True",
                    "videoIframe": "<iframe width=&quot;560&quot; height=&quot;315&quot; src=&quot;https://www.youtube.com/embed/sCQfTNOC5aE&quot; title=&quot;YouTube video player&quot; frameborder=&quot;0&quot; allow=&quot;accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share&quot; allowfullscreen></iframe>"
                },
{
                    "flexDataId": "1541",
                    "hideTitle": "False",
                    "title": "grid 2",
                    "hideCaption": "False",
                    "caption": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident.<br/>",
                    "imageSrc": "https://mpatzem.schoolwires.net/cms/lib/SWCS000010/Centricity/Domain/4/gator.jpg",
                    "imageWidth": "300",
                    "imageHeight": "350",
                    "imageAltText": "gator",
                    "isLinked": "True",
                    "linkUrl": "#",
                    "linkText": "More Info",
                    "openLinkInNewWindow": "False",
                    "videoLinkText": "",
                    "videoIsEmbedded": "False",
                    "videoIframe": ""
                },
{
                    "flexDataId": "1542",
                    "hideTitle": "False",
                    "title": "grid 3",
                    "hideCaption": "False",
                    "caption": "Lorem ipsum dolor sit amet, consectetur adipiscing elit.<br/>",
                    "imageSrc": "https://mpatzem.schoolwires.net/cms/lib/SWCS000010/Centricity/Domain/4/koala.jpg",
                    "imageWidth": "300",
                    "imageHeight": "350",
                    "imageAltText": "koala",
                    "isLinked": "False",
                    "linkUrl": "",
                    "linkText": "",
                    "openLinkInNewWindow": "False",
                    "videoLinkText": "Watch Video",
                    "videoIsEmbedded": "True",
                    "videoIframe": "<iframe width=&quot;2425&quot; height=&quot;1164&quot; src=&quot;https://www.youtube.com/embed/wzhJrXnh3ak&quot; title=&quot;Snow| Winter | Nature | Relaxing | Free HD Videos - No Copyright footage&quot; frameborder=&quot;0&quot; allow=&quot;accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share&quot; allowfullscreen></iframe>"
                },
{
                    "flexDataId": "1543",
                    "hideTitle": "False",
                    "title": "grid 4",
                    "hideCaption": "False",
                    "caption": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu.<br/>",
                    "imageSrc": "https://mpatzem.schoolwires.net/cms/lib/SWCS000010/Centricity/Domain/4/lynx.jpg",
                    "imageWidth": "300",
                    "imageHeight": "350",
                    "imageAltText": "lynx",
                    "isLinked": "False",
                    "linkUrl": "",
                    "linkText": "",
                    "openLinkInNewWindow": "False",
                    "videoLinkText": "",
                    "videoIsEmbedded": "False",
                    "videoIframe": ""
                },
{
                    "flexDataId": "1544",
                    "hideTitle": "False",
                    "title": "grid 5",
                    "hideCaption": "False",
                    "caption": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident.<br/>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident.Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incidi.",
                    "imageSrc": "https://mpatzem.schoolwires.net/cms/lib/SWCS000010/Centricity/Domain/4/mallard.jpg",
                    "imageWidth": "300",
                    "imageHeight": "350",
                    "imageAltText": "lynx",
                    "isLinked": "False",
                    "linkUrl": "",
                    "linkText": "",
                    "openLinkInNewWindow": "False",
                    "videoLinkText": "",
                    "videoIsEmbedded": "False",
                    "videoIframe": ""
                },
{
                    "flexDataId": "1545",
                    "hideTitle": "False",
                    "title": "grid 6",
                    "hideCaption": "False",
                    "caption": "",
                    "imageSrc": "https://mpatzem.schoolwires.net/cms/lib/SWCS000010/Centricity/Domain/4/polar-bear.jpg",
                    "imageWidth": "300",
                    "imageHeight": "350",
                    "imageAltText": "polar bear",
                    "isLinked": "False",
                    "linkUrl": "",
                    "linkText": "",
                    "openLinkInNewWindow": "False",
                    "videoLinkText": "",
                    "videoIsEmbedded": "False",
                    "videoIframe": ""
                },
{
                    "flexDataId": "1546",
                    "hideTitle": "False",
                    "title": "grid 7 ",
                    "hideCaption": "False",
                    "caption": "Accumsan sit amet nulla facilisi morbi tempus. Diam ut venenatis tellus in. Id cursus metus aliquam eleifend mi in nulla posuere sollicitudin. Ut lectus arcu bibendum at. Arcu risus quis varius quam quisque id.",
                    "imageSrc": "https://mpatzem.schoolwires.net/cms/lib/SWCS000010/Centricity/Domain/4/puffins.jpg",
                    "imageWidth": "300",
                    "imageHeight": "350",
                    "imageAltText": "puffins",
                    "isLinked": "False",
                    "linkUrl": "",
                    "linkText": "",
                    "openLinkInNewWindow": "False",
                    "videoLinkText": "",
                    "videoIsEmbedded": "False",
                    "videoIframe": ""
                },
{
                    "flexDataId": "1547",
                    "hideTitle": "False",
                    "title": "grid 8",
                    "hideCaption": "False",
                    "caption": "Id cursus metus aliquam eleifend mi in nulla posuere sollicitudin. Ut lectus arcu bibendum at. Arcu risus quis varius quam quisque id.",
                    "imageSrc": "https://mpatzem.schoolwires.net/cms/lib/SWCS000010/Centricity/Domain/4/wolf.jpg",
                    "imageWidth": "300",
                    "imageHeight": "350",
                    "imageAltText": "wolf",
                    "isLinked": "False",
                    "linkUrl": "",
                    "linkText": "",
                    "openLinkInNewWindow": "False",
                    "videoLinkText": "",
                    "videoIsEmbedded": "False",
                    "videoIframe": ""
                },
{
                    "flexDataId": "1548",
                    "hideTitle": "False",
                    "title": "grid 9",
                    "hideCaption": "False",
                    "caption": "Mi in nulla posuere sollicitudin. Ut lectus arcu bibendum at. Arcu risus quis varius quam quisque id.",
                    "imageSrc": "https://mpatzem.schoolwires.net/cms/lib/SWCS000010/Centricity/Domain/4/yak.jpg",
                    "imageWidth": "300",
                    "imageHeight": "350",
                    "imageAltText": "yak",
                    "isLinked": "False",
                    "linkUrl": "",
                    "linkText": "",
                    "openLinkInNewWindow": "False",
                    "videoLinkText": "",
                    "videoIsEmbedded": "False",
                    "videoIframe": ""
                },
{
                    "flexDataId": "1549",
                    "hideTitle": "False",
                    "title": "grid 10",
                    "hideCaption": "False",
                    "caption": "Short caption",
                    "imageSrc": "https://mpatzem.schoolwires.net/cms/lib/SWCS000010/Centricity/Domain/4/hawk.jpg",
                    "imageWidth": "300",
                    "imageHeight": "350",
                    "imageAltText": "happy hawk",
                    "isLinked": "False",
                    "linkUrl": "",
                    "linkText": "",
                    "openLinkInNewWindow": "False",
                    "videoLinkText": "",
                    "videoIsEmbedded": "False",
                    "videoIframe": ""
                },
{}]
			};
		</script>
	</div>
	<div class="ui-widget-footer ui-clear"></div>
</div>

<script type="text/javascript" src="//extend.schoolwires.com/creative/scripts/creative/rotate/multimedia-gallery/cs.multimedia.gallery.min.js"></script>
<script type="text/javascript">
	$(function() {
    	if(multimediaGallery1967.props.defaultGallery) {
        	$("#pmi-1967 .ui-widget.app.multimedia-gallery").csMultimediaGallery({
                "pageModuleInstanceId": 1967,
                "moduleInstanceId": 1795,
                "imageWidth" : multimediaGallery1967.props.imageWidth,
                "imageHeight" : multimediaGallery1967.props.imageHeight,
                "playPauseControl" : true,
                "bullets" : true
            });
        }
    });
</script>

<style type="text/css">
#pmi-1967:before {
    content: "960";
    display: none;
}
@media (max-width: 1023px) {
    #pmi-1967:before {
        content: "768";
    }
}
@media (max-width: 767px) {
    #pmi-1967:before {
        content: "640";
    }
}
@media (max-width: 639px) {
    #pmi-1967:before {
        content: "480";
    }
}
@media (max-width: 479px) {
    #pmi-1967:before {
        content: "320";
    }
}
</style>
<script type="text/javascript">
$(document).ready(function (){
$(".tag-list li a").keypress(function(e) { if(e.which == 13) { $(this).click();   }});
});
function LoadGroupedData(container, MIID, PMI, groupYear, groupMonth, groupBy, tag) {
  //ViewToUse looks at the hidden Sidebar List View defined in the Builder View.
  var viewToUse = "";
    if($('#hid-'+MIID+'-SidebarListView').length > 0){
      viewToUse = $("#hid-" + MIID + "-SidebarListView").val();
    };
  GetContent('https://mpatzem.schoolwires.net//cms/UserControls/ModuleView/ModuleViewRendererWrapper.aspx?DomainID=4&PageID=1&ModuleInstanceID=' + MIID + '&PageModuleInstanceID=' + PMI + '&RenderLoc=0&FromRenderLoc=0&GroupYear=' + groupYear + '&GroupMonth=' + groupMonth + '&GroupByField=' + groupBy + '&EnableQuirksMode=0&ViewID=' + viewToUse + '&Tag=' + tag, container, 2, 'chkSidebar();');
}
function LoadData(container, MIID, PMI, flexDataID, groupYear, groupMonth, groupBy, targetView, tag) {
  if(targetView !== undefined || targetView.Length() == 0){
  //targetView looks at the hidden Detail View defined in the Builder View.
      targetView = $('#hid-'+MIID+'-DetailView').val();
   }
  GetContent('https://mpatzem.schoolwires.net//cms/UserControls/ModuleView/ModuleViewRendererWrapper.aspx?DomainID=4&PageID=1&ModuleInstanceID=' + MIID + '&PageModuleInstanceID=' + PMI + '&FlexDataID=' + flexDataID + '&GroupYear=' + groupYear + '&GroupMonth=' + groupMonth + '&GroupByField=' + groupBy + '&RenderLoc=0&FromRenderLoc=0&EnableQuirksMode=0&Tag=' + tag + '&ViewID=' + targetView, container, 2, 'chkSidebar();');
}
function LoadTaggedData(container, MIID, PMI, tag) {
  //ViewToUse looks at the hidden Sidebar List View defined in the Builder View.
  var viewToUse = "";
    if($('#hid-'+MIID+'-SidebarListView').length > 0){
      viewToUse = $("#hid-" + MIID + "-SidebarListView").val();
    };
  GetContent('https://mpatzem.schoolwires.net//cms/UserControls/ModuleView/ModuleViewRendererWrapper.aspx?DomainID=4&PageID=1&ModuleInstanceID=' + MIID + '&PageModuleInstanceID=' + PMI + '&RenderLoc=0&FromRenderLoc=0&Tag=' + tag + '&EnableQuirksMode=0&ViewID='+viewToUse, container, 2, 'chkSidebar();');
  setTimeout(function(){ $("#module-content-"+ MIID +"").find('[tabindex]:first').focus(); }, 200);
}
</script>
</div>

</div></div>
</div></div>
<div style="border:5px solid black;margin:10px;"><div id="sw-content-container5" class="region ui-hp"></div></div>
<div style="border:5px solid black;margin:10px;"><div id="sw-content-container6" class="region ui-hp"></div></div>
<div style="border:5px solid black;margin:10px;"><div id="sw-content-container7" class="region ui-hp"></div></div>
<div style="border:5px solid black;margin:10px;"><div id="sw-content-container8" class="region ui-hp"></div></div>
<div style="border:5px solid black;margin:10px;"><div id="sw-content-container9" class="region ui-hp"></div></div>
<div style="border:5px solid black;margin:10px;"><div id="sw-content-container11" class="region ui-hp"></div></div>
<div style="border:5px solid black;margin:10px;"><div id="sw-content-container12" class="region ui-hp"></div></div>
<div style="border:5px solid black;margin:10px;"><div id="sw-content-container13" class="region ui-hp"><div id='pmi-1946'>



<div id='sw-module-11050'>
    <script type="text/javascript">
        $(document).ready(function() {
            var DomainID = '4';
            var PageID = '1';
            var RenderLoc = '0';
            var MIID = '1105';

            //added to check to make sure moderated content doesn't bleed through the dialog
            if ($('#dialog-overlay-WindowLargeModal-body.moderateContent').length > 0) {
                $("#module-content-" + MIID).find(".ui-widget-detail").find(".ui-article").append("<p>&nbsp;</p>");
            }
        });
    </script>

    <script type="text/javascript">$(document).ready(function() {CheckScript('ModuleView');CheckScript('Mustache');
 });</script>
    
    <div id="module-content-1105" >
<div class="ui-widget app announcements">
	<div class="ui-widget-header">
		<h1>Announcements</h1>
	</div>
	
	<div class="ui-widget-detail">
		<ul class="ui-articles">
<li>
    <div class="ui-article">
		<div class="ui-article-description">
			<span><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p></span>
		</div>
		<div class="ui-article-controls">
        	<a class="sub-link" title="Go to comments for Hello World!" href="https://mpatzem.schoolwires.net/../site/default.aspx?PageType=3&ModuleInstanceID=1105&ViewID=ed695a1c-ef13-4546-b4eb-4fefcdd4f389&RenderLoc=0&FlexDataID=1504&PageID=1&Comments=true"><span>Comments (-1)</span></a>
			
		</div>
		<div class="clear"></div>
	</div>
</li>
<li>
    <div class="ui-article">
		<div class="ui-article-description">
			<span><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p></span>
		</div>
		<div class="ui-article-controls">
        	<a class="sub-link" title="Go to comments for This is a test" href="https://mpatzem.schoolwires.net/../site/default.aspx?PageType=3&ModuleInstanceID=1105&ViewID=ed695a1c-ef13-4546-b4eb-4fefcdd4f389&RenderLoc=0&FlexDataID=1039&PageID=1&Comments=true"><span>Comments (-1)</span></a>
			
		</div>
		<div class="clear"></div>
	</div>
</li>
<li>
    <div class="ui-article">
		<div class="ui-article-description">
			<span><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p></span>
		</div>
		<div class="ui-article-controls">
        	<a class="sub-link" title="Go to comments for Title here" href="https://mpatzem.schoolwires.net/../site/default.aspx?PageType=3&ModuleInstanceID=1105&ViewID=ed695a1c-ef13-4546-b4eb-4fefcdd4f389&RenderLoc=0&FlexDataID=1038&PageID=1&Comments=true"><span>Comments (-1)</span></a>
			
		</div>
		<div class="clear"></div>
	</div>
</li>
<li>
    <div class="ui-article">
		<div class="ui-article-description">
			<span><p>Quam a nibh at hendrerit dui ac a at nascetur vestibulum arcu cum molestie erat netus litora vestibulum netus consectetur urna interdum curabitur a ligula ac torquent dolor. A a a tellus parturievestibulum curabitur iaculis vestibulum auctor lorem a arcu fames condimentum elementum nec ac parturient placerat parturient vivamus fermentum a vehicula. Quam a nibhs litora vestibulum netus consectetur urna interdum curabitur a ligula ac torquent</p></span>
		</div>
		<div class="ui-article-controls">
        	<a class="sub-link" title="Go to comments for Title Here" href="https://mpatzem.schoolwires.net/../site/default.aspx?PageType=3&ModuleInstanceID=1105&ViewID=ed695a1c-ef13-4546-b4eb-4fefcdd4f389&RenderLoc=0&FlexDataID=1539&PageID=1&Comments=true"><span>Comments (-1)</span></a>
			
		</div>
		<div class="clear"></div>
	</div>
</li>
</ul>
</div>
	<div class="ui-widget-footer">
		
		<div class="app-level-social-follow"></div> 
		<div class="clear"></div>
	</div>
</div>
<script type="text/javascript">
$(document).ready(function (){
$(".tag-list li a").keypress(function(e) { if(e.which == 13) { $(this).click();   }});
});
function LoadGroupedData(container, MIID, PMI, groupYear, groupMonth, groupBy, tag) {
  //ViewToUse looks at the hidden Sidebar List View defined in the Builder View.
  var viewToUse = "";
    if($('#hid-'+MIID+'-SidebarListView').length > 0){
      viewToUse = $("#hid-" + MIID + "-SidebarListView").val();
    };
  GetContent('https://mpatzem.schoolwires.net//cms/UserControls/ModuleView/ModuleViewRendererWrapper.aspx?DomainID=4&PageID=1&ModuleInstanceID=' + MIID + '&PageModuleInstanceID=' + PMI + '&RenderLoc=0&FromRenderLoc=0&GroupYear=' + groupYear + '&GroupMonth=' + groupMonth + '&GroupByField=' + groupBy + '&EnableQuirksMode=0&ViewID=' + viewToUse + '&Tag=' + tag, container, 2, 'chkSidebar();');
}
function LoadData(container, MIID, PMI, flexDataID, groupYear, groupMonth, groupBy, targetView, tag) {
  if(targetView !== undefined || targetView.Length() == 0){
  //targetView looks at the hidden Detail View defined in the Builder View.
      targetView = $('#hid-'+MIID+'-DetailView').val();
   }
  GetContent('https://mpatzem.schoolwires.net//cms/UserControls/ModuleView/ModuleViewRendererWrapper.aspx?DomainID=4&PageID=1&ModuleInstanceID=' + MIID + '&PageModuleInstanceID=' + PMI + '&FlexDataID=' + flexDataID + '&GroupYear=' + groupYear + '&GroupMonth=' + groupMonth + '&GroupByField=' + groupBy + '&RenderLoc=0&FromRenderLoc=0&EnableQuirksMode=0&Tag=' + tag + '&ViewID=' + targetView, container, 2, 'chkSidebar();');
}
function LoadTaggedData(container, MIID, PMI, tag) {
  //ViewToUse looks at the hidden Sidebar List View defined in the Builder View.
  var viewToUse = "";
    if($('#hid-'+MIID+'-SidebarListView').length > 0){
      viewToUse = $("#hid-" + MIID + "-SidebarListView").val();
    };
  GetContent('https://mpatzem.schoolwires.net//cms/UserControls/ModuleView/ModuleViewRendererWrapper.aspx?DomainID=4&PageID=1&ModuleInstanceID=' + MIID + '&PageModuleInstanceID=' + PMI + '&RenderLoc=0&FromRenderLoc=0&Tag=' + tag + '&EnableQuirksMode=0&ViewID='+viewToUse, container, 2, 'chkSidebar();');
  setTimeout(function(){ $("#module-content-"+ MIID +"").find('[tabindex]:first').focus(); }, 200);
}
</script>
</div>

</div></div>
</div></div>
<div style="border:5px solid black;margin:10px;"><div id="sw-content-container14" class="region ui-hp"></div></div>
<div style="border:5px solid black;margin:10px;"><div id="sw-content-container15" class="region ui-hp"></div></div>
<div style="border:5px solid black;margin:10px;"><div id="sw-content-container16" class="region ui-hp"></div></div>
<div style="border:5px solid black;margin:10px;"><div id="sw-content-container17" class="region ui-hp"></div></div>
<div style="border:5px solid black;margin:10px;"><div id="sw-content-container18" class="region ui-hp"></div></div>
<div style="border:5px solid black;margin:10px;"><div id="sw-content-container19" class="region ui-hp"></div></div>
<div style="border:5px solid black;margin:10px;"><div id="sw-content-container20" class="region ui-hp"><div id='pmi-1952'>



<div id='sw-module-17890'>
    <script type="text/javascript">
        $(document).ready(function() {
            var DomainID = '4';
            var PageID = '1';
            var RenderLoc = '0';
            var MIID = '1789';

            //added to check to make sure moderated content doesn't bleed through the dialog
            if ($('#dialog-overlay-WindowLargeModal-body.moderateContent').length > 0) {
                $("#module-content-" + MIID).find(".ui-widget-detail").find(".ui-article").append("<p>&nbsp;</p>");
            }
        });
    </script>

    <script type="text/javascript">$(document).ready(function() {CheckScript('ModuleView');CheckScript('Mustache');
 });</script>
    
    <div id="module-content-1789" >
<link rel="stylesheet" type="text/css" href="//extend.schoolwires.com/creative/css/sw-icon-library/sw-icon-library.min.css" />
<link rel="stylesheet" type="text/css" href="//extend.schoolwires.com/creative/module_library/icons/horizontal/css/icons-horizontal.min.css" />
<style type="text/css">
    #module-content-1789 .border-box {
        -webkit-box-sizing: border-box;
        -moz-box-sizing: border-box;
        box-sizing: border-box;
    }
    #module-content-1789 .flex {
        display: -ms-flexbox;
        display: -webkit-flex;
        display: flex;
    }
	#module-content-1789 .flex-justify-center {
        -webkit-justify-content: center;
        -ms-flex-pack: center;
        justify-content: center;
	}
    #module-content-1789 .flex-items-center {
        -webkit-align-items: center;
        -ms-flex-align: center;
        align-items: center;
    }
    
    #module-content-1789 .ui-articles {
        display: -ms-flexbox;
        display: -webkit-flex;
        display: flex;
        -webkit-flex-wrap: wrap;
        -ms-flex-wrap: wrap;
        flex-wrap: wrap;
        margin: 0 -20px;
    }
    #module-content-1789 .ui-articles > li {
    	width: 100%;
        min-width: calc(25% - 40px);
        max-width: 150px;
        margin: 0 20px 10px;
        padding: 0;
    }
    #module-content-1789 .ui-articles > li .ui-article {
    	padding: 5px 0;
        text-align: center;
    }
    #module-content-1789 .ui-articles > li .ui-article .icon {
    	margin: 0 0 10px 0;
    }
    #module-content-1789 .ui-articles > li .ui-article h2 {
    	margin: 0 0 10px 0;
    }
    #module-content-1789 .ui-articles > li .ui-article .icon,
    #module-content-1789 .ui-articles > li .ui-article h2[role="button"] {
        cursor: pointer;
    }
    #module-content-1789 .ui-articles > li .ui-article .icon {
    	position: relative;
        top: 0;
        -webkit-transition: all 0.25s;
        -moz-transition: all 0.25s;
        -ms-transition: all 0.25s;
        -o-transition: all 0.25s;
        transition: all 0.25s;        
    }
    #module-content-1789 .ui-articles > li .ui-article .icon:hover,
    #module-content-1789 .ui-articles > li .ui-article .icon:focus {
    	top: -5px;
    }
    #module-content-1789 .ui-articles > li .ui-article-overlay-outer {
        width: 100vw;
        height: 100vh;
    	display: none;
        position: fixed;
        top: 0;
        left: 0;
        z-index: 99999;
        background: rgba(0,0,0,0.5);
    }
    #module-content-1789 .ui-articles > li .ui-article-overlay {
		width: 100%;
        height: 100%;
    }
    #module-content-1789 .ui-articles > li .ui-article-overlay-inner {
    	width: 50vw;
        margin: 0 auto;
    	padding: 10px 20px 39px;
        background: #fff;
        -webkit-box-shadow: 0 0 3px 0 rgba(0,0,0,0.5);
        box-shadow: 0 0 3px 0 rgba(0,0,0,0.5);
    }
    #module-content-1789 .ui-articles > li .ui-article-overlay-close-button {
    	width: 30px;
        height: 30px;
        margin: 0 0 10px auto;
        cursor: pointer;
    }
    #module-content-1789 .ui-articles > li .ui-article-overlay-close-button:before {
        content: "\2715";
        color: #757575;
        font-size: 30px;
        font-weight: 300;
    }
    #module-content-1789 .ui-articles > li .ui-article-overlay-content {
    	max-height: calc(100vh - 200px);
        overflow-x: hidden;
        overflow-y: auto;
    }
    #module-content-1789 .ui-articles > li .ui-article-overlay-content * {
    	color: #000;
    }
    [data-primary-text-color="White"] #module-content-1789 .ui-articles > li .ui-article-overlay-content .Primary_Color_Box,
    [data-primary-text-color="White"] #module-content-1789 .ui-articles > li .ui-article-overlay-content .Primary_Color_Box .H1_Template,
    [data-primary-text-color="White"] #module-content-1789 .ui-articles > li .ui-article-overlay-content .Primary_Color_Box .H2_Template,
    [data-primary-text-color="White"] #module-content-1789 .ui-articles > li .ui-article-overlay-content .Primary_Color_Box .H3_Template,
    [data-primary-text-color="White"] #module-content-1789 .ui-articles > li .ui-article-overlay-content .Primary_Color_Box .H4_Template,
    [data-primary-text-color="White"] #module-content-1789 .ui-articles > li .ui-article-overlay-content .Primary_Color_Box a,
    [data-secondary-text-color="White"] #module-content-1789 .ui-articles > li .ui-article-overlay-content .Secondary_Color_Box,
    [data-secondary-text-color="White"] #module-content-1789 .ui-articles > li .ui-article-overlay-content .Secondary_Color_Box .H1_Template,
    [data-secondary-text-color="White"] #module-content-1789 .ui-articles > li .ui-article-overlay-content .Secondary_Color_Box .H2_Template,
    [data-secondary-text-color="White"] #module-content-1789 .ui-articles > li .ui-article-overlay-content .Secondary_Color_Box .H3_Template,
    [data-secondary-text-color="White"] #module-content-1789 .ui-articles > li .ui-article-overlay-content .Secondary_Color_Box .H4_Template,
    [data-secondary-text-color="White"] #module-content-1789 .ui-articles > li .ui-article-overlay-content .Secondary_Color_Box {
    	color: #fff;
    }
	
    /* Responsive */
    #module-content-1789 .mobile-large-view .ui-articles > li {
        min-width: calc(50% - 40px);
    }
    #module-content-1789 .mobile-large-view .ui-articles > li .ui-article-overlay-inner {
    	width: calc(80vw - 20px);
    }
    #module-content-1789 .mobile-large-view .ui-articles > li .ui-article-overlay-content {
    	max-height: calc(100vh - 100px);
        overflow-x: hidden;
        overflow-y: auto;
    }
    #module-content-1789 .mobile-small-view .ui-articles {
        -webkit-justify-content: center;
        -ms-flex-pack: center;
        justify-content: center;
    }
    #module-content-1789 .mobile-small-view .ui-articles > li {
    	min-width: calc(100% - 40px);
    }
</style>

<script tyle="text/javascript" src="//extend.schoolwires.com/creative/scripts/creative/tools/hex-to-filter/css-hex-to-filter.min.js"></script>

<div class="ui-widget app lightbox">
	<div class="ui-widget-header">
		<h1>Media</h1>
	</div>
	
	<div class="ui-widget-detail">
		<ul class="ui-articles">
<li data-id="1526">

	<!-- Record Article -->
	<div class="ui-article">
        
        	
            	<!-- Custom Image Is Selected -->
            	<div role="button" class="icon custom" aria-label="Open lightbox content for Test Lightbox Header." aria-controls="ui-article-overlay-1526" tabindex="0"><img src="https://mpatzem.schoolwires.net/cms/lib/SWCS000010/Centricity/Domain/4/new-pdcst.png" alt="" /></div>
             
        
		
        <!-- Record Title -->
        
        	<!-- Record Title Is Hidden -->
        
        <!-- End Record Title -->

		<!-- Record Description -->
        
            
            	<!-- Record Decription Text Is Blank -->
            
        
        <!-- End Record Description -->
    </div>
    
    <!-- Record Overlay -->
    <div id="ui-article-overlay-1526" class="ui-article-overlay-outer" aria-hidden="true" role="dialog">
    	<div class="ui-article-overlay flex flex-justify-center flex-items-center">
            <div class="ui-article-overlay-inner border-box">
                <div class="ui-article-overlay-close-button flex flex-justify-center flex-items-center" role="button" aria-label="Close the lightbox content." aria-controls="ui-article-overlay-1526" tabindex="0"></div>
                <div class="ui-article-overlay-content">
                	<div class="ui-article-overlay-content-inner" tabindex="0">
                   		<div style="padding: 56.25% 0 0 0; position: relative;"><iframe style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;" src="https://player.vimeo.com/video/161801298?h=376fe72dbd&amp;color=f7f7f7&amp;title=0&amp;byline=0&amp;portrait=0" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen="allowfullscreen"></iframe></div>
<p>
<script src="https://player.vimeo.com/api/player.js"></script>
</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <!-- Record Icon Size -->
    <style type="text/css">    
    	
        	#module-content-1789 .ui-articles > li[data-id="1526"] {
                max-width: 150px;
                max-width: px;
            }
            #module-content-1789 .ui-articles > li[data-id="1526"] .icon.custom img {
                max-width: 150px;
                max-height: 150px;
                max-width: px;
                max-height: px;
            }
                
    </style>
    
    <!-- If Custom Image -->
    
    	<!-- If Use Icon Color -->
        
        
        
        <!-- End If Use Icon Color -->
    
</li>
<li data-id="1528">

	<!-- Record Article -->
	<div class="ui-article">
        
        	
            	<!-- Custom Image Is Selected -->
            	<div role="button" class="icon custom" aria-label="Open lightbox content for Test." aria-controls="ui-article-overlay-1528" tabindex="0"><img src="https://mpatzem.schoolwires.net/cms/lib/SWCS000010/Centricity//Domain/4/test-lightbox.png" alt="" /></div>
             
        
		
        <!-- Record Title -->
        
        	<!-- Record Title Is Hidden -->
        
        <!-- End Record Title -->

		<!-- Record Description -->
        
            
            	<!-- Record Decription Text Is Blank -->
            
        
        <!-- End Record Description -->
    </div>
    
    <!-- Record Overlay -->
    <div id="ui-article-overlay-1528" class="ui-article-overlay-outer" aria-hidden="true" role="dialog">
    	<div class="ui-article-overlay flex flex-justify-center flex-items-center">
            <div class="ui-article-overlay-inner border-box">
                <div class="ui-article-overlay-close-button flex flex-justify-center flex-items-center" role="button" aria-label="Close the lightbox content." aria-controls="ui-article-overlay-1528" tabindex="0"></div>
                <div class="ui-article-overlay-content">
                	<div class="ui-article-overlay-content-inner" tabindex="0">
                   		<div style="padding: 56.25% 0 0 0; position: relative;"><iframe style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;" src="https://player.vimeo.com/video/336892239?h=1335474bbe&amp;color=f7f7f7&amp;title=0&amp;byline=0&amp;portrait=0" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen="allowfullscreen"></iframe></div>
<p>
<script src="https://player.vimeo.com/api/player.js"></script>
</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <!-- Record Icon Size -->
    <style type="text/css">    
    	
        	#module-content-1789 .ui-articles > li[data-id="1528"] {
                max-width: 150px;
                max-width: px;
            }
            #module-content-1789 .ui-articles > li[data-id="1528"] .icon.custom img {
                max-width: 150px;
                max-height: 150px;
                max-width: px;
                max-height: px;
            }
                
    </style>
    
    <!-- If Custom Image -->
    
    	<!-- If Use Icon Color -->
        
        
        
        <!-- End If Use Icon Color -->
    
</li>
<li data-id="1529">

	<!-- Record Article -->
	<div class="ui-article">
        
        	
            	<!-- Custom Image Is Selected -->
            	<div role="button" class="icon custom" aria-label="Open lightbox content for Test 3." aria-controls="ui-article-overlay-1529" tabindex="0"><img src="https://mpatzem.schoolwires.net/cms/lib/SWCS000010/Centricity//Domain/4/test-lightbox.png" alt="" /></div>
             
        
		
        <!-- Record Title -->
        
        	<!-- Record Title Is Hidden -->
        
        <!-- End Record Title -->

		<!-- Record Description -->
        
            
            	<!-- Record Decription Text Is Blank -->
            
        
        <!-- End Record Description -->
    </div>
    
    <!-- Record Overlay -->
    <div id="ui-article-overlay-1529" class="ui-article-overlay-outer" aria-hidden="true" role="dialog">
    	<div class="ui-article-overlay flex flex-justify-center flex-items-center">
            <div class="ui-article-overlay-inner border-box">
                <div class="ui-article-overlay-close-button flex flex-justify-center flex-items-center" role="button" aria-label="Close the lightbox content." aria-controls="ui-article-overlay-1529" tabindex="0"></div>
                <div class="ui-article-overlay-content">
                	<div class="ui-article-overlay-content-inner" tabindex="0">
                   		<div style="padding: 56.25% 0 0 0; position: relative;"><iframe style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;" src="https://player.vimeo.com/video/336892239?h=1335474bbe&amp;color=f7f7f7&amp;title=0&amp;byline=0&amp;portrait=0" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen="allowfullscreen"></iframe></div>
<p>
<script src="https://player.vimeo.com/api/player.js"></script>
</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <!-- Record Icon Size -->
    <style type="text/css">    
    	
        	#module-content-1789 .ui-articles > li[data-id="1529"] {
                max-width: 150px;
                max-width: px;
            }
            #module-content-1789 .ui-articles > li[data-id="1529"] .icon.custom img {
                max-width: 150px;
                max-height: 150px;
                max-width: px;
                max-height: px;
            }
                
    </style>
    
    <!-- If Custom Image -->
    
    	<!-- If Use Icon Color -->
        
        
        
        <!-- End If Use Icon Color -->
    
</li>
<li data-id="1530">

	<!-- Record Article -->
	<div class="ui-article">
        
        	
            	<!-- Custom Image Is Selected -->
            	<div role="button" class="icon custom" aria-label="Open lightbox content for Test 4." aria-controls="ui-article-overlay-1530" tabindex="0"><img src="https://mpatzem.schoolwires.net/cms/lib/SWCS000010/Centricity//Domain/4/test-lightbox.png" alt="" /></div>
             
        
		
        <!-- Record Title -->
        
        	<!-- Record Title Is Hidden -->
        
        <!-- End Record Title -->

		<!-- Record Description -->
        
            
            	<!-- Record Decription Text Is Blank -->
            
        
        <!-- End Record Description -->
    </div>
    
    <!-- Record Overlay -->
    <div id="ui-article-overlay-1530" class="ui-article-overlay-outer" aria-hidden="true" role="dialog">
    	<div class="ui-article-overlay flex flex-justify-center flex-items-center">
            <div class="ui-article-overlay-inner border-box">
                <div class="ui-article-overlay-close-button flex flex-justify-center flex-items-center" role="button" aria-label="Close the lightbox content." aria-controls="ui-article-overlay-1530" tabindex="0"></div>
                <div class="ui-article-overlay-content">
                	<div class="ui-article-overlay-content-inner" tabindex="0">
                   		<div style="padding: 56.25% 0 0 0; position: relative;"><iframe style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;" src="https://player.vimeo.com/video/336892239?h=1335474bbe&amp;color=f7f7f7&amp;title=0&amp;byline=0&amp;portrait=0" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen="allowfullscreen"></iframe></div>
<p>
<script src="https://player.vimeo.com/api/player.js"></script>
</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <!-- Record Icon Size -->
    <style type="text/css">    
    	
        	#module-content-1789 .ui-articles > li[data-id="1530"] {
                max-width: 150px;
                max-width: px;
            }
            #module-content-1789 .ui-articles > li[data-id="1530"] .icon.custom img {
                max-width: 150px;
                max-height: 150px;
                max-width: px;
                max-height: px;
            }
                
    </style>
    
    <!-- If Custom Image -->
    
    	<!-- If Use Icon Color -->
        
        
        
        <!-- End If Use Icon Color -->
    
</li>
<li data-id="1531">

	<!-- Record Article -->
	<div class="ui-article">
        
        	
            	<!-- Custom Image Is Selected -->
            	<div role="button" class="icon custom" aria-label="Open lightbox content for Test 5." aria-controls="ui-article-overlay-1531" tabindex="0"><img src="https://mpatzem.schoolwires.net/cms/lib/SWCS000010/Centricity//Domain/4/test-lightbox.png" alt="" /></div>
             
        
		
        <!-- Record Title -->
        
        	<!-- Record Title Is Hidden -->
        
        <!-- End Record Title -->

		<!-- Record Description -->
        
            
            	<!-- Record Decription Text Is Blank -->
            
        
        <!-- End Record Description -->
    </div>
    
    <!-- Record Overlay -->
    <div id="ui-article-overlay-1531" class="ui-article-overlay-outer" aria-hidden="true" role="dialog">
    	<div class="ui-article-overlay flex flex-justify-center flex-items-center">
            <div class="ui-article-overlay-inner border-box">
                <div class="ui-article-overlay-close-button flex flex-justify-center flex-items-center" role="button" aria-label="Close the lightbox content." aria-controls="ui-article-overlay-1531" tabindex="0"></div>
                <div class="ui-article-overlay-content">
                	<div class="ui-article-overlay-content-inner" tabindex="0">
                   		<div style="padding: 56.25% 0 0 0; position: relative;"><iframe style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;" src="https://player.vimeo.com/video/161801298?h=376fe72dbd&amp;color=f7f7f7&amp;title=0&amp;byline=0&amp;portrait=0" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen="allowfullscreen"></iframe></div>
<p>
<script src="https://player.vimeo.com/api/player.js"></script>
</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <!-- Record Icon Size -->
    <style type="text/css">    
    	
        	#module-content-1789 .ui-articles > li[data-id="1531"] {
                max-width: 150px;
                max-width: px;
            }
            #module-content-1789 .ui-articles > li[data-id="1531"] .icon.custom img {
                max-width: 150px;
                max-height: 150px;
                max-width: px;
                max-height: px;
            }
                
    </style>
    
    <!-- If Custom Image -->
    
    	<!-- If Use Icon Color -->
        
        
        
        <!-- End If Use Icon Color -->
    
</li>
</ul><div class="ui-read-more"><a id='MoreLinkButton1789' class='more-link' aria-label='Go to more records' onclick='MoreViewClick(this);' href='https://mpatzem.schoolwires.net/site/default.aspx?PageType=14&DomainID=4&PageID=1&ModuleInstanceID=1789&IsMoreExpandedView=True'><span>more</span></a><div class='more-link-under'>&nbsp;</div></div>
</ul>
	</div>
	<div class="ui-widget-footer ui-clear"></div>
</div>

<script tyle="text/javascript" src="//extend.schoolwires.com/creative/module_library/icons/horizontal/js/icons-horizontal.min.js"></script>
<script tyle="text/javascript" src="//extend.schoolwires.com/creative/module_library/lightbox/js/cs.lightbox.app.min.js"></script>
<script type="text/javascript">
    $(function() {
        CsIconsHorizontalApp.Init(1952);
        CsLightboxApp.Init(1952, 1789);
    });
</script>
<script type="text/javascript">
$(document).ready(function (){
$(".tag-list li a").keypress(function(e) { if(e.which == 13) { $(this).click();   }});
});
function LoadGroupedData(container, MIID, PMI, groupYear, groupMonth, groupBy, tag) {
  //ViewToUse looks at the hidden Sidebar List View defined in the Builder View.
  var viewToUse = "";
    if($('#hid-'+MIID+'-SidebarListView').length > 0){
      viewToUse = $("#hid-" + MIID + "-SidebarListView").val();
    };
  GetContent('https://mpatzem.schoolwires.net//cms/UserControls/ModuleView/ModuleViewRendererWrapper.aspx?DomainID=4&PageID=1&ModuleInstanceID=' + MIID + '&PageModuleInstanceID=' + PMI + '&RenderLoc=0&FromRenderLoc=0&GroupYear=' + groupYear + '&GroupMonth=' + groupMonth + '&GroupByField=' + groupBy + '&EnableQuirksMode=0&ViewID=' + viewToUse + '&Tag=' + tag, container, 2, 'chkSidebar();');
}
function LoadData(container, MIID, PMI, flexDataID, groupYear, groupMonth, groupBy, targetView, tag) {
  if(targetView !== undefined || targetView.Length() == 0){
  //targetView looks at the hidden Detail View defined in the Builder View.
      targetView = $('#hid-'+MIID+'-DetailView').val();
   }
  GetContent('https://mpatzem.schoolwires.net//cms/UserControls/ModuleView/ModuleViewRendererWrapper.aspx?DomainID=4&PageID=1&ModuleInstanceID=' + MIID + '&PageModuleInstanceID=' + PMI + '&FlexDataID=' + flexDataID + '&GroupYear=' + groupYear + '&GroupMonth=' + groupMonth + '&GroupByField=' + groupBy + '&RenderLoc=0&FromRenderLoc=0&EnableQuirksMode=0&Tag=' + tag + '&ViewID=' + targetView, container, 2, 'chkSidebar();');
}
function LoadTaggedData(container, MIID, PMI, tag) {
  //ViewToUse looks at the hidden Sidebar List View defined in the Builder View.
  var viewToUse = "";
    if($('#hid-'+MIID+'-SidebarListView').length > 0){
      viewToUse = $("#hid-" + MIID + "-SidebarListView").val();
    };
  GetContent('https://mpatzem.schoolwires.net//cms/UserControls/ModuleView/ModuleViewRendererWrapper.aspx?DomainID=4&PageID=1&ModuleInstanceID=' + MIID + '&PageModuleInstanceID=' + PMI + '&RenderLoc=0&FromRenderLoc=0&Tag=' + tag + '&EnableQuirksMode=0&ViewID='+viewToUse, container, 2, 'chkSidebar();');
  setTimeout(function(){ $("#module-content-"+ MIID +"").find('[tabindex]:first').focus(); }, 200);
}
</script>
</div>

</div></div>
</div></div>
<!-- BEGIN - STANDARD FOOTER -->
<div id='sw-footer-outer'>
<div id='sw-footer-inner'>
<div id='sw-footer-left'></div>
<div id='sw-footer-right'>
<div id='sw-footer-links'>
<ul>
<li><a title='Click to email the primary contact' href='mailto:noreply@schoolwires.com'>Questions or Feedback?</a> | </li>
<li><a href='https://www.blackboard.com/blackboard-web-community-manager-privacy-statement' target="_blank">Blackboard Web Community Manager Privacy Policy (Updated)</a> | </li>
<li><a href='https://help.blackboard.com/Terms_of_Use' target="_blank">Terms of Use</a></li>
</ul>
</div>
<div id='sw-footer-copyright'>Copyright &copy; 2002-2023 Blackboard, Inc. All rights reserved.</div>
<div id='sw-footer-logo'><a href='http://www.blackboard.com' title="Blackboard, Inc. All rights reserved.">
<img src='https://mpatzem.schoolwires.net/Static//GlobalAssets/Images/Navbar/blackboard_logo.png'
 alt="Blackboard, Inc. All rights reserved."/>
</a></div>
</div>
</div>
</div>
<!-- END - STANDARD FOOTER -->
<script type="text/javascript">
   $(document).ready(function(){
      var beaconURL='https://analytics.schoolwires.com/analytics.asmx/Insert?AccountNumber=6boZxfPfyUa2SJk0fiN5BA%3d%3d&SessionID=09a9b949-98aa-47f5-9940-09cee6816c80&SiteID=4&ChannelID=0&SectionID=0&PageID=1&HitDate=4%2f5%2f2023+3%3a55%3a38+PM&Browser=Chrome+87.0&OS=Unknown&IPAddress=10.61.92.252';
      try {
         $.getJSON(beaconURL + '&jsonp=?', function(myData) {});
      } catch(err) { 
         // prevent site error for analytics
      }
   });
</script>

    <input type="hidden" id="hid-pageid" value="1" />

    

    <div id='dialog-overlay-WindowMedium-base' class='ui-dialog-overlay-base' ><div id='WindowMedium' role='dialog' tabindex='-1'  class='ui-dialog-overlay medium' ><div class='ui-dialog-overlay-title-bar' id='dialog-overlay-WindowMedium-title-bar' ></div><div class='ui-dialog-overlay-body' id='dialog-overlay-WindowMedium-body' ></div><button class='ui-dialog-overlay-close' title='Close' aria-label='Close' onclick='CloseDialogOverlay("WindowMedium");' ></button><div class='ui-dialog-overlay-footer' id='dialog-overlay-WindowMedium-footer' ></div></div></div>

<script type="text/javascript">
    $(document).ready(function() {
        $('#dialog-overlay-WindowMedium-base').appendTo('body');
        
        $('body').on('keydown', '.ui-dialog-overlay-base-modal, .ui-dialog-overlay, .ui-sw-alert', function (e) {
            var swAlertOpen = $(".ui-sw-alert").length;
            if (swAlertOpen > 1) {
                if (e.keyCode == 27) {//escape key
                    e.stopImmediatePropagation();
                    e.preventDefault();
                    //get id of open alert
                    var alertboxid = $('.ui-sw-alert').attr('id');
                    //click ok or no
                    if ($('#' + alertboxid + 'ok').length > 0) {
                        $('#' + alertboxid + 'ok').click();
                    } else {
                        $('#' + alertboxid + 'no').click();
                    }
                }
            } else {
                if (e.keyCode == 27) {//escape key
                    e.stopImmediatePropagation();
                    e.preventDefault();
                    // Check if ESC was pressed on DatePicker
                    var raisedByDatepicker = e.target.classList.contains("datepicker");
                    if (!raisedByDatepicker) {
                        e.stopImmediatePropagation();
                        e.preventDefault();
                        // Close Dialog
                        $('.ui-dialog-overlay-close.modal:visible').last().click();
                    } else {
                        // Remove focus
                        e.target.blur();
                        e.target.classList.remove("focus");
                    }
                }
            }
        });
 		
        

    });
</script>
    <div id='dialog-overlay-WindowSmall-base' class='ui-dialog-overlay-base' ><div id='WindowSmall' role='dialog' tabindex='-1'  class='ui-dialog-overlay small' ><div class='ui-dialog-overlay-title-bar' id='dialog-overlay-WindowSmall-title-bar' ></div><div class='ui-dialog-overlay-body' id='dialog-overlay-WindowSmall-body' ></div><button class='ui-dialog-overlay-close' title='Close' aria-label='Close' onclick='CloseDialogOverlay("WindowSmall");' ></button><div class='ui-dialog-overlay-footer' id='dialog-overlay-WindowSmall-footer' ></div></div></div>

<script type="text/javascript">
    $(document).ready(function() {
        $('#dialog-overlay-WindowSmall-base').appendTo('body');
        
        $('body').on('keydown', '.ui-dialog-overlay-base-modal, .ui-dialog-overlay, .ui-sw-alert', function (e) {
            var swAlertOpen = $(".ui-sw-alert").length;
            if (swAlertOpen > 1) {
                if (e.keyCode == 27) {//escape key
                    e.stopImmediatePropagation();
                    e.preventDefault();
                    //get id of open alert
                    var alertboxid = $('.ui-sw-alert').attr('id');
                    //click ok or no
                    if ($('#' + alertboxid + 'ok').length > 0) {
                        $('#' + alertboxid + 'ok').click();
                    } else {
                        $('#' + alertboxid + 'no').click();
                    }
                }
            } else {
                if (e.keyCode == 27) {//escape key
                    e.stopImmediatePropagation();
                    e.preventDefault();
                    // Check if ESC was pressed on DatePicker
                    var raisedByDatepicker = e.target.classList.contains("datepicker");
                    if (!raisedByDatepicker) {
                        e.stopImmediatePropagation();
                        e.preventDefault();
                        // Close Dialog
                        $('.ui-dialog-overlay-close.modal:visible').last().click();
                    } else {
                        // Remove focus
                        e.target.blur();
                        e.target.classList.remove("focus");
                    }
                }
            }
        });
 		
        

    });
</script>
    <div id='dialog-overlay-WindowLarge-base' class='ui-dialog-overlay-base' ><div id='WindowLarge' role='dialog' tabindex='-1'  class='ui-dialog-overlay large' ><div class='ui-dialog-overlay-title-bar' id='dialog-overlay-WindowLarge-title-bar' ></div><div class='ui-dialog-overlay-body' id='dialog-overlay-WindowLarge-body' ></div><button class='ui-dialog-overlay-close' title='Close' aria-label='Close' onclick='CloseDialogOverlay("WindowLarge");' ></button><div class='ui-dialog-overlay-footer' id='dialog-overlay-WindowLarge-footer' ></div></div></div>

<script type="text/javascript">
    $(document).ready(function() {
        $('#dialog-overlay-WindowLarge-base').appendTo('body');
        
        $('body').on('keydown', '.ui-dialog-overlay-base-modal, .ui-dialog-overlay, .ui-sw-alert', function (e) {
            var swAlertOpen = $(".ui-sw-alert").length;
            if (swAlertOpen > 1) {
                if (e.keyCode == 27) {//escape key
                    e.stopImmediatePropagation();
                    e.preventDefault();
                    //get id of open alert
                    var alertboxid = $('.ui-sw-alert').attr('id');
                    //click ok or no
                    if ($('#' + alertboxid + 'ok').length > 0) {
                        $('#' + alertboxid + 'ok').click();
                    } else {
                        $('#' + alertboxid + 'no').click();
                    }
                }
            } else {
                if (e.keyCode == 27) {//escape key
                    e.stopImmediatePropagation();
                    e.preventDefault();
                    // Check if ESC was pressed on DatePicker
                    var raisedByDatepicker = e.target.classList.contains("datepicker");
                    if (!raisedByDatepicker) {
                        e.stopImmediatePropagation();
                        e.preventDefault();
                        // Close Dialog
                        $('.ui-dialog-overlay-close.modal:visible').last().click();
                    } else {
                        // Remove focus
                        e.target.blur();
                        e.target.classList.remove("focus");
                    }
                }
            }
        });
 		
        

    });
</script>

    <div id='dialog-overlay-WindowMediumModal-base' class='ui-dialog-overlay-base-modal' ><div id='WindowMediumModal' role='dialog' tabindex='-1'  class='ui-dialog-overlay medium' ><div class='ui-dialog-overlay-title-bar' id='dialog-overlay-WindowMediumModal-title-bar' ></div><div class='ui-dialog-overlay-body' id='dialog-overlay-WindowMediumModal-body' ></div><button class='ui-dialog-overlay-close modal' title='Close' aria-label='Close' onclick='CloseDialogOverlay("WindowMediumModal");' ></button><div class='ui-dialog-overlay-footer' id='dialog-overlay-WindowMediumModal-footer' ></div></div></div>

<script type="text/javascript">
    $(document).ready(function() {
        $('#dialog-overlay-WindowMediumModal-base').appendTo('body');
        
        $('body').on('keydown', '.ui-dialog-overlay-base-modal, .ui-dialog-overlay, .ui-sw-alert', function (e) {
            var swAlertOpen = $(".ui-sw-alert").length;
            if (swAlertOpen > 1) {
                if (e.keyCode == 27) {//escape key
                    e.stopImmediatePropagation();
                    e.preventDefault();
                    //get id of open alert
                    var alertboxid = $('.ui-sw-alert').attr('id');
                    //click ok or no
                    if ($('#' + alertboxid + 'ok').length > 0) {
                        $('#' + alertboxid + 'ok').click();
                    } else {
                        $('#' + alertboxid + 'no').click();
                    }
                }
            } else {
                if (e.keyCode == 27) {//escape key
                    e.stopImmediatePropagation();
                    e.preventDefault();
                    // Check if ESC was pressed on DatePicker
                    var raisedByDatepicker = e.target.classList.contains("datepicker");
                    if (!raisedByDatepicker) {
                        e.stopImmediatePropagation();
                        e.preventDefault();
                        // Close Dialog
                        $('.ui-dialog-overlay-close.modal:visible').last().click();
                    } else {
                        // Remove focus
                        e.target.blur();
                        e.target.classList.remove("focus");
                    }
                }
            }
        });
 		
        

    });
</script>
    <div id='dialog-overlay-WindowSmallModal-base' class='ui-dialog-overlay-base-modal' ><div id='WindowSmallModal' role='dialog' tabindex='-1'  class='ui-dialog-overlay small' ><div class='ui-dialog-overlay-title-bar' id='dialog-overlay-WindowSmallModal-title-bar' ></div><div class='ui-dialog-overlay-body' id='dialog-overlay-WindowSmallModal-body' ></div><button class='ui-dialog-overlay-close modal' title='Close' aria-label='Close' onclick='CloseDialogOverlay("WindowSmallModal");' ></button><div class='ui-dialog-overlay-footer' id='dialog-overlay-WindowSmallModal-footer' ></div></div></div>

<script type="text/javascript">
    $(document).ready(function() {
        $('#dialog-overlay-WindowSmallModal-base').appendTo('body');
        
        $('body').on('keydown', '.ui-dialog-overlay-base-modal, .ui-dialog-overlay, .ui-sw-alert', function (e) {
            var swAlertOpen = $(".ui-sw-alert").length;
            if (swAlertOpen > 1) {
                if (e.keyCode == 27) {//escape key
                    e.stopImmediatePropagation();
                    e.preventDefault();
                    //get id of open alert
                    var alertboxid = $('.ui-sw-alert').attr('id');
                    //click ok or no
                    if ($('#' + alertboxid + 'ok').length > 0) {
                        $('#' + alertboxid + 'ok').click();
                    } else {
                        $('#' + alertboxid + 'no').click();
                    }
                }
            } else {
                if (e.keyCode == 27) {//escape key
                    e.stopImmediatePropagation();
                    e.preventDefault();
                    // Check if ESC was pressed on DatePicker
                    var raisedByDatepicker = e.target.classList.contains("datepicker");
                    if (!raisedByDatepicker) {
                        e.stopImmediatePropagation();
                        e.preventDefault();
                        // Close Dialog
                        $('.ui-dialog-overlay-close.modal:visible').last().click();
                    } else {
                        // Remove focus
                        e.target.blur();
                        e.target.classList.remove("focus");
                    }
                }
            }
        });
 		
        

    });
</script>
    <div id='dialog-overlay-WindowLargeModal-base' class='ui-dialog-overlay-base-modal' ><div id='WindowLargeModal' role='dialog' tabindex='-1'  class='ui-dialog-overlay large' ><div class='ui-dialog-overlay-title-bar' id='dialog-overlay-WindowLargeModal-title-bar' ></div><div class='ui-dialog-overlay-body' id='dialog-overlay-WindowLargeModal-body' ></div><button class='ui-dialog-overlay-close modal' title='Close' aria-label='Close' onclick='CloseDialogOverlay("WindowLargeModal");' ></button><div class='ui-dialog-overlay-footer' id='dialog-overlay-WindowLargeModal-footer' ></div></div></div>

<script type="text/javascript">
    $(document).ready(function() {
        $('#dialog-overlay-WindowLargeModal-base').appendTo('body');
        
        $('body').on('keydown', '.ui-dialog-overlay-base-modal, .ui-dialog-overlay, .ui-sw-alert', function (e) {
            var swAlertOpen = $(".ui-sw-alert").length;
            if (swAlertOpen > 1) {
                if (e.keyCode == 27) {//escape key
                    e.stopImmediatePropagation();
                    e.preventDefault();
                    //get id of open alert
                    var alertboxid = $('.ui-sw-alert').attr('id');
                    //click ok or no
                    if ($('#' + alertboxid + 'ok').length > 0) {
                        $('#' + alertboxid + 'ok').click();
                    } else {
                        $('#' + alertboxid + 'no').click();
                    }
                }
            } else {
                if (e.keyCode == 27) {//escape key
                    e.stopImmediatePropagation();
                    e.preventDefault();
                    // Check if ESC was pressed on DatePicker
                    var raisedByDatepicker = e.target.classList.contains("datepicker");
                    if (!raisedByDatepicker) {
                        e.stopImmediatePropagation();
                        e.preventDefault();
                        // Close Dialog
                        $('.ui-dialog-overlay-close.modal:visible').last().click();
                    } else {
                        // Remove focus
                        e.target.blur();
                        e.target.classList.remove("focus");
                    }
                }
            }
        });
 		
        

    });
</script>
    <div id='dialog-overlay-WindowXLargeModal-base' class='ui-dialog-overlay-base-modal' ><div id='WindowXLargeModal' role='dialog' tabindex='-1'  class='ui-dialog-overlay xlarge' ><div class='ui-dialog-overlay-title-bar' id='dialog-overlay-WindowXLargeModal-title-bar' ></div><div class='ui-dialog-overlay-body' id='dialog-overlay-WindowXLargeModal-body' ></div><button class='ui-dialog-overlay-close modal' title='Close' aria-label='Close' onclick='CloseDialogOverlay("WindowXLargeModal");' ></button><div class='ui-dialog-overlay-footer' id='dialog-overlay-WindowXLargeModal-footer' ></div></div></div>

<script type="text/javascript">
    $(document).ready(function() {
        $('#dialog-overlay-WindowXLargeModal-base').appendTo('body');
        
        $('body').on('keydown', '.ui-dialog-overlay-base-modal, .ui-dialog-overlay, .ui-sw-alert', function (e) {
            var swAlertOpen = $(".ui-sw-alert").length;
            if (swAlertOpen > 1) {
                if (e.keyCode == 27) {//escape key
                    e.stopImmediatePropagation();
                    e.preventDefault();
                    //get id of open alert
                    var alertboxid = $('.ui-sw-alert').attr('id');
                    //click ok or no
                    if ($('#' + alertboxid + 'ok').length > 0) {
                        $('#' + alertboxid + 'ok').click();
                    } else {
                        $('#' + alertboxid + 'no').click();
                    }
                }
            } else {
                if (e.keyCode == 27) {//escape key
                    e.stopImmediatePropagation();
                    e.preventDefault();
                    // Check if ESC was pressed on DatePicker
                    var raisedByDatepicker = e.target.classList.contains("datepicker");
                    if (!raisedByDatepicker) {
                        e.stopImmediatePropagation();
                        e.preventDefault();
                        // Close Dialog
                        $('.ui-dialog-overlay-close.modal:visible').last().click();
                    } else {
                        // Remove focus
                        e.target.blur();
                        e.target.classList.remove("focus");
                    }
                }
            }
        });
 		
        

    });
</script>

    <div id='dialog-overlay-MyAccountSubscriptionOverlay-base' class='ui-dialog-overlay-base-modal' ><div id='MyAccountSubscriptionOverlay' role='dialog' tabindex='-1'  class='ui-dialog-overlay medium' ><div class='ui-dialog-overlay-title-bar' id='dialog-overlay-MyAccountSubscriptionOverlay-title-bar' ></div><div class='ui-dialog-overlay-body' id='dialog-overlay-MyAccountSubscriptionOverlay-body' ></div><button class='ui-dialog-overlay-close modal' title='Close' aria-label='Close' onclick='CloseDialogOverlay("MyAccountSubscriptionOverlay");' ></button><div class='ui-dialog-overlay-footer' id='dialog-overlay-MyAccountSubscriptionOverlay-footer' ></div></div></div>

<script type="text/javascript">
    $(document).ready(function() {
        $('#dialog-overlay-MyAccountSubscriptionOverlay-base').appendTo('body');
        
        $('body').on('keydown', '.ui-dialog-overlay-base-modal, .ui-dialog-overlay, .ui-sw-alert', function (e) {
            var swAlertOpen = $(".ui-sw-alert").length;
            if (swAlertOpen > 1) {
                if (e.keyCode == 27) {//escape key
                    e.stopImmediatePropagation();
                    e.preventDefault();
                    //get id of open alert
                    var alertboxid = $('.ui-sw-alert').attr('id');
                    //click ok or no
                    if ($('#' + alertboxid + 'ok').length > 0) {
                        $('#' + alertboxid + 'ok').click();
                    } else {
                        $('#' + alertboxid + 'no').click();
                    }
                }
            } else {
                if (e.keyCode == 27) {//escape key
                    e.stopImmediatePropagation();
                    e.preventDefault();
                    // Check if ESC was pressed on DatePicker
                    var raisedByDatepicker = e.target.classList.contains("datepicker");
                    if (!raisedByDatepicker) {
                        e.stopImmediatePropagation();
                        e.preventDefault();
                        // Close Dialog
                        $('.ui-dialog-overlay-close.modal:visible').last().click();
                    } else {
                        // Remove focus
                        e.target.blur();
                        e.target.classList.remove("focus");
                    }
                }
            }
        });
 		
        

    });
</script>

    <div id='dialog-overlay-InsertOverlay-base' class='ui-dialog-overlay-base-modal' ><div id='InsertOverlay' role='dialog' tabindex='-1'  class='ui-dialog-overlay large' ><div class='ui-dialog-overlay-title-bar' id='dialog-overlay-InsertOverlay-title-bar' ></div><div class='ui-dialog-overlay-body' id='dialog-overlay-InsertOverlay-body' ></div><button class='ui-dialog-overlay-close modal' title='Close' aria-label='Close' onclick='CloseDialogOverlay("InsertOverlay");' ></button><div class='ui-dialog-overlay-footer' id='dialog-overlay-InsertOverlay-footer' ></div></div></div>

<script type="text/javascript">
    $(document).ready(function() {
        $('#dialog-overlay-InsertOverlay-base').appendTo('body');
        
        $('body').on('keydown', '.ui-dialog-overlay-base-modal, .ui-dialog-overlay, .ui-sw-alert', function (e) {
            var swAlertOpen = $(".ui-sw-alert").length;
            if (swAlertOpen > 1) {
                if (e.keyCode == 27) {//escape key
                    e.stopImmediatePropagation();
                    e.preventDefault();
                    //get id of open alert
                    var alertboxid = $('.ui-sw-alert').attr('id');
                    //click ok or no
                    if ($('#' + alertboxid + 'ok').length > 0) {
                        $('#' + alertboxid + 'ok').click();
                    } else {
                        $('#' + alertboxid + 'no').click();
                    }
                }
            } else {
                if (e.keyCode == 27) {//escape key
                    e.stopImmediatePropagation();
                    e.preventDefault();
                    // Check if ESC was pressed on DatePicker
                    var raisedByDatepicker = e.target.classList.contains("datepicker");
                    if (!raisedByDatepicker) {
                        e.stopImmediatePropagation();
                        e.preventDefault();
                        // Close Dialog
                        $('.ui-dialog-overlay-close.modal:visible').last().click();
                    } else {
                        // Remove focus
                        e.target.blur();
                        e.target.classList.remove("focus");
                    }
                }
            }
        });
 		
        

    });
</script>
    <div id='dialog-overlay-InsertOverlay2-base' class='ui-dialog-overlay-base-modal' ><div id='InsertOverlay2' role='dialog' tabindex='-1'  class='ui-dialog-overlay large' ><div class='ui-dialog-overlay-title-bar' id='dialog-overlay-InsertOverlay2-title-bar' ></div><div class='ui-dialog-overlay-body' id='dialog-overlay-InsertOverlay2-body' ></div><button class='ui-dialog-overlay-close modal' title='Close' aria-label='Close' onclick='CloseDialogOverlay("InsertOverlay2");' ></button><div class='ui-dialog-overlay-footer' id='dialog-overlay-InsertOverlay2-footer' ></div></div></div>

<script type="text/javascript">
    $(document).ready(function() {
        $('#dialog-overlay-InsertOverlay2-base').appendTo('body');
        
        $('body').on('keydown', '.ui-dialog-overlay-base-modal, .ui-dialog-overlay, .ui-sw-alert', function (e) {
            var swAlertOpen = $(".ui-sw-alert").length;
            if (swAlertOpen > 1) {
                if (e.keyCode == 27) {//escape key
                    e.stopImmediatePropagation();
                    e.preventDefault();
                    //get id of open alert
                    var alertboxid = $('.ui-sw-alert').attr('id');
                    //click ok or no
                    if ($('#' + alertboxid + 'ok').length > 0) {
                        $('#' + alertboxid + 'ok').click();
                    } else {
                        $('#' + alertboxid + 'no').click();
                    }
                }
            } else {
                if (e.keyCode == 27) {//escape key
                    e.stopImmediatePropagation();
                    e.preventDefault();
                    // Check if ESC was pressed on DatePicker
                    var raisedByDatepicker = e.target.classList.contains("datepicker");
                    if (!raisedByDatepicker) {
                        e.stopImmediatePropagation();
                        e.preventDefault();
                        // Close Dialog
                        $('.ui-dialog-overlay-close.modal:visible').last().click();
                    } else {
                        // Remove focus
                        e.target.blur();
                        e.target.classList.remove("focus");
                    }
                }
            }
        });
 		
        

    });
</script>
    
    <div id="videowrapper" class="ui-helper-hidden">
        <div id="videodialog" role="application">
            <a id="videodialog-close" role="button" href="javascript:;" aria-label="Close Overlay" class="close-btn" onclick="closeVideoDialog();">CLOSE</a>
            <div id="videodialog-video" ></div>
            <div id="videodialog-foot" tabindex="0"></div>
        </div>
    </div>
    <div id="attachmentwrapper" class="ui-helper-hidden">
        <div id="attachmentdialog" role="application">
            <a id="attachmentdialog-close" role="button" href="javascript:;" aria-label="Close Overlay" class="close-btn" onclick="closeAttachmentDialog();">CLOSE</a>
            <div id="attachmentdialog-container"></div>
        </div>
    </div>
    <script type="text/javascript">

        $(document).ready(function () {

            removeBrokenImages();
            checkSidebar();
            RemoveCookie();

            $('div.bullet').attr('tabindex', '');

            $('.navigation li.collapsible').each(function () {
                if ($(this).find('ul').length == 0) {
                    $(this).removeClass('collapsible');
                }
            });

            // find page nav state cookie and add open chevron
            var arrValues = GetCookie('SWPageNavState').split('~');

            $.each(arrValues, function () {
                if (this != '') {
                    $('#' + this).addClass('collapsible').prepend("<div class='bullet collapsible' aria-label='Close Page Submenu'/>");
                }
            });

            // find remaining sub menus and add closed chevron and close menu
            $('.navigation li > ul').each(function () {
                var list = $(this);

                if (list.parent().hasClass('active') && !list.parent().hasClass('collapsible')) {
                    // open sub for currently selected page                    
                    list.parent().addClass('collapsible').prepend("<div class='bullet collapsible'aria-label='Close Page Submenu' />");
                } else {
                    if (list.parent().hasClass('collapsible') && !list.siblings('div').hasClass('collapsible')) {
                        // open sub for page with auto expand
                        list.siblings('div.expandable').remove();
                        list.parent().prepend("<div class='bullet collapsible' aria-label='Close Page Submenu' />");
                    }
                }

                if (!list.siblings('div').hasClass('collapsible')) {
                    // keep all closed that aren't already set to open
                    list.parent().addClass('expandable').prepend("<div class='bullet expandable' aria-label='Open Page Submenu' />");
                    ClosePageSubMenu(list.parent());
                } else {
                    OpenPageSubMenu(list.parent());
                }
            });

            // remove bullet from hierarchy if no-bullet set
            $('.navigation li.collapsible').each(function () {
                if ($(this).hasClass('no-bullet')) {
                    if (!$(this).hasClass('navigationgroup')) { $(this).removeClass('collapsible'); }
                    $(this).children('div.collapsible').remove();
                }
            });

            $('.navigation li.expandable').each(function () {
                if ($(this).hasClass('no-bullet')) {
                    if (!$(this).hasClass('navigationgroup')) { $(this).removeClass('expandable'); }
                    $(this).children('div.expandable').remove();
                }
            });

            $('.navigation li:not(.collapsible,.expandable,.no-bullet)').each(function () {
                $(this).prepend("<div class='bullet'/>");
            });

            $('.navigation li.active').parents('ul').each(function () {
                if (!$(this).hasClass('page-navigation')) {
                    OpenPageSubMenu($(this).parent());
                }
            });

            // Set aria ttributes
            $('li.collapsible').each(function () {
                $(this).attr("aria-expanded", "true");
                $(this).find('div:first').attr('aria-pressed', 'true');
            });

            $('li.expandable').each(function () {
                $(this).attr("aria-expanded", "false");
                $(this).find('div:first').attr('aria-pressed', 'false');
            });

            $('div.bullet').each(function () {
                $(this).attr("aria-hidden", "true");
            });

            // set click event for chevron
            $(document).on('click', '.navigation div.collapsible', function () {
                ClosePageSubMenu($(this).parent());
            });

            $(document).on('click', '.navigation div.expandable', function () {
                OpenPageSubMenu($(this).parent());
            });

            // set navigation grouping links
            $(document).on('click', '.navigationgroup.collapsible > a', function () {
                ClosePageSubMenu($(this).parent());
            });

            $(document).on('click', '.navigationgroup.expandable > a', function () {
                OpenPageSubMenu($(this).parent());
            });

            //SW MYSTART DROPDOWNS
            $(document).on('click', '.sw-mystart-dropdown', function () {
                $(this).children(".sw-dropdown").css("display", "block");
            });

            $(".sw-mystart-dropdown").hover(function () { }, function () {
                $(this).children(".sw-dropdown").hide();
                $(this).blur();
            });

            //SW ACCOUNT DROPDOWN
            $(document).on('click', '#sw-mystart-account', function () {
                $(this).children("#sw-myaccount-list").show();
                $(this).addClass("clicked-state");
            });

            $("#sw-mystart-account, #sw-myaccount-list").hover(function () { }, function () {
                $(this).children("#sw-myaccount-list").hide();
                $(this).removeClass("clicked-state");
                $("#sw-myaccount").blur();
            });

            // set hover class for page and section navigation
            $('.ui-widget.app.pagenavigation, .ui-widget.app.sectionnavigation').find('li > a').hover(function () {
                $(this).addClass('hover');
            }, function () {
                $(this).removeClass('hover');
            });

            //set aria-label for home
            $('#navc-HP > a').attr('aria-label', 'Home');

            // set active class on channel and section
            var activeChannelNavType = $('input#hidActiveChannelNavType').val();
            if (activeChannelNavType == -1) {
                // homepage is active
                $('#navc-HP').addClass('active');
            } else if (activeChannelNavType == 1) {
                // calendar page is active
                $('#navc-CA').addClass('active');
            } else {
                // channel is active - set the active class on the channel
                var activeSelectorID = $('input#hidActiveChannel').val();
                $('#navc-' + activeSelectorID).addClass('active');

                // set the breadcrumb channel href to the channel nav href
                $('li[data-bccID=' + activeSelectorID + '] a').attr('href', $('#navc-' + activeSelectorID + ' a').attr('href'));
                $('li[data-bccID=' + activeSelectorID + '] a span').text($('#navc-' + activeSelectorID + ' a span').first().text());

                // set the active class on the section
                activeSelectorID = $('input#hidActiveSection').val();
                $('#navs-' + activeSelectorID).addClass('active');

                // set the breadcrumb section href to the channel nav href
                $('li[data-bcsID=' + activeSelectorID + '] a').attr('href', $('#navs-' + activeSelectorID + ' a').attr('href'));
                if ($('#navs-' + activeSelectorID + ' a').attr('target') !== undefined) {
                    $('li[data-bcsID=' + activeSelectorID + '] a').attr('target', $('#navs-' + activeSelectorID + ' a').attr('target'));
                }
                $('li[data-bcsID=' + activeSelectorID + '] span').text($('#navs-' + activeSelectorID + ' a span').text());

                if ($('.sw-directory-columns').length > 0) {
                    $('ul.ui-breadcrumbs li:last-child').remove();
                    $('ul.ui-breadcrumbs li:last-child a').replaceWith(function() { return $('span', this); });
                    $('ul.ui-breadcrumbs li:last-child span').append(' Directory');
                }
            }
        }); // end document ready

        function OpenPageSubMenu(li) {
            if (li.prop('tagName').toLowerCase() == "li") {
                if (li.hasClass('expandable')) {
                    li.removeClass('expandable').addClass('collapsible');
                }
                if (li.find('div:first').hasClass('expandable')) {
                    li.find('div:first').removeClass('expandable').addClass('collapsible').attr('aria-pressed', 'true').attr('aria-label','Close Page Submenu');
                }
                li.find('ul:first').attr('aria-hidden', 'false').show();

                li.attr("aria-expanded", "true");

                PageNavigationStateCookie();
            }
        }

        function ClosePageSubMenu(li) {
            if (li.prop('tagName').toLowerCase() == "li") {
                li.removeClass('collapsible').addClass('expandable');
                li.find('div:first').removeClass('collapsible').addClass('expandable').attr('aria-pressed', 'false').attr('aria-label','Open Page Submenu');
                li.find('ul:first').attr('aria-hidden', 'true').hide();

                li.attr("aria-expanded", "false");

                PageNavigationStateCookie();
            }
        }

        function PageNavigationStateCookie() {
            var strCookie = "";

            $('.pagenavigation li > ul').each(function () {
                var item = $(this).parent('li');
                if (item.hasClass('collapsible') && !item.hasClass('no-bullet')) {
                    strCookie += $(this).parent().attr('id') + '~';
                }
            });

            SetCookie('SWPageNavState', strCookie);
        }

        function checkSidebar() {
            $(".ui-widget-sidebar").each(function () {
                if ($.trim($(this).html()) != "") {
                    $(this).show();
                    $(this).siblings(".ui-widget-detail").addClass("with-sidebar");
                }
            });
        }

        function removeBrokenImages() {
            //REMOVES ANY BROKEN IMAGES
            $("span.img img").each(function () {
                if ($(this).attr("src") !== undefined && $(this).attr("src") != '../../') {
                    $(this).parent().parent().show();
                    $(this).parent().parent().siblings().addClass("has-thumb");
                }
            });
        }

        function LoadEventDetailUE(moduleInstanceID, eventDateID, userRegID, isEdit) {
            (userRegID === undefined ? userRegID = 0 : '');
            (isEdit === undefined ? isEdit = false : '');
            OpenDialogOverlay("WindowMediumModal", { LoadType: "U", LoadURL: "https://mpatzem.schoolwires.net//site/UserControls/Calendar/EventDetailWrapper.aspx?ModuleInstanceID=" + moduleInstanceID + "&EventDateID=" + eventDateID + "&UserRegID=" + userRegID + "&IsEdit=" + isEdit });
        }

        function RemoveCookie() {
            // There are no sub page            
            if ($('.pagenavigation li li').length == 0) {
                //return false;
                PageNavigationStateCookie();
            }
        }
    </script>

    <script type="text/javascript">

        function AddOffCanvasMenuHeightForSiteNav() {
            var sitenavulHeight = 0;

            if ($('#sw-pg-sitenav-ul').length > 0) {
                sitenavulHeight = parseInt($("#sw-pg-sitenav-ul").height());
            }

            var swinnerwrapHeight = 0;

            if ($('#sw-inner-wrap').length > 0) {
                swinnerwrapHeight = parseInt($("#sw-inner-wrap").height());
            }

            // 360px is abount 5 li height
            if (sitenavulHeight + 360 >= swinnerwrapHeight) {
                $("#sw-inner-wrap").height(sitenavulHeight + 360);
            }
        }

        function AddOffCanvasMenuHeightForSelectSchool() {
            var selectschoolulHeight = 0;

            if ($('#sw-pg-selectschool-ul').length > 0) {
                selectschoolulHeight = parseInt($("#sw-pg-selectschool-ul").height());
            }

            var swinnerwrapHeight = 0;

            if ($('#sw-inner-wrap').length > 0) {
                swinnerwrapHeight = parseInt($("#sw-inner-wrap").height());
            }

            // 360px is abount 5 li height
            if (selectschoolulHeight + 360 >= swinnerwrapHeight) {
                $("#sw-inner-wrap").height(selectschoolulHeight + 360);
            }
        }

        $(document).ready(function () {
            if ($("#sw-pg-sitenav-a").length > 0) {
                $(document).on('click', '#sw-pg-sitenav-a', function () {
                    if ($("#sw-pg-sitenav-ul").hasClass('sw-pgmenu-closed')) {
                        AddOffCanvasMenuHeightForSiteNav();

                        $("ul.sw-pgmenu-toplevel").removeClass('sw-pgmenu-open').addClass('sw-pgmenu-closed');
                        $("#sw-pg-sitenav-ul").removeClass('sw-pgmenu-closed');
                        $("#sw-pg-sitenav-ul").addClass('sw-pgmenu-open');
                    } else {
                        $("#sw-pg-sitenav-ul").removeClass('sw-pgmenu-open');
                        $("#sw-pg-sitenav-ul").addClass('sw-pgmenu-closed');
                    }
                });

                $(document).on('click', '#sw-pg-selectschool-a', function () {
                    if ($("#sw-pg-selectschool-ul").hasClass('sw-pgmenu-closed')) {
                        AddOffCanvasMenuHeightForSelectSchool();

                        $("ul.sw-pgmenu-toplevel").removeClass('sw-pgmenu-open').addClass('sw-pgmenu-closed');
                        $("#sw-pg-selectschool-ul").removeClass('sw-pgmenu-closed');
                        $("#sw-pg-selectschool-ul").addClass('sw-pgmenu-open');
                    } else {
                        $("#sw-pg-selectschool-ul").removeClass('sw-pgmenu-open');
                        $("#sw-pg-selectschool-ul").addClass('sw-pgmenu-closed');
                    }
                });

                $(document).on('click', '#sw-pg-myaccount-a', function () {
                    if ($("#sw-pg-myaccount-ul").hasClass('sw-pgmenu-closed')) {
                        $("ul.sw-pgmenu-toplevel").removeClass('sw-pgmenu-open').addClass('sw-pgmenu-closed');
                        $("#sw-pg-myaccount-ul").removeClass('sw-pgmenu-closed');
                        $("#sw-pg-myaccount-ul").addClass('sw-pgmenu-open');
                    } else {
                        $("#sw-pg-myaccount-ul").removeClass('sw-pgmenu-open');
                        $("#sw-pg-myaccount-ul").addClass('sw-pgmenu-closed');
                    }
                });

                $(document).on('click', '.pg-list-bullet', function () {
                    $(this).prev().toggle();

                    if ($(this).hasClass('closed')) {
                        AddOffCanvasMenuHeightForSiteNav();

                        $(this).removeClass('closed');
                        $(this).addClass('open');
                    } else {
                        $(this).removeClass('open');
                        $(this).addClass('closed');
                    }
                });

                $(document).on('mouseover', '#sw-pg-selectschool', function () {
                    $("#sw-pg-selectschool-firstli").removeClass('sw-pg-selectschool-firstli-mouseout').addClass('sw-pg-selectschool-firstli-mouseover');
                    $("#sw-pg-selectschool-firstli a").addClass('sw-pg-selectschool-firstli-a-mouseover').removeClass('sw-pg-selectschool-firstli-a-mouseout');
                });

                $(document).on('mouseout', '#sw-pg-selectschool', function () {
                    $("#sw-pg-selectschool-firstli").removeClass('sw-pg-selectschool-firstli-mouseover').addClass('sw-pg-selectschool-firstli-mouseout');
                    $("#sw-pg-selectschool-firstli a").addClass('sw-pg-selectschool-firstli-a-mouseout').removeClass('sw-pg-selectschool-firstli-a-mouseover');
                });

                $(document).on('mouseover', '#sw-pg-myaccount', function () {
                    $("#sw-pg-myaccount-firstli").removeClass('sw-pg-myaccount-firstli-mouseout').addClass('sw-pg-myaccount-firstli-mouseover');
                    $("#sw-pg-myaccount-firstli a").addClass('sw-pg-myaccount-firstli-a-mouseover').removeClass('sw-pg-myaccount-firstli-a-mouseout');
                });

                $(document).on('mouseout', '#sw-pg-myaccount', function () {
                    $("#sw-pg-myaccount-firstli").removeClass('sw-pg-myaccount-firstli-mouseover').addClass('sw-pg-myaccount-firstli-mouseout');
                    $("#sw-pg-myaccount-firstli a").addClass('sw-pg-myaccount-firstli-a-mouseout').removeClass('sw-pg-myaccount-firstli-a-mouseover');
                });

                $(document).on('mouseover', '#sw-pg-sitenav', function () {
                    $("#sw-pg-sitenav-firstli").removeClass('sw-pg-sitenav-firstli-mouseout').addClass('sw-pg-sitenav-firstli-mouseover');
                    $("#sw-pg-sitenav-firstli a").addClass('sw-pg-sitenav-firstli-a-mouseover').removeClass('sw-pg-sitenav-firstli-a-mouseout');
                });

                $(document).on('mouseout', '#sw-pg-sitenav', function () {
                    $("#sw-pg-sitenav-firstli").removeClass('sw-pg-sitenav-firstli-mouseover').addClass('sw-pg-sitenav-firstli-mouseout');
                    $("#sw-pg-sitenav-firstli a").addClass('sw-pg-sitenav-firstli-a-mouseout').removeClass('sw-pg-sitenav-firstli-a-mouseover');
                });

                $(document).on('mouseover', '#sw-pg-district', function () {
                    $("#sw-pg-district-firstli").removeClass('sw-pg-district-firstli-mouseout').addClass('sw-pg-district-firstli-mouseover');
                    $("#sw-pg-district-firstli a").addClass('sw-pg-district-firstli-a-mouseover').removeClass('sw-pg-district-firstli-a-mouseout');
                });

                $(document).on('mouseout', '#sw-pg-district', function () {
                    $("#sw-pg-district-firstli").removeClass('sw-pg-district-firstli-mouseover').addClass('sw-pg-district-firstli-mouseout');
                    $("#sw-pg-district-firstli a").addClass('sw-pg-district-firstli-a-mouseout').removeClass('sw-pg-district-firstli-a-mouseover');
                });
            }
        });


    </script>
    <script src='https://mpatzem.schoolwires.net/Static//GlobalAssets/Scripts/min/jquery-ui-1.12.0.min.js' type='text/javascript'></script>
    <script src="https://mpatzem.schoolwires.net/Static/GlobalAssets/Scripts/min/SW-UI_2680.min.js" type='text/javascript'></script>
    <script src="https://mpatzem.schoolwires.net/Static/GlobalAssets/Scripts/jquery.sectionlayer.js" type='text/javascript'></script>
    
    <script src="https://mpatzem.schoolwires.net/Static/GlobalAssets/Scripts/min/swfobject.min.js" type="text/javascript"></script>
    <script src="https://mpatzem.schoolwires.net/Static/GlobalAssets/Scripts/min/jquery.ajaxupload_2440.min.js" type="text/javascript"></script>

    <!-- Begin swuc.CheckScript -->
  <script type="text/javascript" src="https://mpatzem.schoolwires.net/Static/GlobalAssets/Scripts/ThirdParty/json2.js"></script>
  
<script>var homeURL = location.protocol + "//" + window.location.hostname;

function parseXML(xml) {
    if (window.ActiveXObject && window.GetObject) {
        var dom = new ActiveXObject('Microsoft.XMLDOM');
        dom.loadXML(xml);
        return dom;
    }

    if (window.DOMParser) {
        return new DOMParser().parseFromString(xml, 'text/xml');
    } else {
        throw new Error('No XML parser available');
    }
}

function GetContent(URL, TargetClientID, Loadingtype, SuccessCallback, FailureCallback, IsOverlay, Append) {
    (Loadingtype === undefined ? LoadingType = 3 : '');
    (SuccessCallback === undefined ? SuccessCallback = '' : '');
    (FailureCallback === undefined ? FailureCallback = '' : '');
    (IsOverlay === undefined ? IsOverlay = '0' : '');
    (Append === undefined ? Append = false : '');

    var LoadingHTML;
    var Selector;

    switch (Loadingtype) {
        //Small
        case 1:
            LoadingHTML = "SMALL LOADER HERE";
            break;
            //Large
        case 2:
            LoadingHTML = "<div class='ui-loading large' role='alert' aria-label='Loading content'></div>";
            break;
            // None
        case 3:
            LoadingHTML = "";
            break;
    }

    Selector = "#" + TargetClientID;

    ajaxCall = $.ajax({
        url: URL,
        cache: false,
        beforeSend: function () {
            if (Loadingtype != 3) { BlockUserInteraction(TargetClientID, LoadingHTML, Loadingtype, 0, IsOverlay); }
        },
        success: function (strhtml) {

            // check for calendar and empty div directly surrounding eventlist uc first 
            //      to avoid memory error in IE (Access violation reading location 0x00000018)
            //      need to figure out exactly why this is happening..
            //      Partially to do with this? http://support.microsoft.com/kb/927917/en-us
            //      The error/crash happens when .empty() is called on Selector 
            //          (.html() calls .empty().append() in jquery)
            //      * no one has come across this issue anywhere else in the product so far

            if ($(Selector).find('#calendar-pnl-calendarlist').length > 0) {
                $('#calendar-pnl-calendarlist').empty();
                $(Selector).html(strhtml);
            } else if (Append) {
                $(Selector).append(strhtml);
            }
            else {
                $(Selector).html(strhtml);
            }


            // check for tabindex 
            if ($(Selector).find(":input[tabindex='1']:first").length > 0) {
                $(Selector).find(":input[tabindex='1']:first").focus();
            } else {
                $(Selector).find(":input[type='text']:first").not('.nofocus').focus();
            }

            //if (CheckDirty(Selector) === true) { BindSetDirty(Selector); }
            //CheckDirty(Selector);
            BlockUserInteraction(TargetClientID, '', '', 1);
            (SuccessCallback != '' ? eval(SuccessCallback) : '');
        },
        failure: function () {
            BlockUserInteraction(TargetClientID, '', '', 1);
            (FailureCallback != '' ? eval(FailureCallback) : '');
        }
    });
}

function BlockUserInteraction(TargetClientID, LoadingHTML, Loadingtype, Unblock, IsOverlay) {
    if (LoadingHTML === undefined) {
        LoadingHTML = "<div class='ui-loading large'></div>";
    }

    if (Unblock == 1) {
        $('#' + TargetClientID).unblockinteraction();
    } else {
        if (IsOverlay == 1) {
            $('#' + TargetClientID).blockinteraction({ message: LoadingHTML, type: Loadingtype, isOverlay: true });
        } else {
            $('#' + TargetClientID).blockinteraction({ message: LoadingHTML, type: Loadingtype });
        }
    }
}

function OpenUltraDialogOverlay(OverlayClientID, options, Callback) {
    lastItemClicked = document.activeElement;
    var defaults = {
        LoadType: "U",
        LoadURL: "",
        TargetDivID: "",
        LoadContent: "",
        NoResize: false,
        ScrollTop: false,
        CloseCallback: undefined
    };

    jQuery.extend(defaults, options);

    // check what browser/version we're on
    var isIE = GetIEVersion();

    if (isIE == 0) {
        if ($.browser.mozilla) {
            //Firefox/Chrome
            $("body").css("overflow", "hidden");
        } else {
            //Safari
            $("html").css("overflow", "hidden");
        }
    } else {
        // IE
        $("html").css("overflow", "hidden");
    }

    var OverlaySelector;
    var BodyClientID;
    var TargetDivSelector;
    var CurrentScrollPosition;

    if (defaults.ScrollTop) {
        $.scrollTo(0, { duration: 0 });
    }

    OverlaySelector = "#dialog-ultra-overlay-" + OverlayClientID + "-base";
    BodyClientID = "dialog-ultra-overlay-" + OverlayClientID + "-holder";
    TargetDivSelector = "#" + defaults.TargetDivID;

    if ($.trim($("#dialog-ultra-overlay-" + OverlayClientID + "-holder").html()) != "") {
        CloseUltraDialogOverlay(OverlayClientID);
    }

    // U = URL
    // H - HTML
    // D - Divider

    var success = "";

    if (Callback !== undefined) {
        success += Callback;
    }

    // block user interaction
    BlockUserInteraction(BodyClientID, "<div class='ui-loading large'></div>", 2, 0, 1);

    switch (defaults.LoadType) {
        case 'U':
            GetContent(defaults.LoadURL, BodyClientID, 3, success);
            break;
        case 'H':
            $("#" + BodyClientID).html(defaults.LoadContent);
            break;
        case 'D':
            $("#" + BodyClientID).html($(TargetDivSelector).html());
            break;
    };

    // open the lateral panel
    var browserWidth = $(document).width();

    if (OverlayClientID == "UltraOverlayLarge") {
        browserWidth = browserWidth - 200;
    } else {
        browserWidth = browserWidth - 280;
    }

    $("#dialog-ultra-overlay-" + OverlayClientID + "-body").css("width", browserWidth);
    $(OverlaySelector).addClass("is-visible");

    // hide 'X' button if second window opened
    if (OverlayClientID == "UltraOverlayMedium") {
        $("#dialog-ultra-overlay-UltraOverlayLarge-close").hide();
    }
}

function CloseUltraDialogOverlay(OverlayClientID) {
    $("#dialog-ultra-overlay-" + OverlayClientID + "-base").removeClass("is-visible");

    // show 'X' button if second window closed
    if (OverlayClientID == "UltraOverlayMedium") {
        $("#dialog-ultra-overlay-UltraOverlayLarge-close").show();
    }

    if (OverlayClientID != "UltraOverlayMedium") {
        // check what browser/version we're on
        var isIE = GetIEVersion();

        if (isIE == 0) {
            if ($.browser.mozilla) {
                //Firefox/Chrome
                $("body").css("overflow", "auto");
            } else {
                //Safari
                $("html").css("overflow", "auto");
            }
        } else {
            // IE
            $("html").css("overflow", "auto");
        }
    }
    //focus back on the element clicked to open the dialog
    if (lastItemClicked !== undefined) {
        lastItemClicked.focus();
        lastItemClicked = undefined;
    }
}

//Save the last item clicked when opening a dialog overlay so the focus can go there upon close
var lastItemClicked;
function OpenDialogOverlay(OverlayClientID, options, Callback) {
    lastItemClicked = document.activeElement;
    var defaults = {
        LoadType: 'U',
        LoadURL: '',
        TargetDivID: '',
        LoadContent: '',
        NoResize: false,
        ScrollTop: false,
        CloseCallback: undefined
    };

    jQuery.extend(defaults, options);

    //check what browser/version we're on
    var isIE = GetIEVersion();

    //if (isIE == 0) {
    //    if ($.browser.mozilla) {
    //        //Firefox/Chrome
    //        $('body').css('overflow', 'hidden');
    //    } else {
    //        //Safari
    //        $('html').css('overflow', 'hidden');
    //    }
    //} else {
    //    // IE
    //    $('html').css('overflow', 'hidden');
    //}
    $('html').css('overflow', 'hidden');

    var OverlaySelector;
    var BodyClientID;
    var TargetDivSelector;

    if (defaults.ScrollTop) {
        $.scrollTo(0, { duration: 0 });
    }

    OverlaySelector = "#dialog-overlay-" + OverlayClientID + "-base";
    BodyClientID = "dialog-overlay-" + OverlayClientID + "-body";
    TargetDivSelector = "#" + defaults.TargetDivID;

    $(OverlaySelector).appendTo('body');

    $("#" + OverlayClientID).css("top", "5%");

    $("#" + BodyClientID).html("");

    if (isIEorEdge()) {
        $(OverlaySelector).show();
    } else {
        $(OverlaySelector).fadeIn();
    }

    if ($.trim($('#dialog-overlay-' + OverlayClientID + '-body').html()) != "") {
        CloseDialogOverlay(OverlayClientID);
    }

    // U = URL
    // H - HTML
    // D - Divider

    var success = "";

    if (Callback !== undefined) {
        success += Callback;
    }

    // Block user interaction
    $('#' + BodyClientID).css({ 'min-height': '100px' });
    BlockUserInteraction(BodyClientID, "<div class='ui-loading large'></div>", 2, 0, 1);

    switch (defaults.LoadType) {
        case 'U':
            GetContent(defaults.LoadURL, BodyClientID, 3, success);
            break;
        case 'H':
            $("#" + BodyClientID).html(defaults.LoadContent);
            break;
        case 'D':
            $("#" + BodyClientID).html($(TargetDivSelector).html());
            break;

    };

    if (defaults.CloseCallback !== undefined) {
        $(OverlaySelector + ' .ui-dialog-overlay-close').attr('onclick', 'CloseDialogOverlay(\'' + OverlayClientID + '\',' + defaults.CloseCallback + ')')
    }

    // check for tabindex 
    if ($("#" + BodyClientID).find(":input[tabindex='1']:first").length > 0) {
        $("#" + BodyClientID).find(":input[tabindex='1']:first").focus();
    } else if ($("#" + BodyClientID).find(":input[type='text']:first").length > 0) {
        $("#" + BodyClientID).find(":input[type='text']:first").focus();
    } else {
        $("#" + BodyClientID).parent().focus();
    }

    $('#dialog-overlay-' + OverlayClientID + '-base').css({'overflow': 'auto', 'top': '0', 'width': '100%', 'left': '0' });
}

function GetIEVersion() {
    var sAgent = window.navigator.userAgent;
    var Idx = sAgent.indexOf("MSIE");

    if (Idx > 0) {
        // If IE, return version number
        return parseInt(sAgent.substring(Idx + 5, sAgent.indexOf(".", Idx)));
    } else if (!!navigator.userAgent.match(/Trident\/7\./)) {
        // If IE 11 then look for Updated user agent string
        return 11;
    } else {
        //It is not IE
        return 0;
    }
}

function isIEorEdge() {
    var agent = window.navigator.userAgent;

    var ie = agent.indexOf('MSIE ');
    if (ie > 0) {
        // IE <= 10
        return parseInt(agent.substring(ie + 5, uaagentindexOf('.', ie)), 10);
    }

    var gum = agent.indexOf('Trident/');
    if (gum > 0) {
        // IE 11
        var camper = agent.indexOf('rv:');
        return parseInt(agent.substring(camper + 3, agent.indexOf('.', camper)), 10);
    }

    var linkinPark = agent.indexOf('Edge/');
    if (linkinPark > 0) {
        return parseInt(agent.substring(linkinPark + 5, agent.indexOf('.', linkinPark)), 10);
    }

    // other browser
    return false;
}

function SendEmail(to, from, subject, body, callback) {

    var data = "{ToEmailAddress: '" + to + "', " +
        "FromEmailAddress: '" + from + "', " +
        "Subject: '" + subject + "', " +
        "Body: '" + body + "'}";
    var url = homeURL + "/GlobalUserControls/SE/SEController.aspx/SE";

    $.ajax({
        type: "POST",
        url: url,
        data: data,
        async: false,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        cache: false,
        success: function (msg) {
            callback(msg.d);
        },
        headers: { 'X-Csrf-Token': GetCookie('CSRFToken') }
    });
}

// DETERMINE TEXT COLOR 

function rgbstringToTriplet(rgbstring) {
    var commadelim = rgbstring.substring(4, rgbstring.length - 1);
    var strings = commadelim.split(",");
    var numeric = [];

    for (var i = 0; i < 3; i++) {
        numeric[i] = parseInt(strings[i]);
    }

    return numeric;
}

function adjustColour(someelement) {
    var rgbstring = someelement.css('background-color');
    var triplet = [];
    var newtriplet = [];

    if (rgbstring != 'transparent') {
        if (/rgba\(0, 0, 0, 0\)/.exec(rgbstring)) {
            triplet = [255, 255, 255];
        } else {
            if (rgbstring.substring(0, 1).toLowerCase() != 'r') {
                CheckScript('RGBColor', staticURL + '/GlobalAssets/Scripts/ThirdParty/rgbcolor.js');
                // not rgb, convert it
                var color = new RGBColor(rgbstring);
                rgbstring = color.toRGB();
            }
            triplet = rgbstringToTriplet(rgbstring);
        }
    } else {
        triplet = [255, 255, 255];
    }

    // black or white:
    var total = 0; for (var i = 0; i < triplet.length; i++) { total += triplet[i]; }

    if (total > (3 * 256 / 2)) {
        newtriplet = [0, 0, 0];
    } else {
        newtriplet = [255, 255, 255];
    }

    var newstring = "rgb(" + newtriplet.join(",") + ")";

    someelement.css('color', newstring);
    someelement.find('*').css('color', newstring);

    return true;
}


// END DETERMINE TEXT color

function CheckScript2(ModuleName, ScriptSRC) {
    $.ajax({
        url: ScriptSRC,
        async: false,
        //context: document.body,
        success: function (html) {
            var script =
				document.createElement('script');
            document.getElementsByTagName('head')[0].appendChild(script);
            script.text = html;
        }
    });
}

// AREA / SCREEN CODES

function setCurrentScreenCode(screenCode) {
    SetCookie('currentScreenCode', screenCode);
    AddAnalyticsEvent(getCurrentAreaCode(), screenCode, 'Page View');
}

function getCurrentScreenCode() {
    var cookieValue = GetCookie('currentScreenCode');
    return (cookieValue != '' ? cookieValue : 0);
}

function setCurrentAreaCode(areaCode) {
    SetCookie('currentAreaCode', areaCode);
}

function getCurrentAreaCode() {
    var cookieValue = GetCookie('currentAreaCode');
    return (cookieValue != '' ? cookieValue : 0);
}

// END AREA / SCREEN CODES

// CLICK HOME TAB IN HEADER SECTION

function GoHome() {
    window.location.href = homeURL + "/cms/Workspace";
}

// END CLICK HOME TAB IN HEADER SECTION

// HELP PANEL

function OpenHelpPanel() {
    var URLScreenCode = getCurrentScreenCode();
    
    if (URLScreenCode == "" || URLScreenCode == 0) {
        URLScreenCode = getCurrentAreaCode();
        if (URLScreenCode == 0) {
            URLScreenCode = "";
        }
    } 

    AddAnalyticsEvent("Help", "How Do I...?", URLScreenCode);

    //help site url stored in webconfig, passed in to BBHelpURL from GlobalJSVar and GlobalJS.cs
    var HelpURL = BBHelpURL + URLScreenCode;
    window.open(HelpURL,"_blank");
}

// END HELP PANEL

// COOKIES
function SetCookie(name, value, days, ms) {
    var expires = "";

    if (ms) {
        var date = new Date();

        date.setMilliseconds(date.getMilliseconds() + ms);
        expires = "; expires=" + date.toGMTString();
    } else if (days) {
        var date = new Date();

        date.setDate(date.getDate() + days);
        expires = "; expires=" + date.toGMTString();
    }

    document.cookie = name + "=" + escape(value) + expires + "; path=/";
}

function GetCookie(name) {
    var value = "";

    if (document.cookie.length > 0) {
        var start = document.cookie.indexOf(name + "=");

        if (start != -1) {
            start = start + name.length + 1;

            var end = document.cookie.indexOf(";", start);

            if (end == -1) end = document.cookie.length;
            value = unescape(document.cookie.substring(start, end));
        }
    }

    return value;
}

function DeleteCookie(name) {
    SetCookie(name, '', -1);
}

function SetUnescapedCookie(name, value, days, ms) {
    var expires = "";

    if (ms) {
        var date = new Date();

        date.setMilliseconds(date.getMilliseconds() + ms);
        expires = "; expires=" + date.toGMTString();
    } else if (days) {
        var date = new Date();

        date.setDate(date.getDate() + days);
        expires = "; expires=" + date.toGMTString();
    }

    document.cookie = name + "=" + value + expires + "; path=/";
}
// END COOKIES



// IFRAME FUNCTIONS
function BindResizeFrame(FrameID) {
    $(document).on('load', "#" + FrameID, function () {
        var bodyHeight = $("#" + FrameID).contents().height() + 40;
        $("#" + FrameID).attr("height", bodyHeight + "px");
    });
}

function AdjustLinkTarget(FrameID) {
    $(document).on('load', "#" + FrameID, function () {
        $("#" + FrameID).contents().find("a").attr("target", "_parent");
    });
}

function ReloadDocViewer(moduleInstanceID, retryCount) {
    var iframe = document.getElementById('doc-viewer-' + moduleInstanceID);

    //if document failed to load and retry count is less or equal to 3, reload document and check again
    if (iframe.contentWindow.frames.length == 0 && retryCount <= 3) {
        retryCount = retryCount + 1;

        //reload the iFrame
        document.getElementById('doc-viewer-' + moduleInstanceID).src += '';

        //Check if document loaded again in 7.5 seconds
        setTimeout(ReloadDocViewer, (1000 * retryCount), moduleInstanceID, retryCount);

    } else if (iframe.contentWindow.frames.length == 0 && retryCount > 3) {
        $('#doc-viewer-' + moduleInstanceID).css('background', '');
        iframe.src = "/Errors/ReloadPage.aspx";
        iframe.height = 200;
    }

    if (iframe.contentWindow.frames.length == 1) {
        $('#doc-viewer-' + moduleInstanceID).css('background', '');
    }
}
// END IFRAME FUNCTIONS


// SCROLL TOP

function ScrollTop() {
    $.scrollTo(0, { duration: 1000 });
}

// END SCROLL TOP

// ANALYTICS TRACKING

function AddAnalyticsEvent(category, action, label) {
    ga('BBTracker.send', 'event', category, action, label);
}

// END ANYLYTICS TRACKING


// BEGIN INCLUDE DOC READY SCRIPTS

function IncludeDocReadyScripts() {
    var arrScripts = [
		staticURL + '/GlobalAssets/Scripts/min/external-combined.min.js',
        staticURL + '/GlobalAssets/Scripts/Utilities_2560.js'

    ];

    var script = document.createElement('script');
    script.type = 'text/javascript';
    $.each(arrScripts, function () {script.src = this;$('head').append(script);;
        
    });

}
// END INCLUDE DOC READY SCRIPTS

//BEGIN ONSCREEN ALERT SCRIPTS
function OnScreenAlertDialogInit() {
    $("#onscreenalert-message-dialog").hide();
    $(".titleMessage").hide();
    $("#onscreenalert-ctrl-msglist").hide();
    $(".onscreenalert-ctrl-footer").hide();

    $(".icon-icon_alert_round_message").addClass("noBorder");
    $('.onscreenalert-ctrl-modal').addClass('MoveIt2 MoveIt1 Smaller');
    $('.onscreenalert-ctrl-modal').css({ "top": "88%" });
    $("#onscreenalert-message-dialog").show();
    $('#onscreenalert-message-dialog-icon').focus();
}

function OnScreenAlertGotItDialog(serverTime) {
    var alertsID = '';
    alertsID = GetCookie("Alerts");
    var arr = [];

    if (alertsID.length > 0) {
        arr = alertsID.split(',')
    }

    $("#onscreenalertdialoglist li").each(function () {
        arr.push($(this).attr('id'));
    });

    arr.sort();
    var newarr = $.unique(arr);

    SetUnescapedCookie("Alerts", newarr.join(','), 365);
    OnScreenAlertMaskShow(false);

    $('.onscreenalert-ctrl-modal').removeClass("notransition");
    $(".onscreenalert-ctrl-footer").slideUp(200);
    $("#onscreenalert-ctrl-msglist").slideUp(200);

    // check what browser/version we're on
    // if IE 9 or less, don't do CSS 3 transitions (not supported)
    var isIE = GetIEVersion();

    if (isIE == 0) {
        // not IE
        hideOnScreenAlertTransitions();
    } else {
        // IE
        if (isIE == 8 || isIE == 9) {
            hideOnScreenAlertNoTransitions();
        } else if (isIE >= 10) {
            hideOnScreenAlertTransitions();
        }
    }
}

function OnScreenAlertOpenDialog(SiteID) {
    $('#sw-important-message-tooltip').hide();
    var onscreenAlertCookie = GetCookie('Alerts');

    if (onscreenAlertCookie != '' && onscreenAlertCookie != undefined) {
        GetContent(homeURL + "/cms/Tools/OnScreenAlerts/UserControls/OnScreenAlertDialogListWrapper.aspx?OnScreenAlertCookie=" + onscreenAlertCookie + "&SiteID=" + SiteID, "onscreenalert-ctrl-cookielist", 2, "OnScreenAlertCookieSuccess();");
        $('.onscreenalert-ctrl-content').focus();
    }
}

function OnScreenAlertCookieSuccess() {
    OnScreenAlertMaskShow(true);

    $('.onscreenalert-ctrl-modal').removeClass("notransition");
    $('.onscreenalert-ctrl-modal').removeClass("Smaller");

    // check what browser/version we're on
    // if IE 9 or less, don't do CSS 3 transitions (not supported)
    var isIE = GetIEVersion();

    if (isIE == 0) {
        // not IE
        showOnScreenAlertTransitions();
    } else {
        // IE
        if (isIE == 8 || isIE == 9) {
            showOnScreenAlertNoTransitions();
        } else if (isIE >= 10) {
            showOnScreenAlertTransitions();
        }
    }

    $('.onscreenalert-ctrl-modal').addClass('MoveIt3');
    $('.onscreenalert-ctrl-modal').attr('style', '');

    // move div to top of viewport
    $('.onscreenalert-ctrl-modal').one('transitionend', function (e) {
        var top = $('.onscreenalert-ctrl-modal').offset().top;

        $('.onscreenalert-ctrl-modal').addClass("notransition").removeClass('MoveIt1').addClass('MoveIt4');

        $('.onscreenalert-ctrl-modal').removeClass('MoveIt2');
    });
}

function showOnScreenAlertTransitions() {
    $('.onscreenalert-ctrl-modal').one('transitionend', function (e) {
        $(".icon-icon_alert_round_message").removeClass("noBorder");
        $(".titleMessage").show();
        $("#onscreenalert-ctrl-msglist").slideDown(200);
        $(".onscreenalert-ctrl-footer").slideDown(200);
    });
}

function showOnScreenAlertNoTransitions() {
    $(".icon-icon_alert_round_message").removeClass("noBorder");
    $(".titleMessage").show();
    $("#onscreenalert-ctrl-msglist").slideDown(200);
    $(".onscreenalert-ctrl-footer").slideDown(200);
}

function hideOnScreenAlertTransitions() {
    $(".titleMessage").fadeOut(200, function () {
        $('.onscreenalert-ctrl-modal').addClass("notransition").removeClass('MoveIt4').addClass('MoveIt1');
        $('.onscreenalert-ctrl-modal').attr('style', '');
        $('.onscreenalert-ctrl-modal').addClass("Smaller");
        $(".icon-icon_alert_round_message").addClass("noBorder");

        $('.onscreenalert-ctrl-modal').one('transitionend', function (e) {
            $('.onscreenalert-ctrl-modal').removeClass("notransition");
            $('.onscreenalert-ctrl-modal').removeClass('MoveIt3').addClass('MoveIt2');
            $('.onscreenalert-ctrl-modal').css({
                "top": "88%"
            });
        });
    });
}

function hideOnScreenAlertNoTransitions() {
    $(".titleMessage").fadeOut(200, function () {
        $('.onscreenalert-ctrl-modal').addClass("notransition").removeClass('MoveIt4').addClass('MoveIt1');
        $('.onscreenalert-ctrl-modal').attr('style', '');
        $('.onscreenalert-ctrl-modal').addClass("Smaller");
        $(".icon-icon_alert_round_message").addClass("noBorder");

        $('.onscreenalert-ctrl-modal').removeClass("notransition");
        $('.onscreenalert-ctrl-modal').removeClass('MoveIt3').addClass('MoveIt2');
        $('.onscreenalert-ctrl-modal').css({
            "top": "88%"
        });
    });
}

function OnScreenAlertCheckListItem() {
    var ShowOkGotIt = $("#onscreenalertdialoglist-hid-showokgotit").val();
    var ShowStickyBar = $("#onscreenalertdialoglist-hid-showstickybar").val();

    if (ShowOkGotIt == "True" && ShowStickyBar == "False") {
        OnScreenAlertShowCtrls(true, true, false);
    } else if (ShowOkGotIt == "False" && ShowStickyBar == "True") {
        OnScreenAlertShowCtrls(true, false, true);
    } else {
        OnScreenAlertShowCtrls(false, false, false);
    }
}

function OnScreenAlertShowCtrls(boolOkGotIt, boolMask, boolStickyBar) {
    if (boolOkGotIt == false) {
        $("#onscreenalert-message-dialog").hide()
    }

    (boolMask == true) ? (OnScreenAlertMaskShow(true)) : (OnScreenAlertMaskShow(false));

    if (boolStickyBar == true) {
        OnScreenAlertDialogInit();
    }
    else {
        $('.onscreenalert-ctrl-content').focus();
    }
    
}

function OnScreenAlertMaskShow(boolShow) {
    if (boolShow == true) {
        $("#onscreenalert-ctrl-mask").css({ "position": "absolute", "background": "#000", "filter": "alpha(opacity=70)", "-moz-opacity": "0.7", "-khtml-opacity": "0.7", "opacity": "0.7", "z-index": "9991", "top": "0", "left": "0" });
        $("#onscreenalert-ctrl-mask").css({ "height": function () { return $(document).height(); } });
        $("#onscreenalert-ctrl-mask").css({ "width": function () { return $(document).width(); } });
        $("#onscreenalert-ctrl-mask").show();
        $('body').css('overflow', 'hidden');
    } else {
        $("#onscreenalert-ctrl-mask").hide();
        $('body').css('overflow', 'scroll');
    }
}
// END ONSCREEN ALERT SCRIPTS

// Encoding - obfuscation
function swrot13(s) {
    return s.replace(/[a-zA-Z]/g, function(c) {return String.fromCharCode((c<="Z"?90:122)>=(c=c.charCodeAt(0)+13)?c:c-26);})
}

// START SIDEBAR TOUR SCRIPTS
/* 
 *  Wrapping sidebar tour in a function 
 *  to be called when needed.
 */

var hasPasskeys = false;
var hasStudents = false;
var isParentLinkStudent = false;
var hasNotifications = false;

function startSidebarTour() {
    // define tour
    var tour = new Shepherd.Tour({
        defaults: {
            classes: 'shepherd-theme-default'
        }
    });

    if ($('#dashboard-sidebar-student0').length > 0) {
        hasStudents = true;
    }

    if ($('#dashboard-sidebar-profile').length > 0) {
        isParentLinkStudent = true;
    }

    if ($("#dashboard-sidebar-passkeys").get(0)) {
        hasPasskeys = true;
    }

    if ($("#dashboard-sidebar-notification").get(0)) {
        hasNotifications = true;
    }


    // define steps

    tour.addStep('Intro', {
        //title: '',
        text: '<div id="tour-lightbulb-icon"></div><p>Introducing your new personalized dashboard. Would you like to take a quick tour?</p>',
        attachTo: 'body',
        classes: 'dashboard-sidebar-tour-firststep shepherd-theme-default',
        when: {
            show: function () {
                AddAnalyticsEvent('Dashboard', 'Tour', 'View');
            }
        },
        buttons: [
          {
              text: 'Yes! Let\'s go.',
              action: function () {
                  AddAnalyticsEvent('Dashboard', 'Tour', 'Continue');
                  tour.next();
                  TourADAFocus();
              },
              events: {
                  'keydown': function (e) {
                      TourButtonPress(e, tour);
                  }
              }
          },
          {
              text: 'Maybe later.',
              classes: 'shepherd-button-sec',
              action: function () {
                  AddAnalyticsEvent('Dashboard', 'Tour', 'Hide For Now');
                  tour.show('Later');
                  TourADAFocus();
              },
              events: {
                  'keydown': function (e) {
                      TourButtonPress(e, tour, 'Later');
                  }
              }
          },
          {
              text: 'No thanks. I\'ll explore on my own.',
              classes: 'shepherd-button-suboption',
              action: function () {
                  AddAnalyticsEvent('Dashboard', 'Tour', 'Hide Forever');
                  tour.show('Never');
                  TourADAFocus();
              },
              events: {
                  'keydown': function (e) {
                      TourButtonPress(e, tour, 'Never');
                  }
              }
          }
        ]
    }).addStep('Avatar', {
        text: 'Click on your user avatar to access and update your personal information and subscriptions.',
        attachTo: '#dashboard-sidebar-avatar-container right',
        when: {
            show: function() {
                $('h3.shepherd-title').attr('role', 'none'); //ADA compliance
            }
        },
        classes: 'shepherd-theme-default shepherd-element-custom',
        buttons: [
          {
              text: 'Continue',
              action: function () {
                tour.next();
                TourADAFocus();

              },
              events: {
                  'keydown': function (e) {
                      if (hasPasskeys) {
                          TourButtonPress(e, tour);
                      } else {
                          TourButtonPress(e, tour, 'Finish');
                      }
                  }
              }
          }
        ]
    }).addStep('Stream', {
        text: 'Go to your stream to see updates from your district and schools.',
        attachTo: '#dashboard-sidebar-stream right',
        buttons: [
          {
              text: 'Continue',
              action: function () {
                  if (hasPasskeys) {
                      tour.next();
                      TourADAFocus();
                  } else {
                      tour.show('Finish');
                      TourADAFocus();
                  }
              },
              events: {
                  'keydown': function (e) {
                      if(hasPasskeys){
                          TourButtonPress(e, tour);
                      } else {
                          TourButtonPress(e, tour, 'Finish');
                      }
                  }
              }
          }
        ]
    });

    if (hasPasskeys) {
        tour.addStep('Passkeys', {
            text: 'Use your passkeys to log into other district applications or websites.',
            attachTo: '#dashboard-sidebar-passkeys right',
            buttons: [
              {
                  text: 'Continue',
                  action: function () {
                      if (hasNotifications) {
                          tour.show('Notifications');
                          TourADAFocus();
                      }
                      else if (hasStudents) {
                          tour.show('Students');
                          TourADAFocus();
                      }
                      else if (isParentLinkStudent) {
                          tour.show('ParentLinkStudent');
                          TourADAFocus();
                      }
                      else {
                          tour.show('Finish');
                          TourADAFocus();
                      }
                  },
                  events: {
                      'keydown': function (e) {
                          if (hasStudents) {
                              TourButtonPress(e, tour, 'Notifications');
                          }
                          else {
                              TourButtonPress(e, tour, 'Finish');
                          }
                      }
                  }
              }
            ]
        })
    }

    if (hasNotifications) {
        tour.addStep('Notifications', {
            text: 'Open your notifications to review messages.',
            attachTo: '#dashboard-sidebar-notification right',
            buttons: [
              {
                  text: 'Continue',
                  action: function () {
                      if (hasStudents) {
                          tour.show('Students');
                          TourADAFocus();
                      }
                      else if (isParentLinkStudent) {
                          tour.show('ParentLinkStudent');
                          TourADAFocus();
                      }
                      else {
                          tour.show('Finish');
                          TourADAFocus();
                      }
                  },
                  events: {
                      'keydown': function (e) {
                          if (hasStudents) {
                              TourButtonPress(e, tour, 'Students');
                          }
                          else {
                              TourButtonPress(e, tour, 'Finish');
                          }
                      }
                  }
              }
            ]
        })
    }

    if (hasStudents) {
        tour.addStep('Students', {
            text: 'Select a student to view his or her information and records.',
            attachTo: '#dashboard-sidebar-student0 right',
            buttons: [
              {
                  text: 'Continue',
                  action: function () {
                      tour.show('Finish');
                      TourADAFocus();
                  },
                  events: {
                      'keydown': function (e) {
                          TourButtonPress(e, tour, 'Finish');
                      }
                  }
              }
            ]
        })
    }

    if (isParentLinkStudent) {
        tour.addStep('ParentLinkStudent', {
            text: 'View your information or other helpful resources.',
            attachTo: '#dashboard-sidebar-profile right',
            buttons: [
              {
                  text: 'Continue',
                  action: function () {
                      tour.show('Finish');
                      TourADAFocus();
                  },
                  events: {
                      'keydown': function (e) {
                          TourButtonPress(e, tour, 'Finish');
                      }
                  }
              }
            ]
        })
    }

    tour.addStep('Later', {
        text: 'Ok, we\'ll remind you later. You can access the tour here any time you want.',
        attachTo: '#dashboard-sidebar-help right',
        classes: 'shepherd-theme-default shepherd-element-custom-bottom',
        buttons: [
          {
              text: 'Finish',
              action: function () {
                  SetCookie("DashboardSidebarTour", "true");
                  tour.cancel();
              },
              events: {
                  'keydown': function (e) {
                      e.stopImmediatePropagation();
                      if (e.keyCode == 13) {
                          SetCookie("DashboardSidebarTour", "true");
                          tour.cancel();
                      }
                  }
              }
          }
        ]

    }).addStep('Never', {
        text: 'Ok, we won\'t ask you again. If you change your mind, you can access the tour here any time you want.',
        attachTo: '#dashboard-sidebar-help right',
        classes: 'shepherd-theme-default shepherd-element-custom-bottom',
        buttons: [
          {
              text: 'Finish',
              action: function () {
                  SetCookie("DashboardSidebarTour", "true");
                  DenyTour();
              },
              events: {
                  'keydown': function (e) {
                      e.stopImmediatePropagation();
                      if (e.keyCode == 13) {
                          SetCookie("DashboardSidebarTour", "true");
                          DenyTour();
                      }
                  }
              }
          }
        ]

    }).addStep('Finish', {
        text: 'All finished! Go here any time to view this tour again.',
        attachTo: '#dashboard-sidebar-help right',
        classes: 'shepherd-theme-default shepherd-element-custom-bottom',
        buttons: [
          {
              text: 'Finish',
              action: function () {
                  SetCookie("DashboardSidebarTour", "true");
                  FinishTour();
              },
              events: {
                  'keydown': function (e) {
                      e.stopImmediatePropagation();
                      if (e.keyCode == 13) {
                          SetCookie("DashboardSidebarTour", "true");
                          FinishTour();
                      }
                  }
              }
          }
        ]
    });

    // start tour
    tour.start();
    TourADA();
}

function TourADA() {
    $(".shepherd-content").attr("tabindex", "0");
    $(".shepherd-button").each(function () {
        var $this = $(this);
        $this.attr("title", $this.text());
        $this.attr("tabindex", "0");
    });
}

function TourADAFocus() {
    TourADA();
    $(".shepherd-content").focus();
}

function TourButtonPress(e, tour, next) {
    e.stopImmediatePropagation();
    if (e.keyCode == 13) {
        if (next == undefined) {
            tour.next();
        } else {
            tour.show(next);
        }
        TourADAFocus();
    }
}

function FinishTour() {
    var data = "";
    var success = 'ActiveTourCancel();';
    var failure = 'CallControllerFailure(result[0].errormessage);';
    CallController(homeURL + "/GlobalUserControls/DashBoardSideBar/DashBoardSideBarController.aspx/FinishTour", data, success, failure);
}

function DenyTour() {
    var data = "";
    var success = 'ActiveTourCancel();';
    var failure = 'CallControllerFailure(result[0].errormessage);';
    CallController(homeURL + "/GlobalUserControls/DashBoardSideBar/DashBoardSideBarController.aspx/DenyTour", data, success, failure);
}

function ActiveTourCancel() {
    Shepherd.activeTour.cancel();
}
// END SIDEBAR TOUR SCRIPTS

// BEGIN DOCUMENT READY
$(document).ready(function () {

    if ($('#sw-sidebar').height() > 1500) {
        $('#sw-page').css('min-height', $('#sw-sidebar').height() + 'px');
        $('#sw-inner').css('min-height', $('#sw-sidebar').height() + 'px');
    } else {
        $('#sw-page').css('min-height', $(document).height() + 'px');
        $('#sw-inner').css('min-height', $(document).height() + 'px');
    }

    $('#sw-footer').show();

    // add focus class to textboxes for IE
    $(document).on('focus', 'input', function () {
        $(this).addClass('focus');
    });

    $(document).on('blur', 'input', function () {
        $(this).removeClass('focus');
    });

    // default ajax setup
    $.ajaxSetup({
        cache: false,
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            //swalert(XMLHttpRequest.status + ' ' + textStatus + ': ' + errorThrown, 'Ajax Error', 'critical', 'ok');
            swalert("Something went wrong. We're sorry this happened. Please refresh your page and try again.", "Error", "critical", "ok");
        }
    });

    // make :Contains (case-insensitive version of :contains)
    jQuery.expr[':'].Contains = function (a, i, m) {
        return jQuery(a).text().toUpperCase().indexOf(m[3].toUpperCase()) >= 0;
    };

    // HIDE / SHOW DETAILS IN LIST SCREEN
    $(document).on('click', 'span.ui-show-detail', function () {
        var $this = $(this);

        if ($this.hasClass('open')) {
            // do nothing
        } else {
            $this.addClass('open').parent('div.ui-article-header').nextAll('div.ui-article-detail').slideDown(function () {
                // add close button
                $this.append("<span class='ui-article-detail-close'></span>");
            });
        }
    });

    $(document).on('click', 'span.ui-article-detail-close', function () {
        $this = $(this);
        $this.parents('.ui-article-header').nextAll('.ui-article-detail').slideUp(function () {
            $this.parent('.ui-show-detail').removeClass('open');
            // remove close button
            $this.remove();
        });
    });


    // LIST/EXPANDED VIEW ON LIST PAGES
    $(document).on('click', '#show-list-view', function () {
        $(this).addClass('ui-btn-toolbar-primary').removeClass('ui-btn-toolbar');
        $('#show-expanded-view').addClass('ui-btn-toolbar').removeClass('ui-btn-toolbar-primary');
        $('div.ui-article-detail').slideUp(function () {
            // remove close buttons
            $(this).prevAll('.ui-article-header').find('.ui-article-detail-close').remove();
            //$this.children('.ui-article-detail-close').remove();
        });
        $('span.ui-show-detail').removeClass('open');
    });

    $(document).on('click', '#show-expanded-view', function (i) {
        $(this).addClass('ui-btn-toolbar-primary').removeClass('ui-btn-toolbar');
        $('#show-list-view').addClass('ui-btn-toolbar').removeClass('ui-btn-toolbar-primary');
        $('div.ui-article-detail').slideDown('', function () {
            // add close buttons
            $(this).prevAll('.ui-article-header').children('.ui-show-detail').append("<span class='ui-article-detail-close'></span>");
        });
        $('span.ui-show-detail').addClass('open');
    });

    //Important Message Ok, got it tab=0
    $('#onscreenalert-ctrl-gotit').keydown(function (e) {
        e.stopImmediatePropagation();
        if (e.keyCode == 13) {
            $(this).click();
        }
    });

    $('#onscreenalert-message-dialog-icon').keydown(function (e) {
        e.stopImmediatePropagation();
        if (e.keyCode == 13) {
            $(this).click();
        }
    });
    
}); // end document ready

// load scripts after everything else
$(window).on('load',  IncludeDocReadyScripts);
﻿/// <reference path="../../../../Scripts/Slick/slick.min.js" />
/// <reference path="../../../../Scripts/Slick/slick.min.js" />
/// <reference path="../../../../Scripts/Slick/slick.min.js" />
/// <reference path="../../../../Scripts/Slick/slick.min.js" />
// Check for included script
function CheckScript(ModuleName, ScriptSRC, FunctionName) {

    var loadScriptFile = true;

    switch (ModuleName.toLowerCase()) {
        case 'sectionrobot':
            FunctionName = 'CheckSectionRobotScript';
            ScriptSRC = homeURL + '/cms/Tools/SectionRobot/SectionRobot.js';
            break;
        case 'assignments':
            FunctionName = 'CheckAssignmentsScript';
            ScriptSRC = homeURL + '/cms/Module/Assignments/Assignments.js';
            break;
        case 'spacedirectory':
            FunctionName = 'CheckSpaceDirectoryScript';
            ScriptSRC = homeURL + '/cms/Module/SpaceDirectory/SpaceDirectory.js';
            break;
        case 'calendar':
            FunctionName = 'CheckCalendarScript';
            ScriptSRC = homeURL + '/cms/Module/Calendar/Calendar.js';
            break;
        case 'fullcalendar':
            FunctionName = '$.fn.fullCalendar';
            ScriptSRC = staticURL + '/GlobalAssets/Scripts/ThirdParty/jquery.fullcalendar1.6.1.js';
            break;
        case 'links':
            FunctionName = 'CheckLinksScript';
            ScriptSRC = staticURL + '/cms/Module/Links/Links.js';
            break;
        case 'minibase':
            FunctionName = 'CheckMinibaseScript';
            ScriptSRC = homeURL + '/cms/Module/Minibase/Minibase.js';
            break;
        case 'moduleinstance':
            var randNum = Math.floor(Math.random() * (1000 - 10 + 1) + 1000);
            FunctionName = 'CheckModuleInstanceScript';
            ScriptSRC = homeURL + '/cms/Module/ModuleInstance/ModuleInstance.js?rand=' + randNum;
            break;
        case 'photogallery':
            FunctionName = 'CheckPhotoGalleryScript';
            ScriptSRC = staticURL + '/cms/Module/PhotoGallery/PhotoGallery_2520.js';
            break;
        case 'comments':
            FunctionName = 'CheckModerateCommentsScript';
            ScriptSRC = homeURL + '/cms/Tools/ModerateComments/ModerateComments.js';
            break;
        case 'postings':
            FunctionName = 'CheckModeratePostingsScript';
            ScriptSRC = homeURL + '/cms/Tools/ModerateContribution/ModerateContribution.js';
            break;
        case 'myaccount':
            FunctionName = 'CheckMyAccountScript';
            ScriptSRC = homeURL + '/cms/UserControls/MyAccount/MyAccount.js';
            break;
        case 'formsurvey':
            FunctionName = 'CheckFormSurveyScript';
            ScriptSRC = homeURL + '/cms/tools/FormsAndSurveys/Surveys.js';
            break;
        case 'alerts':
            FunctionName = 'CheckAlertsScript';
            ScriptSRC = homeURL + '/cms/tools/Alerts/Alerts.js';
            break;
        case 'onscreenalerts':
            FunctionName = 'CheckOnScreenalertsScript';
            ScriptSRC = homeURL + '/cms/tools/OnScreenAlerts/OnScreenAlerts.js';
            break;
        case 'workspace':
            FunctionName = 'CheckWorkspaceScript';
            ScriptSRC = staticURL + '/cms/Workspace/PageList_2680.js';
            break;
        case 'moduleview':
            FunctionName = 'CheckModuleViewScript';
            ScriptSRC = staticURL + '/GlobalAssets/Scripts/min/ModuleViewRenderer_2640.js';
            break;
        case 'pageeditingmoduleview':
            FunctionName = 'CheckPageEditingModuleViewScript';
            //ScriptSRC = staticURL + '/GlobalAssets/Scripts/min/PageEditingModuleViewRenderer.js';
            ScriptSRC = homeURL + '/cms/UserControls/ModuleView/PageEditingModuleViewRenderer_2640.js';
            break;
        case 'editarea':
            FunctionName = 'CheckEditAreaScript';
            ScriptSRC = homeURL + '/GlobalUserControls/EditArea/edit_area_full.js';
            break;
        case 'rating':
            FunctionName = '$.fn.rating';
            ScriptSRC = staticURL + '/GlobalAssets/Scripts/min/jquery.rating.min.js';
            break;
        case 'metadata':
            FunctionName = '$.fn.metadata';
            ScriptSRC = staticURL + '/GlobalAssets/Scripts/min/jquery.metadata.min.js';
            break;
        case 'pwcalendar':
            FunctionName = 'CheckPWCalendarScript';
            ScriptSRC = homeURL + '/myview/UserControls/Calendar/Calendar.js';
            break;
        case 'importwizard':
            FunctionName = 'CheckImportWizardScript';
            ScriptSRC = homeURL + '/cms/UserControls/ImportDialog/ImportWizard.js';
            break;
        case 'mustache':
            FunctionName = 'CheckMustacheScript';
            ScriptSRC = staticURL + '/GlobalAssets/Scripts/ThirdParty/mustache.js';
            break;
        case 'slick':
            FunctionName = 'CheckSlickScript';
            ScriptSRC = staticURL + '/GlobalAssets/Scripts/ThirdParty/Slick/slick.min.js';
            break;
        case 'galleria':
            FunctionName = 'CheckGalleriaScript';
            ScriptSRC = staticURL + '/GlobalAssets/Scripts/ThirdParty/galleria-custom-129_2520/galleria-1.2.9.min.js';
            break;
        case 'fine-uploader':
            FunctionName = 'CheckFineUploaderScript';
            ScriptSRC = staticURL + '/GlobalAssets/Scripts/ThirdParty/fine-uploader/fine-uploader.min.js';
            break;
        case 'tinymce':
            FunctionName = 'tinyMCE';
            ScriptSRC = homeURL + '/cms/Module/selectsurvey/ClientInclude/tinyMCE/jquery.tinymce.min.js';
            break;
        case 'attachmentview':
            FunctionName = 'CheckAttachmentScript';
            ScriptSRC = homeURL + '/GlobalUserControls/Attachment/AttachmentView.js';
            break;
        default:
            // module name not found
            if (ScriptSRC !== undefined && ScriptSRC !== null && ScriptSRC.length > 0) {
                // script src was specified in parameter
                if (FunctionName === undefined && ModuleName.length > 0) {
                    // default the function name for lookup from the module name
                    FunctionName = 'Check' + ModuleName + 'Script';
                }
            } else {
                // can't load a script file without at least a src and function name to test
                loadScriptFile = false;
            }
            break;
    }

    if (loadScriptFile === true) {
        try {
            if (eval("typeof " + FunctionName + " == 'function'")) {
                // do nothing, it's already included
            } else {
                var script = document.createElement('script');script.type = 'text/javascript';script.src = ScriptSRC;$('head').append(script);;
            }
        } catch (err) {
        }
    }
}
// End Check for included script
</script><!-- End swuc.CheckScript -->


    <!-- Server Load Time (04): 0.1406184 Seconds -->

    

    <!-- off-canvas menu enabled-->
    

    <!-- Ally Alternative Formats Configure START   -->
    
    <!-- Ally Alternative Formats Configure END     -->

</body>
</html>
